<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix('v1')->group(function () {
    //Country, Region/State and City
    Route::prefix('countries')->group(function () {
        Route::get('/','App\Http\Controllers\Api\CountryController@index');
        Route::prefix('/{id}/regions')->group(function () {
            Route::get('/','App\Http\Controllers\Api\RegionController@index');
            Route::get('/{regionId}/cities','App\Http\Controllers\Api\CityController@index');
        });
    });
    Route::prefix('regions')->group(function () {
        Route::get('/{id}/cities','App\Http\Controllers\Api\CityController@index');
    });
});
