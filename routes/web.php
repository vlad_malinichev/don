<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use PragmaRX\Countries\Package\Countries;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('locale/{locale}', function ($locale) {
    Session::put('locale', $locale);
    //dd($locale, Session::get('locale'));
    return redirect()->back();
});

Route::group(['prefix' => 'admin'], function () {
    Route::get('', [App\Http\Controllers\Admin\HomeController::class, 'index'])->name('admin.home.index');
    Route::get('user', [App\Http\Controllers\Admin\UserController::class, 'index'])->name('admin.user.index');
});

Route::group(['prefix' => 'search'], function () {
    Route::get('query', [App\Http\Controllers\SearchController::class, 'query'])->name('search.query');
    Route::get('search', [App\Http\Controllers\SearchController::class, 'search'])->name('search.search');
});

Route::get('/articles', [App\Http\Controllers\ArticleController::class, 'index'])->name('articles');
Route::get('/article/{slug}', [App\Http\Controllers\ArticleController::class, 'show'])->name('article.show');

Route::get('/page/model-instructions', [App\Http\Controllers\PageController::class, 'modelInstructions'])->name('page.model-instructions');
Route::get('/page/customer-instructions', [App\Http\Controllers\PageController::class, 'customerInstructions'])->name('page.customer-instructions');
Route::get('/page/feedback', [App\Http\Controllers\PageController::class, 'feedback'])->name('page.feedback');
Route::get('/page/privacy', [App\Http\Controllers\PageController::class, 'privacy'])->name('page.privacy');
Route::get('/page/{slug}', [App\Http\Controllers\PageController::class, 'show'])->name('page.show');

Route::resource('questionnaire', App\Http\Controllers\QuestionnaireController::class);

Route::group(['prefix' => 'cabinet'], function () {
    Route::group(['prefix' => 'messenger'], function () {
        Route::get('', [App\Http\Controllers\Cabinet\MessengerController::class, 'index'])->name('cabinet.messenger.index');
        Route::post('send', [App\Http\Controllers\Cabinet\MessengerController::class, 'send'])->name('cabinet.messenger.send');
    });

    Route::group(['prefix' => 'setting'], function () {
        Route::get('edit', [App\Http\Controllers\Cabinet\SettingController::class, 'edit'])->name('cabinet.setting.edit');
        Route::post('update', [App\Http\Controllers\Cabinet\SettingController::class, 'update'])->name('cabinet.setting.update');
    });

    Route::group(['prefix' => 'blacklist'], function () {
        Route::get('', [App\Http\Controllers\Cabinet\BlacklistController::class, 'index'])->name('cabinet.blacklist.index');
        Route::get('my', [App\Http\Controllers\Cabinet\BlacklistController::class, 'my'])->name('cabinet.blacklist.my');
        Route::get('create', [App\Http\Controllers\Cabinet\BlacklistController::class, 'create'])->name('cabinet.blacklist.create');
        Route::post('store', [App\Http\Controllers\Cabinet\BlacklistController::class, 'store'])->name('cabinet.blacklist.store');
        Route::delete('destroy', [App\Http\Controllers\Cabinet\BlacklistController::class, 'destroy'])->name('cabinet.blacklist.destroy');
    });

    Route::resource('questionnaire', App\Http\Controllers\Cabinet\QuestionnaireController::class);
    Route::resource('review', App\Http\Controllers\Cabinet\ReviewController::class);
    Route::resource('banner', App\Http\Controllers\Cabinet\BannerController::class);
    Route::resource('gift', App\Http\Controllers\Cabinet\GiftController::class);
    Route::resource('favorite', App\Http\Controllers\Cabinet\FavoriteController::class);
});

Auth::routes();
