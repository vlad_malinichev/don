<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'login' => 'Войти',
    'register' => 'Регистрация',
    'reset_password' => 'Сброс пароля',
    'logout' => 'Выход',

    'name' => 'Имя',
    'email' => 'Email',
    'password' => 'Пароль',
    'confirm_password' => 'Повторите пароль',
    'remember_me' => 'Запомнить меня',
    'forgot_your_password' => 'Забыли пароль?',
    'send_password_reset_link' => 'Сбросить',

    'failed' => 'These credentials do not match our records.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',

];
