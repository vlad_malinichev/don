<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'login' => 'Sign in',
    'register' => 'Sign up',
    'reset_password' => 'Reset password',
    'logout' => 'Logout',

    'name' => 'Name',
    'email' => 'Email',
    'password' => 'Password',
    'confirm_password' => 'Confirm password',
    'remember_me' => 'Remember me',
    'forgot_your_password' => 'Forgot your password?',
    'send_password_reset_link' => 'Reset',

    'failed' => 'These credentials do not match our records.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',

];
