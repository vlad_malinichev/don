@if ($paginator->hasPages())
    <div class="pagination">
        <div class="pagination__wrap">
            {{-- Previous Page Link --}}
            @if ($paginator->onFirstPage())
                <button class="pagination__button pagination__button--back pagination__button--disabled" rel="prev" aria-label="@lang('pagination.previous')">
                    <svg width="7" height="12" viewBox="0 0 7 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M6 1L1 6L6 11" stroke="#5C5C5C" stroke-width="1.8" stroke-linecap="round"
                              stroke-linejoin="round" />
                    </svg>
                    Назад
                </button>
            @else
                <button class="pagination__button pagination__button--back" rel="prev" aria-label="@lang('pagination.previous')" onclick="location.href='{{ $paginator->previousPageUrl() }}'">
                    <svg width="7" height="12" viewBox="0 0 7 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M6 1L1 6L6 11" stroke="#5C5C5C" stroke-width="1.8" stroke-linecap="round"
                              stroke-linejoin="round" />
                    </svg>
                    Назад
                </button>
            @endif

            <ul class="pagination__list">
                {{-- Pagination Elements --}}
                @foreach ($elements as $element)
                    {{-- "Three Dots" Separator --}}
                    @if (is_string($element))
                        <li class="disabled" aria-disabled="true"><span>{{ $element }}</span></li>
                    @endif

                    {{-- Array Of Links --}}
                    @if (is_array($element))
                        @foreach ($element as $page => $url)
                            @if ($page == $paginator->currentPage())
                                <li class="pagination__item pagination__item--active" aria-current="page">
                                    <span>{{ $page }}</span>
                                </li>
                            @else
                                <li class="pagination__item">
                                    <a class="pagination__link" href="{{ $url }}">{{ $page }}</a>
                                </li>
                            @endif
                        @endforeach
                    @endif
                @endforeach
            </ul>

            {{-- Next Page Link --}}
            @if ($paginator->hasMorePages())
                <button class="pagination__button" onclick="location.href='{{ $paginator->nextPageUrl() }}'" rel="next" aria-label="@lang('pagination.next')">
                    Далее
                    <svg width="7" height="12" viewBox="0 0 7 12" fill="none"
                         xmlns="http://www.w3.org/2000/svg">
                        <path d="M1 11L6 6L1 1" stroke="#5C5C5C" stroke-width="1.8" stroke-linecap="round"
                              stroke-linejoin="round" />
                    </svg>
                </button>
            @else
                <button class="pagination__button pagination__button--disabled" rel="next" aria-label="@lang('pagination.next')">
                    Далее
                    <svg width="7" height="12" viewBox="0 0 7 12" fill="none"
                         xmlns="http://www.w3.org/2000/svg">
                        <path d="M1 11L6 6L1 1" stroke="#5C5C5C" stroke-width="1.8" stroke-linecap="round"
                              stroke-linejoin="round" />
                    </svg>
                </button>
            @endif

        </div>
    </div>
@endif
