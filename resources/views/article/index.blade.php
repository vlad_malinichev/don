@extends('layouts.main')

@section('content')
    <div class="articles">
        <div class="articles__wrapper">
            <div class="articles__container container">
                <div class="articles__breadcrumbs breadcrumbs">
                    <div class="breadcrumbs__wrap">
                        <ul class="breadcrumbs__list">
                            <li class="breadcrumbs__item breadcrumbs__item--back"><a class="breadcrumbs__link" href="#"><img
                                        src="/images/arrow-left-34b34338.svg"><span>Назад</span></a>
                            </li>
                            <li class="breadcrumbs__item breadcrumbs__item--before"><a class="breadcrumbs__link"
                                                                                       href="">Объявления Москва</a></li>
                            <li class="breadcrumbs__item"><a class="breadcrumbs__link" href="">Индивидуалки Москва</a></li>
                            <li class="breadcrumbs__item breadcrumbs__item--active"><a class="breadcrumbs__link"
                                                                                       href="">Анжелика</a></li>
                        </ul>
                    </div>
                </div>
                <ul class="articles__tabs">
                    <li class="articles__tab articles__tab--active">Все статьи</li>
                    <li class="articles__tab">Для девушек</li>
                    <li class="articles__tab">Для парней</li>
                </ul>
                <div class="articles__items">
                    <div class="articles__item"><a class="articles-item" href="/article.html">
                            <div class="articles-item__aside">
                                <div class="articles-item__rubric">Название рубрики</div>
                                <div class="articles-item__info">
                                    <div class="articles-item__date">06 мая 2020 14:14</div>
                                    <div class="articles-item__views">12011</div>
                                </div>
                            </div>
                            <div class="articles-item__content">
                                <div class="articles-item__title">Секс на одну ночь</div><svg class="articles-item__arrow"
                                                                                              xmlns="http://www.w3.org/2000/svg" width="36" height="18" viewBox="0 0 36 18" fill="none"
                                                                                              xmlns:v="https://vecta.io/nano">
                                    <path
                                        d="M35.588 8.005L28.238.69c-.55-.548-1.44-.546-1.99.005s-.546 1.44.005 1.99l4.933 4.91H1.406C.63 7.594 0 8.223 0 9s.63 1.406 1.406 1.406h29.78l-4.933 4.91c-.55.548-.553 1.438-.005 1.99s1.438.552 1.99.005l7.348-7.313a1.41 1.41 0 0 0 .001-1.992z"
                                        fill="#fff" /></svg>
                            </div>
                        </a></div>
                    <div class="articles__item"><a class="articles-item" href="/article.html">
                            <div class="articles-item__aside">
                                <div class="articles-item__rubric">Название рубрики</div>
                                <div class="articles-item__info">
                                    <div class="articles-item__date">06 мая 2020 14:14</div>
                                    <div class="articles-item__views">12011</div>
                                </div>
                            </div>
                            <div class="articles-item__content">
                                <div class="articles-item__title">Как стать лучшим любовником</div><svg class="articles-item__arrow"
                                                                                                        xmlns="http://www.w3.org/2000/svg" width="36" height="18" viewBox="0 0 36 18" fill="none"
                                                                                                        xmlns:v="https://vecta.io/nano">
                                    <path
                                        d="M35.588 8.005L28.238.69c-.55-.548-1.44-.546-1.99.005s-.546 1.44.005 1.99l4.933 4.91H1.406C.63 7.594 0 8.223 0 9s.63 1.406 1.406 1.406h29.78l-4.933 4.91c-.55.548-.553 1.438-.005 1.99s1.438.552 1.99.005l7.348-7.313a1.41 1.41 0 0 0 .001-1.992z"
                                        fill="#fff" /></svg>
                            </div>
                        </a></div>
                    <div class="articles__item"><a class="articles-item" href="/article.html">
                            <div class="articles-item__aside">
                                <div class="articles-item__rubric">Название рубрики</div>
                                <div class="articles-item__info">
                                    <div class="articles-item__date">06 мая 2020 14:14</div>
                                    <div class="articles-item__views">12011</div>
                                </div>
                            </div>
                            <div class="articles-item__content">
                                <div class="articles-item__title">Клубы для секса</div><svg class="articles-item__arrow"
                                                                                            xmlns="http://www.w3.org/2000/svg" width="36" height="18" viewBox="0 0 36 18" fill="none"
                                                                                            xmlns:v="https://vecta.io/nano">
                                    <path
                                        d="M35.588 8.005L28.238.69c-.55-.548-1.44-.546-1.99.005s-.546 1.44.005 1.99l4.933 4.91H1.406C.63 7.594 0 8.223 0 9s.63 1.406 1.406 1.406h29.78l-4.933 4.91c-.55.548-.553 1.438-.005 1.99s1.438.552 1.99.005l7.348-7.313a1.41 1.41 0 0 0 .001-1.992z"
                                        fill="#fff" /></svg>
                            </div>
                        </a></div>
                    <div class="articles__item"><a class="articles-item" href="/article.html">
                            <div class="articles-item__aside">
                                <div class="articles-item__rubric">Название рубрики</div>
                                <div class="articles-item__info">
                                    <div class="articles-item__date">06 мая 2020 14:14</div>
                                    <div class="articles-item__views">12011</div>
                                </div>
                            </div>
                            <div class="articles-item__content">
                                <div class="articles-item__title">Найти секс по телефону</div><svg class="articles-item__arrow"
                                                                                                   xmlns="http://www.w3.org/2000/svg" width="36" height="18" viewBox="0 0 36 18" fill="none"
                                                                                                   xmlns:v="https://vecta.io/nano">
                                    <path
                                        d="M35.588 8.005L28.238.69c-.55-.548-1.44-.546-1.99.005s-.546 1.44.005 1.99l4.933 4.91H1.406C.63 7.594 0 8.223 0 9s.63 1.406 1.406 1.406h29.78l-4.933 4.91c-.55.548-.553 1.438-.005 1.99s1.438.552 1.99.005l7.348-7.313a1.41 1.41 0 0 0 .001-1.992z"
                                        fill="#fff" /></svg>
                            </div>
                        </a></div>
                    <div class="articles__item"><a class="articles-item" href="/article.html">
                            <div class="articles-item__aside">
                                <div class="articles-item__rubric">Название рубрики</div>
                                <div class="articles-item__info">
                                    <div class="articles-item__date">06 мая 2020 14:14</div>
                                    <div class="articles-item__views">12011</div>
                                </div>
                            </div>
                            <div class="articles-item__content">
                                <div class="articles-item__title">Секс вечеринки в Киеве</div><svg class="articles-item__arrow"
                                                                                                   xmlns="http://www.w3.org/2000/svg" width="36" height="18" viewBox="0 0 36 18" fill="none"
                                                                                                   xmlns:v="https://vecta.io/nano">
                                    <path
                                        d="M35.588 8.005L28.238.69c-.55-.548-1.44-.546-1.99.005s-.546 1.44.005 1.99l4.933 4.91H1.406C.63 7.594 0 8.223 0 9s.63 1.406 1.406 1.406h29.78l-4.933 4.91c-.55.548-.553 1.438-.005 1.99s1.438.552 1.99.005l7.348-7.313a1.41 1.41 0 0 0 .001-1.992z"
                                        fill="#fff" /></svg>
                            </div>
                        </a></div>
                    <div class="articles__item"><a class="articles-item" href="/article.html">
                            <div class="articles-item__aside">
                                <div class="articles-item__rubric">Название рубрики</div>
                                <div class="articles-item__info">
                                    <div class="articles-item__date">06 мая 2020 14:14</div>
                                    <div class="articles-item__views">12011</div>
                                </div>
                            </div>
                            <div class="articles-item__content">
                                <div class="articles-item__title">Знакомство для секса в Киеве</div><svg
                                    class="articles-item__arrow" xmlns="http://www.w3.org/2000/svg" width="36" height="18"
                                    viewBox="0 0 36 18" fill="none" xmlns:v="https://vecta.io/nano">
                                    <path
                                        d="M35.588 8.005L28.238.69c-.55-.548-1.44-.546-1.99.005s-.546 1.44.005 1.99l4.933 4.91H1.406C.63 7.594 0 8.223 0 9s.63 1.406 1.406 1.406h29.78l-4.933 4.91c-.55.548-.553 1.438-.005 1.99s1.438.552 1.99.005l7.348-7.313a1.41 1.41 0 0 0 .001-1.992z"
                                        fill="#fff" /></svg>
                            </div>
                        </a></div>
                    <div class="articles__item"><a class="articles-item" href="/article.html">
                            <div class="articles-item__aside">
                                <div class="articles-item__rubric">Название рубрики</div>
                                <div class="articles-item__info">
                                    <div class="articles-item__date">06 мая 2020 14:14</div>
                                    <div class="articles-item__views">12011</div>
                                </div>
                            </div>
                            <div class="articles-item__content">
                                <div class="articles-item__title">Секс веб-чаты</div><svg class="articles-item__arrow"
                                                                                          xmlns="http://www.w3.org/2000/svg" width="36" height="18" viewBox="0 0 36 18" fill="none"
                                                                                          xmlns:v="https://vecta.io/nano">
                                    <path
                                        d="M35.588 8.005L28.238.69c-.55-.548-1.44-.546-1.99.005s-.546 1.44.005 1.99l4.933 4.91H1.406C.63 7.594 0 8.223 0 9s.63 1.406 1.406 1.406h29.78l-4.933 4.91c-.55.548-.553 1.438-.005 1.99s1.438.552 1.99.005l7.348-7.313a1.41 1.41 0 0 0 .001-1.992z"
                                        fill="#fff" /></svg>
                            </div>
                        </a></div>
                    <div class="articles__item"><a class="articles-item" href="/article.html">
                            <div class="articles-item__aside">
                                <div class="articles-item__rubric">Название рубрики</div>
                                <div class="articles-item__info">
                                    <div class="articles-item__date">06 мая 2020 14:14</div>
                                    <div class="articles-item__views">12011</div>
                                </div>
                            </div>
                            <div class="articles-item__content">
                                <div class="articles-item__title">Свингеры из Киева</div><svg class="articles-item__arrow"
                                                                                              xmlns="http://www.w3.org/2000/svg" width="36" height="18" viewBox="0 0 36 18" fill="none"
                                                                                              xmlns:v="https://vecta.io/nano">
                                    <path
                                        d="M35.588 8.005L28.238.69c-.55-.548-1.44-.546-1.99.005s-.546 1.44.005 1.99l4.933 4.91H1.406C.63 7.594 0 8.223 0 9s.63 1.406 1.406 1.406h29.78l-4.933 4.91c-.55.548-.553 1.438-.005 1.99s1.438.552 1.99.005l7.348-7.313a1.41 1.41 0 0 0 .001-1.992z"
                                        fill="#fff" /></svg>
                            </div>
                        </a></div>
                    <div class="articles__item"><a class="articles-item" href="/article.html">
                            <div class="articles-item__aside">
                                <div class="articles-item__rubric">Название рубрики</div>
                                <div class="articles-item__info">
                                    <div class="articles-item__date">06 мая 2020 14:14</div>
                                    <div class="articles-item__views">12011</div>
                                </div>
                            </div>
                            <div class="articles-item__content">
                                <div class="articles-item__title">Где снять шлюх в Киеве?</div><svg class="articles-item__arrow"
                                                                                                    xmlns="http://www.w3.org/2000/svg" width="36" height="18" viewBox="0 0 36 18" fill="none"
                                                                                                    xmlns:v="https://vecta.io/nano">
                                    <path
                                        d="M35.588 8.005L28.238.69c-.55-.548-1.44-.546-1.99.005s-.546 1.44.005 1.99l4.933 4.91H1.406C.63 7.594 0 8.223 0 9s.63 1.406 1.406 1.406h29.78l-4.933 4.91c-.55.548-.553 1.438-.005 1.99s1.438.552 1.99.005l7.348-7.313a1.41 1.41 0 0 0 .001-1.992z"
                                        fill="#fff" /></svg>
                            </div>
                        </a></div>
                    <div class="articles__item"><a class="articles-item" href="/article.html">
                            <div class="articles-item__aside">
                                <div class="articles-item__rubric">Название рубрики</div>
                                <div class="articles-item__info">
                                    <div class="articles-item__date">06 мая 2020 14:14</div>
                                    <div class="articles-item__views">12011</div>
                                </div>
                            </div>
                            <div class="articles-item__content">
                                <div class="articles-item__title">Уроки кунилингуса для мужчин</div><svg
                                    class="articles-item__arrow" xmlns="http://www.w3.org/2000/svg" width="36" height="18"
                                    viewBox="0 0 36 18" fill="none" xmlns:v="https://vecta.io/nano">
                                    <path
                                        d="M35.588 8.005L28.238.69c-.55-.548-1.44-.546-1.99.005s-.546 1.44.005 1.99l4.933 4.91H1.406C.63 7.594 0 8.223 0 9s.63 1.406 1.406 1.406h29.78l-4.933 4.91c-.55.548-.553 1.438-.005 1.99s1.438.552 1.99.005l7.348-7.313a1.41 1.41 0 0 0 .001-1.992z"
                                        fill="#fff" /></svg>
                            </div>
                        </a></div>
                </div>
                <div class="articles__pagination">
                    <div class="pagination">
                        <div class="pagination__wrap">
                            <button class="pagination__button pagination__button--back pagination__button--disabled">
                                <svg width="7" height="12" viewBox="0 0 7 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M6 1L1 6L6 11" stroke="#5C5C5C" stroke-width="1.8" stroke-linecap="round"
                                          stroke-linejoin="round" />
                                </svg> Назад </button>
                            <ul class="pagination__list">
                                <li class="pagination__item pagination__item--active"><a class="pagination__link" href="#">1</a>
                                </li>
                                <li class="pagination__item"><a class="pagination__link" href="#">2</a></li>
                                <li class="pagination__item"><a class="pagination__link pagination__link--dot" href="#">...</a></li>
                            </ul>
                            <button class="pagination__button"> Далее<svg width="7" height="12" viewBox="0 0 7 12" fill="none"
                                                                          xmlns="http://www.w3.org/2000/svg">
                                    <path d="M1 11L6 6L1 1" stroke="#5C5C5C" stroke-width="1.8" stroke-linecap="round"
                                          stroke-linejoin="round" />
                                </svg>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="articles__addition">
            <div class="tabs">
                <div class="tabs__head">
                    <div class="tabs__btns">
                        <button class="tabs__btn tabs__btn--active" data-type="questionnaire"><span class="tabs__count"></span>
                            <div class="tabs__btn-text">Похожие анкеты</div>
                        </button>
                        <button class="tabs__btn" data-type="viewed"><span class="tabs__count">18</span>
                            <div class="tabs__btn-text">Недавно просмотренные</div>
                        </button>
                        <button class="tabs__btn" data-type="Favorites"><span class="tabs__count">3</span>
                            <div class="tabs__btn-text">Избранные анкеты</div>
                        </button>
                    </div>
                </div>
                <div class="tabs__inner">
                    <div class="tabs__container">
                        <div class="tabs__content" data-type="questionnaire">
                            <div class="tabs__slider swiper-container">
                                <div class="tabs__wrapper swiper-wrapper">
                                    <div class="tabs__slide swiper-slide">
                                        <div class="tabs__slide-head">
                                            <div class="tabs__slide-name">Оля</div>
                                            <button class="tabs__slide-favorites"><svg class="add-favorites js-favorites"
                                                                                       xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24">
                                                    <path class="path-empty"
                                                          d="M6.28 3c3.236.001 4.973 3.491 5.72 5.031.75-1.547 2.469-5.021 5.726-5.021 2.058 0 4.274 1.309 4.274 4.182 0 3.442-4.744 7.851-10 13-5.258-5.151-10-9.559-10-13 0-2.676 1.965-4.193 4.28-4.192zm.001-2c-3.183 0-6.281 2.187-6.281 6.192 0 4.661 5.57 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-4.011-3.097-6.182-6.274-6.182-2.204 0-4.446 1.042-5.726 3.238-1.285-2.206-3.522-3.248-5.719-3.248z">
                                                    </path>
                                                    <path class="path-full" xmlns="http://www.w3.org/2000/svg"
                                                          d="M12 4.248c-3.148-5.402-12-3.825-12 2.944 0 4.661 5.571 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-6.792-8.875-8.306-12-2.944z">
                                                    </path>
                                                </svg></button>
                                        </div>
                                        <div class="tabs__slide-body"><img src="./images/img-1-db8e7456.jpg" alt="Оля">
                                            <div class="tabs__slide-stars">
                                                <div class="tabs__slide-title">Массажный салон</div>
                                                <div class="tabs__slide-rating">
                                                    <div class="rating-static rating-static--five">
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tabs__slide-bottom">
                                            <div class="tabs__slide-line">
                                                <div class="tabs__slide-title">Апартаменты</div>
                                                <div class="tabs__slide-price">от&nbsp<span>2 500 ₽</span> /час </div>
                                            </div>
                                            <div class="tabs__slide-line">
                                                <div class="tabs__slide-title">Выезд</div>
                                                <div class="tabs__slide-price">от&nbsp<span>4 500 ₽</span> /час </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tabs__slide swiper-slide">
                                        <div class="tabs__slide-head">
                                            <div class="tabs__slide-name">Катя</div>
                                            <button class="tabs__slide-favorites"><svg class="add-favorites js-favorites"
                                                                                       xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24">
                                                    <path class="path-empty"
                                                          d="M6.28 3c3.236.001 4.973 3.491 5.72 5.031.75-1.547 2.469-5.021 5.726-5.021 2.058 0 4.274 1.309 4.274 4.182 0 3.442-4.744 7.851-10 13-5.258-5.151-10-9.559-10-13 0-2.676 1.965-4.193 4.28-4.192zm.001-2c-3.183 0-6.281 2.187-6.281 6.192 0 4.661 5.57 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-4.011-3.097-6.182-6.274-6.182-2.204 0-4.446 1.042-5.726 3.238-1.285-2.206-3.522-3.248-5.719-3.248z">
                                                    </path>
                                                    <path class="path-full" xmlns="http://www.w3.org/2000/svg"
                                                          d="M12 4.248c-3.148-5.402-12-3.825-12 2.944 0 4.661 5.571 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-6.792-8.875-8.306-12-2.944z">
                                                    </path>
                                                </svg></button>
                                        </div>
                                        <div class="tabs__slide-body"><img src="./images/img-1-db8e7456.jpg" alt="Катя">
                                            <div class="tabs__slide-stars">
                                                <div class="tabs__slide-title">Массажный салон</div>
                                                <div class="tabs__slide-rating">
                                                    <div class="rating-static rating-static--two">
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tabs__slide-bottom">
                                            <div class="tabs__slide-line">
                                                <div class="tabs__slide-title">Апартаменты</div>
                                                <div class="tabs__slide-price">от&nbsp<span>2 500 ₽</span> /час </div>
                                            </div>
                                            <div class="tabs__slide-line">
                                                <div class="tabs__slide-title">Выезд</div>
                                                <div class="tabs__slide-price">от&nbsp<span>4 500 ₽</span> /час </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tabs__slide swiper-slide">
                                        <div class="tabs__slide-head">
                                            <div class="tabs__slide-name">Настя</div>
                                            <button class="tabs__slide-favorites"><svg class="add-favorites js-favorites"
                                                                                       xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24">
                                                    <path class="path-empty"
                                                          d="M6.28 3c3.236.001 4.973 3.491 5.72 5.031.75-1.547 2.469-5.021 5.726-5.021 2.058 0 4.274 1.309 4.274 4.182 0 3.442-4.744 7.851-10 13-5.258-5.151-10-9.559-10-13 0-2.676 1.965-4.193 4.28-4.192zm.001-2c-3.183 0-6.281 2.187-6.281 6.192 0 4.661 5.57 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-4.011-3.097-6.182-6.274-6.182-2.204 0-4.446 1.042-5.726 3.238-1.285-2.206-3.522-3.248-5.719-3.248z">
                                                    </path>
                                                    <path class="path-full" xmlns="http://www.w3.org/2000/svg"
                                                          d="M12 4.248c-3.148-5.402-12-3.825-12 2.944 0 4.661 5.571 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-6.792-8.875-8.306-12-2.944z">
                                                    </path>
                                                </svg></button>
                                        </div>
                                        <div class="tabs__slide-body"><img src="./images/img-1-db8e7456.jpg" alt="Настя">
                                            <div class="tabs__slide-stars">
                                                <div class="tabs__slide-title">Массажный салон</div>
                                                <div class="tabs__slide-rating">
                                                    <div class="rating-static rating-static--three">
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tabs__slide-bottom">
                                            <div class="tabs__slide-line">
                                                <div class="tabs__slide-title">Апартаменты</div>
                                                <div class="tabs__slide-price">от&nbsp<span>2 500 ₽</span> /час </div>
                                            </div>
                                            <div class="tabs__slide-line">
                                                <div class="tabs__slide-title">Выезд</div>
                                                <div class="tabs__slide-price">от&nbsp<span>4 500 ₽</span> /час </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tabs__slide swiper-slide">
                                        <div class="tabs__slide-head">
                                            <div class="tabs__slide-name">Алена</div>
                                            <button class="tabs__slide-favorites"><svg class="add-favorites js-favorites"
                                                                                       xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24">
                                                    <path class="path-empty"
                                                          d="M6.28 3c3.236.001 4.973 3.491 5.72 5.031.75-1.547 2.469-5.021 5.726-5.021 2.058 0 4.274 1.309 4.274 4.182 0 3.442-4.744 7.851-10 13-5.258-5.151-10-9.559-10-13 0-2.676 1.965-4.193 4.28-4.192zm.001-2c-3.183 0-6.281 2.187-6.281 6.192 0 4.661 5.57 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-4.011-3.097-6.182-6.274-6.182-2.204 0-4.446 1.042-5.726 3.238-1.285-2.206-3.522-3.248-5.719-3.248z">
                                                    </path>
                                                    <path class="path-full" xmlns="http://www.w3.org/2000/svg"
                                                          d="M12 4.248c-3.148-5.402-12-3.825-12 2.944 0 4.661 5.571 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-6.792-8.875-8.306-12-2.944z">
                                                    </path>
                                                </svg></button>
                                        </div>
                                        <div class="tabs__slide-body"><img src="./images/img-1-db8e7456.jpg" alt="Алена">
                                            <div class="tabs__slide-stars">
                                                <div class="tabs__slide-title">Массажный салон</div>
                                                <div class="tabs__slide-rating">
                                                    <div class="rating-static rating-static--one">
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tabs__slide-bottom">
                                            <div class="tabs__slide-line">
                                                <div class="tabs__slide-title">Апартаменты</div>
                                                <div class="tabs__slide-price">от&nbsp<span>2 500 ₽</span> /час </div>
                                            </div>
                                            <div class="tabs__slide-line">
                                                <div class="tabs__slide-title">Выезд</div>
                                                <div class="tabs__slide-price">от&nbsp<span>4 500 ₽</span> /час </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tabs__slide swiper-slide">
                                        <div class="tabs__slide-head">
                                            <div class="tabs__slide-name">Оля</div>
                                            <button class="tabs__slide-favorites"><svg class="add-favorites js-favorites"
                                                                                       xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24">
                                                    <path class="path-empty"
                                                          d="M6.28 3c3.236.001 4.973 3.491 5.72 5.031.75-1.547 2.469-5.021 5.726-5.021 2.058 0 4.274 1.309 4.274 4.182 0 3.442-4.744 7.851-10 13-5.258-5.151-10-9.559-10-13 0-2.676 1.965-4.193 4.28-4.192zm.001-2c-3.183 0-6.281 2.187-6.281 6.192 0 4.661 5.57 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-4.011-3.097-6.182-6.274-6.182-2.204 0-4.446 1.042-5.726 3.238-1.285-2.206-3.522-3.248-5.719-3.248z">
                                                    </path>
                                                    <path class="path-full" xmlns="http://www.w3.org/2000/svg"
                                                          d="M12 4.248c-3.148-5.402-12-3.825-12 2.944 0 4.661 5.571 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-6.792-8.875-8.306-12-2.944z">
                                                    </path>
                                                </svg></button>
                                        </div>
                                        <div class="tabs__slide-body"><img src="./images/img-1-db8e7456.jpg" alt="Оля">
                                            <div class="tabs__slide-stars">
                                                <div class="tabs__slide-title">Массажный салон</div>
                                                <div class="tabs__slide-rating">
                                                    <div class="rating-static rating-static--five">
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tabs__slide-bottom">
                                            <div class="tabs__slide-line">
                                                <div class="tabs__slide-title">Апартаменты</div>
                                                <div class="tabs__slide-price">от&nbsp<span>2 500 ₽</span> /час </div>
                                            </div>
                                            <div class="tabs__slide-line">
                                                <div class="tabs__slide-title">Выезд</div>
                                                <div class="tabs__slide-price">от&nbsp<span>4 500 ₽</span> /час </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tabs__slide swiper-slide">
                                        <div class="tabs__slide-head">
                                            <div class="tabs__slide-name">Катя</div>
                                            <button class="tabs__slide-favorites"><svg class="add-favorites js-favorites"
                                                                                       xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24">
                                                    <path class="path-empty"
                                                          d="M6.28 3c3.236.001 4.973 3.491 5.72 5.031.75-1.547 2.469-5.021 5.726-5.021 2.058 0 4.274 1.309 4.274 4.182 0 3.442-4.744 7.851-10 13-5.258-5.151-10-9.559-10-13 0-2.676 1.965-4.193 4.28-4.192zm.001-2c-3.183 0-6.281 2.187-6.281 6.192 0 4.661 5.57 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-4.011-3.097-6.182-6.274-6.182-2.204 0-4.446 1.042-5.726 3.238-1.285-2.206-3.522-3.248-5.719-3.248z">
                                                    </path>
                                                    <path class="path-full" xmlns="http://www.w3.org/2000/svg"
                                                          d="M12 4.248c-3.148-5.402-12-3.825-12 2.944 0 4.661 5.571 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-6.792-8.875-8.306-12-2.944z">
                                                    </path>
                                                </svg></button>
                                        </div>
                                        <div class="tabs__slide-body"><img src="./images/img-1-db8e7456.jpg" alt="Катя">
                                            <div class="tabs__slide-stars">
                                                <div class="tabs__slide-title">Массажный салон</div>
                                                <div class="tabs__slide-rating">
                                                    <div class="rating-static rating-static--two">
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tabs__slide-bottom">
                                            <div class="tabs__slide-line">
                                                <div class="tabs__slide-title">Апартаменты</div>
                                                <div class="tabs__slide-price">от&nbsp<span>2 500 ₽</span> /час </div>
                                            </div>
                                            <div class="tabs__slide-line">
                                                <div class="tabs__slide-title">Выезд</div>
                                                <div class="tabs__slide-price">от&nbsp<span>4 500 ₽</span> /час </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tabs__slide swiper-slide">
                                        <div class="tabs__slide-head">
                                            <div class="tabs__slide-name">Настя</div>
                                            <button class="tabs__slide-favorites"><svg class="add-favorites js-favorites"
                                                                                       xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24">
                                                    <path class="path-empty"
                                                          d="M6.28 3c3.236.001 4.973 3.491 5.72 5.031.75-1.547 2.469-5.021 5.726-5.021 2.058 0 4.274 1.309 4.274 4.182 0 3.442-4.744 7.851-10 13-5.258-5.151-10-9.559-10-13 0-2.676 1.965-4.193 4.28-4.192zm.001-2c-3.183 0-6.281 2.187-6.281 6.192 0 4.661 5.57 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-4.011-3.097-6.182-6.274-6.182-2.204 0-4.446 1.042-5.726 3.238-1.285-2.206-3.522-3.248-5.719-3.248z">
                                                    </path>
                                                    <path class="path-full" xmlns="http://www.w3.org/2000/svg"
                                                          d="M12 4.248c-3.148-5.402-12-3.825-12 2.944 0 4.661 5.571 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-6.792-8.875-8.306-12-2.944z">
                                                    </path>
                                                </svg></button>
                                        </div>
                                        <div class="tabs__slide-body"><img src="./images/img-1-db8e7456.jpg" alt="Настя">
                                            <div class="tabs__slide-stars">
                                                <div class="tabs__slide-title">Массажный салон</div>
                                                <div class="tabs__slide-rating">
                                                    <div class="rating-static rating-static--three">
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tabs__slide-bottom">
                                            <div class="tabs__slide-line">
                                                <div class="tabs__slide-title">Апартаменты</div>
                                                <div class="tabs__slide-price">от&nbsp<span>2 500 ₽</span> /час </div>
                                            </div>
                                            <div class="tabs__slide-line">
                                                <div class="tabs__slide-title">Выезд</div>
                                                <div class="tabs__slide-price">от&nbsp<span>4 500 ₽</span> /час </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tabs__slide swiper-slide">
                                        <div class="tabs__slide-head">
                                            <div class="tabs__slide-name">Алена</div>
                                            <button class="tabs__slide-favorites"><svg class="add-favorites js-favorites"
                                                                                       xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24">
                                                    <path class="path-empty"
                                                          d="M6.28 3c3.236.001 4.973 3.491 5.72 5.031.75-1.547 2.469-5.021 5.726-5.021 2.058 0 4.274 1.309 4.274 4.182 0 3.442-4.744 7.851-10 13-5.258-5.151-10-9.559-10-13 0-2.676 1.965-4.193 4.28-4.192zm.001-2c-3.183 0-6.281 2.187-6.281 6.192 0 4.661 5.57 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-4.011-3.097-6.182-6.274-6.182-2.204 0-4.446 1.042-5.726 3.238-1.285-2.206-3.522-3.248-5.719-3.248z">
                                                    </path>
                                                    <path class="path-full" xmlns="http://www.w3.org/2000/svg"
                                                          d="M12 4.248c-3.148-5.402-12-3.825-12 2.944 0 4.661 5.571 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-6.792-8.875-8.306-12-2.944z">
                                                    </path>
                                                </svg></button>
                                        </div>
                                        <div class="tabs__slide-body"><img src="./images/img-1-db8e7456.jpg" alt="Алена">
                                            <div class="tabs__slide-stars">
                                                <div class="tabs__slide-title">Массажный салон</div>
                                                <div class="tabs__slide-rating">
                                                    <div class="rating-static rating-static--one">
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tabs__slide-bottom">
                                            <div class="tabs__slide-line">
                                                <div class="tabs__slide-title">Апартаменты</div>
                                                <div class="tabs__slide-price">от&nbsp<span>2 500 ₽</span> /час </div>
                                            </div>
                                            <div class="tabs__slide-line">
                                                <div class="tabs__slide-title">Выезд</div>
                                                <div class="tabs__slide-price">от&nbsp<span>4 500 ₽</span> /час </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tabs__slider-pagination slider-pagination swiper-pagination">
                                    <!-- Add Arrows-->
                                </div>
                                <div class="tabs__slider-next slider-next swiper-button-next"></div>
                                <div class="tabs__slider-prev slider-prev swiper-button-prev"></div>
                            </div>
                        </div>
                        <div class="tabs__content tabs__content--hidden" data-type="viewed">
                            <div class="tabs__slider swiper-container">
                                <div class="tabs__wrapper swiper-wrapper">
                                    <div class="tabs__slide swiper-slide">
                                        <div class="tabs__slide-head">
                                            <div class="tabs__slide-name">Оля</div>
                                            <button class="tabs__slide-favorites"><svg class="add-favorites js-favorites"
                                                                                       xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24">
                                                    <path class="path-empty"
                                                          d="M6.28 3c3.236.001 4.973 3.491 5.72 5.031.75-1.547 2.469-5.021 5.726-5.021 2.058 0 4.274 1.309 4.274 4.182 0 3.442-4.744 7.851-10 13-5.258-5.151-10-9.559-10-13 0-2.676 1.965-4.193 4.28-4.192zm.001-2c-3.183 0-6.281 2.187-6.281 6.192 0 4.661 5.57 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-4.011-3.097-6.182-6.274-6.182-2.204 0-4.446 1.042-5.726 3.238-1.285-2.206-3.522-3.248-5.719-3.248z">
                                                    </path>
                                                    <path class="path-full" xmlns="http://www.w3.org/2000/svg"
                                                          d="M12 4.248c-3.148-5.402-12-3.825-12 2.944 0 4.661 5.571 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-6.792-8.875-8.306-12-2.944z">
                                                    </path>
                                                </svg></button>
                                        </div>
                                        <div class="tabs__slide-body"><img src="./images/img-1-db8e7456.jpg" alt="Оля">
                                            <div class="tabs__slide-stars">
                                                <div class="tabs__slide-title">Массажный салон</div>
                                                <div class="tabs__slide-rating">
                                                    <div class="rating-static rating-static--five">
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tabs__slide-bottom">
                                            <div class="tabs__slide-line">
                                                <div class="tabs__slide-title">Апартаменты</div>
                                                <div class="tabs__slide-price">от&nbsp<span>2 500 ₽</span> /час </div>
                                            </div>
                                            <div class="tabs__slide-line">
                                                <div class="tabs__slide-title">Выезд</div>
                                                <div class="tabs__slide-price">от&nbsp<span>4 500 ₽</span> /час </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tabs__slide swiper-slide">
                                        <div class="tabs__slide-head">
                                            <div class="tabs__slide-name">Катя</div>
                                            <button class="tabs__slide-favorites"><svg class="add-favorites js-favorites"
                                                                                       xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24">
                                                    <path class="path-empty"
                                                          d="M6.28 3c3.236.001 4.973 3.491 5.72 5.031.75-1.547 2.469-5.021 5.726-5.021 2.058 0 4.274 1.309 4.274 4.182 0 3.442-4.744 7.851-10 13-5.258-5.151-10-9.559-10-13 0-2.676 1.965-4.193 4.28-4.192zm.001-2c-3.183 0-6.281 2.187-6.281 6.192 0 4.661 5.57 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-4.011-3.097-6.182-6.274-6.182-2.204 0-4.446 1.042-5.726 3.238-1.285-2.206-3.522-3.248-5.719-3.248z">
                                                    </path>
                                                    <path class="path-full" xmlns="http://www.w3.org/2000/svg"
                                                          d="M12 4.248c-3.148-5.402-12-3.825-12 2.944 0 4.661 5.571 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-6.792-8.875-8.306-12-2.944z">
                                                    </path>
                                                </svg></button>
                                        </div>
                                        <div class="tabs__slide-body"><img src="./images/img-1-db8e7456.jpg" alt="Катя">
                                            <div class="tabs__slide-stars">
                                                <div class="tabs__slide-title">Массажный салон</div>
                                                <div class="tabs__slide-rating">
                                                    <div class="rating-static rating-static--two">
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tabs__slide-bottom">
                                            <div class="tabs__slide-line">
                                                <div class="tabs__slide-title">Апартаменты</div>
                                                <div class="tabs__slide-price">от&nbsp<span>2 500 ₽</span> /час </div>
                                            </div>
                                            <div class="tabs__slide-line">
                                                <div class="tabs__slide-title">Выезд</div>
                                                <div class="tabs__slide-price">от&nbsp<span>4 500 ₽</span> /час </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tabs__slide swiper-slide">
                                        <div class="tabs__slide-head">
                                            <div class="tabs__slide-name">Настя</div>
                                            <button class="tabs__slide-favorites"><svg class="add-favorites js-favorites"
                                                                                       xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24">
                                                    <path class="path-empty"
                                                          d="M6.28 3c3.236.001 4.973 3.491 5.72 5.031.75-1.547 2.469-5.021 5.726-5.021 2.058 0 4.274 1.309 4.274 4.182 0 3.442-4.744 7.851-10 13-5.258-5.151-10-9.559-10-13 0-2.676 1.965-4.193 4.28-4.192zm.001-2c-3.183 0-6.281 2.187-6.281 6.192 0 4.661 5.57 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-4.011-3.097-6.182-6.274-6.182-2.204 0-4.446 1.042-5.726 3.238-1.285-2.206-3.522-3.248-5.719-3.248z">
                                                    </path>
                                                    <path class="path-full" xmlns="http://www.w3.org/2000/svg"
                                                          d="M12 4.248c-3.148-5.402-12-3.825-12 2.944 0 4.661 5.571 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-6.792-8.875-8.306-12-2.944z">
                                                    </path>
                                                </svg></button>
                                        </div>
                                        <div class="tabs__slide-body"><img src="./images/img-1-db8e7456.jpg" alt="Настя">
                                            <div class="tabs__slide-stars">
                                                <div class="tabs__slide-title">Массажный салон</div>
                                                <div class="tabs__slide-rating">
                                                    <div class="rating-static rating-static--three">
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tabs__slide-bottom">
                                            <div class="tabs__slide-line">
                                                <div class="tabs__slide-title">Апартаменты</div>
                                                <div class="tabs__slide-price">от&nbsp<span>2 500 ₽</span> /час </div>
                                            </div>
                                            <div class="tabs__slide-line">
                                                <div class="tabs__slide-title">Выезд</div>
                                                <div class="tabs__slide-price">от&nbsp<span>4 500 ₽</span> /час </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tabs__slide swiper-slide">
                                        <div class="tabs__slide-head">
                                            <div class="tabs__slide-name">Алена</div>
                                            <button class="tabs__slide-favorites"><svg class="add-favorites js-favorites"
                                                                                       xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24">
                                                    <path class="path-empty"
                                                          d="M6.28 3c3.236.001 4.973 3.491 5.72 5.031.75-1.547 2.469-5.021 5.726-5.021 2.058 0 4.274 1.309 4.274 4.182 0 3.442-4.744 7.851-10 13-5.258-5.151-10-9.559-10-13 0-2.676 1.965-4.193 4.28-4.192zm.001-2c-3.183 0-6.281 2.187-6.281 6.192 0 4.661 5.57 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-4.011-3.097-6.182-6.274-6.182-2.204 0-4.446 1.042-5.726 3.238-1.285-2.206-3.522-3.248-5.719-3.248z">
                                                    </path>
                                                    <path class="path-full" xmlns="http://www.w3.org/2000/svg"
                                                          d="M12 4.248c-3.148-5.402-12-3.825-12 2.944 0 4.661 5.571 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-6.792-8.875-8.306-12-2.944z">
                                                    </path>
                                                </svg></button>
                                        </div>
                                        <div class="tabs__slide-body"><img src="./images/img-1-db8e7456.jpg" alt="Алена">
                                            <div class="tabs__slide-stars">
                                                <div class="tabs__slide-title">Массажный салон</div>
                                                <div class="tabs__slide-rating">
                                                    <div class="rating-static rating-static--one">
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tabs__slide-bottom">
                                            <div class="tabs__slide-line">
                                                <div class="tabs__slide-title">Апартаменты</div>
                                                <div class="tabs__slide-price">от&nbsp<span>2 500 ₽</span> /час </div>
                                            </div>
                                            <div class="tabs__slide-line">
                                                <div class="tabs__slide-title">Выезд</div>
                                                <div class="tabs__slide-price">от&nbsp<span>4 500 ₽</span> /час </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tabs__slide swiper-slide">
                                        <div class="tabs__slide-head">
                                            <div class="tabs__slide-name">Оля</div>
                                            <button class="tabs__slide-favorites"><svg class="add-favorites js-favorites"
                                                                                       xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24">
                                                    <path class="path-empty"
                                                          d="M6.28 3c3.236.001 4.973 3.491 5.72 5.031.75-1.547 2.469-5.021 5.726-5.021 2.058 0 4.274 1.309 4.274 4.182 0 3.442-4.744 7.851-10 13-5.258-5.151-10-9.559-10-13 0-2.676 1.965-4.193 4.28-4.192zm.001-2c-3.183 0-6.281 2.187-6.281 6.192 0 4.661 5.57 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-4.011-3.097-6.182-6.274-6.182-2.204 0-4.446 1.042-5.726 3.238-1.285-2.206-3.522-3.248-5.719-3.248z">
                                                    </path>
                                                    <path class="path-full" xmlns="http://www.w3.org/2000/svg"
                                                          d="M12 4.248c-3.148-5.402-12-3.825-12 2.944 0 4.661 5.571 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-6.792-8.875-8.306-12-2.944z">
                                                    </path>
                                                </svg></button>
                                        </div>
                                        <div class="tabs__slide-body"><img src="./images/img-1-db8e7456.jpg" alt="Оля">
                                            <div class="tabs__slide-stars">
                                                <div class="tabs__slide-title">Массажный салон</div>
                                                <div class="tabs__slide-rating">
                                                    <div class="rating-static rating-static--five">
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tabs__slide-bottom">
                                            <div class="tabs__slide-line">
                                                <div class="tabs__slide-title">Апартаменты</div>
                                                <div class="tabs__slide-price">от&nbsp<span>2 500 ₽</span> /час </div>
                                            </div>
                                            <div class="tabs__slide-line">
                                                <div class="tabs__slide-title">Выезд</div>
                                                <div class="tabs__slide-price">от&nbsp<span>4 500 ₽</span> /час </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tabs__slide swiper-slide">
                                        <div class="tabs__slide-head">
                                            <div class="tabs__slide-name">Катя</div>
                                            <button class="tabs__slide-favorites"><svg class="add-favorites js-favorites"
                                                                                       xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24">
                                                    <path class="path-empty"
                                                          d="M6.28 3c3.236.001 4.973 3.491 5.72 5.031.75-1.547 2.469-5.021 5.726-5.021 2.058 0 4.274 1.309 4.274 4.182 0 3.442-4.744 7.851-10 13-5.258-5.151-10-9.559-10-13 0-2.676 1.965-4.193 4.28-4.192zm.001-2c-3.183 0-6.281 2.187-6.281 6.192 0 4.661 5.57 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-4.011-3.097-6.182-6.274-6.182-2.204 0-4.446 1.042-5.726 3.238-1.285-2.206-3.522-3.248-5.719-3.248z">
                                                    </path>
                                                    <path class="path-full" xmlns="http://www.w3.org/2000/svg"
                                                          d="M12 4.248c-3.148-5.402-12-3.825-12 2.944 0 4.661 5.571 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-6.792-8.875-8.306-12-2.944z">
                                                    </path>
                                                </svg></button>
                                        </div>
                                        <div class="tabs__slide-body"><img src="./images/img-1-db8e7456.jpg" alt="Катя">
                                            <div class="tabs__slide-stars">
                                                <div class="tabs__slide-title">Массажный салон</div>
                                                <div class="tabs__slide-rating">
                                                    <div class="rating-static rating-static--two">
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tabs__slide-bottom">
                                            <div class="tabs__slide-line">
                                                <div class="tabs__slide-title">Апартаменты</div>
                                                <div class="tabs__slide-price">от&nbsp<span>2 500 ₽</span> /час </div>
                                            </div>
                                            <div class="tabs__slide-line">
                                                <div class="tabs__slide-title">Выезд</div>
                                                <div class="tabs__slide-price">от&nbsp<span>4 500 ₽</span> /час </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tabs__slide swiper-slide">
                                        <div class="tabs__slide-head">
                                            <div class="tabs__slide-name">Настя</div>
                                            <button class="tabs__slide-favorites"><svg class="add-favorites js-favorites"
                                                                                       xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24">
                                                    <path class="path-empty"
                                                          d="M6.28 3c3.236.001 4.973 3.491 5.72 5.031.75-1.547 2.469-5.021 5.726-5.021 2.058 0 4.274 1.309 4.274 4.182 0 3.442-4.744 7.851-10 13-5.258-5.151-10-9.559-10-13 0-2.676 1.965-4.193 4.28-4.192zm.001-2c-3.183 0-6.281 2.187-6.281 6.192 0 4.661 5.57 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-4.011-3.097-6.182-6.274-6.182-2.204 0-4.446 1.042-5.726 3.238-1.285-2.206-3.522-3.248-5.719-3.248z">
                                                    </path>
                                                    <path class="path-full" xmlns="http://www.w3.org/2000/svg"
                                                          d="M12 4.248c-3.148-5.402-12-3.825-12 2.944 0 4.661 5.571 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-6.792-8.875-8.306-12-2.944z">
                                                    </path>
                                                </svg></button>
                                        </div>
                                        <div class="tabs__slide-body"><img src="./images/img-1-db8e7456.jpg" alt="Настя">
                                            <div class="tabs__slide-stars">
                                                <div class="tabs__slide-title">Массажный салон</div>
                                                <div class="tabs__slide-rating">
                                                    <div class="rating-static rating-static--three">
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tabs__slide-bottom">
                                            <div class="tabs__slide-line">
                                                <div class="tabs__slide-title">Апартаменты</div>
                                                <div class="tabs__slide-price">от&nbsp<span>2 500 ₽</span> /час </div>
                                            </div>
                                            <div class="tabs__slide-line">
                                                <div class="tabs__slide-title">Выезд</div>
                                                <div class="tabs__slide-price">от&nbsp<span>4 500 ₽</span> /час </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tabs__slide swiper-slide">
                                        <div class="tabs__slide-head">
                                            <div class="tabs__slide-name">Алена</div>
                                            <button class="tabs__slide-favorites"><svg class="add-favorites js-favorites"
                                                                                       xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24">
                                                    <path class="path-empty"
                                                          d="M6.28 3c3.236.001 4.973 3.491 5.72 5.031.75-1.547 2.469-5.021 5.726-5.021 2.058 0 4.274 1.309 4.274 4.182 0 3.442-4.744 7.851-10 13-5.258-5.151-10-9.559-10-13 0-2.676 1.965-4.193 4.28-4.192zm.001-2c-3.183 0-6.281 2.187-6.281 6.192 0 4.661 5.57 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-4.011-3.097-6.182-6.274-6.182-2.204 0-4.446 1.042-5.726 3.238-1.285-2.206-3.522-3.248-5.719-3.248z">
                                                    </path>
                                                    <path class="path-full" xmlns="http://www.w3.org/2000/svg"
                                                          d="M12 4.248c-3.148-5.402-12-3.825-12 2.944 0 4.661 5.571 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-6.792-8.875-8.306-12-2.944z">
                                                    </path>
                                                </svg></button>
                                        </div>
                                        <div class="tabs__slide-body"><img src="./images/img-1-db8e7456.jpg" alt="Алена">
                                            <div class="tabs__slide-stars">
                                                <div class="tabs__slide-title">Массажный салон</div>
                                                <div class="tabs__slide-rating">
                                                    <div class="rating-static rating-static--one">
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tabs__slide-bottom">
                                            <div class="tabs__slide-line">
                                                <div class="tabs__slide-title">Апартаменты</div>
                                                <div class="tabs__slide-price">от&nbsp<span>2 500 ₽</span> /час </div>
                                            </div>
                                            <div class="tabs__slide-line">
                                                <div class="tabs__slide-title">Выезд</div>
                                                <div class="tabs__slide-price">от&nbsp<span>4 500 ₽</span> /час </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tabs__slider-pagination slider-pagination swiper-pagination">
                                    <!-- Add Arrows-->
                                </div>
                                <div class="tabs__slider-next slider-next swiper-button-next"></div>
                                <div class="tabs__slider-prev slider-prev swiper-button-prev"></div>
                            </div>
                        </div>
                        <div class="tabs__content tabs__content--hidden" data-type="Favorites">
                            <div class="tabs__slider swiper-container">
                                <div class="tabs__wrapper swiper-wrapper">
                                    <div class="tabs__slide swiper-slide">
                                        <div class="tabs__slide-head">
                                            <div class="tabs__slide-name">Оля</div>
                                            <button class="tabs__slide-favorites"><svg class="add-favorites js-favorites"
                                                                                       xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24">
                                                    <path class="path-empty"
                                                          d="M6.28 3c3.236.001 4.973 3.491 5.72 5.031.75-1.547 2.469-5.021 5.726-5.021 2.058 0 4.274 1.309 4.274 4.182 0 3.442-4.744 7.851-10 13-5.258-5.151-10-9.559-10-13 0-2.676 1.965-4.193 4.28-4.192zm.001-2c-3.183 0-6.281 2.187-6.281 6.192 0 4.661 5.57 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-4.011-3.097-6.182-6.274-6.182-2.204 0-4.446 1.042-5.726 3.238-1.285-2.206-3.522-3.248-5.719-3.248z">
                                                    </path>
                                                    <path class="path-full" xmlns="http://www.w3.org/2000/svg"
                                                          d="M12 4.248c-3.148-5.402-12-3.825-12 2.944 0 4.661 5.571 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-6.792-8.875-8.306-12-2.944z">
                                                    </path>
                                                </svg></button>
                                        </div>
                                        <div class="tabs__slide-body"><img src="./images/img-1-db8e7456.jpg" alt="Оля">
                                            <div class="tabs__slide-stars">
                                                <div class="tabs__slide-title">Массажный салон</div>
                                                <div class="tabs__slide-rating">
                                                    <div class="rating-static rating-static--five">
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tabs__slide-bottom">
                                            <div class="tabs__slide-line">
                                                <div class="tabs__slide-title">Апартаменты</div>
                                                <div class="tabs__slide-price">от&nbsp<span>2 500 ₽</span> /час </div>
                                            </div>
                                            <div class="tabs__slide-line">
                                                <div class="tabs__slide-title">Выезд</div>
                                                <div class="tabs__slide-price">от&nbsp<span>4 500 ₽</span> /час </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tabs__slide swiper-slide">
                                        <div class="tabs__slide-head">
                                            <div class="tabs__slide-name">Катя</div>
                                            <button class="tabs__slide-favorites"><svg class="add-favorites js-favorites"
                                                                                       xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24">
                                                    <path class="path-empty"
                                                          d="M6.28 3c3.236.001 4.973 3.491 5.72 5.031.75-1.547 2.469-5.021 5.726-5.021 2.058 0 4.274 1.309 4.274 4.182 0 3.442-4.744 7.851-10 13-5.258-5.151-10-9.559-10-13 0-2.676 1.965-4.193 4.28-4.192zm.001-2c-3.183 0-6.281 2.187-6.281 6.192 0 4.661 5.57 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-4.011-3.097-6.182-6.274-6.182-2.204 0-4.446 1.042-5.726 3.238-1.285-2.206-3.522-3.248-5.719-3.248z">
                                                    </path>
                                                    <path class="path-full" xmlns="http://www.w3.org/2000/svg"
                                                          d="M12 4.248c-3.148-5.402-12-3.825-12 2.944 0 4.661 5.571 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-6.792-8.875-8.306-12-2.944z">
                                                    </path>
                                                </svg></button>
                                        </div>
                                        <div class="tabs__slide-body"><img src="./images/img-1-db8e7456.jpg" alt="Катя">
                                            <div class="tabs__slide-stars">
                                                <div class="tabs__slide-title">Массажный салон</div>
                                                <div class="tabs__slide-rating">
                                                    <div class="rating-static rating-static--two">
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tabs__slide-bottom">
                                            <div class="tabs__slide-line">
                                                <div class="tabs__slide-title">Апартаменты</div>
                                                <div class="tabs__slide-price">от&nbsp<span>2 500 ₽</span> /час </div>
                                            </div>
                                            <div class="tabs__slide-line">
                                                <div class="tabs__slide-title">Выезд</div>
                                                <div class="tabs__slide-price">от&nbsp<span>4 500 ₽</span> /час </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tabs__slide swiper-slide">
                                        <div class="tabs__slide-head">
                                            <div class="tabs__slide-name">Настя</div>
                                            <button class="tabs__slide-favorites"><svg class="add-favorites js-favorites"
                                                                                       xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24">
                                                    <path class="path-empty"
                                                          d="M6.28 3c3.236.001 4.973 3.491 5.72 5.031.75-1.547 2.469-5.021 5.726-5.021 2.058 0 4.274 1.309 4.274 4.182 0 3.442-4.744 7.851-10 13-5.258-5.151-10-9.559-10-13 0-2.676 1.965-4.193 4.28-4.192zm.001-2c-3.183 0-6.281 2.187-6.281 6.192 0 4.661 5.57 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-4.011-3.097-6.182-6.274-6.182-2.204 0-4.446 1.042-5.726 3.238-1.285-2.206-3.522-3.248-5.719-3.248z">
                                                    </path>
                                                    <path class="path-full" xmlns="http://www.w3.org/2000/svg"
                                                          d="M12 4.248c-3.148-5.402-12-3.825-12 2.944 0 4.661 5.571 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-6.792-8.875-8.306-12-2.944z">
                                                    </path>
                                                </svg></button>
                                        </div>
                                        <div class="tabs__slide-body"><img src="./images/img-1-db8e7456.jpg" alt="Настя">
                                            <div class="tabs__slide-stars">
                                                <div class="tabs__slide-title">Массажный салон</div>
                                                <div class="tabs__slide-rating">
                                                    <div class="rating-static rating-static--three">
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tabs__slide-bottom">
                                            <div class="tabs__slide-line">
                                                <div class="tabs__slide-title">Апартаменты</div>
                                                <div class="tabs__slide-price">от&nbsp<span>2 500 ₽</span> /час </div>
                                            </div>
                                            <div class="tabs__slide-line">
                                                <div class="tabs__slide-title">Выезд</div>
                                                <div class="tabs__slide-price">от&nbsp<span>4 500 ₽</span> /час </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tabs__slide swiper-slide">
                                        <div class="tabs__slide-head">
                                            <div class="tabs__slide-name">Алена</div>
                                            <button class="tabs__slide-favorites"><svg class="add-favorites js-favorites"
                                                                                       xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24">
                                                    <path class="path-empty"
                                                          d="M6.28 3c3.236.001 4.973 3.491 5.72 5.031.75-1.547 2.469-5.021 5.726-5.021 2.058 0 4.274 1.309 4.274 4.182 0 3.442-4.744 7.851-10 13-5.258-5.151-10-9.559-10-13 0-2.676 1.965-4.193 4.28-4.192zm.001-2c-3.183 0-6.281 2.187-6.281 6.192 0 4.661 5.57 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-4.011-3.097-6.182-6.274-6.182-2.204 0-4.446 1.042-5.726 3.238-1.285-2.206-3.522-3.248-5.719-3.248z">
                                                    </path>
                                                    <path class="path-full" xmlns="http://www.w3.org/2000/svg"
                                                          d="M12 4.248c-3.148-5.402-12-3.825-12 2.944 0 4.661 5.571 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-6.792-8.875-8.306-12-2.944z">
                                                    </path>
                                                </svg></button>
                                        </div>
                                        <div class="tabs__slide-body"><img src="./images/img-1-db8e7456.jpg" alt="Алена">
                                            <div class="tabs__slide-stars">
                                                <div class="tabs__slide-title">Массажный салон</div>
                                                <div class="tabs__slide-rating">
                                                    <div class="rating-static rating-static--one">
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tabs__slide-bottom">
                                            <div class="tabs__slide-line">
                                                <div class="tabs__slide-title">Апартаменты</div>
                                                <div class="tabs__slide-price">от&nbsp<span>2 500 ₽</span> /час </div>
                                            </div>
                                            <div class="tabs__slide-line">
                                                <div class="tabs__slide-title">Выезд</div>
                                                <div class="tabs__slide-price">от&nbsp<span>4 500 ₽</span> /час </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tabs__slide swiper-slide">
                                        <div class="tabs__slide-head">
                                            <div class="tabs__slide-name">Оля</div>
                                            <button class="tabs__slide-favorites"><svg class="add-favorites js-favorites"
                                                                                       xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24">
                                                    <path class="path-empty"
                                                          d="M6.28 3c3.236.001 4.973 3.491 5.72 5.031.75-1.547 2.469-5.021 5.726-5.021 2.058 0 4.274 1.309 4.274 4.182 0 3.442-4.744 7.851-10 13-5.258-5.151-10-9.559-10-13 0-2.676 1.965-4.193 4.28-4.192zm.001-2c-3.183 0-6.281 2.187-6.281 6.192 0 4.661 5.57 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-4.011-3.097-6.182-6.274-6.182-2.204 0-4.446 1.042-5.726 3.238-1.285-2.206-3.522-3.248-5.719-3.248z">
                                                    </path>
                                                    <path class="path-full" xmlns="http://www.w3.org/2000/svg"
                                                          d="M12 4.248c-3.148-5.402-12-3.825-12 2.944 0 4.661 5.571 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-6.792-8.875-8.306-12-2.944z">
                                                    </path>
                                                </svg></button>
                                        </div>
                                        <div class="tabs__slide-body"><img src="./images/img-1-db8e7456.jpg" alt="Оля">
                                            <div class="tabs__slide-stars">
                                                <div class="tabs__slide-title">Массажный салон</div>
                                                <div class="tabs__slide-rating">
                                                    <div class="rating-static rating-static--five">
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tabs__slide-bottom">
                                            <div class="tabs__slide-line">
                                                <div class="tabs__slide-title">Апартаменты</div>
                                                <div class="tabs__slide-price">от&nbsp<span>2 500 ₽</span> /час </div>
                                            </div>
                                            <div class="tabs__slide-line">
                                                <div class="tabs__slide-title">Выезд</div>
                                                <div class="tabs__slide-price">от&nbsp<span>4 500 ₽</span> /час </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tabs__slide swiper-slide">
                                        <div class="tabs__slide-head">
                                            <div class="tabs__slide-name">Катя</div>
                                            <button class="tabs__slide-favorites"><svg class="add-favorites js-favorites"
                                                                                       xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24">
                                                    <path class="path-empty"
                                                          d="M6.28 3c3.236.001 4.973 3.491 5.72 5.031.75-1.547 2.469-5.021 5.726-5.021 2.058 0 4.274 1.309 4.274 4.182 0 3.442-4.744 7.851-10 13-5.258-5.151-10-9.559-10-13 0-2.676 1.965-4.193 4.28-4.192zm.001-2c-3.183 0-6.281 2.187-6.281 6.192 0 4.661 5.57 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-4.011-3.097-6.182-6.274-6.182-2.204 0-4.446 1.042-5.726 3.238-1.285-2.206-3.522-3.248-5.719-3.248z">
                                                    </path>
                                                    <path class="path-full" xmlns="http://www.w3.org/2000/svg"
                                                          d="M12 4.248c-3.148-5.402-12-3.825-12 2.944 0 4.661 5.571 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-6.792-8.875-8.306-12-2.944z">
                                                    </path>
                                                </svg></button>
                                        </div>
                                        <div class="tabs__slide-body"><img src="./images/img-1-db8e7456.jpg" alt="Катя">
                                            <div class="tabs__slide-stars">
                                                <div class="tabs__slide-title">Массажный салон</div>
                                                <div class="tabs__slide-rating">
                                                    <div class="rating-static rating-static--two">
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tabs__slide-bottom">
                                            <div class="tabs__slide-line">
                                                <div class="tabs__slide-title">Апартаменты</div>
                                                <div class="tabs__slide-price">от&nbsp<span>2 500 ₽</span> /час </div>
                                            </div>
                                            <div class="tabs__slide-line">
                                                <div class="tabs__slide-title">Выезд</div>
                                                <div class="tabs__slide-price">от&nbsp<span>4 500 ₽</span> /час </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tabs__slide swiper-slide">
                                        <div class="tabs__slide-head">
                                            <div class="tabs__slide-name">Настя</div>
                                            <button class="tabs__slide-favorites"><svg class="add-favorites js-favorites"
                                                                                       xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24">
                                                    <path class="path-empty"
                                                          d="M6.28 3c3.236.001 4.973 3.491 5.72 5.031.75-1.547 2.469-5.021 5.726-5.021 2.058 0 4.274 1.309 4.274 4.182 0 3.442-4.744 7.851-10 13-5.258-5.151-10-9.559-10-13 0-2.676 1.965-4.193 4.28-4.192zm.001-2c-3.183 0-6.281 2.187-6.281 6.192 0 4.661 5.57 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-4.011-3.097-6.182-6.274-6.182-2.204 0-4.446 1.042-5.726 3.238-1.285-2.206-3.522-3.248-5.719-3.248z">
                                                    </path>
                                                    <path class="path-full" xmlns="http://www.w3.org/2000/svg"
                                                          d="M12 4.248c-3.148-5.402-12-3.825-12 2.944 0 4.661 5.571 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-6.792-8.875-8.306-12-2.944z">
                                                    </path>
                                                </svg></button>
                                        </div>
                                        <div class="tabs__slide-body"><img src="./images/img-1-db8e7456.jpg" alt="Настя">
                                            <div class="tabs__slide-stars">
                                                <div class="tabs__slide-title">Массажный салон</div>
                                                <div class="tabs__slide-rating">
                                                    <div class="rating-static rating-static--three">
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tabs__slide-bottom">
                                            <div class="tabs__slide-line">
                                                <div class="tabs__slide-title">Апартаменты</div>
                                                <div class="tabs__slide-price">от&nbsp<span>2 500 ₽</span> /час </div>
                                            </div>
                                            <div class="tabs__slide-line">
                                                <div class="tabs__slide-title">Выезд</div>
                                                <div class="tabs__slide-price">от&nbsp<span>4 500 ₽</span> /час </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tabs__slide swiper-slide">
                                        <div class="tabs__slide-head">
                                            <div class="tabs__slide-name">Алена</div>
                                            <button class="tabs__slide-favorites"><svg class="add-favorites js-favorites"
                                                                                       xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24">
                                                    <path class="path-empty"
                                                          d="M6.28 3c3.236.001 4.973 3.491 5.72 5.031.75-1.547 2.469-5.021 5.726-5.021 2.058 0 4.274 1.309 4.274 4.182 0 3.442-4.744 7.851-10 13-5.258-5.151-10-9.559-10-13 0-2.676 1.965-4.193 4.28-4.192zm.001-2c-3.183 0-6.281 2.187-6.281 6.192 0 4.661 5.57 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-4.011-3.097-6.182-6.274-6.182-2.204 0-4.446 1.042-5.726 3.238-1.285-2.206-3.522-3.248-5.719-3.248z">
                                                    </path>
                                                    <path class="path-full" xmlns="http://www.w3.org/2000/svg"
                                                          d="M12 4.248c-3.148-5.402-12-3.825-12 2.944 0 4.661 5.571 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-6.792-8.875-8.306-12-2.944z">
                                                    </path>
                                                </svg></button>
                                        </div>
                                        <div class="tabs__slide-body"><img src="./images/img-1-db8e7456.jpg" alt="Алена">
                                            <div class="tabs__slide-stars">
                                                <div class="tabs__slide-title">Массажный салон</div>
                                                <div class="tabs__slide-rating">
                                                    <div class="rating-static rating-static--one">
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tabs__slide-bottom">
                                            <div class="tabs__slide-line">
                                                <div class="tabs__slide-title">Апартаменты</div>
                                                <div class="tabs__slide-price">от&nbsp<span>2 500 ₽</span> /час </div>
                                            </div>
                                            <div class="tabs__slide-line">
                                                <div class="tabs__slide-title">Выезд</div>
                                                <div class="tabs__slide-price">от&nbsp<span>4 500 ₽</span> /час </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tabs__slider-pagination slider-pagination swiper-pagination">
                                    <!-- Add Arrows-->
                                </div>
                                <div class="tabs__slider-next slider-next swiper-button-next"></div>
                                <div class="tabs__slider-prev slider-prev swiper-button-prev"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('includes._bottom-banner')
@endsection
