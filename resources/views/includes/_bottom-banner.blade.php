<div class="bottom-banner {{ $wrapperClass ?? '' }}">
    <div class="bottom-banner__wrap">
        <div class="bottom-banner__title">Заблокировали доступ к сайту?<a class="bottom-banner__link" href="#" title="link">Инструкция по обходу блокировки</a></div>
    </div>
</div>
