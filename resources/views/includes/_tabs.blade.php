<div class="template__tabs">
    <div class="tabs">
        <div class="tabs__head">
            <div class="tabs__btns">
                <button class="tabs__btn tabs__btn--active" data-type="questionnaire"><span class="tabs__count"></span>
                    <div class="tabs__btn-text">Похожие анкеты</div>
                </button>
                <button class="tabs__btn" data-type="viewed"><span class="tabs__count">18</span>
                    <div class="tabs__btn-text">Недавно просмотренные</div>
                </button>
                <button class="tabs__btn" data-type="Favorites"><span class="tabs__count">3</span>
                    <div class="tabs__btn-text">Избранные анкеты</div>
                </button>
            </div>
        </div>
        <div class="tabs__inner">
            <div class="tabs__container">
                <div class="tabs__content" data-type="questionnaire">
                    <div class="tabs__slider swiper-container">
                        <div class="tabs__wrapper swiper-wrapper">
                            <div class="tabs__slide swiper-slide">
                                <div class="tabs__slide-head">
                                    <div class="tabs__slide-name">Оля</div>
                                    <button class="tabs__slide-favorites"><svg class="add-favorites js-favorites"
                                                                               xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24">
                                            <path class="path-empty"
                                                  d="M6.28 3c3.236.001 4.973 3.491 5.72 5.031.75-1.547 2.469-5.021 5.726-5.021 2.058 0 4.274 1.309 4.274 4.182 0 3.442-4.744 7.851-10 13-5.258-5.151-10-9.559-10-13 0-2.676 1.965-4.193 4.28-4.192zm.001-2c-3.183 0-6.281 2.187-6.281 6.192 0 4.661 5.57 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-4.011-3.097-6.182-6.274-6.182-2.204 0-4.446 1.042-5.726 3.238-1.285-2.206-3.522-3.248-5.719-3.248z">
                                            </path>
                                            <path class="path-full" xmlns="http://www.w3.org/2000/svg"
                                                  d="M12 4.248c-3.148-5.402-12-3.825-12 2.944 0 4.661 5.571 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-6.792-8.875-8.306-12-2.944z">
                                            </path>
                                        </svg></button>
                                </div>
                                <div class="tabs__slide-body"><img src="/images/img-1-db8e7456.jpg" alt="Оля">
                                    <div class="tabs__slide-stars">
                                        <div class="tabs__slide-title">Массажный салон</div>
                                        <div class="tabs__slide-rating">
                                            <div class="rating-static rating-static--five">
                                                <div class="rating-static__star"></div>
                                                <div class="rating-static__star"></div>
                                                <div class="rating-static__star"></div>
                                                <div class="rating-static__star"></div>
                                                <div class="rating-static__star"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tabs__slide-bottom">
                                    <div class="tabs__slide-line">
                                        <div class="tabs__slide-title">Апартаменты</div>
                                        <div class="tabs__slide-price">от&nbsp<span>2 500 ₽</span> /час</div>
                                    </div>
                                    <div class="tabs__slide-line">
                                        <div class="tabs__slide-title">Выезд</div>
                                        <div class="tabs__slide-price">от&nbsp<span>4 500 ₽</span> /час</div>
                                    </div>
                                </div>
                            </div>
                            <div class="tabs__slide swiper-slide">
                                <div class="tabs__slide-head">
                                    <div class="tabs__slide-name">Катя</div>
                                    <button class="tabs__slide-favorites"><svg class="add-favorites js-favorites"
                                                                               xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24">
                                            <path class="path-empty"
                                                  d="M6.28 3c3.236.001 4.973 3.491 5.72 5.031.75-1.547 2.469-5.021 5.726-5.021 2.058 0 4.274 1.309 4.274 4.182 0 3.442-4.744 7.851-10 13-5.258-5.151-10-9.559-10-13 0-2.676 1.965-4.193 4.28-4.192zm.001-2c-3.183 0-6.281 2.187-6.281 6.192 0 4.661 5.57 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-4.011-3.097-6.182-6.274-6.182-2.204 0-4.446 1.042-5.726 3.238-1.285-2.206-3.522-3.248-5.719-3.248z">
                                            </path>
                                            <path class="path-full" xmlns="http://www.w3.org/2000/svg"
                                                  d="M12 4.248c-3.148-5.402-12-3.825-12 2.944 0 4.661 5.571 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-6.792-8.875-8.306-12-2.944z">
                                            </path>
                                        </svg></button>
                                </div>
                                <div class="tabs__slide-body"><img src="/images/img-1-db8e7456.jpg" alt="Катя">
                                    <div class="tabs__slide-stars">
                                        <div class="tabs__slide-title">Массажный салон</div>
                                        <div class="tabs__slide-rating">
                                            <div class="rating-static rating-static--two">
                                                <div class="rating-static__star"></div>
                                                <div class="rating-static__star"></div>
                                                <div class="rating-static__star"></div>
                                                <div class="rating-static__star"></div>
                                                <div class="rating-static__star"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tabs__slide-bottom">
                                    <div class="tabs__slide-line">
                                        <div class="tabs__slide-title">Апартаменты</div>
                                        <div class="tabs__slide-price">от&nbsp<span>2 500 ₽</span> /час</div>
                                    </div>
                                    <div class="tabs__slide-line">
                                        <div class="tabs__slide-title">Выезд</div>
                                        <div class="tabs__slide-price">от&nbsp<span>4 500 ₽</span> /час</div>
                                    </div>
                                </div>
                            </div>
                            <div class="tabs__slide swiper-slide">
                                <div class="tabs__slide-head">
                                    <div class="tabs__slide-name">Настя</div>
                                    <button class="tabs__slide-favorites"><svg class="add-favorites js-favorites"
                                                                               xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24">
                                            <path class="path-empty"
                                                  d="M6.28 3c3.236.001 4.973 3.491 5.72 5.031.75-1.547 2.469-5.021 5.726-5.021 2.058 0 4.274 1.309 4.274 4.182 0 3.442-4.744 7.851-10 13-5.258-5.151-10-9.559-10-13 0-2.676 1.965-4.193 4.28-4.192zm.001-2c-3.183 0-6.281 2.187-6.281 6.192 0 4.661 5.57 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-4.011-3.097-6.182-6.274-6.182-2.204 0-4.446 1.042-5.726 3.238-1.285-2.206-3.522-3.248-5.719-3.248z">
                                            </path>
                                            <path class="path-full" xmlns="http://www.w3.org/2000/svg"
                                                  d="M12 4.248c-3.148-5.402-12-3.825-12 2.944 0 4.661 5.571 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-6.792-8.875-8.306-12-2.944z">
                                            </path>
                                        </svg></button>
                                </div>
                                <div class="tabs__slide-body"><img src="/images/img-1-db8e7456.jpg" alt="Настя">
                                    <div class="tabs__slide-stars">
                                        <div class="tabs__slide-title">Массажный салон</div>
                                        <div class="tabs__slide-rating">
                                            <div class="rating-static rating-static--three">
                                                <div class="rating-static__star"></div>
                                                <div class="rating-static__star"></div>
                                                <div class="rating-static__star"></div>
                                                <div class="rating-static__star"></div>
                                                <div class="rating-static__star"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tabs__slide-bottom">
                                    <div class="tabs__slide-line">
                                        <div class="tabs__slide-title">Апартаменты</div>
                                        <div class="tabs__slide-price">от&nbsp<span>2 500 ₽</span> /час</div>
                                    </div>
                                    <div class="tabs__slide-line">
                                        <div class="tabs__slide-title">Выезд</div>
                                        <div class="tabs__slide-price">от&nbsp<span>4 500 ₽</span> /час</div>
                                    </div>
                                </div>
                            </div>
                            <div class="tabs__slide swiper-slide">
                                <div class="tabs__slide-head">
                                    <div class="tabs__slide-name">Алена</div>
                                    <button class="tabs__slide-favorites"><svg class="add-favorites js-favorites"
                                                                               xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24">
                                            <path class="path-empty"
                                                  d="M6.28 3c3.236.001 4.973 3.491 5.72 5.031.75-1.547 2.469-5.021 5.726-5.021 2.058 0 4.274 1.309 4.274 4.182 0 3.442-4.744 7.851-10 13-5.258-5.151-10-9.559-10-13 0-2.676 1.965-4.193 4.28-4.192zm.001-2c-3.183 0-6.281 2.187-6.281 6.192 0 4.661 5.57 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-4.011-3.097-6.182-6.274-6.182-2.204 0-4.446 1.042-5.726 3.238-1.285-2.206-3.522-3.248-5.719-3.248z">
                                            </path>
                                            <path class="path-full" xmlns="http://www.w3.org/2000/svg"
                                                  d="M12 4.248c-3.148-5.402-12-3.825-12 2.944 0 4.661 5.571 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-6.792-8.875-8.306-12-2.944z">
                                            </path>
                                        </svg></button>
                                </div>
                                <div class="tabs__slide-body"><img src="/images/img-1-db8e7456.jpg" alt="Алена">
                                    <div class="tabs__slide-stars">
                                        <div class="tabs__slide-title">Массажный салон</div>
                                        <div class="tabs__slide-rating">
                                            <div class="rating-static rating-static--one">
                                                <div class="rating-static__star"></div>
                                                <div class="rating-static__star"></div>
                                                <div class="rating-static__star"></div>
                                                <div class="rating-static__star"></div>
                                                <div class="rating-static__star"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tabs__slide-bottom">
                                    <div class="tabs__slide-line">
                                        <div class="tabs__slide-title">Апартаменты</div>
                                        <div class="tabs__slide-price">от&nbsp<span>2 500 ₽</span> /час</div>
                                    </div>
                                    <div class="tabs__slide-line">
                                        <div class="tabs__slide-title">Выезд</div>
                                        <div class="tabs__slide-price">от&nbsp<span>4 500 ₽</span> /час</div>
                                    </div>
                                </div>
                            </div>
                            <div class="tabs__slide swiper-slide">
                                <div class="tabs__slide-head">
                                    <div class="tabs__slide-name">Оля</div>
                                    <button class="tabs__slide-favorites"><svg class="add-favorites js-favorites"
                                                                               xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24">
                                            <path class="path-empty"
                                                  d="M6.28 3c3.236.001 4.973 3.491 5.72 5.031.75-1.547 2.469-5.021 5.726-5.021 2.058 0 4.274 1.309 4.274 4.182 0 3.442-4.744 7.851-10 13-5.258-5.151-10-9.559-10-13 0-2.676 1.965-4.193 4.28-4.192zm.001-2c-3.183 0-6.281 2.187-6.281 6.192 0 4.661 5.57 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-4.011-3.097-6.182-6.274-6.182-2.204 0-4.446 1.042-5.726 3.238-1.285-2.206-3.522-3.248-5.719-3.248z">
                                            </path>
                                            <path class="path-full" xmlns="http://www.w3.org/2000/svg"
                                                  d="M12 4.248c-3.148-5.402-12-3.825-12 2.944 0 4.661 5.571 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-6.792-8.875-8.306-12-2.944z">
                                            </path>
                                        </svg></button>
                                </div>
                                <div class="tabs__slide-body"><img src="/images/img-1-db8e7456.jpg" alt="Оля">
                                    <div class="tabs__slide-stars">
                                        <div class="tabs__slide-title">Массажный салон</div>
                                        <div class="tabs__slide-rating">
                                            <div class="rating-static rating-static--five">
                                                <div class="rating-static__star"></div>
                                                <div class="rating-static__star"></div>
                                                <div class="rating-static__star"></div>
                                                <div class="rating-static__star"></div>
                                                <div class="rating-static__star"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tabs__slide-bottom">
                                    <div class="tabs__slide-line">
                                        <div class="tabs__slide-title">Апартаменты</div>
                                        <div class="tabs__slide-price">от&nbsp<span>2 500 ₽</span> /час</div>
                                    </div>
                                    <div class="tabs__slide-line">
                                        <div class="tabs__slide-title">Выезд</div>
                                        <div class="tabs__slide-price">от&nbsp<span>4 500 ₽</span> /час</div>
                                    </div>
                                </div>
                            </div>
                            <div class="tabs__slide swiper-slide">
                                <div class="tabs__slide-head">
                                    <div class="tabs__slide-name">Катя</div>
                                    <button class="tabs__slide-favorites"><svg class="add-favorites js-favorites"
                                                                               xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24">
                                            <path class="path-empty"
                                                  d="M6.28 3c3.236.001 4.973 3.491 5.72 5.031.75-1.547 2.469-5.021 5.726-5.021 2.058 0 4.274 1.309 4.274 4.182 0 3.442-4.744 7.851-10 13-5.258-5.151-10-9.559-10-13 0-2.676 1.965-4.193 4.28-4.192zm.001-2c-3.183 0-6.281 2.187-6.281 6.192 0 4.661 5.57 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-4.011-3.097-6.182-6.274-6.182-2.204 0-4.446 1.042-5.726 3.238-1.285-2.206-3.522-3.248-5.719-3.248z">
                                            </path>
                                            <path class="path-full" xmlns="http://www.w3.org/2000/svg"
                                                  d="M12 4.248c-3.148-5.402-12-3.825-12 2.944 0 4.661 5.571 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-6.792-8.875-8.306-12-2.944z">
                                            </path>
                                        </svg></button>
                                </div>
                                <div class="tabs__slide-body"><img src="/images/img-1-db8e7456.jpg" alt="Катя">
                                    <div class="tabs__slide-stars">
                                        <div class="tabs__slide-title">Массажный салон</div>
                                        <div class="tabs__slide-rating">
                                            <div class="rating-static rating-static--two">
                                                <div class="rating-static__star"></div>
                                                <div class="rating-static__star"></div>
                                                <div class="rating-static__star"></div>
                                                <div class="rating-static__star"></div>
                                                <div class="rating-static__star"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tabs__slide-bottom">
                                    <div class="tabs__slide-line">
                                        <div class="tabs__slide-title">Апартаменты</div>
                                        <div class="tabs__slide-price">от&nbsp<span>2 500 ₽</span> /час</div>
                                    </div>
                                    <div class="tabs__slide-line">
                                        <div class="tabs__slide-title">Выезд</div>
                                        <div class="tabs__slide-price">от&nbsp<span>4 500 ₽</span> /час</div>
                                    </div>
                                </div>
                            </div>
                            <div class="tabs__slide swiper-slide">
                                <div class="tabs__slide-head">
                                    <div class="tabs__slide-name">Настя</div>
                                    <button class="tabs__slide-favorites"><svg class="add-favorites js-favorites"
                                                                               xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24">
                                            <path class="path-empty"
                                                  d="M6.28 3c3.236.001 4.973 3.491 5.72 5.031.75-1.547 2.469-5.021 5.726-5.021 2.058 0 4.274 1.309 4.274 4.182 0 3.442-4.744 7.851-10 13-5.258-5.151-10-9.559-10-13 0-2.676 1.965-4.193 4.28-4.192zm.001-2c-3.183 0-6.281 2.187-6.281 6.192 0 4.661 5.57 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-4.011-3.097-6.182-6.274-6.182-2.204 0-4.446 1.042-5.726 3.238-1.285-2.206-3.522-3.248-5.719-3.248z">
                                            </path>
                                            <path class="path-full" xmlns="http://www.w3.org/2000/svg"
                                                  d="M12 4.248c-3.148-5.402-12-3.825-12 2.944 0 4.661 5.571 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-6.792-8.875-8.306-12-2.944z">
                                            </path>
                                        </svg></button>
                                </div>
                                <div class="tabs__slide-body"><img src="/images/img-1-db8e7456.jpg" alt="Настя">
                                    <div class="tabs__slide-stars">
                                        <div class="tabs__slide-title">Массажный салон</div>
                                        <div class="tabs__slide-rating">
                                            <div class="rating-static rating-static--three">
                                                <div class="rating-static__star"></div>
                                                <div class="rating-static__star"></div>
                                                <div class="rating-static__star"></div>
                                                <div class="rating-static__star"></div>
                                                <div class="rating-static__star"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tabs__slide-bottom">
                                    <div class="tabs__slide-line">
                                        <div class="tabs__slide-title">Апартаменты</div>
                                        <div class="tabs__slide-price">от&nbsp<span>2 500 ₽</span> /час</div>
                                    </div>
                                    <div class="tabs__slide-line">
                                        <div class="tabs__slide-title">Выезд</div>
                                        <div class="tabs__slide-price">от&nbsp<span>4 500 ₽</span> /час</div>
                                    </div>
                                </div>
                            </div>
                            <div class="tabs__slide swiper-slide">
                                <div class="tabs__slide-head">
                                    <div class="tabs__slide-name">Алена</div>
                                    <button class="tabs__slide-favorites"><svg class="add-favorites js-favorites"
                                                                               xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24">
                                            <path class="path-empty"
                                                  d="M6.28 3c3.236.001 4.973 3.491 5.72 5.031.75-1.547 2.469-5.021 5.726-5.021 2.058 0 4.274 1.309 4.274 4.182 0 3.442-4.744 7.851-10 13-5.258-5.151-10-9.559-10-13 0-2.676 1.965-4.193 4.28-4.192zm.001-2c-3.183 0-6.281 2.187-6.281 6.192 0 4.661 5.57 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-4.011-3.097-6.182-6.274-6.182-2.204 0-4.446 1.042-5.726 3.238-1.285-2.206-3.522-3.248-5.719-3.248z">
                                            </path>
                                            <path class="path-full" xmlns="http://www.w3.org/2000/svg"
                                                  d="M12 4.248c-3.148-5.402-12-3.825-12 2.944 0 4.661 5.571 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-6.792-8.875-8.306-12-2.944z">
                                            </path>
                                        </svg></button>
                                </div>
                                <div class="tabs__slide-body"><img src="/images/img-1-db8e7456.jpg" alt="Алена">
                                    <div class="tabs__slide-stars">
                                        <div class="tabs__slide-title">Массажный салон</div>
                                        <div class="tabs__slide-rating">
                                            <div class="rating-static rating-static--one">
                                                <div class="rating-static__star"></div>
                                                <div class="rating-static__star"></div>
                                                <div class="rating-static__star"></div>
                                                <div class="rating-static__star"></div>
                                                <div class="rating-static__star"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tabs__slide-bottom">
                                    <div class="tabs__slide-line">
                                        <div class="tabs__slide-title">Апартаменты</div>
                                        <div class="tabs__slide-price">от&nbsp<span>2 500 ₽</span> /час</div>
                                    </div>
                                    <div class="tabs__slide-line">
                                        <div class="tabs__slide-title">Выезд</div>
                                        <div class="tabs__slide-price">от&nbsp<span>4 500 ₽</span> /час</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tabs__slider-pagination slider-pagination swiper-pagination">
                            <!-- Add Arrows-->
                        </div>
                        <div class="tabs__slider-next slider-next swiper-button-next"></div>
                        <div class="tabs__slider-prev slider-prev swiper-button-prev"></div>
                    </div>
                </div>
                <div class="tabs__content tabs__content--hidden" data-type="viewed">
                    <div class="tabs__slider swiper-container">
                        <div class="tabs__wrapper swiper-wrapper">
                            <div class="tabs__slide swiper-slide">
                                <div class="tabs__slide-head">
                                    <div class="tabs__slide-name">Оля</div>
                                    <button class="tabs__slide-favorites"><svg class="add-favorites js-favorites"
                                                                               xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24">
                                            <path class="path-empty"
                                                  d="M6.28 3c3.236.001 4.973 3.491 5.72 5.031.75-1.547 2.469-5.021 5.726-5.021 2.058 0 4.274 1.309 4.274 4.182 0 3.442-4.744 7.851-10 13-5.258-5.151-10-9.559-10-13 0-2.676 1.965-4.193 4.28-4.192zm.001-2c-3.183 0-6.281 2.187-6.281 6.192 0 4.661 5.57 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-4.011-3.097-6.182-6.274-6.182-2.204 0-4.446 1.042-5.726 3.238-1.285-2.206-3.522-3.248-5.719-3.248z">
                                            </path>
                                            <path class="path-full" xmlns="http://www.w3.org/2000/svg"
                                                  d="M12 4.248c-3.148-5.402-12-3.825-12 2.944 0 4.661 5.571 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-6.792-8.875-8.306-12-2.944z">
                                            </path>
                                        </svg></button>
                                </div>
                                <div class="tabs__slide-body"><img src="/images/img-1-db8e7456.jpg" alt="Оля">
                                    <div class="tabs__slide-stars">
                                        <div class="tabs__slide-title">Массажный салон</div>
                                        <div class="tabs__slide-rating">
                                            <div class="rating-static rating-static--five">
                                                <div class="rating-static__star"></div>
                                                <div class="rating-static__star"></div>
                                                <div class="rating-static__star"></div>
                                                <div class="rating-static__star"></div>
                                                <div class="rating-static__star"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tabs__slide-bottom">
                                    <div class="tabs__slide-line">
                                        <div class="tabs__slide-title">Апартаменты</div>
                                        <div class="tabs__slide-price">от&nbsp<span>2 500 ₽</span> /час</div>
                                    </div>
                                    <div class="tabs__slide-line">
                                        <div class="tabs__slide-title">Выезд</div>
                                        <div class="tabs__slide-price">от&nbsp<span>4 500 ₽</span> /час</div>
                                    </div>
                                </div>
                            </div>
                            <div class="tabs__slide swiper-slide">
                                <div class="tabs__slide-head">
                                    <div class="tabs__slide-name">Катя</div>
                                    <button class="tabs__slide-favorites"><svg class="add-favorites js-favorites"
                                                                               xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24">
                                            <path class="path-empty"
                                                  d="M6.28 3c3.236.001 4.973 3.491 5.72 5.031.75-1.547 2.469-5.021 5.726-5.021 2.058 0 4.274 1.309 4.274 4.182 0 3.442-4.744 7.851-10 13-5.258-5.151-10-9.559-10-13 0-2.676 1.965-4.193 4.28-4.192zm.001-2c-3.183 0-6.281 2.187-6.281 6.192 0 4.661 5.57 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-4.011-3.097-6.182-6.274-6.182-2.204 0-4.446 1.042-5.726 3.238-1.285-2.206-3.522-3.248-5.719-3.248z">
                                            </path>
                                            <path class="path-full" xmlns="http://www.w3.org/2000/svg"
                                                  d="M12 4.248c-3.148-5.402-12-3.825-12 2.944 0 4.661 5.571 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-6.792-8.875-8.306-12-2.944z">
                                            </path>
                                        </svg></button>
                                </div>
                                <div class="tabs__slide-body"><img src="/images/img-1-db8e7456.jpg" alt="Катя">
                                    <div class="tabs__slide-stars">
                                        <div class="tabs__slide-title">Массажный салон</div>
                                        <div class="tabs__slide-rating">
                                            <div class="rating-static rating-static--two">
                                                <div class="rating-static__star"></div>
                                                <div class="rating-static__star"></div>
                                                <div class="rating-static__star"></div>
                                                <div class="rating-static__star"></div>
                                                <div class="rating-static__star"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tabs__slide-bottom">
                                    <div class="tabs__slide-line">
                                        <div class="tabs__slide-title">Апартаменты</div>
                                        <div class="tabs__slide-price">от&nbsp<span>2 500 ₽</span> /час</div>
                                    </div>
                                    <div class="tabs__slide-line">
                                        <div class="tabs__slide-title">Выезд</div>
                                        <div class="tabs__slide-price">от&nbsp<span>4 500 ₽</span> /час</div>
                                    </div>
                                </div>
                            </div>
                            <div class="tabs__slide swiper-slide">
                                <div class="tabs__slide-head">
                                    <div class="tabs__slide-name">Настя</div>
                                    <button class="tabs__slide-favorites"><svg class="add-favorites js-favorites"
                                                                               xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24">
                                            <path class="path-empty"
                                                  d="M6.28 3c3.236.001 4.973 3.491 5.72 5.031.75-1.547 2.469-5.021 5.726-5.021 2.058 0 4.274 1.309 4.274 4.182 0 3.442-4.744 7.851-10 13-5.258-5.151-10-9.559-10-13 0-2.676 1.965-4.193 4.28-4.192zm.001-2c-3.183 0-6.281 2.187-6.281 6.192 0 4.661 5.57 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-4.011-3.097-6.182-6.274-6.182-2.204 0-4.446 1.042-5.726 3.238-1.285-2.206-3.522-3.248-5.719-3.248z">
                                            </path>
                                            <path class="path-full" xmlns="http://www.w3.org/2000/svg"
                                                  d="M12 4.248c-3.148-5.402-12-3.825-12 2.944 0 4.661 5.571 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-6.792-8.875-8.306-12-2.944z">
                                            </path>
                                        </svg></button>
                                </div>
                                <div class="tabs__slide-body"><img src="/images/img-1-db8e7456.jpg" alt="Настя">
                                    <div class="tabs__slide-stars">
                                        <div class="tabs__slide-title">Массажный салон</div>
                                        <div class="tabs__slide-rating">
                                            <div class="rating-static rating-static--three">
                                                <div class="rating-static__star"></div>
                                                <div class="rating-static__star"></div>
                                                <div class="rating-static__star"></div>
                                                <div class="rating-static__star"></div>
                                                <div class="rating-static__star"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tabs__slide-bottom">
                                    <div class="tabs__slide-line">
                                        <div class="tabs__slide-title">Апартаменты</div>
                                        <div class="tabs__slide-price">от&nbsp<span>2 500 ₽</span> /час</div>
                                    </div>
                                    <div class="tabs__slide-line">
                                        <div class="tabs__slide-title">Выезд</div>
                                        <div class="tabs__slide-price">от&nbsp<span>4 500 ₽</span> /час</div>
                                    </div>
                                </div>
                            </div>
                            <div class="tabs__slide swiper-slide">
                                <div class="tabs__slide-head">
                                    <div class="tabs__slide-name">Алена</div>
                                    <button class="tabs__slide-favorites"><svg class="add-favorites js-favorites"
                                                                               xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24">
                                            <path class="path-empty"
                                                  d="M6.28 3c3.236.001 4.973 3.491 5.72 5.031.75-1.547 2.469-5.021 5.726-5.021 2.058 0 4.274 1.309 4.274 4.182 0 3.442-4.744 7.851-10 13-5.258-5.151-10-9.559-10-13 0-2.676 1.965-4.193 4.28-4.192zm.001-2c-3.183 0-6.281 2.187-6.281 6.192 0 4.661 5.57 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-4.011-3.097-6.182-6.274-6.182-2.204 0-4.446 1.042-5.726 3.238-1.285-2.206-3.522-3.248-5.719-3.248z">
                                            </path>
                                            <path class="path-full" xmlns="http://www.w3.org/2000/svg"
                                                  d="M12 4.248c-3.148-5.402-12-3.825-12 2.944 0 4.661 5.571 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-6.792-8.875-8.306-12-2.944z">
                                            </path>
                                        </svg></button>
                                </div>
                                <div class="tabs__slide-body"><img src="/images/img-1-db8e7456.jpg" alt="Алена">
                                    <div class="tabs__slide-stars">
                                        <div class="tabs__slide-title">Массажный салон</div>
                                        <div class="tabs__slide-rating">
                                            <div class="rating-static rating-static--one">
                                                <div class="rating-static__star"></div>
                                                <div class="rating-static__star"></div>
                                                <div class="rating-static__star"></div>
                                                <div class="rating-static__star"></div>
                                                <div class="rating-static__star"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tabs__slide-bottom">
                                    <div class="tabs__slide-line">
                                        <div class="tabs__slide-title">Апартаменты</div>
                                        <div class="tabs__slide-price">от&nbsp<span>2 500 ₽</span> /час</div>
                                    </div>
                                    <div class="tabs__slide-line">
                                        <div class="tabs__slide-title">Выезд</div>
                                        <div class="tabs__slide-price">от&nbsp<span>4 500 ₽</span> /час</div>
                                    </div>
                                </div>
                            </div>
                            <div class="tabs__slide swiper-slide">
                                <div class="tabs__slide-head">
                                    <div class="tabs__slide-name">Оля</div>
                                    <button class="tabs__slide-favorites"><svg class="add-favorites js-favorites"
                                                                               xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24">
                                            <path class="path-empty"
                                                  d="M6.28 3c3.236.001 4.973 3.491 5.72 5.031.75-1.547 2.469-5.021 5.726-5.021 2.058 0 4.274 1.309 4.274 4.182 0 3.442-4.744 7.851-10 13-5.258-5.151-10-9.559-10-13 0-2.676 1.965-4.193 4.28-4.192zm.001-2c-3.183 0-6.281 2.187-6.281 6.192 0 4.661 5.57 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-4.011-3.097-6.182-6.274-6.182-2.204 0-4.446 1.042-5.726 3.238-1.285-2.206-3.522-3.248-5.719-3.248z">
                                            </path>
                                            <path class="path-full" xmlns="http://www.w3.org/2000/svg"
                                                  d="M12 4.248c-3.148-5.402-12-3.825-12 2.944 0 4.661 5.571 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-6.792-8.875-8.306-12-2.944z">
                                            </path>
                                        </svg></button>
                                </div>
                                <div class="tabs__slide-body"><img src="/images/img-1-db8e7456.jpg" alt="Оля">
                                    <div class="tabs__slide-stars">
                                        <div class="tabs__slide-title">Массажный салон</div>
                                        <div class="tabs__slide-rating">
                                            <div class="rating-static rating-static--five">
                                                <div class="rating-static__star"></div>
                                                <div class="rating-static__star"></div>
                                                <div class="rating-static__star"></div>
                                                <div class="rating-static__star"></div>
                                                <div class="rating-static__star"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tabs__slide-bottom">
                                    <div class="tabs__slide-line">
                                        <div class="tabs__slide-title">Апартаменты</div>
                                        <div class="tabs__slide-price">от&nbsp<span>2 500 ₽</span> /час</div>
                                    </div>
                                    <div class="tabs__slide-line">
                                        <div class="tabs__slide-title">Выезд</div>
                                        <div class="tabs__slide-price">от&nbsp<span>4 500 ₽</span> /час</div>
                                    </div>
                                </div>
                            </div>
                            <div class="tabs__slide swiper-slide">
                                <div class="tabs__slide-head">
                                    <div class="tabs__slide-name">Катя</div>
                                    <button class="tabs__slide-favorites"><svg class="add-favorites js-favorites"
                                                                               xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24">
                                            <path class="path-empty"
                                                  d="M6.28 3c3.236.001 4.973 3.491 5.72 5.031.75-1.547 2.469-5.021 5.726-5.021 2.058 0 4.274 1.309 4.274 4.182 0 3.442-4.744 7.851-10 13-5.258-5.151-10-9.559-10-13 0-2.676 1.965-4.193 4.28-4.192zm.001-2c-3.183 0-6.281 2.187-6.281 6.192 0 4.661 5.57 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-4.011-3.097-6.182-6.274-6.182-2.204 0-4.446 1.042-5.726 3.238-1.285-2.206-3.522-3.248-5.719-3.248z">
                                            </path>
                                            <path class="path-full" xmlns="http://www.w3.org/2000/svg"
                                                  d="M12 4.248c-3.148-5.402-12-3.825-12 2.944 0 4.661 5.571 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-6.792-8.875-8.306-12-2.944z">
                                            </path>
                                        </svg></button>
                                </div>
                                <div class="tabs__slide-body"><img src="/images/img-1-db8e7456.jpg" alt="Катя">
                                    <div class="tabs__slide-stars">
                                        <div class="tabs__slide-title">Массажный салон</div>
                                        <div class="tabs__slide-rating">
                                            <div class="rating-static rating-static--two">
                                                <div class="rating-static__star"></div>
                                                <div class="rating-static__star"></div>
                                                <div class="rating-static__star"></div>
                                                <div class="rating-static__star"></div>
                                                <div class="rating-static__star"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tabs__slide-bottom">
                                    <div class="tabs__slide-line">
                                        <div class="tabs__slide-title">Апартаменты</div>
                                        <div class="tabs__slide-price">от&nbsp<span>2 500 ₽</span> /час</div>
                                    </div>
                                    <div class="tabs__slide-line">
                                        <div class="tabs__slide-title">Выезд</div>
                                        <div class="tabs__slide-price">от&nbsp<span>4 500 ₽</span> /час</div>
                                    </div>
                                </div>
                            </div>
                            <div class="tabs__slide swiper-slide">
                                <div class="tabs__slide-head">
                                    <div class="tabs__slide-name">Настя</div>
                                    <button class="tabs__slide-favorites"><svg class="add-favorites js-favorites"
                                                                               xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24">
                                            <path class="path-empty"
                                                  d="M6.28 3c3.236.001 4.973 3.491 5.72 5.031.75-1.547 2.469-5.021 5.726-5.021 2.058 0 4.274 1.309 4.274 4.182 0 3.442-4.744 7.851-10 13-5.258-5.151-10-9.559-10-13 0-2.676 1.965-4.193 4.28-4.192zm.001-2c-3.183 0-6.281 2.187-6.281 6.192 0 4.661 5.57 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-4.011-3.097-6.182-6.274-6.182-2.204 0-4.446 1.042-5.726 3.238-1.285-2.206-3.522-3.248-5.719-3.248z">
                                            </path>
                                            <path class="path-full" xmlns="http://www.w3.org/2000/svg"
                                                  d="M12 4.248c-3.148-5.402-12-3.825-12 2.944 0 4.661 5.571 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-6.792-8.875-8.306-12-2.944z">
                                            </path>
                                        </svg></button>
                                </div>
                                <div class="tabs__slide-body"><img src="/images/img-1-db8e7456.jpg" alt="Настя">
                                    <div class="tabs__slide-stars">
                                        <div class="tabs__slide-title">Массажный салон</div>
                                        <div class="tabs__slide-rating">
                                            <div class="rating-static rating-static--three">
                                                <div class="rating-static__star"></div>
                                                <div class="rating-static__star"></div>
                                                <div class="rating-static__star"></div>
                                                <div class="rating-static__star"></div>
                                                <div class="rating-static__star"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tabs__slide-bottom">
                                    <div class="tabs__slide-line">
                                        <div class="tabs__slide-title">Апартаменты</div>
                                        <div class="tabs__slide-price">от&nbsp<span>2 500 ₽</span> /час</div>
                                    </div>
                                    <div class="tabs__slide-line">
                                        <div class="tabs__slide-title">Выезд</div>
                                        <div class="tabs__slide-price">от&nbsp<span>4 500 ₽</span> /час</div>
                                    </div>
                                </div>
                            </div>
                            <div class="tabs__slide swiper-slide">
                                <div class="tabs__slide-head">
                                    <div class="tabs__slide-name">Алена</div>
                                    <button class="tabs__slide-favorites"><svg class="add-favorites js-favorites"
                                                                               xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24">
                                            <path class="path-empty"
                                                  d="M6.28 3c3.236.001 4.973 3.491 5.72 5.031.75-1.547 2.469-5.021 5.726-5.021 2.058 0 4.274 1.309 4.274 4.182 0 3.442-4.744 7.851-10 13-5.258-5.151-10-9.559-10-13 0-2.676 1.965-4.193 4.28-4.192zm.001-2c-3.183 0-6.281 2.187-6.281 6.192 0 4.661 5.57 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-4.011-3.097-6.182-6.274-6.182-2.204 0-4.446 1.042-5.726 3.238-1.285-2.206-3.522-3.248-5.719-3.248z">
                                            </path>
                                            <path class="path-full" xmlns="http://www.w3.org/2000/svg"
                                                  d="M12 4.248c-3.148-5.402-12-3.825-12 2.944 0 4.661 5.571 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-6.792-8.875-8.306-12-2.944z">
                                            </path>
                                        </svg></button>
                                </div>
                                <div class="tabs__slide-body"><img src="/images/img-1-db8e7456.jpg" alt="Алена">
                                    <div class="tabs__slide-stars">
                                        <div class="tabs__slide-title">Массажный салон</div>
                                        <div class="tabs__slide-rating">
                                            <div class="rating-static rating-static--one">
                                                <div class="rating-static__star"></div>
                                                <div class="rating-static__star"></div>
                                                <div class="rating-static__star"></div>
                                                <div class="rating-static__star"></div>
                                                <div class="rating-static__star"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tabs__slide-bottom">
                                    <div class="tabs__slide-line">
                                        <div class="tabs__slide-title">Апартаменты</div>
                                        <div class="tabs__slide-price">от&nbsp<span>2 500 ₽</span> /час</div>
                                    </div>
                                    <div class="tabs__slide-line">
                                        <div class="tabs__slide-title">Выезд</div>
                                        <div class="tabs__slide-price">от&nbsp<span>4 500 ₽</span> /час</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tabs__slider-pagination slider-pagination swiper-pagination">
                            <!-- Add Arrows-->
                        </div>
                        <div class="tabs__slider-next slider-next swiper-button-next"></div>
                        <div class="tabs__slider-prev slider-prev swiper-button-prev"></div>
                    </div>
                </div>
                <div class="tabs__content tabs__content--hidden" data-type="Favorites">
                    <div class="tabs__slider swiper-container">
                        <div class="tabs__wrapper swiper-wrapper">
                            <div class="tabs__slide swiper-slide">
                                <div class="tabs__slide-head">
                                    <div class="tabs__slide-name">Оля</div>
                                    <button class="tabs__slide-favorites"><svg class="add-favorites js-favorites"
                                                                               xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24">
                                            <path class="path-empty"
                                                  d="M6.28 3c3.236.001 4.973 3.491 5.72 5.031.75-1.547 2.469-5.021 5.726-5.021 2.058 0 4.274 1.309 4.274 4.182 0 3.442-4.744 7.851-10 13-5.258-5.151-10-9.559-10-13 0-2.676 1.965-4.193 4.28-4.192zm.001-2c-3.183 0-6.281 2.187-6.281 6.192 0 4.661 5.57 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-4.011-3.097-6.182-6.274-6.182-2.204 0-4.446 1.042-5.726 3.238-1.285-2.206-3.522-3.248-5.719-3.248z">
                                            </path>
                                            <path class="path-full" xmlns="http://www.w3.org/2000/svg"
                                                  d="M12 4.248c-3.148-5.402-12-3.825-12 2.944 0 4.661 5.571 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-6.792-8.875-8.306-12-2.944z">
                                            </path>
                                        </svg></button>
                                </div>
                                <div class="tabs__slide-body"><img src="/images/img-1-db8e7456.jpg" alt="Оля">
                                    <div class="tabs__slide-stars">
                                        <div class="tabs__slide-title">Массажный салон</div>
                                        <div class="tabs__slide-rating">
                                            <div class="rating-static rating-static--five">
                                                <div class="rating-static__star"></div>
                                                <div class="rating-static__star"></div>
                                                <div class="rating-static__star"></div>
                                                <div class="rating-static__star"></div>
                                                <div class="rating-static__star"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tabs__slide-bottom">
                                    <div class="tabs__slide-line">
                                        <div class="tabs__slide-title">Апартаменты</div>
                                        <div class="tabs__slide-price">от&nbsp<span>2 500 ₽</span> /час</div>
                                    </div>
                                    <div class="tabs__slide-line">
                                        <div class="tabs__slide-title">Выезд</div>
                                        <div class="tabs__slide-price">от&nbsp<span>4 500 ₽</span> /час</div>
                                    </div>
                                </div>
                            </div>
                            <div class="tabs__slide swiper-slide">
                                <div class="tabs__slide-head">
                                    <div class="tabs__slide-name">Катя</div>
                                    <button class="tabs__slide-favorites"><svg class="add-favorites js-favorites"
                                                                               xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24">
                                            <path class="path-empty"
                                                  d="M6.28 3c3.236.001 4.973 3.491 5.72 5.031.75-1.547 2.469-5.021 5.726-5.021 2.058 0 4.274 1.309 4.274 4.182 0 3.442-4.744 7.851-10 13-5.258-5.151-10-9.559-10-13 0-2.676 1.965-4.193 4.28-4.192zm.001-2c-3.183 0-6.281 2.187-6.281 6.192 0 4.661 5.57 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-4.011-3.097-6.182-6.274-6.182-2.204 0-4.446 1.042-5.726 3.238-1.285-2.206-3.522-3.248-5.719-3.248z">
                                            </path>
                                            <path class="path-full" xmlns="http://www.w3.org/2000/svg"
                                                  d="M12 4.248c-3.148-5.402-12-3.825-12 2.944 0 4.661 5.571 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-6.792-8.875-8.306-12-2.944z">
                                            </path>
                                        </svg></button>
                                </div>
                                <div class="tabs__slide-body"><img src="/images/img-1-db8e7456.jpg" alt="Катя">
                                    <div class="tabs__slide-stars">
                                        <div class="tabs__slide-title">Массажный салон</div>
                                        <div class="tabs__slide-rating">
                                            <div class="rating-static rating-static--two">
                                                <div class="rating-static__star"></div>
                                                <div class="rating-static__star"></div>
                                                <div class="rating-static__star"></div>
                                                <div class="rating-static__star"></div>
                                                <div class="rating-static__star"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tabs__slide-bottom">
                                    <div class="tabs__slide-line">
                                        <div class="tabs__slide-title">Апартаменты</div>
                                        <div class="tabs__slide-price">от&nbsp<span>2 500 ₽</span> /час</div>
                                    </div>
                                    <div class="tabs__slide-line">
                                        <div class="tabs__slide-title">Выезд</div>
                                        <div class="tabs__slide-price">от&nbsp<span>4 500 ₽</span> /час</div>
                                    </div>
                                </div>
                            </div>
                            <div class="tabs__slide swiper-slide">
                                <div class="tabs__slide-head">
                                    <div class="tabs__slide-name">Настя</div>
                                    <button class="tabs__slide-favorites"><svg class="add-favorites js-favorites"
                                                                               xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24">
                                            <path class="path-empty"
                                                  d="M6.28 3c3.236.001 4.973 3.491 5.72 5.031.75-1.547 2.469-5.021 5.726-5.021 2.058 0 4.274 1.309 4.274 4.182 0 3.442-4.744 7.851-10 13-5.258-5.151-10-9.559-10-13 0-2.676 1.965-4.193 4.28-4.192zm.001-2c-3.183 0-6.281 2.187-6.281 6.192 0 4.661 5.57 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-4.011-3.097-6.182-6.274-6.182-2.204 0-4.446 1.042-5.726 3.238-1.285-2.206-3.522-3.248-5.719-3.248z">
                                            </path>
                                            <path class="path-full" xmlns="http://www.w3.org/2000/svg"
                                                  d="M12 4.248c-3.148-5.402-12-3.825-12 2.944 0 4.661 5.571 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-6.792-8.875-8.306-12-2.944z">
                                            </path>
                                        </svg></button>
                                </div>
                                <div class="tabs__slide-body"><img src="/images/img-1-db8e7456.jpg" alt="Настя">
                                    <div class="tabs__slide-stars">
                                        <div class="tabs__slide-title">Массажный салон</div>
                                        <div class="tabs__slide-rating">
                                            <div class="rating-static rating-static--three">
                                                <div class="rating-static__star"></div>
                                                <div class="rating-static__star"></div>
                                                <div class="rating-static__star"></div>
                                                <div class="rating-static__star"></div>
                                                <div class="rating-static__star"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tabs__slide-bottom">
                                    <div class="tabs__slide-line">
                                        <div class="tabs__slide-title">Апартаменты</div>
                                        <div class="tabs__slide-price">от&nbsp<span>2 500 ₽</span> /час</div>
                                    </div>
                                    <div class="tabs__slide-line">
                                        <div class="tabs__slide-title">Выезд</div>
                                        <div class="tabs__slide-price">от&nbsp<span>4 500 ₽</span> /час</div>
                                    </div>
                                </div>
                            </div>
                            <div class="tabs__slide swiper-slide">
                                <div class="tabs__slide-head">
                                    <div class="tabs__slide-name">Алена</div>
                                    <button class="tabs__slide-favorites"><svg class="add-favorites js-favorites"
                                                                               xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24">
                                            <path class="path-empty"
                                                  d="M6.28 3c3.236.001 4.973 3.491 5.72 5.031.75-1.547 2.469-5.021 5.726-5.021 2.058 0 4.274 1.309 4.274 4.182 0 3.442-4.744 7.851-10 13-5.258-5.151-10-9.559-10-13 0-2.676 1.965-4.193 4.28-4.192zm.001-2c-3.183 0-6.281 2.187-6.281 6.192 0 4.661 5.57 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-4.011-3.097-6.182-6.274-6.182-2.204 0-4.446 1.042-5.726 3.238-1.285-2.206-3.522-3.248-5.719-3.248z">
                                            </path>
                                            <path class="path-full" xmlns="http://www.w3.org/2000/svg"
                                                  d="M12 4.248c-3.148-5.402-12-3.825-12 2.944 0 4.661 5.571 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-6.792-8.875-8.306-12-2.944z">
                                            </path>
                                        </svg></button>
                                </div>
                                <div class="tabs__slide-body"><img src="/images/img-1-db8e7456.jpg" alt="Алена">
                                    <div class="tabs__slide-stars">
                                        <div class="tabs__slide-title">Массажный салон</div>
                                        <div class="tabs__slide-rating">
                                            <div class="rating-static rating-static--one">
                                                <div class="rating-static__star"></div>
                                                <div class="rating-static__star"></div>
                                                <div class="rating-static__star"></div>
                                                <div class="rating-static__star"></div>
                                                <div class="rating-static__star"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tabs__slide-bottom">
                                    <div class="tabs__slide-line">
                                        <div class="tabs__slide-title">Апартаменты</div>
                                        <div class="tabs__slide-price">от&nbsp<span>2 500 ₽</span> /час</div>
                                    </div>
                                    <div class="tabs__slide-line">
                                        <div class="tabs__slide-title">Выезд</div>
                                        <div class="tabs__slide-price">от&nbsp<span>4 500 ₽</span> /час</div>
                                    </div>
                                </div>
                            </div>
                            <div class="tabs__slide swiper-slide">
                                <div class="tabs__slide-head">
                                    <div class="tabs__slide-name">Оля</div>
                                    <button class="tabs__slide-favorites"><svg class="add-favorites js-favorites"
                                                                               xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24">
                                            <path class="path-empty"
                                                  d="M6.28 3c3.236.001 4.973 3.491 5.72 5.031.75-1.547 2.469-5.021 5.726-5.021 2.058 0 4.274 1.309 4.274 4.182 0 3.442-4.744 7.851-10 13-5.258-5.151-10-9.559-10-13 0-2.676 1.965-4.193 4.28-4.192zm.001-2c-3.183 0-6.281 2.187-6.281 6.192 0 4.661 5.57 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-4.011-3.097-6.182-6.274-6.182-2.204 0-4.446 1.042-5.726 3.238-1.285-2.206-3.522-3.248-5.719-3.248z">
                                            </path>
                                            <path class="path-full" xmlns="http://www.w3.org/2000/svg"
                                                  d="M12 4.248c-3.148-5.402-12-3.825-12 2.944 0 4.661 5.571 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-6.792-8.875-8.306-12-2.944z">
                                            </path>
                                        </svg></button>
                                </div>
                                <div class="tabs__slide-body"><img src="/images/img-1-db8e7456.jpg" alt="Оля">
                                    <div class="tabs__slide-stars">
                                        <div class="tabs__slide-title">Массажный салон</div>
                                        <div class="tabs__slide-rating">
                                            <div class="rating-static rating-static--five">
                                                <div class="rating-static__star"></div>
                                                <div class="rating-static__star"></div>
                                                <div class="rating-static__star"></div>
                                                <div class="rating-static__star"></div>
                                                <div class="rating-static__star"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tabs__slide-bottom">
                                    <div class="tabs__slide-line">
                                        <div class="tabs__slide-title">Апартаменты</div>
                                        <div class="tabs__slide-price">от&nbsp<span>2 500 ₽</span> /час</div>
                                    </div>
                                    <div class="tabs__slide-line">
                                        <div class="tabs__slide-title">Выезд</div>
                                        <div class="tabs__slide-price">от&nbsp<span>4 500 ₽</span> /час</div>
                                    </div>
                                </div>
                            </div>
                            <div class="tabs__slide swiper-slide">
                                <div class="tabs__slide-head">
                                    <div class="tabs__slide-name">Катя</div>
                                    <button class="tabs__slide-favorites"><svg class="add-favorites js-favorites"
                                                                               xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24">
                                            <path class="path-empty"
                                                  d="M6.28 3c3.236.001 4.973 3.491 5.72 5.031.75-1.547 2.469-5.021 5.726-5.021 2.058 0 4.274 1.309 4.274 4.182 0 3.442-4.744 7.851-10 13-5.258-5.151-10-9.559-10-13 0-2.676 1.965-4.193 4.28-4.192zm.001-2c-3.183 0-6.281 2.187-6.281 6.192 0 4.661 5.57 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-4.011-3.097-6.182-6.274-6.182-2.204 0-4.446 1.042-5.726 3.238-1.285-2.206-3.522-3.248-5.719-3.248z">
                                            </path>
                                            <path class="path-full" xmlns="http://www.w3.org/2000/svg"
                                                  d="M12 4.248c-3.148-5.402-12-3.825-12 2.944 0 4.661 5.571 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-6.792-8.875-8.306-12-2.944z">
                                            </path>
                                        </svg></button>
                                </div>
                                <div class="tabs__slide-body"><img src="/images/img-1-db8e7456.jpg" alt="Катя">
                                    <div class="tabs__slide-stars">
                                        <div class="tabs__slide-title">Массажный салон</div>
                                        <div class="tabs__slide-rating">
                                            <div class="rating-static rating-static--two">
                                                <div class="rating-static__star"></div>
                                                <div class="rating-static__star"></div>
                                                <div class="rating-static__star"></div>
                                                <div class="rating-static__star"></div>
                                                <div class="rating-static__star"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tabs__slide-bottom">
                                    <div class="tabs__slide-line">
                                        <div class="tabs__slide-title">Апартаменты</div>
                                        <div class="tabs__slide-price">от&nbsp<span>2 500 ₽</span> /час</div>
                                    </div>
                                    <div class="tabs__slide-line">
                                        <div class="tabs__slide-title">Выезд</div>
                                        <div class="tabs__slide-price">от&nbsp<span>4 500 ₽</span> /час</div>
                                    </div>
                                </div>
                            </div>
                            <div class="tabs__slide swiper-slide">
                                <div class="tabs__slide-head">
                                    <div class="tabs__slide-name">Настя</div>
                                    <button class="tabs__slide-favorites"><svg class="add-favorites js-favorites"
                                                                               xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24">
                                            <path class="path-empty"
                                                  d="M6.28 3c3.236.001 4.973 3.491 5.72 5.031.75-1.547 2.469-5.021 5.726-5.021 2.058 0 4.274 1.309 4.274 4.182 0 3.442-4.744 7.851-10 13-5.258-5.151-10-9.559-10-13 0-2.676 1.965-4.193 4.28-4.192zm.001-2c-3.183 0-6.281 2.187-6.281 6.192 0 4.661 5.57 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-4.011-3.097-6.182-6.274-6.182-2.204 0-4.446 1.042-5.726 3.238-1.285-2.206-3.522-3.248-5.719-3.248z">
                                            </path>
                                            <path class="path-full" xmlns="http://www.w3.org/2000/svg"
                                                  d="M12 4.248c-3.148-5.402-12-3.825-12 2.944 0 4.661 5.571 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-6.792-8.875-8.306-12-2.944z">
                                            </path>
                                        </svg></button>
                                </div>
                                <div class="tabs__slide-body"><img src="/images/img-1-db8e7456.jpg" alt="Настя">
                                    <div class="tabs__slide-stars">
                                        <div class="tabs__slide-title">Массажный салон</div>
                                        <div class="tabs__slide-rating">
                                            <div class="rating-static rating-static--three">
                                                <div class="rating-static__star"></div>
                                                <div class="rating-static__star"></div>
                                                <div class="rating-static__star"></div>
                                                <div class="rating-static__star"></div>
                                                <div class="rating-static__star"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tabs__slide-bottom">
                                    <div class="tabs__slide-line">
                                        <div class="tabs__slide-title">Апартаменты</div>
                                        <div class="tabs__slide-price">от&nbsp<span>2 500 ₽</span> /час</div>
                                    </div>
                                    <div class="tabs__slide-line">
                                        <div class="tabs__slide-title">Выезд</div>
                                        <div class="tabs__slide-price">от&nbsp<span>4 500 ₽</span> /час</div>
                                    </div>
                                </div>
                            </div>
                            <div class="tabs__slide swiper-slide">
                                <div class="tabs__slide-head">
                                    <div class="tabs__slide-name">Алена</div>
                                    <button class="tabs__slide-favorites"><svg class="add-favorites js-favorites"
                                                                               xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24">
                                            <path class="path-empty"
                                                  d="M6.28 3c3.236.001 4.973 3.491 5.72 5.031.75-1.547 2.469-5.021 5.726-5.021 2.058 0 4.274 1.309 4.274 4.182 0 3.442-4.744 7.851-10 13-5.258-5.151-10-9.559-10-13 0-2.676 1.965-4.193 4.28-4.192zm.001-2c-3.183 0-6.281 2.187-6.281 6.192 0 4.661 5.57 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-4.011-3.097-6.182-6.274-6.182-2.204 0-4.446 1.042-5.726 3.238-1.285-2.206-3.522-3.248-5.719-3.248z">
                                            </path>
                                            <path class="path-full" xmlns="http://www.w3.org/2000/svg"
                                                  d="M12 4.248c-3.148-5.402-12-3.825-12 2.944 0 4.661 5.571 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-6.792-8.875-8.306-12-2.944z">
                                            </path>
                                        </svg></button>
                                </div>
                                <div class="tabs__slide-body"><img src="/images/img-1-db8e7456.jpg" alt="Алена">
                                    <div class="tabs__slide-stars">
                                        <div class="tabs__slide-title">Массажный салон</div>
                                        <div class="tabs__slide-rating">
                                            <div class="rating-static rating-static--one">
                                                <div class="rating-static__star"></div>
                                                <div class="rating-static__star"></div>
                                                <div class="rating-static__star"></div>
                                                <div class="rating-static__star"></div>
                                                <div class="rating-static__star"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tabs__slide-bottom">
                                    <div class="tabs__slide-line">
                                        <div class="tabs__slide-title">Апартаменты</div>
                                        <div class="tabs__slide-price">от&nbsp<span>2 500 ₽</span> /час</div>
                                    </div>
                                    <div class="tabs__slide-line">
                                        <div class="tabs__slide-title">Выезд</div>
                                        <div class="tabs__slide-price">от&nbsp<span>4 500 ₽</span> /час</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tabs__slider-pagination slider-pagination swiper-pagination">
                            <!-- Add Arrows-->
                        </div>
                        <div class="tabs__slider-next slider-next swiper-button-next"></div>
                        <div class="tabs__slider-prev slider-prev swiper-button-prev"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
