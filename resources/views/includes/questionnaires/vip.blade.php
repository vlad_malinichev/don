<div class="page__main-card">
    <div class="main-card  ">
        <div class="main-card__header">
            @if ($questionnaire->active && !$questionnaire->active)
            <div class="main-card__alert" style="display: block !important;">Девушка временно не работает</div>
            @endif
            <div class="main-card__info">
                <div class="main-card__personal">
                    <a class="main-card__name" href="{{ url('/questionnaire/' . $questionnaire->id) }}">
                        {{ $questionnaire->name }}
                        <div class="main-card__labels main-card__labels--mobile">
                            <div class="main-card__label main-card__label--new">
                                <img src="/images/new-1ad6a40b.png" alt="icon new">
                            </div>
                        </div>
                    </a>
                    <div class="main-card__separator"></div>
                    <div class="main-card__classification"> Индивидуалка <div
                            class="main-card__rating main-card__rating--mobile">
                            <div class="rating-static rating-static--four">
                                <div class="rating-static__star"></div>
                                <div class="rating-static__star"></div>
                                <div class="rating-static__star"></div>
                                <div class="rating-static__star"></div>
                                <div class="rating-static__star"></div>
                            </div>
                            <div class="main-card__voted">Проголосовало людей: 15</div>
                        </div>
                    </div>
                </div><a class="main-card__address" href="#"><span
                        class="main-card__city">Москва&nbsp;</span><span class="main-card__street">м. Улица
																	Народного ополчения</span></a>
            </div>
            <div class="main-card__block">
                <div class="main-card__labels">
                    <div class="main-card__label main-card__label--vip"><img src="/images/vip-009ffddc.png"
                                                                             alt="icon vip">
                    </div>
                    <div class="main-card__label main-card__label--new"><img src="/images/new-1ad6a40b.png"
                                                                             alt="icon new">
                    </div>
                </div>
                <div class="main-card__rating">
                    <div class="rating-static rating-static--four">
                        <div class="rating-static__star"></div>
                        <div class="rating-static__star"></div>
                        <div class="rating-static__star"></div>
                        <div class="rating-static__star"></div>
                        <div class="rating-static__star"></div>
                    </div>
                    <div class="main-card__voted">Проголосовало людей: 15 </div>
                </div>
            </div>
        </div>
        <div class="main-card__body">
            <div class="main-card__content">
                <div class="main-card__slider">
                    <div class="swiper-container js-main-card-slider">
                        <div class="swiper-wrapper">
                            <div class="swiper-slide">
                                <div class="swiper-slide__video">
                                    <video class="js-main-card-video" poster="/images/img-1-db8e7456.jpg">
                                        <source src="videos/test.mp4" type="video/mp4">
                                    </video>
                                </div>
                            </div>
                            <div class="swiper-slide"><img class="swiper-slide__img swiper-lazy"
                                                           data-src="/images/card-faf45814.jpg" alt="image">
                                <div class="swiper-lazy-preloader"></div>
                            </div>
                            <div class="swiper-slide"><img class="swiper-slide__img swiper-lazy"
                                                           data-src="/images/card-faf45814.jpg" alt="image">
                                <div class="swiper-lazy-preloader"></div>
                            </div>
                            <div class="swiper-slide"><img class="swiper-slide__img swiper-lazy"
                                                           data-src="/images/card-faf45814.jpg" alt="image">
                                <div class="swiper-lazy-preloader"></div>
                            </div>
                            <div class="swiper-slide"><img class="swiper-slide__img swiper-lazy"
                                                           data-src="/images/card-faf45814.jpg" alt="image">
                                <div class="swiper-lazy-preloader"></div>
                            </div>
                            <div class="swiper-slide">
                                <div class="swiper-slide__video">
                                    <video class="js-main-card-video" poster="/images/img-1-db8e7456.jpg">
                                        <source src="videos/test.mp4" type="video/mp4">
                                    </video>
                                </div>
                            </div>
                            <div class="swiper-slide"><img class="swiper-slide__img swiper-lazy"
                                                           data-src="/images/card-faf45814.jpg" alt="image">
                                <div class="swiper-lazy-preloader"></div>
                            </div>
                            <div class="swiper-slide"><img class="swiper-slide__img swiper-lazy"
                                                           data-src="/images/card-faf45814.jpg" alt="image">
                                <div class="swiper-lazy-preloader"></div>
                            </div>
                            <div class="swiper-slide"><img class="swiper-slide__img swiper-lazy"
                                                           data-src="/images/card-faf45814.jpg" alt="image">
                                <div class="swiper-lazy-preloader"></div>
                            </div>
                            <div class="swiper-slide"><img class="swiper-slide__img swiper-lazy"
                                                           data-src="/images/card-faf45814.jpg" alt="image">
                                <div class="swiper-lazy-preloader"></div>
                            </div>
                        </div>
                        <div class="swiper-pagination"></div>
                    </div>
                </div>
                <div class="main-card__buttons">
                    <div class="main-card__navigation">
                        <div class="swiper-button-prev"></div>
                        <div class="swiper-button-next"></div>
                    </div>
                    <div class="rating-score main-card__rating-score">
                        <div class="rating-score__title">Отзывы</div>
                        <div class="rating-score__items">
                            <div class="rating-score__item rating-score__item--like"> 7</div>
                            <div class="rating-score__item rating-score__item--dislike"> 3</div>
                        </div>
                    </div>
                </div>
                <div class="main-card__description">Эффектная красотка для состоятельных мужчин! Красивая,
                    ухоженная, темпераментная девушка, без вредных привычек! ТОЛЬКО для состоятельных я
                    красотка для состоятельных мужчин!</div>
            </div>
            <div class="main-card__aside">
                <div class="main-card__elems">
                    <div class="main-card__elem">
                        <div class="main-card__classification">Индивидуалка </div>
                        <ul class="main-card__items">
                            <li class="main-card__item">
                                <span class="main-card__legend">Возраст</span>
                                <span class="main-card__value">{{ $questionnaire->age }}</span>
                            </li>
                            <li class="main-card__item">
                                <span class="main-card__legend">Рост</span>
                                <span class="main-card__value">{{ $questionnaire->height }} см</span>
                            </li>
                            <li class="main-card__item">
                                <span class="main-card__legend">Вес</span>
                                <span class="main-card__value">{{ $questionnaire->weight }} кг</span>
                            </li>
                            <li class="main-card__item">
                                <span class="main-card__legend">Бюст</span>
                                <span class="main-card__value">{{ $questionnaire->breast_size }}</span>
                            </li>
                        </ul>
                    </div>
                    <div class="main-card__elem">
                        <div class="main-card__title"> Апартаменты<span class="main-card__value">от 2 500 ₽ /
																			час</span></div>
                        <ul class="main-card__items">
                            <li class="main-card__item"><span class="main-card__legend">1 час</span><span
                                    class="main-card__value">2 500 ₽</span>
                            </li>
                            <li class="main-card__item"><span class="main-card__legend">2 часа</span><span
                                    class="main-card__value">5 000 ₽</span>
                            </li>
                            <li class="main-card__item"><span class="main-card__legend">Ночь</span><span
                                    class="main-card__value">8 000 ₽</span>
                            </li>
                        </ul>
                    </div>
                    <div class="main-card__elem">
                        <div class="main-card__title"> Выезд<span class="main-card__value">от 2 500 ₽ /
																			час</span></div>
                        <ul class="main-card__items">
                            <li class="main-card__item"><span class="main-card__legend">1 час</span><span
                                    class="main-card__value">2 500 ₽</span>
                            </li>
                            <li class="main-card__item"><span class="main-card__legend">2 часа</span><span
                                    class="main-card__value">5 000 ₽</span>
                            </li>
                            <li class="main-card__item"><span class="main-card__legend">Ночь</span><span
                                    class="main-card__value">8 000 ₽</span>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="main-card__bottom">
                    <div class="main-card__date">06 мая 2020 14:14</div>
                    <div class="main-card__favorites"><svg class="add-favorites js-favorites"
                                                           xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24">
                            <path class="path-empty"
                                  d="M6.28 3c3.236.001 4.973 3.491 5.72 5.031.75-1.547 2.469-5.021 5.726-5.021 2.058 0 4.274 1.309 4.274 4.182 0 3.442-4.744 7.851-10 13-5.258-5.151-10-9.559-10-13 0-2.676 1.965-4.193 4.28-4.192zm.001-2c-3.183 0-6.281 2.187-6.281 6.192 0 4.661 5.57 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-4.011-3.097-6.182-6.274-6.182-2.204 0-4.446 1.042-5.726 3.238-1.285-2.206-3.522-3.248-5.719-3.248z">
                            </path>
                            <path class="path-full" xmlns="http://www.w3.org/2000/svg"
                                  d="M12 4.248c-3.148-5.402-12-3.825-12 2.944 0 4.661 5.571 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-6.792-8.875-8.306-12-2.944z">
                            </path>
                        </svg></div>
                </div>
            </div>
        </div>
    </div>
</div>
