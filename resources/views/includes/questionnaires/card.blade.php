<div class="page__card">
    <div class="card">
        <div class="card__header">
            <div class="card__info">
                <div class="card__classification">Индивидуалка</div>
                <div class="card__name">{{ $questionnaire->name }}</div>
            </div>
            <div class="card__block">
                <div class="card__rating">
                    <div class="rating-static rating-static--four">
                        <div class="rating-static__star"></div>
                        <div class="rating-static__star"></div>
                        <div class="rating-static__star"></div>
                        <div class="rating-static__star"></div>
                        <div class="rating-static__star"></div>
                    </div>
                    <div class="card__voted">Проголосовало людей: 15 </div>
                </div>
                <div class="card__labels">
                    <div class="card__label card__label--new">
                        <img src="/images/new-1ad6a40b.png" alt="label new">
                    </div>
                </div>
            </div>
        </div>
        <div class="card__body">
            <div class="card__content">
                <div class="card__section">
                    <div class="swiper-container card__slider js-card-slider">
                        <div class="swiper-wrapper">
                            <div class="swiper-slide">
                                <img class="swiper-slide__img swiper-lazy" data-src="/images/card-2-5496507d.jpg" alt="picture">
                                <div class="swiper-lazy-preloader">
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <img class="swiper-slide__img swiper-lazy" data-src="/images/card-2-5496507d.jpg" alt="picture">
                                <div class="swiper-lazy-preloader">
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <img class="swiper-slide__img swiper-lazy" data-src="/images/card-2-5496507d.jpg" alt="picture">
                                <div class="swiper-lazy-preloader">
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <img class="swiper-slide__img swiper-lazy"
                                                           data-src="/images/card-2-5496507d.jpg" alt="picture">
                                <div class="swiper-lazy-preloader">
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <img class="swiper-slide__img swiper-lazy"
                                                           data-src="/images/card-2-5496507d.jpg" alt="picture">
                                <div class="swiper-lazy-preloader">
                                </div>
                            </div>
                            <div class="swiper-slide"><img class="swiper-slide__img swiper-lazy"
                                                           data-src="/images/card-2-5496507d.jpg" alt="picture">
                                <div class="swiper-lazy-preloader">
                                </div>
                            </div>
                            <div class="swiper-slide"><img class="swiper-slide__img swiper-lazy"
                                                           data-src="/images/card-2-5496507d.jpg" alt="picture">
                                <div class="swiper-lazy-preloader">
                                </div>
                            </div>
                            <div class="swiper-slide"><img class="swiper-slide__img swiper-lazy"
                                                           data-src="/images/card-2-5496507d.jpg" alt="picture">
                                <div class="swiper-lazy-preloader">
                                </div>
                            </div>
                        </div>
                        <div class="swiper-pagination"></div>
                    </div>
                    <div class="rating-score card__rating-score">
                        <div class="rating-score__title">Отзывы</div>
                        <div class="rating-score__items">
                            <div class="rating-score__item rating-score__item--like"> 10</div>
                            <div class="rating-score__item rating-score__item--dislike"> 0</div>
                        </div>
                    </div>
                </div>
                <div class="card__elems">
                    <div class="card__elem">
                        <ul class="card__items">
                            <li class="card__item"><span class="card__legend">Страна</span><span
                                    class="card__value">Россия</span>
                            </li>
                            <li class="card__item"><span class="card__legend">Область</span><span
                                    class="card__value">Московская</span>
                            </li>
                            <li class="card__item"><span class="card__legend">Город</span><span
                                    class="card__value">Москва</span>
                            </li>
                            <li class="card__item"><span class="card__legend">Метро</span><span
                                    class="card__value">Выхино</span>
                            </li>
                        </ul>
                    </div>
                    <div class="card__elem">
                        <ul class="card__items">
                            <li class="card__item">
                                <span class="card__legend">Возраст</span>
                                <span class="card__value">{{ $questionnaire->age }}</span>
                            </li>
                            <li class="card__item">
                                <span class="card__legend">Рост</span>
                                <span class="card__value">{{ $questionnaire->height }} см</span>
                            </li>
                            <li class="card__item">
                                <span class="card__legend">Вес</span>
                                <span class="card__value">{{ $questionnaire->weight }} кг</span>
                            </li>
                            <li class="card__item">
                                <span class="card__legend">Бюст</span>
                                <span class="card__value">{{ $questionnaire->breast_size }}</span>
                            </li>
                        </ul>
                    </div>
                    <div class="card__elem card__elem--price">
                        <ul class="card__items">
                            <li class="card__item">
                                <span class="card__legend">Апартаменты</span>
                                <span class="card__value">от 2 500 ₽</span>
                            </li>
                            <li class="card__item">
                                <span class="card__legend">Выезд</span>
                                <span class="card__value">от 4 500 ₽</span>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="card__footer">
                <div class="card__date">06 мая 2020 14:14</div>
                <div class="card__favicons">
                    <svg class="add-favorites js-favorites" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24">
                        <path class="path-empty"
                              d="M6.28 3c3.236.001 4.973 3.491 5.72 5.031.75-1.547 2.469-5.021 5.726-5.021 2.058 0 4.274 1.309 4.274 4.182 0 3.442-4.744 7.851-10 13-5.258-5.151-10-9.559-10-13 0-2.676 1.965-4.193 4.28-4.192zm.001-2c-3.183 0-6.281 2.187-6.281 6.192 0 4.661 5.57 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-4.011-3.097-6.182-6.274-6.182-2.204 0-4.446 1.042-5.726 3.238-1.285-2.206-3.522-3.248-5.719-3.248z">
                        </path>
                        <path class="path-full" xmlns="http://www.w3.org/2000/svg"
                              d="M12 4.248c-3.148-5.402-12-3.825-12 2.944 0 4.661 5.571 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-6.792-8.875-8.306-12-2.944z">
                        </path>
                    </svg>
                </div>
            </div>
        </div>
    </div>
</div>
