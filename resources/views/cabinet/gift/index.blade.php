@extends('layouts.cabinet')

@section('title')
    Подарки
@endsection

@section('content')
    @include('cabinet._header', ['title' => 'Подарки'])
    <div class="gifts">
        <div class="gifts__container container">
            <div class="gifts__inner">
                <div class="gifts__items">
                    <div class="gifts__item">
                        <div class="gifts-card">
                            <div class="gifts-card__header">
                                <div class="gifts-card__date">06 мая 2020 14:14</div>
                            </div>
                            <div class="gifts-card__body">
                                <div class="gifts-card__entry"><span class="gifts-card__by">от</span><span
                                        class="gifts-card__user">Анонимный пользователь</span></div>
                                <div class="gifts-card__title">Название анкеты</div>
                                <div class="gifts-card__picture"><img class="gifts-card__image" src="./images/gifts-1-a6473595.png"
                                                                      alt="gift image"></div><a class="gifts-card__button button button js-gifts-modal"
                                                                                                href="#gifts-modal">Обменять на токены</a>
                            </div>
                        </div>
                    </div>
                    <div class="gifts__item">
                        <div class="gifts-card">
                            <div class="gifts-card__header">
                                <div class="gifts-card__date">06 мая 2020 14:14</div>
                            </div>
                            <div class="gifts-card__body">
                                <div class="gifts-card__entry"><span class="gifts-card__by">от</span><span
                                        class="gifts-card__user">Анонимный пользователь</span></div>
                                <div class="gifts-card__title">Название анкеты</div>
                                <div class="gifts-card__picture"><img class="gifts-card__image" src="./images/gifts-2-e68ac2d7.png"
                                                                      alt="gift image"></div><a class="gifts-card__button button button js-gifts-modal"
                                                                                                href="#gifts-modal">Обменять на токены</a>
                            </div>
                        </div>
                    </div>
                    <div class="gifts__item">
                        <div class="gifts-card">
                            <div class="gifts-card__header">
                                <div class="gifts-card__date">06 мая 2020 14:14</div>
                            </div>
                            <div class="gifts-card__body">
                                <div class="gifts-card__entry"><span class="gifts-card__by">от</span><span
                                        class="gifts-card__user">Анонимный пользователь</span></div>
                                <div class="gifts-card__title">Название анкеты</div>
                                <div class="gifts-card__picture"><img class="gifts-card__image" src="./images/gifts-3-1e57a2ea.png"
                                                                      alt="gift image"></div><a class="gifts-card__button button button js-gifts-modal"
                                                                                                href="#gifts-modal">Обменять на токены</a>
                            </div>
                        </div>
                    </div>
                    <div class="gifts__item">
                        <div class="gifts-card">
                            <div class="gifts-card__header">
                                <div class="gifts-card__date">06 мая 2020 14:14</div>
                            </div>
                            <div class="gifts-card__body">
                                <div class="gifts-card__entry"><span class="gifts-card__by">от</span><span
                                        class="gifts-card__user">Анонимный пользователь</span></div>
                                <div class="gifts-card__title">Название анкеты</div>
                                <div class="gifts-card__picture"><img class="gifts-card__image" src="./images/gifts-4-a544ab4b.png"
                                                                      alt="gift image"></div><a class="gifts-card__button button button js-gifts-modal"
                                                                                                href="#gifts-modal">Обменять на токены</a>
                            </div>
                        </div>
                    </div>
                    <div class="gifts__item">
                        <div class="gifts-card">
                            <div class="gifts-card__header">
                                <div class="gifts-card__date">06 мая 2020 14:14</div>
                            </div>
                            <div class="gifts-card__body">
                                <div class="gifts-card__entry"><span class="gifts-card__by">от</span><span
                                        class="gifts-card__user">Анонимный пользователь</span></div>
                                <div class="gifts-card__title">Название анкеты</div>
                                <div class="gifts-card__picture"><img class="gifts-card__image" src="./images/gifts-3-1e57a2ea.png"
                                                                      alt="gift image"></div><a class="gifts-card__button button button js-gifts-modal"
                                                                                                href="#gifts-modal">Обменять на токены</a>
                            </div>
                        </div>
                    </div>
                    <div class="gifts__item">
                        <div class="gifts-card">
                            <div class="gifts-card__header">
                                <div class="gifts-card__date">06 мая 2020 14:14</div>
                            </div>
                            <div class="gifts-card__body">
                                <div class="gifts-card__entry"><span class="gifts-card__by">от</span><span
                                        class="gifts-card__user">Анонимный пользователь</span></div>
                                <div class="gifts-card__title">Название анкеты</div>
                                <div class="gifts-card__picture"><img class="gifts-card__image" src="./images/gifts-4-a544ab4b.png"
                                                                      alt="gift image"></div><a class="gifts-card__button button button js-gifts-modal"
                                                                                                href="#gifts-modal">Обменять на токены</a>
                            </div>
                        </div>
                    </div>
                    <div class="gifts__item">
                        <div class="gifts-card">
                            <div class="gifts-card__header">
                                <div class="gifts-card__date">06 мая 2020 14:14</div>
                            </div>
                            <div class="gifts-card__body">
                                <div class="gifts-card__entry"><span class="gifts-card__by">от</span><span
                                        class="gifts-card__user">Анонимный пользователь</span></div>
                                <div class="gifts-card__title">Название анкеты</div>
                                <div class="gifts-card__picture"><img class="gifts-card__image" src="./images/gifts-2-e68ac2d7.png"
                                                                      alt="gift image"></div><a class="gifts-card__button button button js-gifts-modal"
                                                                                                href="#gifts-modal">Обменять на токены</a>
                            </div>
                        </div>
                    </div>
                    <div class="gifts__item">
                        <div class="gifts-card">
                            <div class="gifts-card__header">
                                <div class="gifts-card__date">06 мая 2020 14:14</div>
                            </div>
                            <div class="gifts-card__body">
                                <div class="gifts-card__entry"><span class="gifts-card__by">от</span><span
                                        class="gifts-card__user">Анонимный пользователь</span></div>
                                <div class="gifts-card__title">Название анкеты</div>
                                <div class="gifts-card__picture"><img class="gifts-card__image" src="./images/gifts-1-a6473595.png"
                                                                      alt="gift image"></div><a class="gifts-card__button button button js-gifts-modal"
                                                                                                href="#gifts-modal">Обменять на токены</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="gifts__pagination">
                    <div class="pagination">
                        <div class="pagination__wrap">
                            <button class="pagination__button pagination__button--back pagination__button--disabled">
                                <svg width="7" height="12" viewBox="0 0 7 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M6 1L1 6L6 11" stroke="#5C5C5C" stroke-width="1.8" stroke-linecap="round"
                                          stroke-linejoin="round" />
                                </svg> Назад </button>
                            <ul class="pagination__list">
                                <li class="pagination__item pagination__item--active"><a class="pagination__link" href="#">1</a>
                                </li>
                                <li class="pagination__item"><a class="pagination__link" href="#">2</a></li>
                                <li class="pagination__item"><a class="pagination__link pagination__link--dot" href="#">...</a></li>
                            </ul>
                            <button class="pagination__button"> Далее<svg width="7" height="12" viewBox="0 0 7 12" fill="none"
                                                                          xmlns="http://www.w3.org/2000/svg">
                                    <path d="M1 11L6 6L1 1" stroke="#5C5C5C" stroke-width="1.8" stroke-linecap="round"
                                          stroke-linejoin="round" />
                                </svg>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <form class="gifts-modal mfp-hide" action="/" id="gifts-modal">
        <div class="gifts-modal__header">
            <div class="gifts-modal__title">Обмен подарка</div>
        </div>
        <div class="gifts-modal__body">
            <div class="gifts-modal__entry"><span class="gifts-modal__by">от</span><span
                    class="gifts-modal__user">Анонимный пользователь</span></div>
            <div class="gifts-modal__name">Название анкеты Название анкеты</div>
            <div class="gifts-modal__block">
                <div class="gifts-modal__picture"><img class="gifts-modal__image" src="./images/gifts-1-a6473595.png"
                                                       alt="gifts image"></div>
                <div class="gifts-modal__arrow"></div>
                <div class="gifts-modal__token">Вы получите <span>100&nbsp;</span>токенов</div>
            </div>
        </div>
        <div class="gifts-modal__footer">
            <button class="gifts-modal__button button" type="submit">Подтвердить</button>
            <button class="gifts-modal__reset" type="reset">Отменить</button>
        </div>
    </form>
    @include('includes._bottom-banner', ['wrapperClass' => 'bottom-banner--transparent'])
@endsection
