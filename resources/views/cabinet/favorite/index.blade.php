@extends('layouts.cabinet')

@section('title')
    Избранное
@endsection

@section('content')
    @include('cabinet._header', ['title' => 'Избранное'])
    <div class="template">
        <div class="template__container container">
            <div class="template__body">
                <div class="template__sidebar">
                    <aside class="sidebar">
                        <div class="sidebar__wrap">
                            <ul class="sidebar__list">
                                <li class="sidebar__item sidebar__item--active"><a class="sidebar__link js-btn-screen"
                                                                                   href="#">Избранные анкеты<span class="sidebar__value">20</span></a></li>
                                <li class="sidebar__item"><a class="sidebar__link js-btn-screen"
                                                             href="favorites-search.html">Избранные поиски<span class="sidebar__value">18</span></a></li>
                            </ul>
                        </div>
                    </aside>
                </div>
                <div class="template__content">
                    <div class="favorites">
                        <div class="favorites__filter">
                            <form class="cabinet-filter" action="/">
                                <div class="cabinet-filter__toggle js-filter-toggle"><svg xmlns="http://www.w3.org/2000/svg"
                                                                                          width="18" height="18" fill="none" xmlns:v="https://vecta.io/nano">
                                        <g clip-path="url(#A)" fill="#f90">
                                            <path
                                                d="M17.357 8.357H6.98a2.56 2.56 0 0 0-4.96 0H.643C.288 8.357 0 8.645 0 9s.288.643.643.643H2.02a2.56 2.56 0 0 0 4.96 0h10.377c.355 0 .643-.288.643-.643s-.288-.643-.643-.643zM4.5 10.286c-.7 0-1.286-.576-1.286-1.286S3.8 7.714 4.5 7.714A1.3 1.3 0 0 1 5.786 9 1.3 1.3 0 0 1 4.5 10.286zm12.857-8.358h-2.02a2.56 2.56 0 0 0-4.96 0H.643c-.355 0-.643.288-.643.643s.288.643.643.643h9.734a2.56 2.56 0 0 0 4.96 0h2.02c.355 0 .643-.288.643-.643s-.288-.643-.643-.643zm-4.5 1.93c-.7 0-1.286-.576-1.286-1.286s.576-1.286 1.286-1.286 1.286.576 1.286 1.286-.576 1.286-1.286 1.286zm4.5 10.928h-3.305a2.56 2.56 0 0 0-4.96 0H.643c-.355 0-.643.288-.643.643s.288.643.643.643H9.1a2.56 2.56 0 0 0 4.96 0h3.305c.355 0 .643-.288.643-.643s-.288-.643-.643-.643zm-5.786 1.93c-.7 0-1.286-.576-1.286-1.286s.576-1.286 1.286-1.286 1.286.576 1.286 1.286-.576 1.286-1.286 1.286z" />
                                        </g>
                                        <defs>
                                            <clipPath id="A">
                                                <path fill="#fff" d="M0 0h18v18H0z" />
                                            </clipPath>
                                        </defs>
                                    </svg>
                                    <div class="cabinet-filter__title">Фильтр</div>
                                    <div class="cabinet-filter__plus"></div>
                                </div>
                                <div class="cabinet-filter__body">
                                    <div class="cabinet-filter__content">
                                        <div class="cabinet-filter__items">
                                            <div class="cabinet-filter__item">
                                                <div class="cabinet-filter__label">Страна</div>
                                                <select class="js-select" name="country">
                                                    <option value="Россия">Россия</option>
                                                    <option value="Россия">Россия</option>
                                                </select>
                                            </div>
                                            <div class="cabinet-filter__item">
                                                <div class="cabinet-filter__label">Область</div>
                                                <select class="js-select" name="region">
                                                    <option value="Московская">Московская</option>
                                                    <option value="Московская">Московская</option>
                                                </select>
                                            </div>
                                            <div class="cabinet-filter__item">
                                                <div class="cabinet-filter__label">Город</div>
                                                <select class="js-select" name="city">
                                                    <option value="Москва">Москва</option>
                                                    <option value="Москва">Москва</option>
                                                </select>
                                            </div>
                                            <div class="cabinet-filter__item">
                                                <div class="cabinet-filter__label">Тип</div>
                                                <select class="js-select" name="type">
                                                    <option value="Все">Все</option>
                                                    <option value="Все">Все</option>
                                                </select>
                                            </div>
                                            <div class="cabinet-filter__item">
                                                <div class="cabinet-filter__label">Статус анкеты</div>
                                                <select class="js-select" name="profile_status">
                                                    <option value="VIP">VIP</option>
                                                    <option value="VIP">VIP</option>
                                                </select>
                                            </div>
                                            <div class="cabinet-filter__item">
                                                <div class="cabinet-filter__label">Статус публикации</div>
                                                <select class="js-select" name="publication_status">
                                                    <option value="Активна">Активна</option>
                                                    <option value="Активна">Активна</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="cabinet-filter__footer">
                                        <button class="cabinet-filter__button button" type="submit">Найти</button>
                                        <button class="cabinet-filter__reset" type="reset">Сбросить</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="favorites__cards">
                            <div class="favorites__card">
                                <div class="main-card  ">
                                    <div class="main-card__header">
                                        <div class="main-card__alert">Девушка временно не работает</div>
                                        <div class="main-card__info">
                                            <div class="main-card__personal"><a class="main-card__name" href="questionnaire.html"> Соня
                                                    <div class="main-card__labels main-card__labels--mobile">
                                                        <div class="main-card__label main-card__label--new"><img src="./images/new-1ad6a40b.png"
                                                                                                                 alt="icon new"></div>
                                                    </div></a>
                                                <div class="main-card__separator"></div>
                                                <div class="main-card__classification"> Индивидуалка <div
                                                        class="main-card__rating main-card__rating--mobile">
                                                        <div class="rating-static rating-static--four">
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                        </div>
                                                        <div class="main-card__voted">Проголосовало людей: 15</div>
                                                    </div>
                                                </div>
                                            </div><a class="main-card__address" href="#"><span
                                                    class="main-card__city">Москва&nbsp;</span><span class="main-card__street">м. Улица
															Народного ополчения</span></a>
                                        </div>
                                        <div class="main-card__block">
                                            <div class="main-card__labels">
                                                <div class="main-card__label main-card__label--vip"><img src="./images/vip-009ffddc.png"
                                                                                                         alt="icon vip"></div>
                                                <div class="main-card__label main-card__label--new"><img src="./images/new-1ad6a40b.png"
                                                                                                         alt="icon new"></div>
                                            </div>
                                            <div class="main-card__rating">
                                                <div class="rating-static rating-static--four">
                                                    <div class="rating-static__star"></div>
                                                    <div class="rating-static__star"></div>
                                                    <div class="rating-static__star"></div>
                                                    <div class="rating-static__star"></div>
                                                    <div class="rating-static__star"></div>
                                                </div>
                                                <div class="main-card__voted">Проголосовало людей: 15</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="main-card__body">
                                        <div class="main-card__content">
                                            <div class="main-card__slider">
                                                <div class="swiper-container js-main-card-slider">
                                                    <div class="swiper-wrapper">
                                                        <div class="swiper-slide">
                                                            <div class="swiper-slide__video">
                                                                <video class="js-main-card-video" poster="./images/img-1-db8e7456.jpg">
                                                                    <source src="videos/test.mp4" type="video/mp4">
                                                                </video>
                                                            </div>
                                                        </div>
                                                        <div class="swiper-slide"><img class="swiper-slide__img swiper-lazy"
                                                                                       data-src="./images/card-faf45814.jpg" alt="image">
                                                            <div class="swiper-lazy-preloader"></div>
                                                        </div>
                                                        <div class="swiper-slide"><img class="swiper-slide__img swiper-lazy"
                                                                                       data-src="./images/card-faf45814.jpg" alt="image">
                                                            <div class="swiper-lazy-preloader"></div>
                                                        </div>
                                                        <div class="swiper-slide"><img class="swiper-slide__img swiper-lazy"
                                                                                       data-src="./images/card-faf45814.jpg" alt="image">
                                                            <div class="swiper-lazy-preloader"></div>
                                                        </div>
                                                        <div class="swiper-slide"><img class="swiper-slide__img swiper-lazy"
                                                                                       data-src="./images/card-faf45814.jpg" alt="image">
                                                            <div class="swiper-lazy-preloader"></div>
                                                        </div>
                                                        <div class="swiper-slide">
                                                            <div class="swiper-slide__video">
                                                                <video class="js-main-card-video" poster="./images/img-1-db8e7456.jpg">
                                                                    <source src="videos/test.mp4" type="video/mp4">
                                                                </video>
                                                            </div>
                                                        </div>
                                                        <div class="swiper-slide"><img class="swiper-slide__img swiper-lazy"
                                                                                       data-src="./images/card-faf45814.jpg" alt="image">
                                                            <div class="swiper-lazy-preloader"></div>
                                                        </div>
                                                        <div class="swiper-slide"><img class="swiper-slide__img swiper-lazy"
                                                                                       data-src="./images/card-faf45814.jpg" alt="image">
                                                            <div class="swiper-lazy-preloader"></div>
                                                        </div>
                                                        <div class="swiper-slide"><img class="swiper-slide__img swiper-lazy"
                                                                                       data-src="./images/card-faf45814.jpg" alt="image">
                                                            <div class="swiper-lazy-preloader"></div>
                                                        </div>
                                                        <div class="swiper-slide"><img class="swiper-slide__img swiper-lazy"
                                                                                       data-src="./images/card-faf45814.jpg" alt="image">
                                                            <div class="swiper-lazy-preloader"></div>
                                                        </div>
                                                    </div>
                                                    <div class="swiper-pagination"></div>
                                                </div>
                                            </div>
                                            <div class="main-card__buttons">
                                                <div class="main-card__navigation">
                                                    <div class="swiper-button-prev"></div>
                                                    <div class="swiper-button-next"></div>
                                                </div>
                                                <div class="rating-score main-card__rating-score">
                                                    <div class="rating-score__title">Отзывы</div>
                                                    <div class="rating-score__items">
                                                        <div class="rating-score__item rating-score__item--like">7</div>
                                                        <div class="rating-score__item rating-score__item--dislike">3</div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="main-card__description">Эффектная красотка для состоятельных мужчин! Красивая,
                                                ухоженная, темпераментная девушка, без вредных привычек! ТОЛЬКО для состоятельных я красотка
                                                для состоятельных мужчин!</div>
                                        </div>
                                        <div class="main-card__aside">
                                            <div class="main-card__elems">
                                                <div class="main-card__elem">
                                                    <div class="main-card__classification">Индивидуалка</div>
                                                    <ul class="main-card__items">
                                                        <li class="main-card__item"><span class="main-card__legend">Возраст</span><span
                                                                class="main-card__value">20</span></li>
                                                        <li class="main-card__item"><span class="main-card__legend">Рост</span><span
                                                                class="main-card__value">164 см</span></li>
                                                        <li class="main-card__item"><span class="main-card__legend">Вес</span><span
                                                                class="main-card__value">54 кг</span></li>
                                                        <li class="main-card__item"><span class="main-card__legend">Бюст</span><span
                                                                class="main-card__value">2</span></li>
                                                    </ul>
                                                </div>
                                                <div class="main-card__elem">
                                                    <div class="main-card__title"> Апартаменты<span class="main-card__value">от 2 500 ₽ /
																	час</span></div>
                                                    <ul class="main-card__items">
                                                        <li class="main-card__item"><span class="main-card__legend">1 час</span><span
                                                                class="main-card__value">2 500 ₽</span></li>
                                                        <li class="main-card__item"><span class="main-card__legend">2 часа</span><span
                                                                class="main-card__value">5 000 ₽</span></li>
                                                        <li class="main-card__item"><span class="main-card__legend">Ночь</span><span
                                                                class="main-card__value">8 000 ₽</span></li>
                                                    </ul>
                                                </div>
                                                <div class="main-card__elem">
                                                    <div class="main-card__title"> Выезд<span class="main-card__value">от 2 500 ₽ / час</span>
                                                    </div>
                                                    <ul class="main-card__items">
                                                        <li class="main-card__item"><span class="main-card__legend">1 час</span><span
                                                                class="main-card__value">2 500 ₽</span></li>
                                                        <li class="main-card__item"><span class="main-card__legend">2 часа</span><span
                                                                class="main-card__value">5 000 ₽</span></li>
                                                        <li class="main-card__item"><span class="main-card__legend">Ночь</span><span
                                                                class="main-card__value">8 000 ₽</span></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="main-card__bottom">
                                                <div class="main-card__date">06 мая 2020 14:14</div>
                                                <div class="main-card__favorites"><svg class="add-favorites js-favorites"
                                                                                       xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24">
                                                        <path class="path-empty"
                                                              d="M6.28 3c3.236.001 4.973 3.491 5.72 5.031.75-1.547 2.469-5.021 5.726-5.021 2.058 0 4.274 1.309 4.274 4.182 0 3.442-4.744 7.851-10 13-5.258-5.151-10-9.559-10-13 0-2.676 1.965-4.193 4.28-4.192zm.001-2c-3.183 0-6.281 2.187-6.281 6.192 0 4.661 5.57 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-4.011-3.097-6.182-6.274-6.182-2.204 0-4.446 1.042-5.726 3.238-1.285-2.206-3.522-3.248-5.719-3.248z">
                                                        </path>
                                                        <path class="path-full" xmlns="http://www.w3.org/2000/svg"
                                                              d="M12 4.248c-3.148-5.402-12-3.825-12 2.944 0 4.661 5.571 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-6.792-8.875-8.306-12-2.944z">
                                                        </path>
                                                    </svg></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="favorites__card">
                                <div class="main-card  ">
                                    <div class="main-card__header">
                                        <div class="main-card__alert">Девушка временно не работает</div>
                                        <div class="main-card__info">
                                            <div class="main-card__personal"><a class="main-card__name" href="questionnaire.html"> Соня
                                                    <div class="main-card__labels main-card__labels--mobile">
                                                        <div class="main-card__label main-card__label--new"><img src="./images/new-1ad6a40b.png"
                                                                                                                 alt="icon new"></div>
                                                    </div></a>
                                                <div class="main-card__separator"></div>
                                                <div class="main-card__classification"> Индивидуалка <div
                                                        class="main-card__rating main-card__rating--mobile">
                                                        <div class="rating-static rating-static--four">
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                        </div>
                                                        <div class="main-card__voted">Проголосовало людей: 15</div>
                                                    </div>
                                                </div>
                                            </div><a class="main-card__address" href="#"><span
                                                    class="main-card__city">Москва&nbsp;</span><span class="main-card__street">м. Улица
															Народного ополчения</span></a>
                                        </div>
                                        <div class="main-card__block">
                                            <div class="main-card__labels">
                                                <div class="main-card__label main-card__label--vip"><img src="./images/vip-009ffddc.png"
                                                                                                         alt="icon vip"></div>
                                                <div class="main-card__label main-card__label--new"><img src="./images/new-1ad6a40b.png"
                                                                                                         alt="icon new"></div>
                                            </div>
                                            <div class="main-card__rating">
                                                <div class="rating-static rating-static--four">
                                                    <div class="rating-static__star"></div>
                                                    <div class="rating-static__star"></div>
                                                    <div class="rating-static__star"></div>
                                                    <div class="rating-static__star"></div>
                                                    <div class="rating-static__star"></div>
                                                </div>
                                                <div class="main-card__voted">Проголосовало людей: 15</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="main-card__body">
                                        <div class="main-card__content">
                                            <div class="main-card__slider">
                                                <div class="swiper-container js-main-card-slider">
                                                    <div class="swiper-wrapper">
                                                        <div class="swiper-slide">
                                                            <div class="swiper-slide__video">
                                                                <video class="js-main-card-video" poster="./images/img-1-db8e7456.jpg">
                                                                    <source src="videos/test.mp4" type="video/mp4">
                                                                </video>
                                                            </div>
                                                        </div>
                                                        <div class="swiper-slide"><img class="swiper-slide__img swiper-lazy"
                                                                                       data-src="./images/card-faf45814.jpg" alt="image">
                                                            <div class="swiper-lazy-preloader"></div>
                                                        </div>
                                                        <div class="swiper-slide"><img class="swiper-slide__img swiper-lazy"
                                                                                       data-src="./images/card-faf45814.jpg" alt="image">
                                                            <div class="swiper-lazy-preloader"></div>
                                                        </div>
                                                        <div class="swiper-slide"><img class="swiper-slide__img swiper-lazy"
                                                                                       data-src="./images/card-faf45814.jpg" alt="image">
                                                            <div class="swiper-lazy-preloader"></div>
                                                        </div>
                                                        <div class="swiper-slide"><img class="swiper-slide__img swiper-lazy"
                                                                                       data-src="./images/card-faf45814.jpg" alt="image">
                                                            <div class="swiper-lazy-preloader"></div>
                                                        </div>
                                                        <div class="swiper-slide">
                                                            <div class="swiper-slide__video">
                                                                <video class="js-main-card-video" poster="./images/img-1-db8e7456.jpg">
                                                                    <source src="videos/test.mp4" type="video/mp4">
                                                                </video>
                                                            </div>
                                                        </div>
                                                        <div class="swiper-slide"><img class="swiper-slide__img swiper-lazy"
                                                                                       data-src="./images/card-faf45814.jpg" alt="image">
                                                            <div class="swiper-lazy-preloader"></div>
                                                        </div>
                                                        <div class="swiper-slide"><img class="swiper-slide__img swiper-lazy"
                                                                                       data-src="./images/card-faf45814.jpg" alt="image">
                                                            <div class="swiper-lazy-preloader"></div>
                                                        </div>
                                                        <div class="swiper-slide"><img class="swiper-slide__img swiper-lazy"
                                                                                       data-src="./images/card-faf45814.jpg" alt="image">
                                                            <div class="swiper-lazy-preloader"></div>
                                                        </div>
                                                        <div class="swiper-slide"><img class="swiper-slide__img swiper-lazy"
                                                                                       data-src="./images/card-faf45814.jpg" alt="image">
                                                            <div class="swiper-lazy-preloader"></div>
                                                        </div>
                                                    </div>
                                                    <div class="swiper-pagination"></div>
                                                </div>
                                            </div>
                                            <div class="main-card__buttons">
                                                <div class="main-card__navigation">
                                                    <div class="swiper-button-prev"></div>
                                                    <div class="swiper-button-next"></div>
                                                </div>
                                                <div class="rating-score main-card__rating-score">
                                                    <div class="rating-score__title">Отзывы</div>
                                                    <div class="rating-score__items">
                                                        <div class="rating-score__item rating-score__item--like">7</div>
                                                        <div class="rating-score__item rating-score__item--dislike">3</div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="main-card__description">Эффектная красотка для состоятельных мужчин! Красивая,
                                                ухоженная, темпераментная девушка, без вредных привычек! ТОЛЬКО для состоятельных я красотка
                                                для состоятельных мужчин!</div>
                                        </div>
                                        <div class="main-card__aside">
                                            <div class="main-card__elems">
                                                <div class="main-card__elem">
                                                    <div class="main-card__classification">Индивидуалка</div>
                                                    <ul class="main-card__items">
                                                        <li class="main-card__item"><span class="main-card__legend">Возраст</span><span
                                                                class="main-card__value">20</span></li>
                                                        <li class="main-card__item"><span class="main-card__legend">Рост</span><span
                                                                class="main-card__value">164 см</span></li>
                                                        <li class="main-card__item"><span class="main-card__legend">Вес</span><span
                                                                class="main-card__value">54 кг</span></li>
                                                        <li class="main-card__item"><span class="main-card__legend">Бюст</span><span
                                                                class="main-card__value">2</span></li>
                                                    </ul>
                                                </div>
                                                <div class="main-card__elem">
                                                    <div class="main-card__title"> Апартаменты<span class="main-card__value">от 2 500 ₽ /
																	час</span></div>
                                                    <ul class="main-card__items">
                                                        <li class="main-card__item"><span class="main-card__legend">1 час</span><span
                                                                class="main-card__value">2 500 ₽</span></li>
                                                        <li class="main-card__item"><span class="main-card__legend">2 часа</span><span
                                                                class="main-card__value">5 000 ₽</span></li>
                                                        <li class="main-card__item"><span class="main-card__legend">Ночь</span><span
                                                                class="main-card__value">8 000 ₽</span></li>
                                                    </ul>
                                                </div>
                                                <div class="main-card__elem">
                                                    <div class="main-card__title"> Выезд<span class="main-card__value">от 2 500 ₽ / час</span>
                                                    </div>
                                                    <ul class="main-card__items">
                                                        <li class="main-card__item"><span class="main-card__legend">1 час</span><span
                                                                class="main-card__value">2 500 ₽</span></li>
                                                        <li class="main-card__item"><span class="main-card__legend">2 часа</span><span
                                                                class="main-card__value">5 000 ₽</span></li>
                                                        <li class="main-card__item"><span class="main-card__legend">Ночь</span><span
                                                                class="main-card__value">8 000 ₽</span></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="main-card__bottom">
                                                <div class="main-card__date">06 мая 2020 14:14</div>
                                                <div class="main-card__favorites"><svg class="add-favorites js-favorites"
                                                                                       xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24">
                                                        <path class="path-empty"
                                                              d="M6.28 3c3.236.001 4.973 3.491 5.72 5.031.75-1.547 2.469-5.021 5.726-5.021 2.058 0 4.274 1.309 4.274 4.182 0 3.442-4.744 7.851-10 13-5.258-5.151-10-9.559-10-13 0-2.676 1.965-4.193 4.28-4.192zm.001-2c-3.183 0-6.281 2.187-6.281 6.192 0 4.661 5.57 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-4.011-3.097-6.182-6.274-6.182-2.204 0-4.446 1.042-5.726 3.238-1.285-2.206-3.522-3.248-5.719-3.248z">
                                                        </path>
                                                        <path class="path-full" xmlns="http://www.w3.org/2000/svg"
                                                              d="M12 4.248c-3.148-5.402-12-3.825-12 2.944 0 4.661 5.571 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-6.792-8.875-8.306-12-2.944z">
                                                        </path>
                                                    </svg></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="favorites__card">
                                <div class="main-card main-card--new ">
                                    <div class="main-card__header">
                                        <div class="main-card__alert">Девушка временно не работает</div>
                                        <div class="main-card__info">
                                            <div class="main-card__personal"><a class="main-card__name" href="questionnaire.html"> Соня
                                                    <div class="main-card__labels main-card__labels--mobile">
                                                        <div class="main-card__label main-card__label--new"><img src="./images/new-1ad6a40b.png"
                                                                                                                 alt="icon new"></div>
                                                    </div></a>
                                                <div class="main-card__separator"></div>
                                                <div class="main-card__classification"> Индивидуалка <div
                                                        class="main-card__rating main-card__rating--mobile">
                                                        <div class="rating-static rating-static--four">
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                        </div>
                                                        <div class="main-card__voted">Проголосовало людей: 15</div>
                                                    </div>
                                                </div>
                                            </div><a class="main-card__address" href="#"><span
                                                    class="main-card__city">Москва&nbsp;</span><span class="main-card__street">м. Улица
															Народного ополчения</span></a>
                                        </div>
                                        <div class="main-card__block">
                                            <div class="main-card__labels">
                                                <div class="main-card__label main-card__label--vip"><img src="./images/vip-009ffddc.png"
                                                                                                         alt="icon vip"></div>
                                                <div class="main-card__label main-card__label--new"><img src="./images/new-1ad6a40b.png"
                                                                                                         alt="icon new"></div>
                                            </div>
                                            <div class="main-card__rating">
                                                <div class="rating-static rating-static--four">
                                                    <div class="rating-static__star"></div>
                                                    <div class="rating-static__star"></div>
                                                    <div class="rating-static__star"></div>
                                                    <div class="rating-static__star"></div>
                                                    <div class="rating-static__star"></div>
                                                </div>
                                                <div class="main-card__voted">Проголосовало людей: 15</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="main-card__body">
                                        <div class="main-card__content">
                                            <div class="main-card__slider">
                                                <div class="swiper-container js-main-card-slider">
                                                    <div class="swiper-wrapper">
                                                        <div class="swiper-slide">
                                                            <div class="swiper-slide__video">
                                                                <video class="js-main-card-video" poster="./images/img-1-db8e7456.jpg">
                                                                    <source src="videos/test.mp4" type="video/mp4">
                                                                </video>
                                                            </div>
                                                        </div>
                                                        <div class="swiper-slide"><img class="swiper-slide__img swiper-lazy"
                                                                                       data-src="./images/card-faf45814.jpg" alt="image">
                                                            <div class="swiper-lazy-preloader"></div>
                                                        </div>
                                                        <div class="swiper-slide"><img class="swiper-slide__img swiper-lazy"
                                                                                       data-src="./images/card-faf45814.jpg" alt="image">
                                                            <div class="swiper-lazy-preloader"></div>
                                                        </div>
                                                        <div class="swiper-slide"><img class="swiper-slide__img swiper-lazy"
                                                                                       data-src="./images/card-faf45814.jpg" alt="image">
                                                            <div class="swiper-lazy-preloader"></div>
                                                        </div>
                                                        <div class="swiper-slide"><img class="swiper-slide__img swiper-lazy"
                                                                                       data-src="./images/card-faf45814.jpg" alt="image">
                                                            <div class="swiper-lazy-preloader"></div>
                                                        </div>
                                                        <div class="swiper-slide">
                                                            <div class="swiper-slide__video">
                                                                <video class="js-main-card-video" poster="./images/img-1-db8e7456.jpg">
                                                                    <source src="videos/test.mp4" type="video/mp4">
                                                                </video>
                                                            </div>
                                                        </div>
                                                        <div class="swiper-slide"><img class="swiper-slide__img swiper-lazy"
                                                                                       data-src="./images/card-faf45814.jpg" alt="image">
                                                            <div class="swiper-lazy-preloader"></div>
                                                        </div>
                                                        <div class="swiper-slide"><img class="swiper-slide__img swiper-lazy"
                                                                                       data-src="./images/card-faf45814.jpg" alt="image">
                                                            <div class="swiper-lazy-preloader"></div>
                                                        </div>
                                                        <div class="swiper-slide"><img class="swiper-slide__img swiper-lazy"
                                                                                       data-src="./images/card-faf45814.jpg" alt="image">
                                                            <div class="swiper-lazy-preloader"></div>
                                                        </div>
                                                        <div class="swiper-slide"><img class="swiper-slide__img swiper-lazy"
                                                                                       data-src="./images/card-faf45814.jpg" alt="image">
                                                            <div class="swiper-lazy-preloader"></div>
                                                        </div>
                                                    </div>
                                                    <div class="swiper-pagination"></div>
                                                </div>
                                            </div>
                                            <div class="main-card__buttons">
                                                <div class="main-card__navigation">
                                                    <div class="swiper-button-prev"></div>
                                                    <div class="swiper-button-next"></div>
                                                </div>
                                                <div class="rating-score main-card__rating-score">
                                                    <div class="rating-score__title">Отзывы</div>
                                                    <div class="rating-score__items">
                                                        <div class="rating-score__item rating-score__item--like">7</div>
                                                        <div class="rating-score__item rating-score__item--dislike">3</div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="main-card__description">Эффектная красотка для состоятельных мужчин! Красивая,
                                                ухоженная, темпераментная девушка, без вредных привычек! ТОЛЬКО для состоятельных я красотка
                                                для состоятельных мужчин!</div>
                                        </div>
                                        <div class="main-card__aside">
                                            <div class="main-card__elems">
                                                <div class="main-card__elem">
                                                    <div class="main-card__classification">Индивидуалка</div>
                                                    <ul class="main-card__items">
                                                        <li class="main-card__item"><span class="main-card__legend">Возраст</span><span
                                                                class="main-card__value">20</span></li>
                                                        <li class="main-card__item"><span class="main-card__legend">Рост</span><span
                                                                class="main-card__value">164 см</span></li>
                                                        <li class="main-card__item"><span class="main-card__legend">Вес</span><span
                                                                class="main-card__value">54 кг</span></li>
                                                        <li class="main-card__item"><span class="main-card__legend">Бюст</span><span
                                                                class="main-card__value">2</span></li>
                                                    </ul>
                                                </div>
                                                <div class="main-card__elem">
                                                    <div class="main-card__title"> Апартаменты<span class="main-card__value">от 2 500 ₽ /
																	час</span></div>
                                                    <ul class="main-card__items">
                                                        <li class="main-card__item"><span class="main-card__legend">1 час</span><span
                                                                class="main-card__value">2 500 ₽</span></li>
                                                        <li class="main-card__item"><span class="main-card__legend">2 часа</span><span
                                                                class="main-card__value">5 000 ₽</span></li>
                                                        <li class="main-card__item"><span class="main-card__legend">Ночь</span><span
                                                                class="main-card__value">8 000 ₽</span></li>
                                                    </ul>
                                                </div>
                                                <div class="main-card__elem">
                                                    <div class="main-card__title"> Выезд<span class="main-card__value">от 2 500 ₽ / час</span>
                                                    </div>
                                                    <ul class="main-card__items">
                                                        <li class="main-card__item"><span class="main-card__legend">1 час</span><span
                                                                class="main-card__value">2 500 ₽</span></li>
                                                        <li class="main-card__item"><span class="main-card__legend">2 часа</span><span
                                                                class="main-card__value">5 000 ₽</span></li>
                                                        <li class="main-card__item"><span class="main-card__legend">Ночь</span><span
                                                                class="main-card__value">8 000 ₽</span></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="main-card__bottom">
                                                <div class="main-card__date">06 мая 2020 14:14</div>
                                                <div class="main-card__favorites"><svg class="add-favorites js-favorites"
                                                                                       xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24">
                                                        <path class="path-empty"
                                                              d="M6.28 3c3.236.001 4.973 3.491 5.72 5.031.75-1.547 2.469-5.021 5.726-5.021 2.058 0 4.274 1.309 4.274 4.182 0 3.442-4.744 7.851-10 13-5.258-5.151-10-9.559-10-13 0-2.676 1.965-4.193 4.28-4.192zm.001-2c-3.183 0-6.281 2.187-6.281 6.192 0 4.661 5.57 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-4.011-3.097-6.182-6.274-6.182-2.204 0-4.446 1.042-5.726 3.238-1.285-2.206-3.522-3.248-5.719-3.248z">
                                                        </path>
                                                        <path class="path-full" xmlns="http://www.w3.org/2000/svg"
                                                              d="M12 4.248c-3.148-5.402-12-3.825-12 2.944 0 4.661 5.571 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-6.792-8.875-8.306-12-2.944z">
                                                        </path>
                                                    </svg></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="favorites__card">
                                <div class="main-card main-card--new ">
                                    <div class="main-card__header">
                                        <div class="main-card__alert">Девушка временно не работает</div>
                                        <div class="main-card__info">
                                            <div class="main-card__personal"><a class="main-card__name" href="questionnaire.html"> Соня
                                                    <div class="main-card__labels main-card__labels--mobile">
                                                        <div class="main-card__label main-card__label--new"><img src="./images/new-1ad6a40b.png"
                                                                                                                 alt="icon new"></div>
                                                    </div></a>
                                                <div class="main-card__separator"></div>
                                                <div class="main-card__classification"> Индивидуалка <div
                                                        class="main-card__rating main-card__rating--mobile">
                                                        <div class="rating-static rating-static--four">
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                        </div>
                                                        <div class="main-card__voted">Проголосовало людей: 15</div>
                                                    </div>
                                                </div>
                                            </div><a class="main-card__address" href="#"><span
                                                    class="main-card__city">Москва&nbsp;</span><span class="main-card__street">м. Улица
															Народного ополчения</span></a>
                                        </div>
                                        <div class="main-card__block">
                                            <div class="main-card__labels">
                                                <div class="main-card__label main-card__label--vip"><img src="./images/vip-009ffddc.png"
                                                                                                         alt="icon vip"></div>
                                                <div class="main-card__label main-card__label--new"><img src="./images/new-1ad6a40b.png"
                                                                                                         alt="icon new"></div>
                                            </div>
                                            <div class="main-card__rating">
                                                <div class="rating-static rating-static--four">
                                                    <div class="rating-static__star"></div>
                                                    <div class="rating-static__star"></div>
                                                    <div class="rating-static__star"></div>
                                                    <div class="rating-static__star"></div>
                                                    <div class="rating-static__star"></div>
                                                </div>
                                                <div class="main-card__voted">Проголосовало людей: 15</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="main-card__body">
                                        <div class="main-card__content">
                                            <div class="main-card__slider">
                                                <div class="swiper-container js-main-card-slider">
                                                    <div class="swiper-wrapper">
                                                        <div class="swiper-slide">
                                                            <div class="swiper-slide__video">
                                                                <video class="js-main-card-video" poster="./images/img-1-db8e7456.jpg">
                                                                    <source src="videos/test.mp4" type="video/mp4">
                                                                </video>
                                                            </div>
                                                        </div>
                                                        <div class="swiper-slide"><img class="swiper-slide__img swiper-lazy"
                                                                                       data-src="./images/card-faf45814.jpg" alt="image">
                                                            <div class="swiper-lazy-preloader"></div>
                                                        </div>
                                                        <div class="swiper-slide"><img class="swiper-slide__img swiper-lazy"
                                                                                       data-src="./images/card-faf45814.jpg" alt="image">
                                                            <div class="swiper-lazy-preloader"></div>
                                                        </div>
                                                        <div class="swiper-slide"><img class="swiper-slide__img swiper-lazy"
                                                                                       data-src="./images/card-faf45814.jpg" alt="image">
                                                            <div class="swiper-lazy-preloader"></div>
                                                        </div>
                                                        <div class="swiper-slide"><img class="swiper-slide__img swiper-lazy"
                                                                                       data-src="./images/card-faf45814.jpg" alt="image">
                                                            <div class="swiper-lazy-preloader"></div>
                                                        </div>
                                                        <div class="swiper-slide">
                                                            <div class="swiper-slide__video">
                                                                <video class="js-main-card-video" poster="./images/img-1-db8e7456.jpg">
                                                                    <source src="videos/test.mp4" type="video/mp4">
                                                                </video>
                                                            </div>
                                                        </div>
                                                        <div class="swiper-slide"><img class="swiper-slide__img swiper-lazy"
                                                                                       data-src="./images/card-faf45814.jpg" alt="image">
                                                            <div class="swiper-lazy-preloader"></div>
                                                        </div>
                                                        <div class="swiper-slide"><img class="swiper-slide__img swiper-lazy"
                                                                                       data-src="./images/card-faf45814.jpg" alt="image">
                                                            <div class="swiper-lazy-preloader"></div>
                                                        </div>
                                                        <div class="swiper-slide"><img class="swiper-slide__img swiper-lazy"
                                                                                       data-src="./images/card-faf45814.jpg" alt="image">
                                                            <div class="swiper-lazy-preloader"></div>
                                                        </div>
                                                        <div class="swiper-slide"><img class="swiper-slide__img swiper-lazy"
                                                                                       data-src="./images/card-faf45814.jpg" alt="image">
                                                            <div class="swiper-lazy-preloader"></div>
                                                        </div>
                                                    </div>
                                                    <div class="swiper-pagination"></div>
                                                </div>
                                            </div>
                                            <div class="main-card__buttons">
                                                <div class="main-card__navigation">
                                                    <div class="swiper-button-prev"></div>
                                                    <div class="swiper-button-next"></div>
                                                </div>
                                                <div class="rating-score main-card__rating-score">
                                                    <div class="rating-score__title">Отзывы</div>
                                                    <div class="rating-score__items">
                                                        <div class="rating-score__item rating-score__item--like">7</div>
                                                        <div class="rating-score__item rating-score__item--dislike">3</div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="main-card__description">Эффектная красотка для состоятельных мужчин! Красивая,
                                                ухоженная, темпераментная девушка, без вредных привычек! ТОЛЬКО для состоятельных я красотка
                                                для состоятельных мужчин!</div>
                                        </div>
                                        <div class="main-card__aside">
                                            <div class="main-card__elems">
                                                <div class="main-card__elem">
                                                    <div class="main-card__classification">Индивидуалка</div>
                                                    <ul class="main-card__items">
                                                        <li class="main-card__item"><span class="main-card__legend">Возраст</span><span
                                                                class="main-card__value">20</span></li>
                                                        <li class="main-card__item"><span class="main-card__legend">Рост</span><span
                                                                class="main-card__value">164 см</span></li>
                                                        <li class="main-card__item"><span class="main-card__legend">Вес</span><span
                                                                class="main-card__value">54 кг</span></li>
                                                        <li class="main-card__item"><span class="main-card__legend">Бюст</span><span
                                                                class="main-card__value">2</span></li>
                                                    </ul>
                                                </div>
                                                <div class="main-card__elem">
                                                    <div class="main-card__title"> Апартаменты<span class="main-card__value">от 2 500 ₽ /
																	час</span></div>
                                                    <ul class="main-card__items">
                                                        <li class="main-card__item"><span class="main-card__legend">1 час</span><span
                                                                class="main-card__value">2 500 ₽</span></li>
                                                        <li class="main-card__item"><span class="main-card__legend">2 часа</span><span
                                                                class="main-card__value">5 000 ₽</span></li>
                                                        <li class="main-card__item"><span class="main-card__legend">Ночь</span><span
                                                                class="main-card__value">8 000 ₽</span></li>
                                                    </ul>
                                                </div>
                                                <div class="main-card__elem">
                                                    <div class="main-card__title"> Выезд<span class="main-card__value">от 2 500 ₽ / час</span>
                                                    </div>
                                                    <ul class="main-card__items">
                                                        <li class="main-card__item"><span class="main-card__legend">1 час</span><span
                                                                class="main-card__value">2 500 ₽</span></li>
                                                        <li class="main-card__item"><span class="main-card__legend">2 часа</span><span
                                                                class="main-card__value">5 000 ₽</span></li>
                                                        <li class="main-card__item"><span class="main-card__legend">Ночь</span><span
                                                                class="main-card__value">8 000 ₽</span></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="main-card__bottom">
                                                <div class="main-card__date">06 мая 2020 14:14</div>
                                                <div class="main-card__favorites"><svg class="add-favorites js-favorites"
                                                                                       xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24">
                                                        <path class="path-empty"
                                                              d="M6.28 3c3.236.001 4.973 3.491 5.72 5.031.75-1.547 2.469-5.021 5.726-5.021 2.058 0 4.274 1.309 4.274 4.182 0 3.442-4.744 7.851-10 13-5.258-5.151-10-9.559-10-13 0-2.676 1.965-4.193 4.28-4.192zm.001-2c-3.183 0-6.281 2.187-6.281 6.192 0 4.661 5.57 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-4.011-3.097-6.182-6.274-6.182-2.204 0-4.446 1.042-5.726 3.238-1.285-2.206-3.522-3.248-5.719-3.248z">
                                                        </path>
                                                        <path class="path-full" xmlns="http://www.w3.org/2000/svg"
                                                              d="M12 4.248c-3.148-5.402-12-3.825-12 2.944 0 4.661 5.571 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-6.792-8.875-8.306-12-2.944z">
                                                        </path>
                                                    </svg></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="favorites__card">
                                <div class="main-card main-card--new main-card--not-working">
                                    <div class="main-card__header">
                                        <div class="main-card__alert">Девушка временно не работает</div>
                                        <div class="main-card__info">
                                            <div class="main-card__personal"><a class="main-card__name" href="questionnaire.html"> Соня
                                                    <div class="main-card__labels main-card__labels--mobile">
                                                        <div class="main-card__label main-card__label--new"><img src="./images/new-1ad6a40b.png"
                                                                                                                 alt="icon new"></div>
                                                    </div></a>
                                                <div class="main-card__separator"></div>
                                                <div class="main-card__classification"> Индивидуалка <div
                                                        class="main-card__rating main-card__rating--mobile">
                                                        <div class="rating-static rating-static--four">
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                        </div>
                                                        <div class="main-card__voted">Проголосовало людей: 15</div>
                                                    </div>
                                                </div>
                                            </div><a class="main-card__address" href="#"><span
                                                    class="main-card__city">Москва&nbsp;</span><span class="main-card__street">м. Улица
															Народного ополчения</span></a>
                                        </div>
                                        <div class="main-card__block">
                                            <div class="main-card__labels">
                                                <div class="main-card__label main-card__label--vip"><img src="./images/vip-009ffddc.png"
                                                                                                         alt="icon vip"></div>
                                                <div class="main-card__label main-card__label--new"><img src="./images/new-1ad6a40b.png"
                                                                                                         alt="icon new"></div>
                                            </div>
                                            <div class="main-card__rating">
                                                <div class="rating-static rating-static--four">
                                                    <div class="rating-static__star"></div>
                                                    <div class="rating-static__star"></div>
                                                    <div class="rating-static__star"></div>
                                                    <div class="rating-static__star"></div>
                                                    <div class="rating-static__star"></div>
                                                </div>
                                                <div class="main-card__voted">Проголосовало людей: 15</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="main-card__body">
                                        <div class="main-card__content">
                                            <div class="main-card__slider">
                                                <div class="swiper-container js-main-card-slider">
                                                    <div class="swiper-wrapper">
                                                        <div class="swiper-slide">
                                                            <div class="swiper-slide__video">
                                                                <video class="js-main-card-video" poster="./images/img-1-db8e7456.jpg">
                                                                    <source src="videos/test.mp4" type="video/mp4">
                                                                </video>
                                                            </div>
                                                        </div>
                                                        <div class="swiper-slide"><img class="swiper-slide__img swiper-lazy"
                                                                                       data-src="./images/card-faf45814.jpg" alt="image">
                                                            <div class="swiper-lazy-preloader"></div>
                                                        </div>
                                                        <div class="swiper-slide"><img class="swiper-slide__img swiper-lazy"
                                                                                       data-src="./images/card-faf45814.jpg" alt="image">
                                                            <div class="swiper-lazy-preloader"></div>
                                                        </div>
                                                        <div class="swiper-slide"><img class="swiper-slide__img swiper-lazy"
                                                                                       data-src="./images/card-faf45814.jpg" alt="image">
                                                            <div class="swiper-lazy-preloader"></div>
                                                        </div>
                                                        <div class="swiper-slide"><img class="swiper-slide__img swiper-lazy"
                                                                                       data-src="./images/card-faf45814.jpg" alt="image">
                                                            <div class="swiper-lazy-preloader"></div>
                                                        </div>
                                                        <div class="swiper-slide">
                                                            <div class="swiper-slide__video">
                                                                <video class="js-main-card-video" poster="./images/img-1-db8e7456.jpg">
                                                                    <source src="videos/test.mp4" type="video/mp4">
                                                                </video>
                                                            </div>
                                                        </div>
                                                        <div class="swiper-slide"><img class="swiper-slide__img swiper-lazy"
                                                                                       data-src="./images/card-faf45814.jpg" alt="image">
                                                            <div class="swiper-lazy-preloader"></div>
                                                        </div>
                                                        <div class="swiper-slide"><img class="swiper-slide__img swiper-lazy"
                                                                                       data-src="./images/card-faf45814.jpg" alt="image">
                                                            <div class="swiper-lazy-preloader"></div>
                                                        </div>
                                                        <div class="swiper-slide"><img class="swiper-slide__img swiper-lazy"
                                                                                       data-src="./images/card-faf45814.jpg" alt="image">
                                                            <div class="swiper-lazy-preloader"></div>
                                                        </div>
                                                        <div class="swiper-slide"><img class="swiper-slide__img swiper-lazy"
                                                                                       data-src="./images/card-faf45814.jpg" alt="image">
                                                            <div class="swiper-lazy-preloader"></div>
                                                        </div>
                                                    </div>
                                                    <div class="swiper-pagination"></div>
                                                </div>
                                            </div>
                                            <div class="main-card__buttons">
                                                <div class="main-card__navigation">
                                                    <div class="swiper-button-prev"></div>
                                                    <div class="swiper-button-next"></div>
                                                </div>
                                                <div class="rating-score main-card__rating-score">
                                                    <div class="rating-score__title">Отзывы</div>
                                                    <div class="rating-score__items">
                                                        <div class="rating-score__item rating-score__item--like">7</div>
                                                        <div class="rating-score__item rating-score__item--dislike">3</div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="main-card__description">Эффектная красотка для состоятельных мужчин! Красивая,
                                                ухоженная, темпераментная девушка, без вредных привычек! ТОЛЬКО для состоятельных я красотка
                                                для состоятельных мужчин!</div>
                                        </div>
                                        <div class="main-card__aside">
                                            <div class="main-card__elems">
                                                <div class="main-card__elem">
                                                    <div class="main-card__classification">Индивидуалка</div>
                                                    <ul class="main-card__items">
                                                        <li class="main-card__item"><span class="main-card__legend">Возраст</span><span
                                                                class="main-card__value">20</span></li>
                                                        <li class="main-card__item"><span class="main-card__legend">Рост</span><span
                                                                class="main-card__value">164 см</span></li>
                                                        <li class="main-card__item"><span class="main-card__legend">Вес</span><span
                                                                class="main-card__value">54 кг</span></li>
                                                        <li class="main-card__item"><span class="main-card__legend">Бюст</span><span
                                                                class="main-card__value">2</span></li>
                                                    </ul>
                                                </div>
                                                <div class="main-card__elem">
                                                    <div class="main-card__title"> Апартаменты<span class="main-card__value">от 2 500 ₽ /
																	час</span></div>
                                                    <ul class="main-card__items">
                                                        <li class="main-card__item"><span class="main-card__legend">1 час</span><span
                                                                class="main-card__value">2 500 ₽</span></li>
                                                        <li class="main-card__item"><span class="main-card__legend">2 часа</span><span
                                                                class="main-card__value">5 000 ₽</span></li>
                                                        <li class="main-card__item"><span class="main-card__legend">Ночь</span><span
                                                                class="main-card__value">8 000 ₽</span></li>
                                                    </ul>
                                                </div>
                                                <div class="main-card__elem">
                                                    <div class="main-card__title"> Выезд<span class="main-card__value">от 2 500 ₽ / час</span>
                                                    </div>
                                                    <ul class="main-card__items">
                                                        <li class="main-card__item"><span class="main-card__legend">1 час</span><span
                                                                class="main-card__value">2 500 ₽</span></li>
                                                        <li class="main-card__item"><span class="main-card__legend">2 часа</span><span
                                                                class="main-card__value">5 000 ₽</span></li>
                                                        <li class="main-card__item"><span class="main-card__legend">Ночь</span><span
                                                                class="main-card__value">8 000 ₽</span></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="main-card__bottom">
                                                <div class="main-card__date">06 мая 2020 14:14</div>
                                                <div class="main-card__favorites"><svg class="add-favorites js-favorites"
                                                                                       xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24">
                                                        <path class="path-empty"
                                                              d="M6.28 3c3.236.001 4.973 3.491 5.72 5.031.75-1.547 2.469-5.021 5.726-5.021 2.058 0 4.274 1.309 4.274 4.182 0 3.442-4.744 7.851-10 13-5.258-5.151-10-9.559-10-13 0-2.676 1.965-4.193 4.28-4.192zm.001-2c-3.183 0-6.281 2.187-6.281 6.192 0 4.661 5.57 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-4.011-3.097-6.182-6.274-6.182-2.204 0-4.446 1.042-5.726 3.238-1.285-2.206-3.522-3.248-5.719-3.248z">
                                                        </path>
                                                        <path class="path-full" xmlns="http://www.w3.org/2000/svg"
                                                              d="M12 4.248c-3.148-5.402-12-3.825-12 2.944 0 4.661 5.571 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-6.792-8.875-8.306-12-2.944z">
                                                        </path>
                                                    </svg></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="template__pagination">
                            <div class="pagination">
                                <div class="pagination__wrap">
                                    <button class="pagination__button pagination__button--back pagination__button--disabled">
                                        <svg width="7" height="12" viewBox="0 0 7 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M6 1L1 6L6 11" stroke="#5C5C5C" stroke-width="1.8" stroke-linecap="round"
                                                  stroke-linejoin="round" />
                                        </svg> Назад </button>
                                    <ul class="pagination__list">
                                        <li class="pagination__item pagination__item--active"><a class="pagination__link" href="#">1</a>
                                        </li>
                                        <li class="pagination__item"><a class="pagination__link" href="#">2</a></li>
                                        <li class="pagination__item"><a class="pagination__link pagination__link--dot" href="#">...</a>
                                        </li>
                                    </ul>
                                    <button class="pagination__button"> Далее<svg width="7" height="12" viewBox="0 0 7 12" fill="none"
                                                                                  xmlns="http://www.w3.org/2000/svg">
                                            <path d="M1 11L6 6L1 1" stroke="#5C5C5C" stroke-width="1.8" stroke-linecap="round"
                                                  stroke-linejoin="round" />
                                        </svg>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('includes._bottom-banner', ['wrapperClass' => 'bottom-banner--transparent'])
@endsection
