@extends('layouts.cabinet')

@section('title')
    Мессенджер
@endsection

@section('content')
    @include('cabinet._header', ['title' => 'Мессенджер'])
    <div class="container">
        <div class="chat">
            <aside class="chat__aside" data-section="messages">
                <div class="chat__search">
                    <div class="chat__search-input">
                        <div class="input">
                            <input class="input__control" type="text" name="search" placeholder="Поиск"
                                   onblur="placeholder='Поиск'" onfocus="placeholder=''" autocomplete="on" />
                        </div>
                    </div>
                    <div class="chat__edit"><svg xmlns="http://www.w3.org/2000/svg" width="26" height="26" viewBox="0 0 26 26"
                                                 fill="none">
                            <g>
                                <path
                                    d="M20.5833 13.05C19.9842 13.05 19.5 13.5354 19.5 14.1332V22.8C19.5 23.3969 19.0146 23.8832 18.4167 23.8832H3.25C2.65193 23.8832 2.16673 23.3969 2.16673 22.8V7.63324C2.16673 7.03636 2.65193 6.54997 3.25 6.54997H11.9167C12.5158 6.54997 13 6.06458 13 5.46671C13 4.86864 12.5158 4.38324 11.9167 4.38324H3.25C1.45818 4.38324 0 5.84142 0 7.63324V22.8C0 24.5918 1.45818 26.05 3.25 26.05H18.4167C20.2086 26.05 21.6667 24.5918 21.6667 22.8V14.1332C21.6667 13.5342 21.1823 13.05 20.5833 13.05Z"
                                    fill="#FF9900" />
                                <path
                                    d="M10.1572 12.0631C10.0815 12.1389 10.0305 12.2353 10.0089 12.3393L9.24297 16.1701C9.20726 16.3476 9.2636 16.5307 9.39134 16.6596C9.49429 16.7626 9.63295 16.8177 9.77498 16.8177C9.80949 16.8177 9.8454 16.8146 9.8811 16.807L13.7107 16.0411C13.8168 16.0193 13.9133 15.9685 13.988 15.8926L22.5594 7.32124L18.7297 3.49182L10.1572 12.0631Z"
                                    fill="#FF9900" />
                                <path
                                    d="M25.2068 0.843059C24.1507 -0.21323 22.4325 -0.21323 21.3772 0.843059L19.8779 2.3423L23.7076 6.17192L25.2068 4.67248C25.7182 4.16229 25.9998 3.4819 25.9998 2.75827C25.9998 2.03463 25.7182 1.35424 25.2068 0.843059Z"
                                    fill="#FF9900" />
                            </g>
                            <defs>
                                <clipPath id="clip0">
                                    <rect width="26" height="26" fill="white" />
                                </clipPath>
                            </defs>
                        </svg>
                    </div>
                </div>
                <div class="chat__simplebar" data-simplebar>
                    <ul class="chat__items">
                        <div class="chat__item" data-type="chat">
                            <div class="chat__item-avatar"><img src="./images/avatar-chat-ab0a4860.png" alt="photo">
                                <div class="chat__item-online"></div>
                            </div>
                            <div class="chat__item-body">
                                <div class="chat__item-info">
                                    <div class="chat__item-name">Имя пользователя</div>
                                    <div class="chat__item-date">вчера</div>
                                </div>
                                <div class="chat__item-content">
                                    <div class="chat__item-text">Гостиница находится по улице Тихвинская 20</div>
                                </div>
                            </div>
                        </div>
                        <div class="chat__item" data-type="chat">
                            <div class="chat__item-avatar"><img src="./images/avatar-chat-ab0a4860.png" alt="photo">
                            </div>
                            <div class="chat__item-body">
                                <div class="chat__item-info">
                                    <div class="chat__item-name">Имя пользователя</div>
                                    <div class="chat__item-date">04.11</div>
                                </div>
                                <div class="chat__item-content">
                                    <div class="chat__item-text">Гостиница находится по улице Тихвинская 20</div>
                                </div>
                            </div>
                        </div>
                        <div class="chat__item chat__item--unread" data-type="chat">
                            <div class="chat__item-avatar"><img src="./images/avatar-chat-ab0a4860.png" alt="photo">
                            </div>
                            <div class="chat__item-body">
                                <div class="chat__item-info">
                                    <div class="chat__item-name">Имя пользователя</div>
                                    <div class="chat__item-date">03.11</div>
                                </div>
                                <div class="chat__item-content">
                                    <div class="chat__item-text">Гостиница находится по улице Тихвинская 20</div><span
                                        class="chat__unread-value">13</span>
                                </div>
                            </div>
                        </div>
                        <div class="chat__item" data-type="chat">
                            <div class="chat__item-avatar"><img src="./images/avatar-chat-ab0a4860.png" alt="photo">
                            </div>
                            <div class="chat__item-body">
                                <div class="chat__item-info">
                                    <div class="chat__item-name">Имя пользователя</div>
                                    <div class="chat__item-date">02.11</div>
                                </div>
                                <div class="chat__item-content">
                                    <div class="chat__item-text">Гостиница находится по улице Тихвинская 20</div>
                                </div>
                            </div>
                        </div>
                        <div class="chat__item" data-type="chat">
                            <div class="chat__item-avatar"><img src="./images/avatar-chat-ab0a4860.png" alt="photo">
                            </div>
                            <div class="chat__item-body">
                                <div class="chat__item-info">
                                    <div class="chat__item-name">Имя пользователя</div>
                                    <div class="chat__item-date">26.09</div>
                                </div>
                                <div class="chat__item-content">
                                    <div class="chat__item-text">Гостиница находится по улице Тихвинская 20</div>
                                </div>
                            </div>
                        </div>
                        <div class="chat__item" data-type="chat">
                            <div class="chat__item-avatar"><img src="./images/avatar-chat-ab0a4860.png" alt="photo">
                            </div>
                            <div class="chat__item-body">
                                <div class="chat__item-info">
                                    <div class="chat__item-name">Имя пользователя</div>
                                    <div class="chat__item-date">13.08</div>
                                </div>
                                <div class="chat__item-content">
                                    <div class="chat__item-text">Гостиница находится по улице Тихвинская 20</div>
                                </div>
                            </div>
                        </div>
                        <div class="chat__item" data-type="chat">
                            <div class="chat__item-avatar"><img src="./images/avatar-chat-ab0a4860.png" alt="photo">
                            </div>
                            <div class="chat__item-body">
                                <div class="chat__item-info">
                                    <div class="chat__item-name">Имя пользователя</div>
                                    <div class="chat__item-date">6.08</div>
                                </div>
                                <div class="chat__item-content">
                                    <div class="chat__item-text">Гостиница находится по улице Тихвинская 20</div>
                                </div>
                            </div>
                        </div>
                        <div class="chat__item" data-type="chat">
                            <div class="chat__item-avatar"><img src="./images/avatar-chat-ab0a4860.png" alt="photo">
                            </div>
                            <div class="chat__item-body">
                                <div class="chat__item-info">
                                    <div class="chat__item-name">Имя пользователя</div>
                                    <div class="chat__item-date">3.08</div>
                                </div>
                                <div class="chat__item-content">
                                    <div class="chat__item-text">Гостиница находится по улице Тихвинская 20</div>
                                </div>
                            </div>
                        </div>
                        <div class="chat__item" data-type="chat">
                            <div class="chat__item-avatar"><img src="./images/avatar-chat-ab0a4860.png" alt="photo">
                            </div>
                            <div class="chat__item-body">
                                <div class="chat__item-info">
                                    <div class="chat__item-name">Имя пользователя</div>
                                    <div class="chat__item-date">3.08</div>
                                </div>
                                <div class="chat__item-content">
                                    <div class="chat__item-text">Гостиница находится по улице Тихвинская 20</div>
                                </div>
                            </div>
                        </div>
                        <div class="chat__item" data-type="chat">
                            <div class="chat__item-avatar"><img src="./images/avatar-chat-ab0a4860.png" alt="photo">
                            </div>
                            <div class="chat__item-body">
                                <div class="chat__item-info">
                                    <div class="chat__item-name">Имя пользователя</div>
                                    <div class="chat__item-date">2.08</div>
                                </div>
                                <div class="chat__item-content">
                                    <div class="chat__item-text">Гостиница находится по улице Тихвинская 20</div>
                                </div>
                            </div>
                        </div>
                        <div class="chat__item" data-type="chat">
                            <div class="chat__item-avatar"><img src="./images/avatar-chat-ab0a4860.png" alt="photo">
                            </div>
                            <div class="chat__item-body">
                                <div class="chat__item-info">
                                    <div class="chat__item-name">Имя пользователя</div>
                                    <div class="chat__item-date">02.11</div>
                                </div>
                                <div class="chat__item-content">
                                    <div class="chat__item-text">Гостиница находится по улице Тихвинская 20</div>
                                </div>
                            </div>
                        </div>
                        <div class="chat__item" data-type="chat">
                            <div class="chat__item-avatar"><img src="./images/avatar-chat-ab0a4860.png" alt="photo">
                            </div>
                            <div class="chat__item-body">
                                <div class="chat__item-info">
                                    <div class="chat__item-name">Имя пользователя</div>
                                    <div class="chat__item-date">26.09</div>
                                </div>
                                <div class="chat__item-content">
                                    <div class="chat__item-text">Гостиница находится по улице Тихвинская 20</div>
                                </div>
                            </div>
                        </div>
                        <div class="chat__item" data-type="chat">
                            <div class="chat__item-avatar"><img src="./images/avatar-chat-ab0a4860.png" alt="photo">
                            </div>
                            <div class="chat__item-body">
                                <div class="chat__item-info">
                                    <div class="chat__item-name">Имя пользователя</div>
                                    <div class="chat__item-date">13.08</div>
                                </div>
                                <div class="chat__item-content">
                                    <div class="chat__item-text">Гостиница находится по улице Тихвинская 20</div>
                                </div>
                            </div>
                        </div>
                    </ul>
                </div>
            </aside>
            <div class="chat__main" data-section="dialog">
                <div class="chat__blank">
                    <div class="chat__blank-icon"><svg xmlns="http://www.w3.org/2000/svg" width="66" height="66"
                                                       viewBox="0 0 66 66" fill="none">
                            <g clip-path="url(#clip0)">
                                <path
                                    d="M57.3066 24.6393H26.627C26.093 24.6393 25.6602 25.0721 25.6602 25.6061C25.6602 26.14 26.093 26.5729 26.627 26.5729H57.3066C57.8406 26.5729 58.2734 26.14 58.2734 25.6061C58.2734 25.0721 57.8406 24.6393 57.3066 24.6393Z"
                                    fill="#5C5C5C" />
                                <path
                                    d="M57.3066 32.0299H26.627C26.093 32.0299 25.6602 32.4628 25.6602 32.9967C25.6602 33.5306 26.093 33.9635 26.627 33.9635H57.3066C57.8406 33.9635 58.2734 33.5306 58.2734 32.9967C58.2734 32.4628 57.8406 32.0299 57.3066 32.0299Z"
                                    fill="#5C5C5C" />
                                <path
                                    d="M57.3066 39.4205H26.627C26.093 39.4205 25.6602 39.8533 25.6602 40.3873C25.6602 40.9212 26.093 41.3541 26.627 41.3541H57.3066C57.8406 41.3541 58.2734 40.9212 58.2734 40.3873C58.2734 39.8533 57.8406 39.4205 57.3066 39.4205Z"
                                    fill="#5C5C5C" />
                                <path
                                    d="M60.3763 16.1841H23.5548C20.4542 16.1841 17.9316 18.7066 17.9316 21.8073V44.961C17.9316 48.0616 20.4541 50.5842 23.5548 50.5842H49.3632C49.6392 50.5842 49.8956 50.7083 50.0668 50.925L58.4012 61.4764C58.7297 61.8922 59.209 62.1189 59.7108 62.1189C59.8973 62.1189 60.0871 62.0875 60.2732 62.0228C60.9599 61.7843 61.4036 61.1606 61.4036 60.4337V50.4882C64.0107 50.0012 65.9995 47.6962 65.9995 44.961V21.8072C65.9995 18.7066 63.4771 16.1841 60.3763 16.1841ZM64.0659 44.961C64.0659 46.969 62.4323 48.6235 60.4243 48.6491C59.8953 48.6559 59.4699 49.0867 59.4699 49.6158V59.7099L51.5842 49.7265C51.0442 49.0426 50.2346 48.6505 49.3632 48.6505H23.5548C21.5204 48.6505 19.8652 46.9953 19.8652 44.9609V21.8072C19.8652 19.7728 21.5203 18.1176 23.5548 18.1176H60.3763C62.4107 18.1176 64.0659 19.7728 64.0659 21.8072V44.961Z"
                                    fill="#5C5C5C" />
                                <path
                                    d="M14.2434 32.17C13.5821 32.2639 12.9899 32.6043 12.5758 33.1284L5.93446 41.5365V33.1097C5.93446 32.5806 5.50907 32.1498 4.98004 32.1429C3.30026 32.1215 1.93359 30.7375 1.93359 29.0576V8.90166C1.93359 7.19971 3.3183 5.815 5.02025 5.815H37.0745C38.7764 5.815 40.1611 7.19971 40.1611 8.90166V12.6312C40.1611 13.1651 40.594 13.598 41.1279 13.598C41.6619 13.598 42.0947 13.1651 42.0947 12.6312V8.90166C42.0947 6.13353 39.8426 3.88141 37.0745 3.88141H5.02025C2.25212 3.88141 0 6.13353 0 8.90166V29.0576C0 31.4639 1.72361 33.4968 4.00099 33.972V42.527C4.00099 43.2138 4.4202 43.8033 5.06911 44.0286C5.24507 44.0897 5.42425 44.1194 5.60059 44.1194C6.07458 44.1194 6.52768 43.9051 6.83796 43.5123L14.0933 34.327C14.198 34.1944 14.3479 34.1082 14.5152 34.0844C15.0439 34.0094 15.4116 33.5199 15.3365 32.9913C15.2615 32.4626 14.7728 32.0946 14.2434 32.17Z"
                                    fill="#5C5C5C" />
                            </g>
                            <defs>
                                <clipPath id="clip0">
                                    <rect width="66" height="66" fill="white" />
                                </clipPath>
                            </defs>
                        </svg>
                    </div>
                    <div class="chat__blank-text"> Выберите чат<br>или <a class="chat__blank-link" href="#">создайте новую
                            беседу</a></div>
                </div>
                <div class="chat__header">
                    <div class="chat__info" data-section="info">
                        <div class="chat__back js-back-to-messages"><svg xmlns="http://www.w3.org/2000/svg" width="16"
                                                                         height="8" viewBox="0 0 16 8" fill="none">
                                <path
                                    d="M0.183312 4.44241C0.1835 4.4426 0.183656 4.44282 0.183875 4.44301L3.44962 7.693C3.69428 7.93647 4.09 7.93557 4.33353 7.69088C4.57703 7.44622 4.57609 7.0505 4.33144 6.807L2.13881 4.62501L15.375 4.62501C15.7202 4.62501 16 4.34519 16 4.00001C16 3.65482 15.7202 3.37501 15.375 3.37501L2.13884 3.37501L4.33141 1.19301C4.57606 0.949513 4.577 0.553795 4.3335 0.309139C4.08997 0.0644208 3.69422 0.0635773 3.44959 0.307015L0.183843 3.55701C0.183655 3.5572 0.183499 3.55742 0.183281 3.5576C-0.0615009 3.80192 -0.0607189 4.19891 0.183312 4.44241Z"
                                    fill="#FF9900" />
                            </svg>
                        </div>
                        <div class="chat__preview"><img src="./images/img-1-db8e7456.jpg" alt=""></div>
                        <div class="chat__user">
                            <div class="chat__user-info">
                                <div class="chat__user-avatar"><img src="./images/user-icon-d74ee624.jpg" alt="icon"></div>
                                <div class="chat__user-name">Имя пользователя</div>
                                <div class="chat__user-date">Был(а) вчера в 14:11</div>
                            </div>
                            <div class="chat__user-details">
                                <div class="details"><span class="details__text">Название анкеты</span><span
                                        class="details__text">Индивидуалка</span><span class="details__text">Москва</span></div>
                            </div>
                        </div>
                    </div>
                    <div class="chat__messages-selected" data-section="selected-messages"><span class="value">2</span>&nbsp;
                        сообщения<span class="delete js-delete-selected"></span></div>
                    <div class="chat__actions" data-section="actions">
                        <div class="chat-actions">
                            <div class="chat-actions__button js-show-actions"></div>
                            <ul class="chat-actions__list">
                                <li class="chat-actions__item"><a class="chat-actions__link" href="#"><span
                                            class="chat-actions__icon">
													<svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewbox="0 0 14 14"
                                                         fill="none">
														<path
                                                            d="M13.2731 5.09586L6.15814 12.1319C4.97923 13.2978 3.06766 13.2978 1.88875 12.1319C0.709843 10.966 0.709843 9.07589 1.88875 7.90996L8.29259 1.57762C9.0787 0.800493 10.3526 0.800493 11.1387 1.57762C11.9248 2.35472 11.9248 3.61512 11.1387 4.39222L4.73484 10.7246C4.34204 11.1134 3.70485 11.1134 3.31155 10.7246C2.91874 10.3363 2.91874 9.70611 3.31155 9.31728L9.00375 3.68856L8.29209 2.98492L2.60041 8.61412C1.8143 9.39125 1.8143 10.6516 2.60041 11.4287C3.38652 12.2059 4.66039 12.2059 5.4465 11.4287L11.8504 5.09636C13.0293 3.93046 13.0293 2.04039 11.8504 0.874461C10.6714 -0.291446 8.75988 -0.291446 7.58098 0.874461L0.821286 7.55863L0.845782 7.5831C-0.382119 9.14433 -0.274173 11.4012 1.17712 12.836C2.6284 14.2708 4.90976 14.3782 6.48897 13.1633L6.51346 13.1878L13.9843 5.8L13.2731 5.09586Z"
                                                            fill="#5C5C5C"></path>
													</svg></span><span class="chat-actions__name">Показать вложения</span></a></li>
                                <li class="chat-actions__item"><a class="chat-actions__link" href="#"><span
                                            class="chat-actions__icon">
													<svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewbox="0 0 14 14"
                                                         fill="none">
														<path
                                                            d="M7.00002 0.349945C3.14022 0.349945 1.88128e-05 3.17608 1.88128e-05 6.64995C1.88128e-05 7.86421 0.384085 9.04045 1.11255 10.0575C0.974652 11.5828 0.605052 12.7152 0.0683855 13.2516C-0.00254785 13.3226 -0.0202812 13.4311 0.0245188 13.5207C0.0641855 13.6007 0.145852 13.6499 0.233352 13.6499C0.244085 13.6499 0.254819 13.6492 0.265785 13.6476C0.360285 13.6343 2.55549 13.3184 4.14332 12.4018C5.04492 12.7656 6.00532 12.9499 7.00002 12.9499C10.8598 12.9499 14 10.1238 14 6.64995C14 3.17608 10.8598 0.349945 7.00002 0.349945ZM3.73335 7.58328C3.21862 7.58328 2.80002 7.16468 2.80002 6.64995C2.80002 6.13521 3.21862 5.71661 3.73335 5.71661C4.24809 5.71661 4.66669 6.13521 4.66669 6.64995C4.66669 7.16468 4.24809 7.58328 3.73335 7.58328ZM7.00002 7.58328C6.48528 7.58328 6.06669 7.16468 6.06669 6.64995C6.06669 6.13521 6.48528 5.71661 7.00002 5.71661C7.51475 5.71661 7.93335 6.13521 7.93335 6.64995C7.93335 7.16468 7.51475 7.58328 7.00002 7.58328ZM10.2667 7.58328C9.75195 7.58328 9.33335 7.16468 9.33335 6.64995C9.33335 6.13521 9.75195 5.71661 10.2667 5.71661C10.7814 5.71661 11.2 6.13521 11.2 6.64995C11.2 7.16468 10.7814 7.58328 10.2667 7.58328Z"
                                                            fill="#5C5C5C"></path>
													</svg></span><span class="chat-actions__name">Пожаловаться</span></a></li>
                                <li class="chat-actions__item"><a class="chat-actions__link" href="#"><span
                                            class="chat-actions__icon">
													<svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewbox="0 0 14 14"
                                                         fill="none">
														<path
                                                            d="M7 -6.10352e-05C3.134 -6.10352e-05 0 3.13394 0 6.99994C0 10.8659 3.134 13.9999 7 13.9999C10.866 13.9999 14 10.8659 14 6.99994C14 3.13394 10.866 -6.10352e-05 7 -6.10352e-05ZM4.24753 2.82741C5.06425 2.28816 6.02131 2.00044 7 1.99994C7.98009 1.99891 8.93859 2.28769 9.755 2.82994L2.83 9.75494C1.30847 7.4505 1.94309 4.34897 4.24753 2.82741ZM9.75247 11.1725C8.93575 11.7117 7.97872 11.9994 7 11.9999C6.01991 12.001 5.06141 11.7122 4.245 11.1699L11.17 4.24494C12.6915 6.54935 12.0569 9.65091 9.75247 11.1725Z"
                                                            fill="#5C5C5C"></path>
													</svg></span><span class="chat-actions__name">Заблокировать</span></a></li>
                                <li class="chat-actions__item"><a class="chat-actions__link" href="#"><span
                                            class="chat-actions__icon">
													<svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewbox="0 0 14 14"
                                                         fill="none">
														<g>
															<path
                                                                d="M11.5938 1.74994H9.1875V1.31244C9.1875 0.587556 8.59988 -6.10352e-05 7.875 -6.10352e-05H6.125C5.40012 -6.10352e-05 4.8125 0.587556 4.8125 1.31244V1.74994H2.40625C1.8022 1.74994 1.3125 2.23964 1.3125 2.84369V3.71869C1.3125 3.96033 1.50836 4.15619 1.75 4.15619H12.25C12.4916 4.15619 12.6875 3.96033 12.6875 3.71869V2.84369C12.6875 2.23964 12.1978 1.74994 11.5938 1.74994ZM5.6875 1.31244C5.6875 1.07127 5.88383 0.874939 6.125 0.874939H7.875C8.11617 0.874939 8.3125 1.07127 8.3125 1.31244V1.74994H5.6875V1.31244Z"
                                                                fill="#5C5C5C"></path>
															<path
                                                                d="M2.14453 5.03119C2.06646 5.03119 2.00425 5.09643 2.00797 5.17442L2.36891 12.7498C2.40227 13.4509 2.97813 13.9999 3.67977 13.9999H10.3238C11.0254 13.9999 11.6013 13.4509 11.6346 12.7498L11.9955 5.17442C11.9993 5.09643 11.9371 5.03119 11.859 5.03119H2.14453ZM8.75176 6.12494C8.75176 5.88322 8.94754 5.68744 9.18926 5.68744C9.43098 5.68744 9.62676 5.88322 9.62676 6.12494V11.8124C9.62676 12.0542 9.43098 12.2499 9.18926 12.2499C8.94754 12.2499 8.75176 12.0542 8.75176 11.8124V6.12494ZM6.56426 6.12494C6.56426 5.88322 6.76004 5.68744 7.00176 5.68744C7.24348 5.68744 7.43926 5.88322 7.43926 6.12494V11.8124C7.43926 12.0542 7.24348 12.2499 7.00176 12.2499C6.76004 12.2499 6.56426 12.0542 6.56426 11.8124V6.12494ZM4.37676 6.12494C4.37676 5.88322 4.57254 5.68744 4.81426 5.68744C5.05598 5.68744 5.25176 5.88322 5.25176 6.12494V11.8124C5.25176 12.0542 5.05598 12.2499 4.81426 12.2499C4.57254 12.2499 4.37676 12.0542 4.37676 11.8124V6.12494Z"
                                                                fill="#5C5C5C"></path>
														</g>
														<defs>
															<clippath id="clip0">
																<rect width="14" height="14" fill="white"></rect>
															</clippath>
														</defs>
													</svg></span><span class="chat-actions__name">Очистить историю сообщений</span></a></li>
                                <li class="chat-actions__item"><a class="chat-actions__link" href="#"><span
                                            class="chat-actions__icon">
													<svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewbox="0 0 14 14"
                                                         fill="none">
														<path
                                                            d="M11.9498 2.05013C10.6276 0.728012 8.86974 -0.00012207 7 -0.00012207C5.13026 -0.00012207 3.37236 0.728012 2.05025 2.05013C0.728134 3.37224 0 5.13014 0 6.99988C0 8.86972 0.728134 10.6275 2.05025 11.9496C3.37236 13.2717 5.13026 13.9999 7 13.9999C8.86974 13.9999 10.6276 13.2717 11.9498 11.9496C13.2719 10.6275 14 8.86972 14 6.99988C14 5.13014 13.2719 3.37224 11.9498 2.05013ZM10.0277 9.0607C10.2946 9.32773 10.2946 9.76053 10.0277 10.0275C9.89417 10.161 9.71921 10.2277 9.54425 10.2277C9.36929 10.2277 9.19434 10.161 9.06082 10.0275L7 7.96663L4.93918 10.0276C4.80566 10.161 4.63071 10.2277 4.45575 10.2277C4.28079 10.2277 4.10583 10.161 3.97232 10.0276C3.7054 9.76053 3.7054 9.32773 3.97232 9.06081L6.03325 6.99988L3.97232 4.93906C3.7054 4.67203 3.7054 4.23923 3.97232 3.97231C4.23935 3.70528 4.67215 3.70528 4.93907 3.97231L7 6.03313L9.06082 3.97231C9.32785 3.70538 9.76065 3.70528 10.0276 3.97231C10.2946 4.23923 10.2946 4.67203 10.0276 4.93906L7.96675 6.99988L10.0277 9.0607Z"
                                                            fill="#5C5C5C"></path>
													</svg></span><span class="chat-actions__name">Удалить диалог</span></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="chat__controls" data-section="controls">
                        <ul class="chat-controls">
                            <li class="chat-controls__item" data-tippy-content="Пометить как важное" data-controls><svg
                                    xmlns="http://www.w3.org/2000/svg" width="16" height="18" viewbox="0 0 16 18" fill="none">
                                    <path
                                        d="M13.5723 6.10352e-05H2.42773C1.11922 6.10352e-05 0.0546875 1.06459 0.0546875 2.37311V18.0001L6.1543 14.9503L12.2539 18.0001V9.5274H15.9453V2.37311C15.9453 1.06459 14.8808 6.10352e-05 13.5723 6.10352e-05ZM11.1992 2.37311V16.2935L6.1543 13.7711L1.10938 16.2935V2.37311C1.10938 1.64615 1.70077 1.05475 2.42773 1.05475H11.6C11.347 1.43205 11.1992 1.8857 11.1992 2.37311ZM14.8906 8.47272H12.2539V2.37311C12.2539 1.64615 12.8453 1.05475 13.5723 1.05475C14.2992 1.05475 14.8906 1.64615 14.8906 2.37311V8.47272Z"
                                        fill="#5C5C5C"></path>
                                </svg>
                            </li>
                            <li class="chat-controls__item" data-tippy-content="Пометить как важное" data-controls><svg
                                    xmlns="http://www.w3.org/2000/svg" width="18" height="16" viewbox="0 0 18 16" fill="none">
                                    <path
                                        d="M16.2131 2.40617H15.3018L14.8578 1.16183C14.8108 1.03014 14.7134 0.92247 14.5871 0.862457C14.4606 0.802582 14.3156 0.795303 14.1839 0.84227L9.80063 2.40617H5.75917V1.86757C5.75917 1.06172 5.10287 0.40625 4.29607 0.40625H1.4631C0.656433 0.40625 0 1.06172 0 1.86757V13.8122C0 14.7961 0.801727 15.5968 1.78706 15.5968H16.2131C17.1983 15.5968 18 14.7961 18 13.8122V4.19063C18 3.20667 17.1983 2.40617 16.2131 2.40617ZM14.0416 2.01286L15.1977 5.25328H4.95909L14.0416 2.01286ZM1.05469 1.86757C1.05469 1.64331 1.23788 1.46094 1.4631 1.46094H4.29607C4.52129 1.46094 4.70448 1.64331 4.70448 1.86757V2.93352C4.70448 3.22479 4.94055 3.46086 5.23183 3.46086H6.84448L1.82043 5.25328H1.05469V1.86757ZM16.9453 13.8122C16.9453 14.2147 16.6168 14.5421 16.2131 14.5421H1.78706C1.38318 14.5421 1.05469 14.2147 1.05469 13.8122V6.30797H16.9453V13.8122ZM16.9453 5.25328H16.3176L15.678 3.46086H16.2131C16.6168 3.46086 16.9453 3.78825 16.9453 4.19063V5.25328Z"
                                        fill="#5C5C5C"></path>
                                </svg>
                            </li>
                            <li class="chat-controls__item" data-tippy-content="Редактировать" data-controls><svg
                                    xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewbox="0 0 18 18" fill="none">
                                    <path
                                        d="M17.4397 2.19031L15.8086 0.559066C15.0633 -0.18621 13.8508 -0.186175 13.1055 0.559066C12.4038 1.26082 1.68623 11.9792 0.969991 12.6955C0.893702 12.7718 0.842585 12.8732 0.824374 12.9723L0.00882121 17.3767C-0.0227842 17.5474 0.0316376 17.7228 0.154438 17.8456C0.277379 17.9686 0.452809 18.0229 0.623351 17.9913L5.02733 17.1756C5.12911 17.1567 5.22916 17.105 5.30419 17.03L17.4397 4.89358C18.1867 4.14658 18.1868 2.93742 17.4397 2.19031ZM1.1855 16.8145L1.67885 14.1502L3.8496 16.3211L1.1855 16.8145ZM4.93132 15.9113L2.08863 13.0684L12.4671 2.68914L15.3098 5.53205L4.93132 15.9113ZM16.6939 4.14781L16.0555 4.78628L13.2128 1.94338L13.8512 1.3049C14.1852 0.97085 14.7287 0.970815 15.0628 1.3049L16.6939 2.93615C17.0288 3.27101 17.0288 3.81291 16.6939 4.14781Z"
                                        fill="#5C5C5C"></path>
                                </svg>
                            </li>
                            <li class="chat-controls__item" data-tippy-content="Удалить" data-controls><svg
                                    xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewbox="0 0 18 18" fill="none">
                                    <g clip-path="url(#clip0)">
                                        <path
                                            d="M16.3081 4.02333L15.9107 2.832C15.7594 2.37827 15.3362 2.07339 14.8581 2.07339H11.5178V0.985886C11.5178 0.442337 11.0759 0 10.5325 0H7.47361C6.93033 0 6.48827 0.442337 6.48827 0.985886V2.07339H3.14815C2.66983 2.07339 2.24672 2.37827 2.09539 2.832L1.69796 4.02333C1.60746 4.29456 1.65332 4.59489 1.82045 4.82684C1.98758 5.05879 2.25798 5.19736 2.5439 5.19736H2.95932L3.87366 16.5038C3.94164 17.3427 4.65355 18 5.49469 18H12.6969C13.5379 18 14.25 17.3427 14.3178 16.5037L15.2322 5.19736H15.4622C15.7481 5.19736 16.0185 5.05879 16.1856 4.82698C16.3528 4.59503 16.3986 4.29456 16.3081 4.02333ZM7.54296 1.05469H10.4631V2.07339H7.54296V1.05469ZM13.2666 16.4187C13.2427 16.7141 12.9925 16.9453 12.6969 16.9453H5.49469C5.19916 16.9453 4.94895 16.7141 4.92505 16.4187L4.01744 5.19736H14.174L13.2666 16.4187ZM2.77008 4.14267L3.09597 3.16571C3.10338 3.14319 3.12439 3.12808 3.14815 3.12808H14.8581C14.8818 3.12808 14.9027 3.14319 14.9103 3.16571L15.2361 4.14267H2.77008Z"
                                            fill="#5C5C5C"></path>
                                        <path
                                            d="M11.5839 16.3813C11.5932 16.3818 11.6024 16.382 11.6118 16.382C11.8904 16.382 12.1233 16.1636 12.1379 15.8821L12.6331 6.3759C12.6482 6.08503 12.4246 5.83688 12.1339 5.82177C11.8424 5.80626 11.595 6.0301 11.5798 6.32097L11.0847 15.8272C11.0696 16.118 11.293 16.3662 11.5839 16.3813Z"
                                            fill="#5C5C5C"></path>
                                        <path
                                            d="M5.89239 15.8833C5.90777 16.1644 6.14041 16.382 6.4185 16.382C6.42811 16.382 6.438 16.3817 6.44775 16.3811C6.73847 16.3653 6.96136 16.1168 6.94556 15.8259L6.42687 6.31973C6.41108 6.02886 6.16251 5.80598 5.87165 5.82191C5.58093 5.8377 5.35804 6.08627 5.37384 6.37713L5.89239 15.8833Z"
                                            fill="#5C5C5C"></path>
                                        <path
                                            d="M9.00781 16.382C9.29909 16.382 9.53516 16.1459 9.53516 15.8546V6.34845C9.53516 6.05717 9.29909 5.82111 9.00781 5.82111C8.71654 5.82111 8.48047 6.05717 8.48047 6.34845V15.8546C8.48047 16.1459 8.71654 16.382 9.00781 16.382Z"
                                            fill="#5C5C5C"></path>
                                    </g>
                                    <defs>
                                        <clippath id="clip0">
                                            <rect width="18" height="18" fill="white"></rect>
                                        </clippath>
                                    </defs>
                                </svg>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="chat__body" data-simplebar>
                    <div class="chat__content">
                        <div class="chat__date">вчера</div>
                        <div class="message-enter message-enter--read">
                            <div class="message-enter__check" data-section="check"><svg width="16" height="16" viewBox="0 0 16 16"
                                                                                        fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <circle cx="8" cy="7.99994" r="8" fill="#FF9900" />
                                    <g clip-path="url(#clip0)">
                                        <path
                                            d="M7.06334 10.9886C6.98698 11.0654 6.88279 11.1082 6.77456 11.1082C6.66634 11.1082 6.56215 11.0654 6.48578 10.9886L4.17951 8.68192C3.94016 8.44258 3.94016 8.05447 4.17951 7.81558L4.46829 7.52672C4.7077 7.28738 5.09536 7.28738 5.3347 7.52672L6.77456 8.96666L10.6653 5.07587C10.9047 4.83653 11.2927 4.83653 11.5317 5.07587L11.8205 5.36472C12.0598 5.60406 12.0598 5.9921 11.8205 6.23106L7.06334 10.9886Z"
                                            fill="#171717" />
                                    </g>
                                    <defs>
                                        <clipPath id="clip0">
                                            <rect width="8" height="8" fill="white" transform="translate(4 3.99994)" />
                                        </clipPath>
                                    </defs>
                                </svg>
                            </div>
                            <div class="message-enter__block">
                                <div class="message-enter__text">Здравствуйте! Удобно будет завтра встретиться в районе 19:00,
                                    недалеко от центра?</div>
                                <div class="message-enter__wrap">
                                    <div class="message-enter__time">17:13</div>
                                    <div class="message-enter__status"><svg width="18" height="10" viewBox="0 0 18 10" fill="none"
                                                                            xmlns="http://www.w3.org/2000/svg">
                                            <path
                                                d="M1.75 5.34031L5.45459 9L12.5681 1.99304L11.5468 1L5.45459 6.99996L2.75712 4.34028L1.75 5.34031Z"
                                                fill="#5C5C5C" />
                                            <path
                                                d="M7.8125 7.65862L9.13574 9L16.2492 1.99304L15.228 1L9.13574 6.99996L8.79403 6.66307L7.8125 7.65862Z"
                                                fill="#5C5C5C" />
                                        </svg>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="message-out">
                            <div class="message-out__check" data-section="check"><svg width="16" height="16" viewBox="0 0 16 16"
                                                                                      fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <circle cx="8" cy="7.99994" r="8" fill="#FF9900" />
                                    <g clip-path="url(#clip0)">
                                        <path
                                            d="M7.06334 10.9886C6.98698 11.0654 6.88279 11.1082 6.77456 11.1082C6.66634 11.1082 6.56215 11.0654 6.48578 10.9886L4.17951 8.68192C3.94016 8.44258 3.94016 8.05447 4.17951 7.81558L4.46829 7.52672C4.7077 7.28738 5.09536 7.28738 5.3347 7.52672L6.77456 8.96666L10.6653 5.07587C10.9047 4.83653 11.2927 4.83653 11.5317 5.07587L11.8205 5.36472C12.0598 5.60406 12.0598 5.9921 11.8205 6.23106L7.06334 10.9886Z"
                                            fill="#171717" />
                                    </g>
                                    <defs>
                                        <clipPath id="clip0">
                                            <rect width="8" height="8" fill="white" transform="translate(4 3.99994)" />
                                        </clipPath>
                                    </defs>
                                </svg>
                            </div>
                            <div class="message-out__block">
                                <div class="message-out__img"><img src="./images/img-1-db8e7456.jpg" alt=""></div>
                                <div class="message-out__info">
                                    <div class="message-out__text">Доброй ночи, да, удобно.</div>
                                    <div class="message-out__time">00:18</div>
                                </div>
                            </div>
                        </div>
                        <div class="message-enter message-enter--read">
                            <div class="message-enter__check" data-section="check"><svg width="16" height="16" viewBox="0 0 16 16"
                                                                                        fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <circle cx="8" cy="7.99994" r="8" fill="#FF9900" />
                                    <g clip-path="url(#clip0)">
                                        <path
                                            d="M7.06334 10.9886C6.98698 11.0654 6.88279 11.1082 6.77456 11.1082C6.66634 11.1082 6.56215 11.0654 6.48578 10.9886L4.17951 8.68192C3.94016 8.44258 3.94016 8.05447 4.17951 7.81558L4.46829 7.52672C4.7077 7.28738 5.09536 7.28738 5.3347 7.52672L6.77456 8.96666L10.6653 5.07587C10.9047 4.83653 11.2927 4.83653 11.5317 5.07587L11.8205 5.36472C12.0598 5.60406 12.0598 5.9921 11.8205 6.23106L7.06334 10.9886Z"
                                            fill="#171717" />
                                    </g>
                                    <defs>
                                        <clipPath id="clip0">
                                            <rect width="8" height="8" fill="white" transform="translate(4 3.99994)" />
                                        </clipPath>
                                    </defs>
                                </svg>
                            </div>
                            <div class="message-enter__block">
                                <div class="message-enter__text">Я вам ближе к 17:00-18:00 позвоню</div>
                                <div class="message-enter__wrap">
                                    <div class="message-enter__time">10:16</div>
                                    <div class="message-enter__status"><svg width="18" height="10" viewBox="0 0 18 10" fill="none"
                                                                            xmlns="http://www.w3.org/2000/svg">
                                            <path
                                                d="M1.75 5.34031L5.45459 9L12.5681 1.99304L11.5468 1L5.45459 6.99996L2.75712 4.34028L1.75 5.34031Z"
                                                fill="#5C5C5C" />
                                            <path
                                                d="M7.8125 7.65862L9.13574 9L16.2492 1.99304L15.228 1L9.13574 6.99996L8.79403 6.66307L7.8125 7.65862Z"
                                                fill="#5C5C5C" />
                                        </svg>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="message-out">
                            <div class="message-out__check" data-section="check"><svg width="16" height="16" viewBox="0 0 16 16"
                                                                                      fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <circle cx="8" cy="7.99994" r="8" fill="#FF9900" />
                                    <g clip-path="url(#clip0)">
                                        <path
                                            d="M7.06334 10.9886C6.98698 11.0654 6.88279 11.1082 6.77456 11.1082C6.66634 11.1082 6.56215 11.0654 6.48578 10.9886L4.17951 8.68192C3.94016 8.44258 3.94016 8.05447 4.17951 7.81558L4.46829 7.52672C4.7077 7.28738 5.09536 7.28738 5.3347 7.52672L6.77456 8.96666L10.6653 5.07587C10.9047 4.83653 11.2927 4.83653 11.5317 5.07587L11.8205 5.36472C12.0598 5.60406 12.0598 5.9921 11.8205 6.23106L7.06334 10.9886Z"
                                            fill="#171717" />
                                    </g>
                                    <defs>
                                        <clipPath id="clip0">
                                            <rect width="8" height="8" fill="white" transform="translate(4 3.99994)" />
                                        </clipPath>
                                    </defs>
                                </svg>
                            </div>
                            <div class="message-out__block">
                                <div class="message-out__img"><img src="./images/img-1-db8e7456.jpg" alt=""></div>
                                <div class="message-out__info">
                                    <div class="message-out__text">Гостиница находится по улице Тихвинская 20</div>
                                    <div class="message-out__time">17:13</div>
                                </div>
                            </div>
                        </div>
                        <div class="message-enter message-enter--read">
                            <div class="message-enter__check" data-section="check"><svg width="16" height="16" viewBox="0 0 16 16"
                                                                                        fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <circle cx="8" cy="7.99994" r="8" fill="#FF9900" />
                                    <g clip-path="url(#clip0)">
                                        <path
                                            d="M7.06334 10.9886C6.98698 11.0654 6.88279 11.1082 6.77456 11.1082C6.66634 11.1082 6.56215 11.0654 6.48578 10.9886L4.17951 8.68192C3.94016 8.44258 3.94016 8.05447 4.17951 7.81558L4.46829 7.52672C4.7077 7.28738 5.09536 7.28738 5.3347 7.52672L6.77456 8.96666L10.6653 5.07587C10.9047 4.83653 11.2927 4.83653 11.5317 5.07587L11.8205 5.36472C12.0598 5.60406 12.0598 5.9921 11.8205 6.23106L7.06334 10.9886Z"
                                            fill="#171717" />
                                    </g>
                                    <defs>
                                        <clipPath id="clip0">
                                            <rect width="8" height="8" fill="white" transform="translate(4 3.99994)" />
                                        </clipPath>
                                    </defs>
                                </svg>
                            </div>
                            <div class="message-enter__block">
                                <div class="message-enter__text">минут 10 идти, знаю</div>
                                <div class="message-enter__wrap">
                                    <div class="message-enter__time">17:12</div>
                                    <div class="message-enter__status"><svg width="18" height="10" viewBox="0 0 18 10" fill="none"
                                                                            xmlns="http://www.w3.org/2000/svg">
                                            <path
                                                d="M1.75 5.34031L5.45459 9L12.5681 1.99304L11.5468 1L5.45459 6.99996L2.75712 4.34028L1.75 5.34031Z"
                                                fill="#5C5C5C" />
                                            <path
                                                d="M7.8125 7.65862L9.13574 9L16.2492 1.99304L15.228 1L9.13574 6.99996L8.79403 6.66307L7.8125 7.65862Z"
                                                fill="#5C5C5C" />
                                        </svg>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="message-enter message-enter--read">
                            <div class="message-enter__check" data-section="check"><svg width="16" height="16" viewBox="0 0 16 16"
                                                                                        fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <circle cx="8" cy="7.99994" r="8" fill="#FF9900" />
                                    <g clip-path="url(#clip0)">
                                        <path
                                            d="M7.06334 10.9886C6.98698 11.0654 6.88279 11.1082 6.77456 11.1082C6.66634 11.1082 6.56215 11.0654 6.48578 10.9886L4.17951 8.68192C3.94016 8.44258 3.94016 8.05447 4.17951 7.81558L4.46829 7.52672C4.7077 7.28738 5.09536 7.28738 5.3347 7.52672L6.77456 8.96666L10.6653 5.07587C10.9047 4.83653 11.2927 4.83653 11.5317 5.07587L11.8205 5.36472C12.0598 5.60406 12.0598 5.9921 11.8205 6.23106L7.06334 10.9886Z"
                                            fill="#171717" />
                                    </g>
                                    <defs>
                                        <clipPath id="clip0">
                                            <rect width="8" height="8" fill="white" transform="translate(4 3.99994)" />
                                        </clipPath>
                                    </defs>
                                </svg>
                            </div>
                            <div class="message-enter__block">
                                <div class="message-enter__text">тогда там и встретимся:)</div>
                                <div class="message-enter__wrap">
                                    <div class="message-enter__time">17:12</div>
                                    <div class="message-enter__status"><svg width="18" height="10" viewBox="0 0 18 10" fill="none"
                                                                            xmlns="http://www.w3.org/2000/svg">
                                            <path
                                                d="M1.75 5.34031L5.45459 9L12.5681 1.99304L11.5468 1L5.45459 6.99996L2.75712 4.34028L1.75 5.34031Z"
                                                fill="#5C5C5C" />
                                            <path
                                                d="M7.8125 7.65862L9.13574 9L16.2492 1.99304L15.228 1L9.13574 6.99996L8.79403 6.66307L7.8125 7.65862Z"
                                                fill="#5C5C5C" />
                                        </svg>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="message-out">
                            <div class="message-out__check" data-section="check"><svg width="16" height="16" viewBox="0 0 16 16"
                                                                                      fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <circle cx="8" cy="7.99994" r="8" fill="#FF9900" />
                                    <g clip-path="url(#clip0)">
                                        <path
                                            d="M7.06334 10.9886C6.98698 11.0654 6.88279 11.1082 6.77456 11.1082C6.66634 11.1082 6.56215 11.0654 6.48578 10.9886L4.17951 8.68192C3.94016 8.44258 3.94016 8.05447 4.17951 7.81558L4.46829 7.52672C4.7077 7.28738 5.09536 7.28738 5.3347 7.52672L6.77456 8.96666L10.6653 5.07587C10.9047 4.83653 11.2927 4.83653 11.5317 5.07587L11.8205 5.36472C12.0598 5.60406 12.0598 5.9921 11.8205 6.23106L7.06334 10.9886Z"
                                            fill="#171717" />
                                    </g>
                                    <defs>
                                        <clipPath id="clip0">
                                            <rect width="8" height="8" fill="white" transform="translate(4 3.99994)" />
                                        </clipPath>
                                    </defs>
                                </svg>
                            </div>
                            <div class="message-out__block">
                                <div class="message-out__img"><img src="./images/img-1-db8e7456.jpg" alt=""></div>
                                <div class="message-out__info">
                                    <div class="message-out__text">отлично, тогда там в 7</div>
                                    <div class="message-out__time">17:13</div>
                                </div>
                            </div>
                        </div>
                        <div class="message-enter">
                            <div class="message-enter__check" data-section="check"><svg width="16" height="16" viewBox="0 0 16 16"
                                                                                        fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <circle cx="8" cy="7.99994" r="8" fill="#FF9900" />
                                    <g clip-path="url(#clip0)">
                                        <path
                                            d="M7.06334 10.9886C6.98698 11.0654 6.88279 11.1082 6.77456 11.1082C6.66634 11.1082 6.56215 11.0654 6.48578 10.9886L4.17951 8.68192C3.94016 8.44258 3.94016 8.05447 4.17951 7.81558L4.46829 7.52672C4.7077 7.28738 5.09536 7.28738 5.3347 7.52672L6.77456 8.96666L10.6653 5.07587C10.9047 4.83653 11.2927 4.83653 11.5317 5.07587L11.8205 5.36472C12.0598 5.60406 12.0598 5.9921 11.8205 6.23106L7.06334 10.9886Z"
                                            fill="#171717" />
                                    </g>
                                    <defs>
                                        <clipPath id="clip0">
                                            <rect width="8" height="8" fill="white" transform="translate(4 3.99994)" />
                                        </clipPath>
                                    </defs>
                                </svg>
                            </div>
                            <div class="message-enter__block">
                                <div class="message-enter__text">ок</div>
                                <div class="message-enter__wrap">
                                    <div class="message-enter__time">17:15</div>
                                    <div class="message-enter__status"><svg width="18" height="10" viewBox="0 0 18 10" fill="none"
                                                                            xmlns="http://www.w3.org/2000/svg">
                                            <path
                                                d="M1.75 5.34031L5.45459 9L12.5681 1.99304L11.5468 1L5.45459 6.99996L2.75712 4.34028L1.75 5.34031Z"
                                                fill="#5C5C5C" />
                                            <path
                                                d="M7.8125 7.65862L9.13574 9L16.2492 1.99304L15.228 1L9.13574 6.99996L8.79403 6.66307L7.8125 7.65862Z"
                                                fill="#5C5C5C" />
                                        </svg>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="chat__footer">
                    <div class="chat__controls" data-section="controls-mobile">
                        <ul class="chat-controls">
                            <li class="chat-controls__item" data-tippy-content="Пометить как важное" data-controls-mobile><svg
                                    xmlns="http://www.w3.org/2000/svg" width="16" height="18" viewbox="0 0 16 18" fill="none">
                                    <path
                                        d="M13.5723 6.10352e-05H2.42773C1.11922 6.10352e-05 0.0546875 1.06459 0.0546875 2.37311V18.0001L6.1543 14.9503L12.2539 18.0001V9.5274H15.9453V2.37311C15.9453 1.06459 14.8808 6.10352e-05 13.5723 6.10352e-05ZM11.1992 2.37311V16.2935L6.1543 13.7711L1.10938 16.2935V2.37311C1.10938 1.64615 1.70077 1.05475 2.42773 1.05475H11.6C11.347 1.43205 11.1992 1.8857 11.1992 2.37311ZM14.8906 8.47272H12.2539V2.37311C12.2539 1.64615 12.8453 1.05475 13.5723 1.05475C14.2992 1.05475 14.8906 1.64615 14.8906 2.37311V8.47272Z"
                                        fill="#5C5C5C"></path>
                                </svg>
                            </li>
                            <li class="chat-controls__item" data-tippy-content="Пометить как важное" data-controls-mobile><svg
                                    xmlns="http://www.w3.org/2000/svg" width="18" height="16" viewbox="0 0 18 16" fill="none">
                                    <path
                                        d="M16.2131 2.40617H15.3018L14.8578 1.16183C14.8108 1.03014 14.7134 0.92247 14.5871 0.862457C14.4606 0.802582 14.3156 0.795303 14.1839 0.84227L9.80063 2.40617H5.75917V1.86757C5.75917 1.06172 5.10287 0.40625 4.29607 0.40625H1.4631C0.656433 0.40625 0 1.06172 0 1.86757V13.8122C0 14.7961 0.801727 15.5968 1.78706 15.5968H16.2131C17.1983 15.5968 18 14.7961 18 13.8122V4.19063C18 3.20667 17.1983 2.40617 16.2131 2.40617ZM14.0416 2.01286L15.1977 5.25328H4.95909L14.0416 2.01286ZM1.05469 1.86757C1.05469 1.64331 1.23788 1.46094 1.4631 1.46094H4.29607C4.52129 1.46094 4.70448 1.64331 4.70448 1.86757V2.93352C4.70448 3.22479 4.94055 3.46086 5.23183 3.46086H6.84448L1.82043 5.25328H1.05469V1.86757ZM16.9453 13.8122C16.9453 14.2147 16.6168 14.5421 16.2131 14.5421H1.78706C1.38318 14.5421 1.05469 14.2147 1.05469 13.8122V6.30797H16.9453V13.8122ZM16.9453 5.25328H16.3176L15.678 3.46086H16.2131C16.6168 3.46086 16.9453 3.78825 16.9453 4.19063V5.25328Z"
                                        fill="#5C5C5C"></path>
                                </svg>
                            </li>
                            <li class="chat-controls__item" data-tippy-content="Редактировать" data-controls-mobile><svg
                                    xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewbox="0 0 18 18" fill="none">
                                    <path
                                        d="M17.4397 2.19031L15.8086 0.559066C15.0633 -0.18621 13.8508 -0.186175 13.1055 0.559066C12.4038 1.26082 1.68623 11.9792 0.969991 12.6955C0.893702 12.7718 0.842585 12.8732 0.824374 12.9723L0.00882121 17.3767C-0.0227842 17.5474 0.0316376 17.7228 0.154438 17.8456C0.277379 17.9686 0.452809 18.0229 0.623351 17.9913L5.02733 17.1756C5.12911 17.1567 5.22916 17.105 5.30419 17.03L17.4397 4.89358C18.1867 4.14658 18.1868 2.93742 17.4397 2.19031ZM1.1855 16.8145L1.67885 14.1502L3.8496 16.3211L1.1855 16.8145ZM4.93132 15.9113L2.08863 13.0684L12.4671 2.68914L15.3098 5.53205L4.93132 15.9113ZM16.6939 4.14781L16.0555 4.78628L13.2128 1.94338L13.8512 1.3049C14.1852 0.97085 14.7287 0.970815 15.0628 1.3049L16.6939 2.93615C17.0288 3.27101 17.0288 3.81291 16.6939 4.14781Z"
                                        fill="#5C5C5C"></path>
                                </svg>
                            </li>
                            <li class="chat-controls__item" data-tippy-content="Удалить" data-controls-mobile><svg
                                    xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewbox="0 0 18 18" fill="none">
                                    <g clip-path="url(#clip0)">
                                        <path
                                            d="M16.3081 4.02333L15.9107 2.832C15.7594 2.37827 15.3362 2.07339 14.8581 2.07339H11.5178V0.985886C11.5178 0.442337 11.0759 0 10.5325 0H7.47361C6.93033 0 6.48827 0.442337 6.48827 0.985886V2.07339H3.14815C2.66983 2.07339 2.24672 2.37827 2.09539 2.832L1.69796 4.02333C1.60746 4.29456 1.65332 4.59489 1.82045 4.82684C1.98758 5.05879 2.25798 5.19736 2.5439 5.19736H2.95932L3.87366 16.5038C3.94164 17.3427 4.65355 18 5.49469 18H12.6969C13.5379 18 14.25 17.3427 14.3178 16.5037L15.2322 5.19736H15.4622C15.7481 5.19736 16.0185 5.05879 16.1856 4.82698C16.3528 4.59503 16.3986 4.29456 16.3081 4.02333ZM7.54296 1.05469H10.4631V2.07339H7.54296V1.05469ZM13.2666 16.4187C13.2427 16.7141 12.9925 16.9453 12.6969 16.9453H5.49469C5.19916 16.9453 4.94895 16.7141 4.92505 16.4187L4.01744 5.19736H14.174L13.2666 16.4187ZM2.77008 4.14267L3.09597 3.16571C3.10338 3.14319 3.12439 3.12808 3.14815 3.12808H14.8581C14.8818 3.12808 14.9027 3.14319 14.9103 3.16571L15.2361 4.14267H2.77008Z"
                                            fill="#5C5C5C"></path>
                                        <path
                                            d="M11.5839 16.3813C11.5932 16.3818 11.6024 16.382 11.6118 16.382C11.8904 16.382 12.1233 16.1636 12.1379 15.8821L12.6331 6.3759C12.6482 6.08503 12.4246 5.83688 12.1339 5.82177C11.8424 5.80626 11.595 6.0301 11.5798 6.32097L11.0847 15.8272C11.0696 16.118 11.293 16.3662 11.5839 16.3813Z"
                                            fill="#5C5C5C"></path>
                                        <path
                                            d="M5.89239 15.8833C5.90777 16.1644 6.14041 16.382 6.4185 16.382C6.42811 16.382 6.438 16.3817 6.44775 16.3811C6.73847 16.3653 6.96136 16.1168 6.94556 15.8259L6.42687 6.31973C6.41108 6.02886 6.16251 5.80598 5.87165 5.82191C5.58093 5.8377 5.35804 6.08627 5.37384 6.37713L5.89239 15.8833Z"
                                            fill="#5C5C5C"></path>
                                        <path
                                            d="M9.00781 16.382C9.29909 16.382 9.53516 16.1459 9.53516 15.8546V6.34845C9.53516 6.05717 9.29909 5.82111 9.00781 5.82111C8.71654 5.82111 8.48047 6.05717 8.48047 6.34845V15.8546C8.48047 16.1459 8.71654 16.382 9.00781 16.382Z"
                                            fill="#5C5C5C"></path>
                                    </g>
                                    <defs>
                                        <clippath id="clip0">
                                            <rect width="18" height="18" fill="white"></rect>
                                        </clippath>
                                    </defs>
                                </svg>
                            </li>
                        </ul>
                    </div>
                    <form class="chat__form" action="/" method="post" data-section="form">
                        <label class="chat__file">
                            <input type="file" name="file" hidden>
                        </label>
                        <div class="chat__enter">
									<textarea class="chat__textarea" id="emojiarea" name="message"
                                              placeholder="Напишите сообщение..."></textarea>
                        </div>
                        <button class="chat__button button" type="submit"><span>Отправить</span><svg width="14" height="14"
                                                                                                     viewBox="0 0 14 14" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M4.375 7.875V12.25L7.875 9.625L11.375 14L14 0L0 7L4.375 7.875Z" fill="#FF9900" />
                            </svg>
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    @include('includes._bottom-banner', ['wrapperClass' => 'bottom-banner--transparent'])
@endsection
