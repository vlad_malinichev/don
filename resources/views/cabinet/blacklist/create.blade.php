@extends('layouts.cabinet')

@section('title')
    Чёрный список
@endsection

@section('content')
    @include('cabinet._header', ['title' => 'Чёрный список'])
    <div class="template">
        <div class="template__container container">
            <div class="template__body">
                <div class="template__sidebar">
                    <aside class="sidebar">
                        <div class="sidebar__wrap">
                            <ul class="sidebar__list">
                                <li class="sidebar__item sidebar__item--active">
                                    <a class="sidebar__link js-btn-screen" href="{{ route('cabinet.blacklist.index') }}">
                                        Список плохих клиентов <span class="sidebar__value">21</span>
                                    </a>
                                </li>
                                <li class="sidebar__item">
                                    <a class="sidebar__link js-btn-screen" href="{{ route('cabinet.blacklist.my') }}">
                                        Мои плохие клиенты <span class="sidebar__value">4</span>
                                    </a>
                                </li>
                                <li class="sidebar__item">
                                    <a class="sidebar__link js-btn-screen" href="{{ route('cabinet.blacklist.create') }}">
                                        Добавить плохого клиента
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </aside>
                </div>
                <div class="template__content">
                    <form class="add-blacklist" method="post" action="{{ route('cabinet.blacklist.store') }}">
                        @csrf
                        <fieldset class="add-blacklist__fieldset">
                            <legend class="add-blacklist__title">Основная информация</legend>
                            <div class="add-blacklist__items">
                                <div class="input add-blacklist__item">
                                    <label class="input__label" for="sex">Пол </label>
                                    <select class="add-blacklist__select js-select" name="sex" id="sex">
                                        <option value="">Выберите пол</option>
                                        <option value="1">Мужской</option>
                                        <option value="2">Женский</option>
                                    </select>
                                    @if($errors->has('sex'))
                                        <div class="error">{{ $errors->first('sex') }}</div>
                                    @endif
                                </div>
                                <div class="input add-blacklist__item">
                                    <label class="input__label" for="phone">Телефон </label>
                                    <input class="input__control" id="phone" type="tel" name="phone" value="+7 (968) 123-45-67"
                                           placeholder="" onblur="placeholder=''" onfocus="placeholder=''" autocomplete="on" />
                                    @if($errors->has('phone'))
                                        <div class="error">{{ $errors->first('phone') }}</div>
                                    @endif
                                </div>
                                <div class="add-blacklist__item">
                                    <div class="add-blacklist__label">Категория</div>
                                    <select class="add-blacklist__select js-select" name="category_id">
                                        <option value="10">Очень проблемный</option>
                                        <option value="8">Проблемный</option>
                                    </select>
                                    @if($errors->has('category_id'))
                                        <div class="error">{{ $errors->first('category_id') }}</div>
                                    @endif
                                </div>
                            </div>
                        </fieldset>
                        <fieldset class="add-blacklist__fieldset">
                            <legend class="add-blacklist__title">Расположение</legend>
                            <div class="add-blacklist__items">
                                <div class="add-blacklist__item">
                                    <div class="add-blacklist__label">Страна</div>
                                    <select class="add-blacklist__select js-select" name="country_id">
                                        <option value="1">Россия</option>
                                        <option value="2">Украина</option>
                                    </select>
                                    @if($errors->has('country_id'))
                                        <div class="error">{{ $errors->first('country_id') }}</div>
                                    @endif
                                </div>
                                <div class="add-blacklist__item">
                                    <div class="add-blacklist__label">Область</div>
                                    <select class="add-blacklist__select js-select" name="region_id">
                                        <option value="7">Московская</option>
                                        <option value="37">Ростовская</option>
                                    </select>
                                    @if($errors->has('region_id'))
                                        <div class="error">{{ $errors->first('region_id') }}</div>
                                    @endif
                                </div>
                                <div class="add-blacklist__item">
                                    <div class="add-blacklist__label">Город</div>
                                    <select class="add-blacklist__select js-select" name="city_id">
                                        <option value="17">Москва</option>
                                        <option value="137">Ростов</option>
                                    </select>
                                    @if($errors->has('city_id'))
                                        <div class="error">{{ $errors->first('city_id') }}</div>
                                    @endif
                                </div>
                            </div>
                        </fieldset>
                        <fieldset class="add-blacklist__fieldset">
                            <legend class="add-blacklist__title">Комментарий</legend>
                            <textarea class="add-blacklist__textarea" name="comment" maxlength="1000"></textarea><span
                                class="add-blacklist__length">не более 1000 символов</span>
                            @if($errors->has('comment'))
                                <div class="error">{{ $errors->first('comment') }}</div>
                            @endif
                            <div class="add-blacklist__actions">
                                <button class="add-blacklist__button button" type="submit">Добавить</button>
                                <button class="add-blacklist__reset" type="reset">Очистить данные</button>
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>
    @include('includes._bottom-banner', ['wrapperClass' => 'bottom-banner--transparent'])
@endsection
