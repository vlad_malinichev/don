@extends('layouts.cabinet')

@section('title')
    Чёрный список
@endsection

@section('content')
    @include('cabinet._header', ['title' => 'Чёрный список'])
    <div class="template">
        <div class="template__container container">
            <div class="template__body">
                <div class="template__sidebar">
                    <aside class="sidebar">
                        <div class="sidebar__wrap">
                            <ul class="sidebar__list">
                                <li class="sidebar__item sidebar__item--active">
                                    <a class="sidebar__link js-btn-screen" href="{{ url('/cabinet/blacklist') }}">
                                        Список плохих клиентов <span class="sidebar__value">21</span>
                                    </a>
                                </li>
                                <li class="sidebar__item">
                                    <a class="sidebar__link js-btn-screen" href="{{ url('/cabinet/blacklist/my') }}">
                                        Мои плохие клиенты <span class="sidebar__value">4</span>
                                    </a>
                                </li>
                                <li class="sidebar__item">
                                    <a class="sidebar__link js-btn-screen" href="{{ url('/cabinet/blacklist/create') }}">
                                        Добавить плохого клиента
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </aside>
                </div>
                <div class="template__content">
                    <div class="blacklist">
                        <div class="blacklist__filter">
                            <form class="cabinet-filter" action="/">
                                <div class="cabinet-filter__toggle js-filter-toggle">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" fill="none" xmlns:v="https://vecta.io/nano">
                                        <g clip-path="url(#A)" fill="#f90">
                                            <path
                                                d="M17.357 8.357H6.98a2.56 2.56 0 0 0-4.96 0H.643C.288 8.357 0 8.645 0 9s.288.643.643.643H2.02a2.56 2.56 0 0 0 4.96 0h10.377c.355 0 .643-.288.643-.643s-.288-.643-.643-.643zM4.5 10.286c-.7 0-1.286-.576-1.286-1.286S3.8 7.714 4.5 7.714A1.3 1.3 0 0 1 5.786 9 1.3 1.3 0 0 1 4.5 10.286zm12.857-8.358h-2.02a2.56 2.56 0 0 0-4.96 0H.643c-.355 0-.643.288-.643.643s.288.643.643.643h9.734a2.56 2.56 0 0 0 4.96 0h2.02c.355 0 .643-.288.643-.643s-.288-.643-.643-.643zm-4.5 1.93c-.7 0-1.286-.576-1.286-1.286s.576-1.286 1.286-1.286 1.286.576 1.286 1.286-.576 1.286-1.286 1.286zm4.5 10.928h-3.305a2.56 2.56 0 0 0-4.96 0H.643c-.355 0-.643.288-.643.643s.288.643.643.643H9.1a2.56 2.56 0 0 0 4.96 0h3.305c.355 0 .643-.288.643-.643s-.288-.643-.643-.643zm-5.786 1.93c-.7 0-1.286-.576-1.286-1.286s.576-1.286 1.286-1.286 1.286.576 1.286 1.286-.576 1.286-1.286 1.286z" />
                                        </g>
                                        <defs>
                                            <clipPath id="A">
                                                <path fill="#fff" d="M0 0h18v18H0z" />
                                            </clipPath>
                                        </defs>
                                    </svg>
                                    <div class="cabinet-filter__title">Фильтр</div>
                                    <div class="cabinet-filter__plus"></div>
                                </div>
                                <div class="cabinet-filter__body">
                                    <div class="cabinet-filter__content">
                                        <div class="cabinet-filter__items">
                                            <div class="cabinet-filter__item">
                                                <div class="cabinet-filter__label">Страна</div>
                                                <select class="js-select" name="country">
                                                    <option value="Россия">Россия</option>
                                                    <option value="Россия">Россия</option>
                                                </select>
                                            </div>
                                            <div class="cabinet-filter__item">
                                                <div class="cabinet-filter__label">Область</div>
                                                <select class="js-select" name="region">
                                                    <option value="Московская">Московская</option>
                                                    <option value="Московская">Московская</option>
                                                </select>
                                            </div>
                                            <div class="cabinet-filter__item">
                                                <div class="cabinet-filter__label">Город</div>
                                                <select class="js-select" name="city">
                                                    <option value="Москва">Москва</option>
                                                    <option value="Москва">Москва</option>
                                                </select>
                                            </div>
                                            <div class="cabinet-filter__item">
                                                <div class="cabinet-filter__label">Тип</div>
                                                <select class="js-select" name="type">
                                                    <option value="Все">Все</option>
                                                    <option value="Все">Все</option>
                                                </select>
                                            </div>
                                            <div class="cabinet-filter__item">
                                                <div class="cabinet-filter__label">Статус анкеты</div>
                                                <select class="js-select" name="profile_status">
                                                    <option value="VIP">VIP</option>
                                                    <option value="VIP">VIP</option>
                                                </select>
                                            </div>
                                            <div class="cabinet-filter__item">
                                                <div class="cabinet-filter__label">Статус публикации</div>
                                                <select class="js-select" name="publication_status">
                                                    <option value="Активна">Активна</option>
                                                    <option value="Активна">Активна</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="cabinet-filter__footer">
                                        <button class="cabinet-filter__button button" type="submit">Найти</button>
                                        <button class="cabinet-filter__reset" type="reset">Сбросить</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="blacklist__list">
                            @foreach($records as $record)
                            <div class="blacklist__card">
                                <div class="blacklist-card">
                                    <div class="blacklist-card__header">
                                        <div class="blacklist-card__info">
                                            <a class="blacklist-card__tel" href="tel:79681234567">
                                                {{ $record->phone }}
                                            </a>
                                            <span class="blacklist-card__separator"></span>
                                            <span class="blacklist-card__status">{{ $record->category_id }}</span>
                                        </div>
                                        <div class="blacklist-card__block">
                                            <div class="blacklist-card__date">{{ $record->created_at->format('j F Y H:i') }} | 06 мая 2020 14:14</div>
                                        </div>
                                    </div>
                                    <div class="blacklist-card__body">
                                        <table class="blacklist-card__table">
                                            <tbody>
                                            <tr>
                                                <td>Пол</td>
                                                <td>{{ $record->sex }}</td>
                                            </tr>
                                            <tr>
                                                <td>Страна</td>
                                                <td>{{ $record->country_id }}</td>
                                            </tr>
                                            <tr>
                                                <td>Область</td>
                                                <td>{{ $record->region_id }}</td>
                                            </tr>
                                            <tr>
                                                <td>Город</td>
                                                <td>{{ $record->city_id }}</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                        <div class="blacklist-card__content">
                                            <div class="blacklist-card__subtitle">Комментарий</div>
                                            <p class="blacklist-card__comment">{{ $record->comment }}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('includes._bottom-banner', ['wrapperClass' => 'bottom-banner--transparent'])
@endsection
