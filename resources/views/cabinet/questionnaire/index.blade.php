@extends('layouts.cabinet')

@section('title')
    Анкеты
@endsection

@section('content')
    @include('cabinet._header', ['title' => 'Анкеты'])
    <div class="template">
        <div class="template__container container">
            <div class="template__body">
                <div class="template__sidebar">
                    <aside class="sidebar">
                        <div class="sidebar__wrap">
                            <ul class="sidebar__list">
                                <li class="sidebar__item sidebar__item--active">
                                    <a class="sidebar__link js-btn-screen" href="{{ url('cabinet/questionnaire') }}">
                                        Мои анкеты
                                    </a>
                                </li>
                                <li class="sidebar__item">
                                    <a class="sidebar__link js-btn-screen" href="{{ url('cabinet/questionnaire/create') }}">
                                        Добавить анкету
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </aside>
                </div>
                <div class="template__content">
                    <div class="questionnaires">
                        <div class="questionnaires__header">
                            <div class="questionnaires__filter">
                                <form class="cabinet-filter" action="/">
                                    <div class="cabinet-filter__toggle js-filter-toggle">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" fill="none" xmlns:v="https://vecta.io/nano">
                                            <g clip-path="url(#A)" fill="#f90">
                                                <path
                                                    d="M17.357 8.357H6.98a2.56 2.56 0 0 0-4.96 0H.643C.288 8.357 0 8.645 0 9s.288.643.643.643H2.02a2.56 2.56 0 0 0 4.96 0h10.377c.355 0 .643-.288.643-.643s-.288-.643-.643-.643zM4.5 10.286c-.7 0-1.286-.576-1.286-1.286S3.8 7.714 4.5 7.714A1.3 1.3 0 0 1 5.786 9 1.3 1.3 0 0 1 4.5 10.286zm12.857-8.358h-2.02a2.56 2.56 0 0 0-4.96 0H.643c-.355 0-.643.288-.643.643s.288.643.643.643h9.734a2.56 2.56 0 0 0 4.96 0h2.02c.355 0 .643-.288.643-.643s-.288-.643-.643-.643zm-4.5 1.93c-.7 0-1.286-.576-1.286-1.286s.576-1.286 1.286-1.286 1.286.576 1.286 1.286-.576 1.286-1.286 1.286zm4.5 10.928h-3.305a2.56 2.56 0 0 0-4.96 0H.643c-.355 0-.643.288-.643.643s.288.643.643.643H9.1a2.56 2.56 0 0 0 4.96 0h3.305c.355 0 .643-.288.643-.643s-.288-.643-.643-.643zm-5.786 1.93c-.7 0-1.286-.576-1.286-1.286s.576-1.286 1.286-1.286 1.286.576 1.286 1.286-.576 1.286-1.286 1.286z" />
                                            </g>
                                            <defs>
                                                <clipPath id="A">
                                                    <path fill="#fff" d="M0 0h18v18H0z" />
                                                </clipPath>
                                            </defs>
                                        </svg>
                                        <div class="cabinet-filter__title">Фильтр</div>
                                        <div class="cabinet-filter__plus"></div>
                                    </div>
                                    <div class="cabinet-filter__body">
                                        <div class="cabinet-filter__content">
                                            <div class="cabinet-filter__items">
                                                <div class="cabinet-filter__item">
                                                    <div class="cabinet-filter__label">Страна</div>
                                                    <select class="js-select" name="country">
                                                        <option value="Россия">Россия</option>
                                                        <option value="Россия">Россия</option>
                                                    </select>
                                                </div>
                                                <div class="cabinet-filter__item">
                                                    <div class="cabinet-filter__label">Область</div>
                                                    <select class="js-select" name="region">
                                                        <option value="Московская">Московская</option>
                                                        <option value="Московская">Московская</option>
                                                    </select>
                                                </div>
                                                <div class="cabinet-filter__item">
                                                    <div class="cabinet-filter__label">Город</div>
                                                    <select class="js-select" name="city">
                                                        <option value="Москва">Москва</option>
                                                        <option value="Москва">Москва</option>
                                                    </select>
                                                </div>
                                                <div class="cabinet-filter__item">
                                                    <div class="cabinet-filter__label">Тип</div>
                                                    <select class="js-select" name="type">
                                                        <option value="Все">Все</option>
                                                        <option value="Все">Все</option>
                                                    </select>
                                                </div>
                                                <div class="cabinet-filter__item">
                                                    <div class="cabinet-filter__label">Статус анкеты</div>
                                                    <select class="js-select" name="profile_status">
                                                        <option value="VIP">VIP</option>
                                                        <option value="VIP">VIP</option>
                                                    </select>
                                                </div>
                                                <div class="cabinet-filter__item">
                                                    <div class="cabinet-filter__label">Статус публикации</div>
                                                    <select class="js-select" name="publication_status">
                                                        <option value="Активна">Активна</option>
                                                        <option value="Активна">Активна</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="cabinet-filter__footer">
                                            <button class="cabinet-filter__button button" type="submit">Найти</button>
                                            <button class="cabinet-filter__reset" type="reset">Сбросить</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="questionnaires__control">
                                <div class="cabinet-control">
                                    <div class="cabinet-control__title">Управление анкетами:</div>
                                    <div class="cabinet-control__items">
                                        <div class="cabinet-control__item"><svg xmlns="http://www.w3.org/2000/svg" width="18"
                                                                                height="18" fill="none" xmlns:v="https://vecta.io/nano">
                                                <path
                                                    d="M14.526.675H3.475L0 5.414l9.037 11.91L18 5.412 14.526.675zM4.94 5.934l2.546 7.607-5.77-7.607H4.94zm1.11 0h5.905l-2.932 8.88-2.973-8.88zm7.012 0h3.23l-5.754 7.648 2.525-7.648zm.93-4.207l2.314 3.155h-3.244l-1.035-3.155h1.966zm-3.073 0l1.035 3.155h-5.91L7.08 1.727h3.84zm-6.912 0h1.966L4.938 4.882H1.694l2.314-3.155z"
                                                    fill="#f90" /></svg></div>
                                        <div class="cabinet-control__item"><svg xmlns="http://www.w3.org/2000/svg"
                                                                                xmlns:xlink="http://www.w3.org/1999/xlink" width="18" height="18" fill="none"
                                                                                xmlns:v="https://vecta.io/nano">
                                                <g clip-path="url(#A)" fill="#f90">
                                                    <path
                                                        d="M14.554 5.262L9.382.146a.57.57 0 0 0-.762 0l-5.17 5.1c-.167.173-.22.427-.136.653.086.22.284.375.517.408h2.45v6.287c0 .286.23.517.516.518l.028-.001h4.355c.285.015.53-.204.544-.49l.001-.028V6.297h2.45c.233-.033.43-.19.517-.408a.6.6 0 0 0-.136-.626zm-3.375-.054c-.3 0-.544.11-.544.408v6.396H7.368V5.616c0-.3-.245-.408-.544-.408h-1.66L9 1.452l3.838 3.756h-1.66z" />
                                                    <use xlink:href="#B" />
                                                    <path
                                                        d="M11.178 15.278H6.824c-.3 0-.544.244-.544.544s.244.544.544.544h4.355c.3 0 .544-.244.544-.544s-.244-.544-.544-.544zm0 1.632H6.824c-.3 0-.544.244-.544.544s.244.544.544.544h4.355c.3 0 .544-.244.544-.544s-.244-.544-.544-.544z" />
                                                </g>
                                                <defs>
                                                    <clipPath id="A">
                                                        <path fill="#fff" d="M0 0h18v18H0z" />
                                                    </clipPath>
                                                    <path id="B"
                                                          d="M11.178 13.645H6.824c-.3 0-.544.244-.544.544s.244.544.544.544h4.355c.3 0 .544-.244.544-.544s-.244-.544-.544-.544z" />
                                                </defs>
                                            </svg></div>
                                        <div class="cabinet-control__item"><svg width="18" height="18" viewBox="0 0 18 18" fill="none"
                                                                                xmlns="http://www.w3.org/2000/svg">
                                                <g clip-path="url(#clip0)">
                                                    <path
                                                        d="M15.3625 2.6371C11.8462 -0.879258 6.15432 -0.879786 2.63747 2.6371C-0.878891 6.15346 -0.879419 11.8453 2.63747 15.3622C6.15382 18.8785 11.8457 18.879 15.3625 15.3622C18.8788 11.8458 18.8794 6.15399 15.3625 2.6371ZM8.99998 16.5195C4.85354 16.5195 1.48014 13.1461 1.48014 8.99961C1.48014 4.85317 4.85354 1.47981 8.99998 1.47981C13.1464 1.47981 16.5198 4.85321 16.5198 8.99965C16.5198 13.1461 13.1464 16.5195 8.99998 16.5195Z"
                                                        fill="#FF9900" />
                                                    <path
                                                        d="M10.5008 5.63312C10.0926 5.63312 9.76172 5.96401 9.76172 6.3722V11.6279C9.76172 12.036 10.0926 12.3669 10.5008 12.3669C10.909 12.3669 11.2399 12.036 11.2399 11.6279V6.3722C11.2399 5.96401 10.909 5.63312 10.5008 5.63312Z"
                                                        fill="#FF9900" />
                                                    <path
                                                        d="M7.49884 5.63312C7.09066 5.63312 6.75977 5.96401 6.75977 6.3722V11.6279C6.75977 12.036 7.09066 12.3669 7.49884 12.3669C7.90702 12.3669 8.23792 12.036 8.23792 11.6279V6.3722C8.23792 5.96401 7.90706 5.63312 7.49884 5.63312Z"
                                                        fill="#FF9900" />
                                                </g>
                                                <defs>
                                                    <clipPath id="clip0">
                                                        <rect width="18" height="18" fill="white" />
                                                    </clipPath>
                                                </defs>
                                            </svg>
                                        </div>
                                        <div class="cabinet-control__item"><svg xmlns="http://www.w3.org/2000/svg" width="18"
                                                                                height="18" fill="none" xmlns:v="https://vecta.io/nano">
                                                <g clip-path="url(#A)" fill="#f90">
                                                    <path
                                                        d="M16.308 4.023l-.397-1.2a1.11 1.11 0 0 0-1.053-.759h-3.34V.986c0-.544-.442-.986-.985-.986h-3.06c-.543 0-.985.442-.985.986v1.088h-3.34a1.11 1.11 0 0 0-1.053.759l-.397 1.2c-.1.27-.045.572.122.804s.438.37.723.37h.415l.914 11.306c.068.84.78 1.496 1.62 1.496h7.202c.84 0 1.553-.657 1.62-1.496l.914-11.306h.23a.9.9 0 0 0 .723-.37c.167-.23.213-.532.122-.804zm-8.765-2.97h2.92v1.02h-2.92v-1.02zm5.724 15.364c-.024.295-.274.527-.57.527H5.495c-.296 0-.546-.23-.57-.527l-.908-11.22h10.157l-.907 11.22zM2.77 4.143l.326-.977c.007-.023.028-.038.052-.038h11.7c.024 0 .045.015.052.038l.326.977H2.77zm8.816 12.237h.028c.28 0 .512-.218.526-.5l.495-9.506a.53.53 0 0 0-.499-.554c-.292-.016-.54.208-.554.5l-.495 9.506c-.015.3.208.54.5.554zm-5.694-.497a.53.53 0 0 0 .526.499l.03-.001a.53.53 0 0 0 .498-.555l-.52-9.506a.53.53 0 0 0-.555-.498c-.29.016-.514.264-.498.555l.52 9.506zm3.108.5a.53.53 0 0 0 .527-.527V6.348A.53.53 0 0 0 9 5.821c-.29 0-.527.236-.527.527v9.506a.53.53 0 0 0 .527.527z" />
                                                </g>
                                                <defs>
                                                    <clipPath id="A">
                                                        <path fill="#fff" d="M0 0h18v18H0z" />
                                                    </clipPath>
                                                </defs>
                                            </svg></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="questionnaires__cards">
                            <div class="questionnaires__card">
                                <div class="profile-card">
                                    <div class="profile-card__header">
                                        <div class="profile-card__info">
                                            <div class="profile-card__personal"><a class="profile-card__name"
                                                                                   href="questionnaire.html">Соня</a>
                                                <div class="profile-card__separator"></div><a class="profile-card__phone"
                                                                                              href="tel:79681234567">+7 (968) 123-45-67</a>
                                            </div>
                                            <div class="profile-card__classification">Индивидуалка</div>
                                        </div>
                                        <div class="profile-card__block">
                                            <div class="profile-card__label">Активна</div>
                                            <div class="profile-card__rating">
                                                <div class="rating-static rating-static--four">
                                                    <div class="rating-static__star"></div>
                                                    <div class="rating-static__star"></div>
                                                    <div class="rating-static__star"></div>
                                                    <div class="rating-static__star"></div>
                                                    <div class="rating-static__star"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="profile-card__body">
                                        <div class="profile-card__content">
                                            <div class="profile-card__slider">
                                                <div class="swiper-container js-profile-card-slider">
                                                    <div class="swiper-wrapper">
                                                        <div class="swiper-slide"><img class="swiper-slide__img swiper-lazy"
                                                                                       data-src="./images/card-faf45814.jpg" alt="image">
                                                            <div class="swiper-lazy-preloader"></div>
                                                        </div>
                                                        <div class="swiper-slide"><img class="swiper-slide__img swiper-lazy"
                                                                                       data-src="./images/card-faf45814.jpg" alt="image">
                                                            <div class="swiper-lazy-preloader"></div>
                                                        </div>
                                                        <div class="swiper-slide"><img class="swiper-slide__img swiper-lazy"
                                                                                       data-src="./images/card-faf45814.jpg" alt="image">
                                                            <div class="swiper-lazy-preloader"></div>
                                                        </div>
                                                        <div class="swiper-slide"><img class="swiper-slide__img swiper-lazy"
                                                                                       data-src="./images/card-faf45814.jpg" alt="image">
                                                            <div class="swiper-lazy-preloader"></div>
                                                        </div>
                                                        <div class="swiper-slide"><img class="swiper-slide__img swiper-lazy"
                                                                                       data-src="./images/card-faf45814.jpg" alt="image">
                                                            <div class="swiper-lazy-preloader"></div>
                                                        </div>
                                                        <div class="swiper-slide"><img class="swiper-slide__img swiper-lazy"
                                                                                       data-src="./images/card-faf45814.jpg" alt="image">
                                                            <div class="swiper-lazy-preloader"></div>
                                                        </div>
                                                        <div class="swiper-slide"><img class="swiper-slide__img swiper-lazy"
                                                                                       data-src="./images/card-faf45814.jpg" alt="image">
                                                            <div class="swiper-lazy-preloader"></div>
                                                        </div>
                                                        <div class="swiper-slide"><img class="swiper-slide__img swiper-lazy"
                                                                                       data-src="./images/card-faf45814.jpg" alt="image">
                                                            <div class="swiper-lazy-preloader"></div>
                                                        </div>
                                                        <div class="swiper-slide"><img class="swiper-slide__img swiper-lazy"
                                                                                       data-src="./images/card-faf45814.jpg" alt="image">
                                                            <div class="swiper-lazy-preloader"></div>
                                                        </div>
                                                        <div class="swiper-slide"><img class="swiper-slide__img swiper-lazy"
                                                                                       data-src="./images/card-faf45814.jpg" alt="image">
                                                            <div class="swiper-lazy-preloader"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="swiper-pagination"></div>
                                            </div>
                                            <div class="profile-card__details">
                                                <table class="profile-card__table">
                                                    <tbody>
                                                    <tr>
                                                        <td>Страна</td>
                                                        <td>Россия</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Область</td>
                                                        <td>Московская</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Город</td>
                                                        <td>Москва</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Дата размещения</td>
                                                        <td>01.01.2020</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Просмотров</td>
                                                        <td>21031</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Поклонников</td>
                                                        <td>737</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Подарков</td>
                                                        <td><span>15</span></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Сообщений</td>
                                                        <td><span>90</span></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Комментариев</td>
                                                        <td><span>10</span></td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="profile-card__aside">
                                            <div class="profile-card__control">
                                                <div class="card-control">
                                                    <div class="card-control__header">
                                                        <div class="card-control__term">Осталось<span class="card-control__amount">8</span>дней
                                                        </div><a class="card-control__extend" href="#">Продлить</a>
                                                    </div>
                                                    <ul class="card-control__items">
                                                        <li class="card-control__item"><a class="card-control__link" href="#">
                                                                <div class="card-control__icon"><svg xmlns="http://www.w3.org/2000/svg" width="18"
                                                                                                     height="18" fill="none" xmlns:v="https://vecta.io/nano">
                                                                        <path
                                                                            d="M14.526.675H3.475L0 5.414l9.037 11.91L18 5.412 14.526.675zM4.94 5.934l2.546 7.607-5.77-7.607H4.94zm1.11 0h5.905l-2.932 8.88-2.973-8.88zm7.012 0h3.23l-5.754 7.648 2.525-7.648zm.93-4.207l2.314 3.155h-3.244l-1.035-3.155h1.966zm-3.073 0l1.035 3.155h-5.91L7.08 1.727h3.84zm-6.912 0h1.966L4.938 4.882H1.694l2.314-3.155z"
                                                                            fill="#f90" /></svg>
                                                                </div>
                                                                <div class="card-control__name">Купить VIP-статус</div>
                                                            </a></li>
                                                        <li class="card-control__item"><a class="card-control__link" href="#">
                                                                <div class="card-control__icon"><svg xmlns="http://www.w3.org/2000/svg"
                                                                                                     xmlns:xlink="http://www.w3.org/1999/xlink" width="18" height="18" fill="none"
                                                                                                     xmlns:v="https://vecta.io/nano">
                                                                        <g clip-path="url(#A)" fill="#1aa520">
                                                                            <path
                                                                                d="M14.554 5.262L9.382.146a.57.57 0 0 0-.762 0l-5.17 5.1c-.167.173-.22.427-.136.653.086.22.284.375.517.408h2.45v6.287c0 .286.23.518.516.518l.028-.001h4.355c.285.015.53-.204.544-.49l.001-.028V6.297h2.45c.233-.033.43-.19.517-.408a.6.6 0 0 0-.136-.626zm-3.375-.054c-.3 0-.544.11-.544.408v6.396H7.368V5.616c0-.3-.245-.408-.544-.408h-1.66L9 1.452l3.838 3.756h-1.66z" />
                                                                            <use xlink:href="#B" />
                                                                            <path
                                                                                d="M11.178 15.278H6.824c-.3 0-.544.244-.544.544s.244.544.544.544h4.355c.3 0 .544-.244.544-.544s-.244-.544-.544-.544zm0 1.632H6.824c-.3 0-.544.244-.544.544s.244.544.544.544h4.355c.3 0 .544-.244.544-.544s-.244-.544-.544-.544z" />
                                                                        </g>
                                                                        <defs>
                                                                            <clipPath id="A">
                                                                                <path fill="#fff" d="M0 0h18v18H0z" />
                                                                            </clipPath>
                                                                            <path id="B"
                                                                                  d="M11.178 13.645H6.824c-.3 0-.544.244-.544.544s.244.544.544.544h4.355c.3 0 .544-.244.544-.544s-.244-.544-.544-.544z" />
                                                                        </defs>
                                                                    </svg>
                                                                </div>
                                                                <div class="card-control__name">Поднять анкету в списке</div>
                                                            </a></li>
                                                        <li class="card-control__item"><a class="card-control__link" href="#">
                                                                <div class="card-control__icon"><svg xmlns="http://www.w3.org/2000/svg" width="18"
                                                                                                     height="18" fill="none" xmlns:v="https://vecta.io/nano">
                                                                        <g clip-path="url(#A)" fill="#5c5c5c">
                                                                            <path
                                                                                d="M16.613 11.308a.45.45 0 0 0-.448.448v3.98c-.001.742-.602 1.344-1.345 1.345H2.24c-.74-.001-1.343-.6-1.343-1.345V4.054C.897 3.312 1.5 2.7 2.24 2.7h3.98a.45.45 0 0 0 .448-.448.45.45 0 0 0-.448-.448H2.24A2.24 2.24 0 0 0 0 4.054v11.682a2.24 2.24 0 0 0 2.24 2.24h12.58a2.24 2.24 0 0 0 2.24-2.24v-3.98a.45.45 0 0 0-.448-.448zM16.884.66a2.02 2.02 0 0 0-2.853 0L6.033 8.657a.45.45 0 0 0-.115.197L4.866 12.65a.45.45 0 0 0 .552.552l3.797-1.052a.45.45 0 0 0 .197-.115L17.4 4.038a2.02 2.02 0 0 0 0-2.853L16.884.66zM7 8.948l6.546-6.546 2.1 2.1L9.12 11.06 7 8.948zm-.422.846l1.687 1.687-2.333.646.646-2.333zm10.188-6.4l-.475.475-2.1-2.1.476-.475a1.12 1.12 0 0 1 1.585 0l.526.526a1.12 1.12 0 0 1 0 1.585z" />
                                                                        </g>
                                                                        <defs>
                                                                            <clipPath id="A">
                                                                                <path fill="#fff" d="M0 0h18v18H0z" />
                                                                            </clipPath>
                                                                        </defs>
                                                                    </svg>
                                                                </div>
                                                                <div class="card-control__name">Редактировать</div>
                                                            </a></li>
                                                        <li class="card-control__item"><a class="card-control__link" href="#">
                                                                <div class="card-control__icon"><svg width="18" height="18" viewBox="0 0 18 18"
                                                                                                     fill="none" xmlns="http://www.w3.org/2000/svg">
                                                                        <g clip-path="url(#clip0)">
                                                                            <path
                                                                                d="M15.3625 2.6371C11.8462 -0.879258 6.15432 -0.879786 2.63747 2.6371C-0.878891 6.15346 -0.879419 11.8453 2.63747 15.3622C6.15382 18.8785 11.8457 18.879 15.3625 15.3622C18.8788 11.8458 18.8794 6.15399 15.3625 2.6371ZM8.99998 16.5195C4.85354 16.5195 1.48014 13.1461 1.48014 8.99961C1.48014 4.85317 4.85354 1.47981 8.99998 1.47981C13.1464 1.47981 16.5198 4.85321 16.5198 8.99965C16.5198 13.1461 13.1464 16.5195 8.99998 16.5195Z"
                                                                                fill="#A6811B" />
                                                                            <path
                                                                                d="M10.5008 5.63306C10.0926 5.63306 9.76172 5.96395 9.76172 6.37213V11.6278C9.76172 12.036 10.0926 12.3669 10.5008 12.3669C10.909 12.3669 11.2399 12.036 11.2399 11.6278V6.37213C11.2399 5.96395 10.909 5.63306 10.5008 5.63306Z"
                                                                                fill="#A6811B" />
                                                                            <path
                                                                                d="M7.49884 5.63306C7.09066 5.63306 6.75977 5.96395 6.75977 6.37213V11.6278C6.75977 12.036 7.09066 12.3669 7.49884 12.3669C7.90702 12.3669 8.23792 12.036 8.23792 11.6278V6.37213C8.23792 5.96395 7.90706 5.63306 7.49884 5.63306Z"
                                                                                fill="#A6811B" />
                                                                        </g>
                                                                        <defs>
                                                                            <clipPath id="clip0">
                                                                                <rect width="18" height="18" fill="white" />
                                                                            </clipPath>
                                                                        </defs>
                                                                    </svg>
                                                                </div>
                                                                <div class="card-control__name">Остановить публикацию</div>
                                                            </a></li>
                                                        <li class="card-control__item"><a class="card-control__link" href="#">
                                                                <div class="card-control__icon"><svg xmlns="http://www.w3.org/2000/svg" width="18"
                                                                                                     height="18" fill="none" xmlns:v="https://vecta.io/nano">
                                                                        <g clip-path="url(#A)" fill="#a61a1a">
                                                                            <path
                                                                                d="M16.308 4.023l-.397-1.2a1.11 1.11 0 0 0-1.053-.759h-3.34V.986c0-.544-.442-.986-.985-.986h-3.06c-.543 0-.985.442-.985.986v1.088h-3.34a1.11 1.11 0 0 0-1.053.759l-.397 1.2c-.1.27-.045.572.122.804s.438.37.723.37h.415l.914 11.306c.068.84.78 1.496 1.62 1.496h7.202c.84 0 1.553-.657 1.62-1.496l.914-11.306h.23a.9.9 0 0 0 .723-.37c.167-.23.213-.532.122-.804zm-8.765-2.97h2.92v1.02h-2.92v-1.02zm5.724 15.364c-.024.295-.274.527-.57.527H5.495c-.296 0-.546-.23-.57-.527l-.908-11.22h10.157l-.907 11.22zM2.77 4.143l.326-.977c.007-.023.028-.038.052-.038h11.7c.024 0 .045.015.052.038l.326.977H2.77zm8.816 12.237h.028c.28 0 .512-.218.526-.5l.495-9.506a.53.53 0 0 0-.499-.554c-.292-.016-.54.208-.554.5l-.495 9.506c-.015.3.208.54.5.554zm-5.694-.497a.53.53 0 0 0 .526.5l.03-.001a.53.53 0 0 0 .498-.555l-.52-9.506a.53.53 0 0 0-.555-.498c-.29.016-.514.264-.498.555l.52 9.506zm3.108.5a.53.53 0 0 0 .527-.527V6.35A.53.53 0 0 0 9 5.823c-.29 0-.527.236-.527.527v9.506a.53.53 0 0 0 .527.527z" />
                                                                        </g>
                                                                        <defs>
                                                                            <clipPath id="A">
                                                                                <path fill="#fff" d="M0 0h18v18H0z" />
                                                                            </clipPath>
                                                                        </defs>
                                                                    </svg>
                                                                </div>
                                                                <div class="card-control__name">Удалить анкету</div>
                                                            </a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="questionnaires__card">
                                <div class="profile-card">
                                    <div class="profile-card__header">
                                        <div class="profile-card__info">
                                            <div class="profile-card__personal"><a class="profile-card__name"
                                                                                   href="questionnaire.html">Соня</a>
                                                <div class="profile-card__separator"></div><a class="profile-card__phone"
                                                                                              href="tel:79681234567">+7 (968) 123-45-67</a>
                                            </div>
                                            <div class="profile-card__classification">Индивидуалка</div>
                                        </div>
                                        <div class="profile-card__block">
                                            <div class="profile-card__label">Активна</div>
                                            <div class="profile-card__rating">
                                                <div class="rating-static rating-static--four">
                                                    <div class="rating-static__star"></div>
                                                    <div class="rating-static__star"></div>
                                                    <div class="rating-static__star"></div>
                                                    <div class="rating-static__star"></div>
                                                    <div class="rating-static__star"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="profile-card__body">
                                        <div class="profile-card__content">
                                            <div class="profile-card__slider">
                                                <div class="swiper-container js-profile-card-slider">
                                                    <div class="swiper-wrapper">
                                                        <div class="swiper-slide"><img class="swiper-slide__img swiper-lazy"
                                                                                       data-src="./images/card-faf45814.jpg" alt="image">
                                                            <div class="swiper-lazy-preloader"></div>
                                                        </div>
                                                        <div class="swiper-slide"><img class="swiper-slide__img swiper-lazy"
                                                                                       data-src="./images/card-faf45814.jpg" alt="image">
                                                            <div class="swiper-lazy-preloader"></div>
                                                        </div>
                                                        <div class="swiper-slide"><img class="swiper-slide__img swiper-lazy"
                                                                                       data-src="./images/card-faf45814.jpg" alt="image">
                                                            <div class="swiper-lazy-preloader"></div>
                                                        </div>
                                                        <div class="swiper-slide"><img class="swiper-slide__img swiper-lazy"
                                                                                       data-src="./images/card-faf45814.jpg" alt="image">
                                                            <div class="swiper-lazy-preloader"></div>
                                                        </div>
                                                        <div class="swiper-slide"><img class="swiper-slide__img swiper-lazy"
                                                                                       data-src="./images/card-faf45814.jpg" alt="image">
                                                            <div class="swiper-lazy-preloader"></div>
                                                        </div>
                                                        <div class="swiper-slide"><img class="swiper-slide__img swiper-lazy"
                                                                                       data-src="./images/card-faf45814.jpg" alt="image">
                                                            <div class="swiper-lazy-preloader"></div>
                                                        </div>
                                                        <div class="swiper-slide"><img class="swiper-slide__img swiper-lazy"
                                                                                       data-src="./images/card-faf45814.jpg" alt="image">
                                                            <div class="swiper-lazy-preloader"></div>
                                                        </div>
                                                        <div class="swiper-slide"><img class="swiper-slide__img swiper-lazy"
                                                                                       data-src="./images/card-faf45814.jpg" alt="image">
                                                            <div class="swiper-lazy-preloader"></div>
                                                        </div>
                                                        <div class="swiper-slide"><img class="swiper-slide__img swiper-lazy"
                                                                                       data-src="./images/card-faf45814.jpg" alt="image">
                                                            <div class="swiper-lazy-preloader"></div>
                                                        </div>
                                                        <div class="swiper-slide"><img class="swiper-slide__img swiper-lazy"
                                                                                       data-src="./images/card-faf45814.jpg" alt="image">
                                                            <div class="swiper-lazy-preloader"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="swiper-pagination"></div>
                                            </div>
                                            <div class="profile-card__details">
                                                <table class="profile-card__table">
                                                    <tbody>
                                                    <tr>
                                                        <td>Страна</td>
                                                        <td>Россия</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Область</td>
                                                        <td>Московская</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Город</td>
                                                        <td>Москва</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Дата размещения</td>
                                                        <td>01.01.2020</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Просмотров</td>
                                                        <td>21031</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Поклонников</td>
                                                        <td>737</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Подарков</td>
                                                        <td><span>15</span></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Сообщений</td>
                                                        <td><span>90</span></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Комментариев</td>
                                                        <td><span>10</span></td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="profile-card__aside">
                                            <div class="profile-card__control">
                                                <div class="card-control">
                                                    <div class="card-control__header">
                                                        <div class="card-control__term">Осталось<span class="card-control__amount">8</span>дней
                                                        </div><a class="card-control__extend" href="#">Продлить</a>
                                                    </div>
                                                    <ul class="card-control__items">
                                                        <li class="card-control__item"><a class="card-control__link" href="#">
                                                                <div class="card-control__icon"><svg xmlns="http://www.w3.org/2000/svg" width="18"
                                                                                                     height="18" fill="none" xmlns:v="https://vecta.io/nano">
                                                                        <path
                                                                            d="M14.526.675H3.475L0 5.414l9.037 11.91L18 5.412 14.526.675zM4.94 5.934l2.546 7.607-5.77-7.607H4.94zm1.11 0h5.905l-2.932 8.88-2.973-8.88zm7.012 0h3.23l-5.754 7.648 2.525-7.648zm.93-4.207l2.314 3.155h-3.244l-1.035-3.155h1.966zm-3.073 0l1.035 3.155h-5.91L7.08 1.727h3.84zm-6.912 0h1.966L4.938 4.882H1.694l2.314-3.155z"
                                                                            fill="#f90" /></svg>
                                                                </div>
                                                                <div class="card-control__name">Купить VIP-статус</div>
                                                            </a></li>
                                                        <li class="card-control__item"><a class="card-control__link" href="#">
                                                                <div class="card-control__icon"><svg xmlns="http://www.w3.org/2000/svg"
                                                                                                     xmlns:xlink="http://www.w3.org/1999/xlink" width="18" height="18" fill="none"
                                                                                                     xmlns:v="https://vecta.io/nano">
                                                                        <g clip-path="url(#A)" fill="#1aa520">
                                                                            <path
                                                                                d="M14.554 5.262L9.382.146a.57.57 0 0 0-.762 0l-5.17 5.1c-.167.173-.22.427-.136.653.086.22.284.375.517.408h2.45v6.287c0 .286.23.518.516.518l.028-.001h4.355c.285.015.53-.204.544-.49l.001-.028V6.297h2.45c.233-.033.43-.19.517-.408a.6.6 0 0 0-.136-.626zm-3.375-.054c-.3 0-.544.11-.544.408v6.396H7.368V5.616c0-.3-.245-.408-.544-.408h-1.66L9 1.452l3.838 3.756h-1.66z" />
                                                                            <use xlink:href="#B" />
                                                                            <path
                                                                                d="M11.178 15.278H6.824c-.3 0-.544.244-.544.544s.244.544.544.544h4.355c.3 0 .544-.244.544-.544s-.244-.544-.544-.544zm0 1.632H6.824c-.3 0-.544.244-.544.544s.244.544.544.544h4.355c.3 0 .544-.244.544-.544s-.244-.544-.544-.544z" />
                                                                        </g>
                                                                        <defs>
                                                                            <clipPath id="A">
                                                                                <path fill="#fff" d="M0 0h18v18H0z" />
                                                                            </clipPath>
                                                                            <path id="B"
                                                                                  d="M11.178 13.645H6.824c-.3 0-.544.244-.544.544s.244.544.544.544h4.355c.3 0 .544-.244.544-.544s-.244-.544-.544-.544z" />
                                                                        </defs>
                                                                    </svg>
                                                                </div>
                                                                <div class="card-control__name">Поднять анкету в списке</div>
                                                            </a></li>
                                                        <li class="card-control__item"><a class="card-control__link" href="#">
                                                                <div class="card-control__icon"><svg xmlns="http://www.w3.org/2000/svg" width="18"
                                                                                                     height="18" fill="none" xmlns:v="https://vecta.io/nano">
                                                                        <g clip-path="url(#A)" fill="#5c5c5c">
                                                                            <path
                                                                                d="M16.613 11.308a.45.45 0 0 0-.448.448v3.98c-.001.742-.602 1.344-1.345 1.345H2.24c-.74-.001-1.343-.6-1.343-1.345V4.054C.897 3.312 1.5 2.7 2.24 2.7h3.98a.45.45 0 0 0 .448-.448.45.45 0 0 0-.448-.448H2.24A2.24 2.24 0 0 0 0 4.054v11.682a2.24 2.24 0 0 0 2.24 2.24h12.58a2.24 2.24 0 0 0 2.24-2.24v-3.98a.45.45 0 0 0-.448-.448zM16.884.66a2.02 2.02 0 0 0-2.853 0L6.033 8.657a.45.45 0 0 0-.115.197L4.866 12.65a.45.45 0 0 0 .552.552l3.797-1.052a.45.45 0 0 0 .197-.115L17.4 4.038a2.02 2.02 0 0 0 0-2.853L16.884.66zM7 8.948l6.546-6.546 2.1 2.1L9.12 11.06 7 8.948zm-.422.846l1.687 1.687-2.333.646.646-2.333zm10.188-6.4l-.475.475-2.1-2.1.476-.475a1.12 1.12 0 0 1 1.585 0l.526.526a1.12 1.12 0 0 1 0 1.585z" />
                                                                        </g>
                                                                        <defs>
                                                                            <clipPath id="A">
                                                                                <path fill="#fff" d="M0 0h18v18H0z" />
                                                                            </clipPath>
                                                                        </defs>
                                                                    </svg>
                                                                </div>
                                                                <div class="card-control__name">Редактировать</div>
                                                            </a></li>
                                                        <li class="card-control__item"><a class="card-control__link" href="#">
                                                                <div class="card-control__icon"><svg width="18" height="18" viewBox="0 0 18 18"
                                                                                                     fill="none" xmlns="http://www.w3.org/2000/svg">
                                                                        <g clip-path="url(#clip0)">
                                                                            <path
                                                                                d="M15.3625 2.6371C11.8462 -0.879258 6.15432 -0.879786 2.63747 2.6371C-0.878891 6.15346 -0.879419 11.8453 2.63747 15.3622C6.15382 18.8785 11.8457 18.879 15.3625 15.3622C18.8788 11.8458 18.8794 6.15399 15.3625 2.6371ZM8.99998 16.5195C4.85354 16.5195 1.48014 13.1461 1.48014 8.99961C1.48014 4.85317 4.85354 1.47981 8.99998 1.47981C13.1464 1.47981 16.5198 4.85321 16.5198 8.99965C16.5198 13.1461 13.1464 16.5195 8.99998 16.5195Z"
                                                                                fill="#A6811B" />
                                                                            <path
                                                                                d="M10.5008 5.63306C10.0926 5.63306 9.76172 5.96395 9.76172 6.37213V11.6278C9.76172 12.036 10.0926 12.3669 10.5008 12.3669C10.909 12.3669 11.2399 12.036 11.2399 11.6278V6.37213C11.2399 5.96395 10.909 5.63306 10.5008 5.63306Z"
                                                                                fill="#A6811B" />
                                                                            <path
                                                                                d="M7.49884 5.63306C7.09066 5.63306 6.75977 5.96395 6.75977 6.37213V11.6278C6.75977 12.036 7.09066 12.3669 7.49884 12.3669C7.90702 12.3669 8.23792 12.036 8.23792 11.6278V6.37213C8.23792 5.96395 7.90706 5.63306 7.49884 5.63306Z"
                                                                                fill="#A6811B" />
                                                                        </g>
                                                                        <defs>
                                                                            <clipPath id="clip0">
                                                                                <rect width="18" height="18" fill="white" />
                                                                            </clipPath>
                                                                        </defs>
                                                                    </svg>
                                                                </div>
                                                                <div class="card-control__name">Остановить публикацию</div>
                                                            </a></li>
                                                        <li class="card-control__item"><a class="card-control__link" href="#">
                                                                <div class="card-control__icon"><svg xmlns="http://www.w3.org/2000/svg" width="18"
                                                                                                     height="18" fill="none" xmlns:v="https://vecta.io/nano">
                                                                        <g clip-path="url(#A)" fill="#a61a1a">
                                                                            <path
                                                                                d="M16.308 4.023l-.397-1.2a1.11 1.11 0 0 0-1.053-.759h-3.34V.986c0-.544-.442-.986-.985-.986h-3.06c-.543 0-.985.442-.985.986v1.088h-3.34a1.11 1.11 0 0 0-1.053.759l-.397 1.2c-.1.27-.045.572.122.804s.438.37.723.37h.415l.914 11.306c.068.84.78 1.496 1.62 1.496h7.202c.84 0 1.553-.657 1.62-1.496l.914-11.306h.23a.9.9 0 0 0 .723-.37c.167-.23.213-.532.122-.804zm-8.765-2.97h2.92v1.02h-2.92v-1.02zm5.724 15.364c-.024.295-.274.527-.57.527H5.495c-.296 0-.546-.23-.57-.527l-.908-11.22h10.157l-.907 11.22zM2.77 4.143l.326-.977c.007-.023.028-.038.052-.038h11.7c.024 0 .045.015.052.038l.326.977H2.77zm8.816 12.237h.028c.28 0 .512-.218.526-.5l.495-9.506a.53.53 0 0 0-.499-.554c-.292-.016-.54.208-.554.5l-.495 9.506c-.015.3.208.54.5.554zm-5.694-.497a.53.53 0 0 0 .526.5l.03-.001a.53.53 0 0 0 .498-.555l-.52-9.506a.53.53 0 0 0-.555-.498c-.29.016-.514.264-.498.555l.52 9.506zm3.108.5a.53.53 0 0 0 .527-.527V6.35A.53.53 0 0 0 9 5.823c-.29 0-.527.236-.527.527v9.506a.53.53 0 0 0 .527.527z" />
                                                                        </g>
                                                                        <defs>
                                                                            <clipPath id="A">
                                                                                <path fill="#fff" d="M0 0h18v18H0z" />
                                                                            </clipPath>
                                                                        </defs>
                                                                    </svg>
                                                                </div>
                                                                <div class="card-control__name">Удалить анкету</div>
                                                            </a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="questionnaires__card">
                                <div class="profile-card">
                                    <div class="profile-card__header">
                                        <div class="profile-card__info">
                                            <div class="profile-card__personal"><a class="profile-card__name"
                                                                                   href="questionnaire.html">Соня</a>
                                                <div class="profile-card__separator"></div><a class="profile-card__phone"
                                                                                              href="tel:79681234567">+7 (968) 123-45-67</a>
                                            </div>
                                            <div class="profile-card__classification">Индивидуалка</div>
                                        </div>
                                        <div class="profile-card__block">
                                            <div class="profile-card__label">Активна</div>
                                            <div class="profile-card__rating">
                                                <div class="rating-static rating-static--four">
                                                    <div class="rating-static__star"></div>
                                                    <div class="rating-static__star"></div>
                                                    <div class="rating-static__star"></div>
                                                    <div class="rating-static__star"></div>
                                                    <div class="rating-static__star"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="profile-card__body">
                                        <div class="profile-card__content">
                                            <div class="profile-card__slider">
                                                <div class="swiper-container js-profile-card-slider">
                                                    <div class="swiper-wrapper">
                                                        <div class="swiper-slide"><img class="swiper-slide__img swiper-lazy"
                                                                                       data-src="./images/card-faf45814.jpg" alt="image">
                                                            <div class="swiper-lazy-preloader"></div>
                                                        </div>
                                                        <div class="swiper-slide"><img class="swiper-slide__img swiper-lazy"
                                                                                       data-src="./images/card-faf45814.jpg" alt="image">
                                                            <div class="swiper-lazy-preloader"></div>
                                                        </div>
                                                        <div class="swiper-slide"><img class="swiper-slide__img swiper-lazy"
                                                                                       data-src="./images/card-faf45814.jpg" alt="image">
                                                            <div class="swiper-lazy-preloader"></div>
                                                        </div>
                                                        <div class="swiper-slide"><img class="swiper-slide__img swiper-lazy"
                                                                                       data-src="./images/card-faf45814.jpg" alt="image">
                                                            <div class="swiper-lazy-preloader"></div>
                                                        </div>
                                                        <div class="swiper-slide"><img class="swiper-slide__img swiper-lazy"
                                                                                       data-src="./images/card-faf45814.jpg" alt="image">
                                                            <div class="swiper-lazy-preloader"></div>
                                                        </div>
                                                        <div class="swiper-slide"><img class="swiper-slide__img swiper-lazy"
                                                                                       data-src="./images/card-faf45814.jpg" alt="image">
                                                            <div class="swiper-lazy-preloader"></div>
                                                        </div>
                                                        <div class="swiper-slide"><img class="swiper-slide__img swiper-lazy"
                                                                                       data-src="./images/card-faf45814.jpg" alt="image">
                                                            <div class="swiper-lazy-preloader"></div>
                                                        </div>
                                                        <div class="swiper-slide"><img class="swiper-slide__img swiper-lazy"
                                                                                       data-src="./images/card-faf45814.jpg" alt="image">
                                                            <div class="swiper-lazy-preloader"></div>
                                                        </div>
                                                        <div class="swiper-slide"><img class="swiper-slide__img swiper-lazy"
                                                                                       data-src="./images/card-faf45814.jpg" alt="image">
                                                            <div class="swiper-lazy-preloader"></div>
                                                        </div>
                                                        <div class="swiper-slide"><img class="swiper-slide__img swiper-lazy"
                                                                                       data-src="./images/card-faf45814.jpg" alt="image">
                                                            <div class="swiper-lazy-preloader"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="swiper-pagination"></div>
                                            </div>
                                            <div class="profile-card__details">
                                                <table class="profile-card__table">
                                                    <tbody>
                                                    <tr>
                                                        <td>Страна</td>
                                                        <td>Россия</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Область</td>
                                                        <td>Московская</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Город</td>
                                                        <td>Москва</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Дата размещения</td>
                                                        <td>01.01.2020</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Просмотров</td>
                                                        <td>21031</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Поклонников</td>
                                                        <td>737</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Подарков</td>
                                                        <td><span>15</span></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Сообщений</td>
                                                        <td><span>90</span></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Комментариев</td>
                                                        <td><span>10</span></td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="profile-card__aside">
                                            <div class="profile-card__control">
                                                <div class="card-control">
                                                    <div class="card-control__header">
                                                        <div class="card-control__term">Осталось<span class="card-control__amount">8</span>дней
                                                        </div><a class="card-control__extend" href="#">Продлить</a>
                                                    </div>
                                                    <ul class="card-control__items">
                                                        <li class="card-control__item"><a class="card-control__link" href="#">
                                                                <div class="card-control__icon"><svg xmlns="http://www.w3.org/2000/svg" width="18"
                                                                                                     height="18" fill="none" xmlns:v="https://vecta.io/nano">
                                                                        <path
                                                                            d="M14.526.675H3.475L0 5.414l9.037 11.91L18 5.412 14.526.675zM4.94 5.934l2.546 7.607-5.77-7.607H4.94zm1.11 0h5.905l-2.932 8.88-2.973-8.88zm7.012 0h3.23l-5.754 7.648 2.525-7.648zm.93-4.207l2.314 3.155h-3.244l-1.035-3.155h1.966zm-3.073 0l1.035 3.155h-5.91L7.08 1.727h3.84zm-6.912 0h1.966L4.938 4.882H1.694l2.314-3.155z"
                                                                            fill="#f90" /></svg>
                                                                </div>
                                                                <div class="card-control__name">Купить VIP-статус</div>
                                                            </a></li>
                                                        <li class="card-control__item"><a class="card-control__link" href="#">
                                                                <div class="card-control__icon"><svg xmlns="http://www.w3.org/2000/svg"
                                                                                                     xmlns:xlink="http://www.w3.org/1999/xlink" width="18" height="18" fill="none"
                                                                                                     xmlns:v="https://vecta.io/nano">
                                                                        <g clip-path="url(#A)" fill="#1aa520">
                                                                            <path
                                                                                d="M14.554 5.262L9.382.146a.57.57 0 0 0-.762 0l-5.17 5.1c-.167.173-.22.427-.136.653.086.22.284.375.517.408h2.45v6.287c0 .286.23.518.516.518l.028-.001h4.355c.285.015.53-.204.544-.49l.001-.028V6.297h2.45c.233-.033.43-.19.517-.408a.6.6 0 0 0-.136-.626zm-3.375-.054c-.3 0-.544.11-.544.408v6.396H7.368V5.616c0-.3-.245-.408-.544-.408h-1.66L9 1.452l3.838 3.756h-1.66z" />
                                                                            <use xlink:href="#B" />
                                                                            <path
                                                                                d="M11.178 15.278H6.824c-.3 0-.544.244-.544.544s.244.544.544.544h4.355c.3 0 .544-.244.544-.544s-.244-.544-.544-.544zm0 1.632H6.824c-.3 0-.544.244-.544.544s.244.544.544.544h4.355c.3 0 .544-.244.544-.544s-.244-.544-.544-.544z" />
                                                                        </g>
                                                                        <defs>
                                                                            <clipPath id="A">
                                                                                <path fill="#fff" d="M0 0h18v18H0z" />
                                                                            </clipPath>
                                                                            <path id="B"
                                                                                  d="M11.178 13.645H6.824c-.3 0-.544.244-.544.544s.244.544.544.544h4.355c.3 0 .544-.244.544-.544s-.244-.544-.544-.544z" />
                                                                        </defs>
                                                                    </svg>
                                                                </div>
                                                                <div class="card-control__name">Поднять анкету в списке</div>
                                                            </a></li>
                                                        <li class="card-control__item"><a class="card-control__link" href="#">
                                                                <div class="card-control__icon"><svg xmlns="http://www.w3.org/2000/svg" width="18"
                                                                                                     height="18" fill="none" xmlns:v="https://vecta.io/nano">
                                                                        <g clip-path="url(#A)" fill="#5c5c5c">
                                                                            <path
                                                                                d="M16.613 11.308a.45.45 0 0 0-.448.448v3.98c-.001.742-.602 1.344-1.345 1.345H2.24c-.74-.001-1.343-.6-1.343-1.345V4.054C.897 3.312 1.5 2.7 2.24 2.7h3.98a.45.45 0 0 0 .448-.448.45.45 0 0 0-.448-.448H2.24A2.24 2.24 0 0 0 0 4.054v11.682a2.24 2.24 0 0 0 2.24 2.24h12.58a2.24 2.24 0 0 0 2.24-2.24v-3.98a.45.45 0 0 0-.448-.448zM16.884.66a2.02 2.02 0 0 0-2.853 0L6.033 8.657a.45.45 0 0 0-.115.197L4.866 12.65a.45.45 0 0 0 .552.552l3.797-1.052a.45.45 0 0 0 .197-.115L17.4 4.038a2.02 2.02 0 0 0 0-2.853L16.884.66zM7 8.948l6.546-6.546 2.1 2.1L9.12 11.06 7 8.948zm-.422.846l1.687 1.687-2.333.646.646-2.333zm10.188-6.4l-.475.475-2.1-2.1.476-.475a1.12 1.12 0 0 1 1.585 0l.526.526a1.12 1.12 0 0 1 0 1.585z" />
                                                                        </g>
                                                                        <defs>
                                                                            <clipPath id="A">
                                                                                <path fill="#fff" d="M0 0h18v18H0z" />
                                                                            </clipPath>
                                                                        </defs>
                                                                    </svg>
                                                                </div>
                                                                <div class="card-control__name">Редактировать</div>
                                                            </a></li>
                                                        <li class="card-control__item"><a class="card-control__link" href="#">
                                                                <div class="card-control__icon"><svg width="18" height="18" viewBox="0 0 18 18"
                                                                                                     fill="none" xmlns="http://www.w3.org/2000/svg">
                                                                        <g clip-path="url(#clip0)">
                                                                            <path
                                                                                d="M15.3625 2.6371C11.8462 -0.879258 6.15432 -0.879786 2.63747 2.6371C-0.878891 6.15346 -0.879419 11.8453 2.63747 15.3622C6.15382 18.8785 11.8457 18.879 15.3625 15.3622C18.8788 11.8458 18.8794 6.15399 15.3625 2.6371ZM8.99998 16.5195C4.85354 16.5195 1.48014 13.1461 1.48014 8.99961C1.48014 4.85317 4.85354 1.47981 8.99998 1.47981C13.1464 1.47981 16.5198 4.85321 16.5198 8.99965C16.5198 13.1461 13.1464 16.5195 8.99998 16.5195Z"
                                                                                fill="#A6811B" />
                                                                            <path
                                                                                d="M10.5008 5.63306C10.0926 5.63306 9.76172 5.96395 9.76172 6.37213V11.6278C9.76172 12.036 10.0926 12.3669 10.5008 12.3669C10.909 12.3669 11.2399 12.036 11.2399 11.6278V6.37213C11.2399 5.96395 10.909 5.63306 10.5008 5.63306Z"
                                                                                fill="#A6811B" />
                                                                            <path
                                                                                d="M7.49884 5.63306C7.09066 5.63306 6.75977 5.96395 6.75977 6.37213V11.6278C6.75977 12.036 7.09066 12.3669 7.49884 12.3669C7.90702 12.3669 8.23792 12.036 8.23792 11.6278V6.37213C8.23792 5.96395 7.90706 5.63306 7.49884 5.63306Z"
                                                                                fill="#A6811B" />
                                                                        </g>
                                                                        <defs>
                                                                            <clipPath id="clip0">
                                                                                <rect width="18" height="18" fill="white" />
                                                                            </clipPath>
                                                                        </defs>
                                                                    </svg>
                                                                </div>
                                                                <div class="card-control__name">Остановить публикацию</div>
                                                            </a></li>
                                                        <li class="card-control__item"><a class="card-control__link" href="#">
                                                                <div class="card-control__icon"><svg xmlns="http://www.w3.org/2000/svg" width="18"
                                                                                                     height="18" fill="none" xmlns:v="https://vecta.io/nano">
                                                                        <g clip-path="url(#A)" fill="#a61a1a">
                                                                            <path
                                                                                d="M16.308 4.023l-.397-1.2a1.11 1.11 0 0 0-1.053-.759h-3.34V.986c0-.544-.442-.986-.985-.986h-3.06c-.543 0-.985.442-.985.986v1.088h-3.34a1.11 1.11 0 0 0-1.053.759l-.397 1.2c-.1.27-.045.572.122.804s.438.37.723.37h.415l.914 11.306c.068.84.78 1.496 1.62 1.496h7.202c.84 0 1.553-.657 1.62-1.496l.914-11.306h.23a.9.9 0 0 0 .723-.37c.167-.23.213-.532.122-.804zm-8.765-2.97h2.92v1.02h-2.92v-1.02zm5.724 15.364c-.024.295-.274.527-.57.527H5.495c-.296 0-.546-.23-.57-.527l-.908-11.22h10.157l-.907 11.22zM2.77 4.143l.326-.977c.007-.023.028-.038.052-.038h11.7c.024 0 .045.015.052.038l.326.977H2.77zm8.816 12.237h.028c.28 0 .512-.218.526-.5l.495-9.506a.53.53 0 0 0-.499-.554c-.292-.016-.54.208-.554.5l-.495 9.506c-.015.3.208.54.5.554zm-5.694-.497a.53.53 0 0 0 .526.5l.03-.001a.53.53 0 0 0 .498-.555l-.52-9.506a.53.53 0 0 0-.555-.498c-.29.016-.514.264-.498.555l.52 9.506zm3.108.5a.53.53 0 0 0 .527-.527V6.35A.53.53 0 0 0 9 5.823c-.29 0-.527.236-.527.527v9.506a.53.53 0 0 0 .527.527z" />
                                                                        </g>
                                                                        <defs>
                                                                            <clipPath id="A">
                                                                                <path fill="#fff" d="M0 0h18v18H0z" />
                                                                            </clipPath>
                                                                        </defs>
                                                                    </svg>
                                                                </div>
                                                                <div class="card-control__name">Удалить анкету</div>
                                                            </a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="questionnaires__card">
                                <div class="profile-card">
                                    <div class="profile-card__header">
                                        <div class="profile-card__info">
                                            <div class="profile-card__personal"><a class="profile-card__name"
                                                                                   href="questionnaire.html">Соня</a>
                                                <div class="profile-card__separator"></div><a class="profile-card__phone"
                                                                                              href="tel:79681234567">+7 (968) 123-45-67</a>
                                            </div>
                                            <div class="profile-card__classification">Индивидуалка</div>
                                        </div>
                                        <div class="profile-card__block">
                                            <div class="profile-card__label">Активна</div>
                                            <div class="profile-card__rating">
                                                <div class="rating-static rating-static--four">
                                                    <div class="rating-static__star"></div>
                                                    <div class="rating-static__star"></div>
                                                    <div class="rating-static__star"></div>
                                                    <div class="rating-static__star"></div>
                                                    <div class="rating-static__star"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="profile-card__body">
                                        <div class="profile-card__content">
                                            <div class="profile-card__slider">
                                                <div class="swiper-container js-profile-card-slider">
                                                    <div class="swiper-wrapper">
                                                        <div class="swiper-slide"><img class="swiper-slide__img swiper-lazy"
                                                                                       data-src="./images/card-faf45814.jpg" alt="image">
                                                            <div class="swiper-lazy-preloader"></div>
                                                        </div>
                                                        <div class="swiper-slide"><img class="swiper-slide__img swiper-lazy"
                                                                                       data-src="./images/card-faf45814.jpg" alt="image">
                                                            <div class="swiper-lazy-preloader"></div>
                                                        </div>
                                                        <div class="swiper-slide"><img class="swiper-slide__img swiper-lazy"
                                                                                       data-src="./images/card-faf45814.jpg" alt="image">
                                                            <div class="swiper-lazy-preloader"></div>
                                                        </div>
                                                        <div class="swiper-slide"><img class="swiper-slide__img swiper-lazy"
                                                                                       data-src="./images/card-faf45814.jpg" alt="image">
                                                            <div class="swiper-lazy-preloader"></div>
                                                        </div>
                                                        <div class="swiper-slide"><img class="swiper-slide__img swiper-lazy"
                                                                                       data-src="./images/card-faf45814.jpg" alt="image">
                                                            <div class="swiper-lazy-preloader"></div>
                                                        </div>
                                                        <div class="swiper-slide"><img class="swiper-slide__img swiper-lazy"
                                                                                       data-src="./images/card-faf45814.jpg" alt="image">
                                                            <div class="swiper-lazy-preloader"></div>
                                                        </div>
                                                        <div class="swiper-slide"><img class="swiper-slide__img swiper-lazy"
                                                                                       data-src="./images/card-faf45814.jpg" alt="image">
                                                            <div class="swiper-lazy-preloader"></div>
                                                        </div>
                                                        <div class="swiper-slide"><img class="swiper-slide__img swiper-lazy"
                                                                                       data-src="./images/card-faf45814.jpg" alt="image">
                                                            <div class="swiper-lazy-preloader"></div>
                                                        </div>
                                                        <div class="swiper-slide"><img class="swiper-slide__img swiper-lazy"
                                                                                       data-src="./images/card-faf45814.jpg" alt="image">
                                                            <div class="swiper-lazy-preloader"></div>
                                                        </div>
                                                        <div class="swiper-slide"><img class="swiper-slide__img swiper-lazy"
                                                                                       data-src="./images/card-faf45814.jpg" alt="image">
                                                            <div class="swiper-lazy-preloader"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="swiper-pagination"></div>
                                            </div>
                                            <div class="profile-card__details">
                                                <table class="profile-card__table">
                                                    <tbody>
                                                    <tr>
                                                        <td>Страна</td>
                                                        <td>Россия</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Область</td>
                                                        <td>Московская</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Город</td>
                                                        <td>Москва</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Дата размещения</td>
                                                        <td>01.01.2020</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Просмотров</td>
                                                        <td>21031</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Поклонников</td>
                                                        <td>737</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Подарков</td>
                                                        <td><span>15</span></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Сообщений</td>
                                                        <td><span>90</span></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Комментариев</td>
                                                        <td><span>10</span></td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="profile-card__aside">
                                            <div class="profile-card__control">
                                                <div class="card-control">
                                                    <div class="card-control__header">
                                                        <div class="card-control__term">Осталось<span class="card-control__amount">8</span>дней
                                                        </div><a class="card-control__extend" href="#">Продлить</a>
                                                    </div>
                                                    <ul class="card-control__items">
                                                        <li class="card-control__item"><a class="card-control__link" href="#">
                                                                <div class="card-control__icon"><svg xmlns="http://www.w3.org/2000/svg" width="18"
                                                                                                     height="18" fill="none" xmlns:v="https://vecta.io/nano">
                                                                        <path
                                                                            d="M14.526.675H3.475L0 5.414l9.037 11.91L18 5.412 14.526.675zM4.94 5.934l2.546 7.607-5.77-7.607H4.94zm1.11 0h5.905l-2.932 8.88-2.973-8.88zm7.012 0h3.23l-5.754 7.648 2.525-7.648zm.93-4.207l2.314 3.155h-3.244l-1.035-3.155h1.966zm-3.073 0l1.035 3.155h-5.91L7.08 1.727h3.84zm-6.912 0h1.966L4.938 4.882H1.694l2.314-3.155z"
                                                                            fill="#f90" /></svg>
                                                                </div>
                                                                <div class="card-control__name">Купить VIP-статус</div>
                                                            </a></li>
                                                        <li class="card-control__item"><a class="card-control__link" href="#">
                                                                <div class="card-control__icon"><svg xmlns="http://www.w3.org/2000/svg"
                                                                                                     xmlns:xlink="http://www.w3.org/1999/xlink" width="18" height="18" fill="none"
                                                                                                     xmlns:v="https://vecta.io/nano">
                                                                        <g clip-path="url(#A)" fill="#1aa520">
                                                                            <path
                                                                                d="M14.554 5.262L9.382.146a.57.57 0 0 0-.762 0l-5.17 5.1c-.167.173-.22.427-.136.653.086.22.284.375.517.408h2.45v6.287c0 .286.23.518.516.518l.028-.001h4.355c.285.015.53-.204.544-.49l.001-.028V6.297h2.45c.233-.033.43-.19.517-.408a.6.6 0 0 0-.136-.626zm-3.375-.054c-.3 0-.544.11-.544.408v6.396H7.368V5.616c0-.3-.245-.408-.544-.408h-1.66L9 1.452l3.838 3.756h-1.66z" />
                                                                            <use xlink:href="#B" />
                                                                            <path
                                                                                d="M11.178 15.278H6.824c-.3 0-.544.244-.544.544s.244.544.544.544h4.355c.3 0 .544-.244.544-.544s-.244-.544-.544-.544zm0 1.632H6.824c-.3 0-.544.244-.544.544s.244.544.544.544h4.355c.3 0 .544-.244.544-.544s-.244-.544-.544-.544z" />
                                                                        </g>
                                                                        <defs>
                                                                            <clipPath id="A">
                                                                                <path fill="#fff" d="M0 0h18v18H0z" />
                                                                            </clipPath>
                                                                            <path id="B"
                                                                                  d="M11.178 13.645H6.824c-.3 0-.544.244-.544.544s.244.544.544.544h4.355c.3 0 .544-.244.544-.544s-.244-.544-.544-.544z" />
                                                                        </defs>
                                                                    </svg>
                                                                </div>
                                                                <div class="card-control__name">Поднять анкету в списке</div>
                                                            </a></li>
                                                        <li class="card-control__item"><a class="card-control__link" href="#">
                                                                <div class="card-control__icon"><svg xmlns="http://www.w3.org/2000/svg" width="18"
                                                                                                     height="18" fill="none" xmlns:v="https://vecta.io/nano">
                                                                        <g clip-path="url(#A)" fill="#5c5c5c">
                                                                            <path
                                                                                d="M16.613 11.308a.45.45 0 0 0-.448.448v3.98c-.001.742-.602 1.344-1.345 1.345H2.24c-.74-.001-1.343-.6-1.343-1.345V4.054C.897 3.312 1.5 2.7 2.24 2.7h3.98a.45.45 0 0 0 .448-.448.45.45 0 0 0-.448-.448H2.24A2.24 2.24 0 0 0 0 4.054v11.682a2.24 2.24 0 0 0 2.24 2.24h12.58a2.24 2.24 0 0 0 2.24-2.24v-3.98a.45.45 0 0 0-.448-.448zM16.884.66a2.02 2.02 0 0 0-2.853 0L6.033 8.657a.45.45 0 0 0-.115.197L4.866 12.65a.45.45 0 0 0 .552.552l3.797-1.052a.45.45 0 0 0 .197-.115L17.4 4.038a2.02 2.02 0 0 0 0-2.853L16.884.66zM7 8.948l6.546-6.546 2.1 2.1L9.12 11.06 7 8.948zm-.422.846l1.687 1.687-2.333.646.646-2.333zm10.188-6.4l-.475.475-2.1-2.1.476-.475a1.12 1.12 0 0 1 1.585 0l.526.526a1.12 1.12 0 0 1 0 1.585z" />
                                                                        </g>
                                                                        <defs>
                                                                            <clipPath id="A">
                                                                                <path fill="#fff" d="M0 0h18v18H0z" />
                                                                            </clipPath>
                                                                        </defs>
                                                                    </svg>
                                                                </div>
                                                                <div class="card-control__name">Редактировать</div>
                                                            </a></li>
                                                        <li class="card-control__item"><a class="card-control__link" href="#">
                                                                <div class="card-control__icon"><svg width="18" height="18" viewBox="0 0 18 18"
                                                                                                     fill="none" xmlns="http://www.w3.org/2000/svg">
                                                                        <g clip-path="url(#clip0)">
                                                                            <path
                                                                                d="M15.3625 2.6371C11.8462 -0.879258 6.15432 -0.879786 2.63747 2.6371C-0.878891 6.15346 -0.879419 11.8453 2.63747 15.3622C6.15382 18.8785 11.8457 18.879 15.3625 15.3622C18.8788 11.8458 18.8794 6.15399 15.3625 2.6371ZM8.99998 16.5195C4.85354 16.5195 1.48014 13.1461 1.48014 8.99961C1.48014 4.85317 4.85354 1.47981 8.99998 1.47981C13.1464 1.47981 16.5198 4.85321 16.5198 8.99965C16.5198 13.1461 13.1464 16.5195 8.99998 16.5195Z"
                                                                                fill="#A6811B" />
                                                                            <path
                                                                                d="M10.5008 5.63306C10.0926 5.63306 9.76172 5.96395 9.76172 6.37213V11.6278C9.76172 12.036 10.0926 12.3669 10.5008 12.3669C10.909 12.3669 11.2399 12.036 11.2399 11.6278V6.37213C11.2399 5.96395 10.909 5.63306 10.5008 5.63306Z"
                                                                                fill="#A6811B" />
                                                                            <path
                                                                                d="M7.49884 5.63306C7.09066 5.63306 6.75977 5.96395 6.75977 6.37213V11.6278C6.75977 12.036 7.09066 12.3669 7.49884 12.3669C7.90702 12.3669 8.23792 12.036 8.23792 11.6278V6.37213C8.23792 5.96395 7.90706 5.63306 7.49884 5.63306Z"
                                                                                fill="#A6811B" />
                                                                        </g>
                                                                        <defs>
                                                                            <clipPath id="clip0">
                                                                                <rect width="18" height="18" fill="white" />
                                                                            </clipPath>
                                                                        </defs>
                                                                    </svg>
                                                                </div>
                                                                <div class="card-control__name">Остановить публикацию</div>
                                                            </a></li>
                                                        <li class="card-control__item"><a class="card-control__link" href="#">
                                                                <div class="card-control__icon"><svg xmlns="http://www.w3.org/2000/svg" width="18"
                                                                                                     height="18" fill="none" xmlns:v="https://vecta.io/nano">
                                                                        <g clip-path="url(#A)" fill="#a61a1a">
                                                                            <path
                                                                                d="M16.308 4.023l-.397-1.2a1.11 1.11 0 0 0-1.053-.759h-3.34V.986c0-.544-.442-.986-.985-.986h-3.06c-.543 0-.985.442-.985.986v1.088h-3.34a1.11 1.11 0 0 0-1.053.759l-.397 1.2c-.1.27-.045.572.122.804s.438.37.723.37h.415l.914 11.306c.068.84.78 1.496 1.62 1.496h7.202c.84 0 1.553-.657 1.62-1.496l.914-11.306h.23a.9.9 0 0 0 .723-.37c.167-.23.213-.532.122-.804zm-8.765-2.97h2.92v1.02h-2.92v-1.02zm5.724 15.364c-.024.295-.274.527-.57.527H5.495c-.296 0-.546-.23-.57-.527l-.908-11.22h10.157l-.907 11.22zM2.77 4.143l.326-.977c.007-.023.028-.038.052-.038h11.7c.024 0 .045.015.052.038l.326.977H2.77zm8.816 12.237h.028c.28 0 .512-.218.526-.5l.495-9.506a.53.53 0 0 0-.499-.554c-.292-.016-.54.208-.554.5l-.495 9.506c-.015.3.208.54.5.554zm-5.694-.497a.53.53 0 0 0 .526.5l.03-.001a.53.53 0 0 0 .498-.555l-.52-9.506a.53.53 0 0 0-.555-.498c-.29.016-.514.264-.498.555l.52 9.506zm3.108.5a.53.53 0 0 0 .527-.527V6.35A.53.53 0 0 0 9 5.823c-.29 0-.527.236-.527.527v9.506a.53.53 0 0 0 .527.527z" />
                                                                        </g>
                                                                        <defs>
                                                                            <clipPath id="A">
                                                                                <path fill="#fff" d="M0 0h18v18H0z" />
                                                                            </clipPath>
                                                                        </defs>
                                                                    </svg>
                                                                </div>
                                                                <div class="card-control__name">Удалить анкету</div>
                                                            </a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('includes._bottom-banner', ['wrapperClass' => 'bottom-banner--transparent'])
@endsection
