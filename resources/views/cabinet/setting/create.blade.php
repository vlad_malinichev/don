@extends('layouts.cabinet')

@section('title')
    Чёрный список
@endsection

@section('content')
    <div class="cabinet-header">
        <div class="cabinet-header__container container">
            <div class="cabinet-header__body">
                <div class="cabinet-header__title">Плохие клиенты</div>
                <div class="cabinet-header__balance">
                    <div class="cabinet-balance">
                        <div class="cabinet-balance__info"><img class="cabinet-balance__icon" src="./images/money-13806401.svg"
                                                                alt="currency icon">
                            <div class="cabinet-balance__item">
                                <div class="cabinet-balance__title">Ваш счёт:</div>
                                <div class="cabinet-balance__score"><span class="cabinet-balance__value">170 </span>токенов</div>
                            </div>
                        </div><a class="cabinet-balance__button button" href="#">Пополнить баланс</a>
                    </div>
                </div>
            </div>
            <div class="cabinet-header__navigation">
                <div class="cabinet-navigation">
                    <ul class="cabinet-navigation__items">
                        <li class="cabinet-navigation__item cabinet-navigation__item--active"><a
                                class="cabinet-navigation__link" href="/questionnaire.html"><span
                                    class="cabinet-navigation__title">Анкеты</span><span
                                    class="cabinet-navigation__value">4</span></a></li>
                        <li class="cabinet-navigation__item"><a class="cabinet-navigation__link" href="banners.html"><span
                                    class="cabinet-navigation__title">Рекламные баннеры</span><span
                                    class="cabinet-navigation__value">4</span></a></li>
                        <li class="cabinet-navigation__item"><a class="cabinet-navigation__link" href="/messenger.html"><span
                                    class="cabinet-navigation__title">Сообщения</span><span
                                    class="cabinet-navigation__value">562</span></a></li>
                        <li class="cabinet-navigation__item"><a class="cabinet-navigation__link" href="favorites.html"><span
                                    class="cabinet-navigation__title">Избранное</span><span
                                    class="cabinet-navigation__value">38</span></a></li>
                        <li class="cabinet-navigation__item"><a class="cabinet-navigation__link" href="gifts.html"><span
                                    class="cabinet-navigation__title">Подарки</span><span
                                    class="cabinet-navigation__value">35</span></a></li>
                        <li class="cabinet-navigation__item"><a class="cabinet-navigation__link" href="/reviews-get.html"><span
                                    class="cabinet-navigation__title">Отзывы</span><span
                                    class="cabinet-navigation__value">90</span></a></li>
                        <li class="cabinet-navigation__item"><a class="cabinet-navigation__link" href="blacklist.html"><span
                                    class="cabinet-navigation__title">Плохие клиенты</span></a></li>
                    </ul>
                </div>
            </div>
            <div class="cabinet-header__dropdown">
                <select class="cabinet-dropdown js-cabinet-navigation" name="menu">
                    <option value="Анкеты">Анкеты</option>
                    <option value="Рекламные баннеры">Рекламные баннеры</option>
                    <option value="Сообщения">Сообщения</option>
                    <option value="Избранное">Избранное</option>
                    <option value="Подарки">Подарки</option>
                    <option value="Отзывы">Отзывы</option>
                    <option value="Плохие клиенты">Плохие клиенты</option>
                </select>
            </div>
        </div>
    </div>
    <div class="template">
        <div class="template__container container">
            <div class="template__body">
                <div class="template__sidebar">
                    <aside class="sidebar">
                        <div class="sidebar__wrap">
                            <ul class="sidebar__list">
                                <li class="sidebar__item sidebar__item--active">
                                    <a class="sidebar__link js-btn-screen" href="{{ url('/cabinet/blacklist') }}">
                                        Список плохих клиентов <span class="sidebar__value">21</span>
                                    </a>
                                </li>
                                <li class="sidebar__item">
                                    <a class="sidebar__link js-btn-screen" href="{{ url('/cabinet/blacklist/my') }}">
                                        Мои плохие клиенты <span class="sidebar__value">4</span>
                                    </a>
                                </li>
                                <li class="sidebar__item">
                                    <a class="sidebar__link js-btn-screen" href="{{ url('/cabinet/blacklist/create') }}">
                                        Добавить плохого клиента
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </aside>
                </div>
                <div class="template__content">
                    <form class="add-blacklist" action="/">
                        <fieldset class="add-blacklist__fieldset">
                            <legend class="add-blacklist__title">Основная информация</legend>
                            <div class="add-blacklist__items">
                                <div class="input add-blacklist__item">
                                    <label class="input__label" for="gender">Пол </label>
                                    <input class="input__control" id="gender" type="text" name="gender" value="Мужской" placeholder=""
                                           onblur="placeholder=''" onfocus="placeholder=''" autocomplete="on" />
                                </div>
                                <div class="input add-blacklist__item">
                                    <label class="input__label" for="phone">Телефон </label>
                                    <input class="input__control" id="phone" type="tel" name="phone" value="+7 (968) 123-45-67"
                                           placeholder="" onblur="placeholder=''" onfocus="placeholder=''" autocomplete="on" />
                                </div>
                                <div class="add-blacklist__item">
                                    <div class="add-blacklist__label">Категория</div>
                                    <select class="add-blacklist__select js-select" name="category">
                                        <option value="Очень проблемный">Очень проблемный</option>
                                        <option value="Проблемный">Проблемный</option>
                                    </select>
                                </div>
                            </div>
                        </fieldset>
                        <fieldset class="add-blacklist__fieldset">
                            <legend class="add-blacklist__title">Расположение</legend>
                            <div class="add-blacklist__items">
                                <div class="add-blacklist__item">
                                    <div class="add-blacklist__label">Страна</div>
                                    <select class="add-blacklist__select js-select" name="country">
                                        <option value="Россия">Россия</option>
                                        <option value="Украина">Украина</option>
                                    </select>
                                </div>
                                <div class="add-blacklist__item">
                                    <div class="add-blacklist__label">Область</div>
                                    <select class="add-blacklist__select js-select" name="region">
                                        <option value="Московская">Московская</option>
                                        <option value="Ростовская">Ростовская</option>
                                    </select>
                                </div>
                                <div class="add-blacklist__item">
                                    <div class="add-blacklist__label">Город</div>
                                    <select class="add-blacklist__select js-select" name="city">
                                        <option value="Москва">Москва</option>
                                        <option value="Ростов">Ростов</option>
                                    </select>
                                </div>
                            </div>
                        </fieldset>
                        <fieldset class="add-blacklist__fieldset">
                            <legend class="add-blacklist__title">Комментарий</legend>
                            <textarea class="add-blacklist__textarea" name="message" maxlength="1000"></textarea><span
                                class="add-blacklist__length">не более 1000 символов</span>
                            <div class="add-blacklist__actions">
                                <button class="add-blacklist__button button" type="submit">Добавить</button>
                                <button class="add-blacklist__reset" type="reset">Очистить данные</button>
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>
    @include('includes._bottom-banner', ['wrapperClass' => 'bottom-banner--transparent'])
@endsection
