@extends('layouts.cabinet')

@section('title')
    Настройки
@endsection

@section('content')
    @include('cabinet._header', ['title' => 'Настройки'])
    <div class="template">
        <div class="template__container container">
            <div class="template__body">
                <div class="template__sidebar">
                    <aside class="sidebar">
                        <div class="sidebar__wrap">
                            <ul class="sidebar__list">
                                <li class="sidebar__item"><a class="sidebar__link js-btn-screen" href="payment-history.html">История
                                        платежей</a></li>
                                <li class="sidebar__item"><a class="sidebar__link js-btn-screen"
                                                             href="balance-replenishment.html">Пополнение баланса</a></li>
                                <li class="sidebar__item sidebar__item--active"><a class="sidebar__link js-btn-screen"
                                                                                   href="#">Настройки</a></li>
                            </ul>
                        </div>
                    </aside>
                </div>
                <div class="template__content">
                    <div class="settings js-settings">
                        <div class="settings__select">
                            <select class="js-settings-tabs">
                                <option value="Личные данные">Личные данные</option>
                                <option value="Номера телефонов">Номера телефонов</option>
                                <option value="Безопасность и вход">Безопасность и вход</option>
                                <option value="Уведомления">Уведомления</option>
                            </select>
                        </div>
                        <ul class="settings__tabs">
                            <li class="settings__tab settings__tab--active">Личные данные</li>
                            <li class="settings__tab">Номера телефонов</li>
                            <li class="settings__tab">Безопасность и вход</li>
                            <li class="settings__tab">Уведомления</li>
                        </ul>
                        <div class="settings__items">
                            <div class="settings__item settings__item--active">
                                <form class="settings-personal" action="/">
                                    <div class="settings-personal__cols">
                                        <div class="settings-personal__col">
                                            <div class="settings-personal__item">
                                                <label class="settings-personal__file profile-picture">
                                                    <input class="profile-picture__input" type="file" accept="image/*" name="image"><span
                                                        class="profile-picture__title">Изображение профиля</span><span
                                                        class="profile-picture__avatar"><span class="profile-picture__camera"></span><img
                                                            class="profile-picture__img" src="./images/pixel-8ecf1ef3.png" alt="avatar"></span>
                                                </label>
                                            </div>
                                            <div class="settings-personal__item">
                                                <div class="input settings-personal__input">
                                                    <label class="input__label" for="name">Имя (логин) </label>
                                                    <input class="input__control" id="name" type="text" name="name" value="Анжелика"
                                                           placeholder="" onblur="placeholder=''" onfocus="placeholder=''" autocomplete="on" />
                                                </div>
                                            </div>
                                            <div class="settings-personal__item">
                                                <div class="settings-personal__label">Пол</div>
                                                <div class="settings-personal__group">
                                                    <div class="settings-personal__gender">
                                                        <input type="radio" name="gender"><span>Женский</span>
                                                    </div>
                                                    <div class="settings-personal__gender">
                                                        <input type="radio" name="gender"><span>Мужской</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="settings-personal__item">
                                                <div class="input settings-personal__input">
                                                    <label class="input__label" for="email">E-mail <div class="input__label-icon"
                                                                                                        style="background-image: url(./images/check-cc671a49.svg);"></div>
                                                    </label>
                                                    <input class="input__control" id="email" type="text" name="email" placeholder=""
                                                           onblur="placeholder=''" onfocus="placeholder=''" autocomplete="on" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="settings-personal__col">
                                            <div class="settings-personal__item">
                                                <div class="input settings-personal__input">
                                                    <label class="input__label" for="phone">Основной номер телефона <div
                                                            class="input__label-icon" style="background-image: url(./images/check-cc671a49.svg);">
                                                        </div>
                                                    </label>
                                                    <input class="input__control" id="phone" type="text" name="phone" placeholder=""
                                                           onblur="placeholder=''" onfocus="placeholder=''" autocomplete="on" />
                                                </div>
                                            </div>
                                            <div class="settings-personal__item">
                                                <div class="settings-personal__label">Страна</div>
                                                <div class="settings-personal__select">
                                                    <select class="js-select" name="country">
                                                        <option value="Россия">Россия</option>
                                                        <option value="Украина">Украина</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="settings-personal__item">
                                                <div class="settings-personal__label">Город</div>
                                                <div class="settings-personal__select">
                                                    <select class="js-select" name="city">
                                                        <option value="Москва">Москва</option>
                                                        <option value="Ростов">Ростов</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <button class="settings-personal__button button" type="submit">Сохранить</button>
                                </form>
                            </div>
                            <div class="settings__item">
                                <form class="settings-phones" action="/">
                                    <div class="settings-phones__content">
                                        <div class="settings-phones__items">
                                            <div class="settings-phones__item">
                                                <div class="settings-phones__input">
                                                    <div class="input input--icon">
                                                        <label class="input__label" for="phones-1">Номер телефона </label>
                                                        <div class="input__field">
                                                            <input class="input__control" id="phones-1" type="tel" name="phones-1" placeholder=""
                                                                   onblur="placeholder=''" onfocus="placeholder=''" autocomplete="on" />
                                                            <div class="input__icon js-delete"
                                                                 style="background-image: url(./images/delete-5f46e6ea.svg);"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="settings-phones__item">
                                                <div class="settings-phones__input">
                                                    <div class="input input--icon">
                                                        <label class="input__label" for="phones-2">Номер телефона </label>
                                                        <div class="input__field">
                                                            <input class="input__control" id="phones-2" type="tel" name="phones-2" placeholder=""
                                                                   onblur="placeholder=''" onfocus="placeholder=''" autocomplete="on" />
                                                            <div class="input__icon js-delete"
                                                                 style="background-image: url(./images/delete-5f46e6ea.svg);"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <button class="settings-phones__button button js-add-phone" type="button">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="12" height="12" viewbox="0 0 12 12"
                                                 fill="none">
                                                <path d="M6 10.9095V1.09131V6.14916" stroke="#171717" stroke-width="2"
                                                      stroke-linecap="round" stroke-linejoin="round"></path>
                                                <path d="M1.0918 6.00049H10.91H6.00089" stroke="#171717" stroke-width="2"
                                                      stroke-linecap="round" stroke-linejoin="round"></path>
                                            </svg>Добавить номер телефона </button>
                                    </div>
                                    <div class="settings-phones__block">
                                        <div class="settings-phones__new">
                                            <div class="phones-form">
                                                <div class="phones-form__header">
                                                    <h2 class="phones-form__title">Добавление нового номера</h2>
                                                </div>
                                                <div class="phones-form__body">
                                                    <div class="phones-form__input">
                                                        <div class="input input--icon">
                                                            <label class="input__label" for="phones-3">Номер телефона </label>
                                                            <div class="input__field">
                                                                <input class="input__control" id="phones-3" type="tel" name="phones-3"
                                                                       placeholder="" onblur="placeholder=''" onfocus="placeholder=''"
                                                                       autocomplete="on" />
                                                                <div class="input__icon js-delete"
                                                                     style="background-image: url(./images/check-cc671a49.svg);"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="phones-form__footer">
                                                    <button class="phones-form__button button" type="submit">Отправить код
                                                        подтверждения</button>
                                                    <button class="phones-form__reset" type="reset">Отменить</button>
                                                </div>
                                            </div>
                                        </div><br>
                                        <div class="settings-phones__confirm">
                                            <div class="phones-form">
                                                <div class="phones-form__header">
                                                    <h2 class="phones-form__title">Подтверждение номера</h2>
                                                </div>
                                                <div class="phones-form__body"><span class="phones-form__info"> Текстовое сообщение с кодом
																подтверждения отправлено на номер <a class="phones-form__tel" href="tel:79681112233">+7
																	(968) 111-22-33</a></span><span class="phones-form__subtitle">Введите код</span>
                                                    <div class="phones-form__codes">
                                                        <div class="input phones-form__code">
                                                            <input class="input__control" type="number" name="code" placeholder="1"
                                                                   onblur="placeholder='1'" onfocus="placeholder=''" autocomplete="on" />
                                                        </div>
                                                        <div class="input phones-form__code">
                                                            <input class="input__control" type="number" name="code" placeholder="1"
                                                                   onblur="placeholder='1'" onfocus="placeholder=''" autocomplete="on" />
                                                        </div>
                                                        <div class="input phones-form__code">
                                                            <input class="input__control" type="number" name="code" placeholder="1"
                                                                   onblur="placeholder='1'" onfocus="placeholder=''" autocomplete="on" />
                                                        </div>
                                                        <div class="input phones-form__code">
                                                            <input class="input__control" type="number" name="code" placeholder="1"
                                                                   onblur="placeholder='1'" onfocus="placeholder=''" autocomplete="on" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="phones-form__footer">
                                                    <button class="phones-form__button button" type="submit">Подтвердить</button><a
                                                        class="phones-form__link link" href="#">Отправить код ещё раз</a>
                                                    <button class="phones-form__reset" type="reset">Отменить</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="settings__item">
                                <form class="settings-security" action="/">
                                    <div class="settings-security__group">
                                        <div class="input settings-security__input">
                                            <label class="input__label" for="old_password">Старый пароль </label>
                                            <div class="input__field">
                                                <input class="input__control" id="old_password" type="password" name="old_password"
                                                       placeholder="" onblur="placeholder=''" onfocus="placeholder=''" autocomplete="on" />
                                                <div class="input__eye js-show-password"></div>
                                            </div>
                                        </div>
                                        <div class="input settings-security__input">
                                            <label class="input__label" for="new_password">Новый пароль </label>
                                            <div class="input__field">
                                                <input class="input__control" id="new_password" type="password" name="new_password"
                                                       placeholder="" onblur="placeholder=''" onfocus="placeholder=''" autocomplete="on" />
                                                <div class="input__eye js-show-password"></div>
                                            </div>
                                        </div>
                                        <div class="input settings-security__input">
                                            <label class="input__label" for="confirm_password">Подтвердите новый пароль </label>
                                            <div class="input__field">
                                                <input class="input__control" id="confirm_password" type="password" name="confirm_password"
                                                       placeholder="" onblur="placeholder=''" onfocus="placeholder=''" autocomplete="on" />
                                                <div class="input__eye js-show-password"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="settings-security__actions">
                                        <button class="settings-security__button button" type="submit">Обновить пароль</button><a
                                            class="settings-security__link link" href="#">Восстановление пароля</a>
                                    </div>
                                </form>
                            </div>
                            <div class="settings__item">
                                <form class="settings-notice" action="/">
                                    <div class="settings-notice__items">
                                        <div class="settings-notice__item">
                                            <label class="settings-notice__checkbox checkbox">
                                                <input class="checkbox__input" type="checkbox" name="message" checked><span
                                                    class="checkbox__label">Получать уведомления на почту о новых сообщениях</span>
                                            </label>
                                        </div>
                                        <div class="settings-notice__item">
                                            <label class="settings-notice__checkbox checkbox">
                                                <input class="checkbox__input" type="checkbox" name="gift" checked><span
                                                    class="checkbox__label">Получать уведомления на почту о новых подарках</span>
                                            </label>
                                        </div>
                                        <div class="settings-notice__item">
                                            <label class="settings-notice__checkbox checkbox">
                                                <input class="checkbox__input" type="checkbox" name="review" checked><span
                                                    class="checkbox__label">Получать уведомления на почту о новых отзывах</span>
                                            </label>
                                        </div>
                                        <div class="settings-notice__item">
                                            <label class="settings-notice__checkbox checkbox">
                                                <input class="checkbox__input" type="checkbox" name="favorites" checked><span
                                                    class="checkbox__label">Получать уведомления на почту о новых в избранном</span>
                                            </label>
                                        </div>
                                    </div>
                                    <button class="settings-notice__button button" type="submit">Сохранить</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('includes._bottom-banner', ['wrapperClass' => 'bottom-banner--transparent'])
@endsection
