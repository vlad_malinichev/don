<div class="cabinet-header">
    <div class="cabinet-header__container container">
        <div class="cabinet-header__body">
            <div class="cabinet-header__title">{{ $title ?? 'Название' }}</div>
            <div class="cabinet-header__balance">
                <div class="cabinet-balance">
                    <div class="cabinet-balance__info">
                        <img class="cabinet-balance__icon" src="/images/money-13806401.svg" alt="currency icon">
                        <div class="cabinet-balance__item">
                            <div class="cabinet-balance__title">Ваш счёт:</div>
                            <div class="cabinet-balance__score"><span class="cabinet-balance__value">170 </span>токенов
                            </div>
                        </div>
                    </div>
                    <a class="cabinet-balance__button button" href="#">Пополнить баланс</a>
                </div>
            </div>
        </div>
        <div class="cabinet-header__navigation">
            <div class="cabinet-navigation">
                <ul class="cabinet-navigation__items">
                    <li class="cabinet-navigation__item cabinet-navigation__item--active">
                        <a class="cabinet-navigation__link" href="{{ url('cabinet/questionnaire') }}">
                            <span class="cabinet-navigation__title">Анкеты</span>
                            <span class="cabinet-navigation__value">4</span>
                        </a>
                    </li>
                    <li class="cabinet-navigation__item">
                        <a class="cabinet-navigation__link" href="{{ url('cabinet/banner') }}">
                            <span class="cabinet-navigation__title">Рекламные баннеры</span>
                            <span class="cabinet-navigation__value">4</span>
                        </a>
                    </li>
                    <li class="cabinet-navigation__item">
                        <a class="cabinet-navigation__link" href="{{ url('cabinet/messenger') }}">
                            <span class="cabinet-navigation__title">Сообщения</span>
                            <span class="cabinet-navigation__value">562</span>
                        </a>
                    </li>
                    <li class="cabinet-navigation__item">
                        <a class="cabinet-navigation__link" href="{{ url('cabinet/favorite') }}">
                            <span class="cabinet-navigation__title">Избранное</span>
                            <span class="cabinet-navigation__value">38</span>
                        </a>
                    </li>
                    <li class="cabinet-navigation__item">
                        <a class="cabinet-navigation__link" href="{{ url('cabinet/gift') }}">
                            <span class="cabinet-navigation__title">Подарки</span>
                            <span class="cabinet-navigation__value">35</span>
                        </a>
                    </li>
                    <li class="cabinet-navigation__item">
                        <a class="cabinet-navigation__link" href="{{ url('cabinet/review') }}">
                            <span class="cabinet-navigation__title">Отзывы</span>
                            <span class="cabinet-navigation__value">90</span>
                        </a>
                    </li>
                    <li class="cabinet-navigation__item">
                        <a class="cabinet-navigation__link" href="{{ url('cabinet/blacklist') }}">
                            <span class="cabinet-navigation__title">Плохие клиенты</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="cabinet-header__dropdown">
            <select class="cabinet-dropdown js-cabinet-navigation" name="menu">
                <option value="Анкеты">Анкеты</option>
                <option value="Рекламные баннеры">Рекламные баннеры</option>
                <option value="Сообщения">Сообщения</option>
                <option value="Избранное">Избранное</option>
                <option value="Подарки">Подарки</option>
                <option value="Отзывы">Отзывы</option>
                <option value="Плохие клиенты">Плохие клиенты</option>
            </select>
        </div>
    </div>
</div>
