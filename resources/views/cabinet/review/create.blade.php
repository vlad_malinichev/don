@extends('layouts.cabinet')

@section('title')
    Добавление отзыва
@endsection

@section('content')
    @include('cabinet._header', ['title' => 'Добавить отзыв'])
    <div class="new-questionnaire">
        <div class="template template--new-questionnaire">
            <div class="container">
                <div class="template__body">
                    <div class="template__sidebar">
                        <aside class="sidebar">
                            <div class="sidebar__wrap">
                                <ul class="sidebar__list">
                                    <li class="sidebar__item sidebar__item--active"><a class="sidebar__link js-btn-screen" href="/">Мои анкеты</a></li>
                                    <li class="sidebar__item"><a class="sidebar__link js-btn-screen" href="{{ route('questionnaire.create') }}">Добавить анкету</a></li>
                                </ul>
                            </div>
                        </aside>
                    </div>
                    <div class="template__content">
                        <form class="questionnaire-form js-form" action="{{ route('questionnaire.store') }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <h2 class="questionnaire-form__title">Новая анкета</h2>
                            <div class="questionnaire-form__wrapper">
                                <div class="questionnaire-form__part">
                                    <div class="questionnaire-form__name">Основная информация</div>
                                    <div class="questionnaire-form__group">
                                        <div class="questionnaire-form__place questionnaire-form__place--width">
                                            <div class="questionnaire-form__label">Тип анкеты *</div>
                                            <select class="js-select-form questionnaire-form__select" data-placeholder="Тип анкеты" name="type_id" required>
                                                <option value=""></option>
                                                <option value="1">Индивидуалка</option>
                                                <option value="2">Индивидуалка</option>
                                            </select>
                                        </div>
                                        <div class="questionnaire-form__place">
                                            <div class="input">
                                                <label class="input__label" for="name">Имя/название
                                                </label>
                                                <input class="input__control" id="name" type="text" name="name" required placeholder="Например, Константин" onblur="placeholder='Например, Константин'" onfocus="placeholder=''" autocomplete="on"/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="questionnaire-form__group questionnaire-form__group--align">
                                        <div class="questionnaire-form__place questionnaire-form__place--width">
                                            <div class="input">
                                                <label class="input__label" for="phone">Телефон
                                                </label>
                                                <input class="input__control" id="phone" type="text" name="phone" required placeholder="" onblur="placeholder=''" onfocus="placeholder=''" autocomplete="on"/>
                                            </div>
                                        </div>
                                        <div class="questionnaire-form__place">
                                            <div class="questionnaire-form__checkbox checkbox questionnaire-form__checkbox--icon">
                                                <input class="checkbox__input" id="viber" type="checkbox" value="1" name="viber">
                                                <label class="checkbox__label" for="viber">Viber</label>
                                            </div>
                                            <div class="questionnaire-form__checkbox checkbox questionnaire-form__checkbox--icon questionnaire-form__checkbox--whatsapp">
                                                <input class="checkbox__input" id="whatsapp" type="checkbox" value="1" name="whatsapp">
                                                <label class="checkbox__label" for="whatsapp">WhatsApp</label>
                                            </div>
                                            <div class="questionnaire-form__checkbox checkbox questionnaire-form__checkbox--icon questionnaire-form__checkbox--telegram">
                                                <input class="checkbox__input" id="telegram" type="checkbox" value="1" name="telegram">
                                                <label class="checkbox__label" for="telegram">Telegram</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="questionnaire-form__group">
                                        <div class="questionnaire-form__place">
                                            <div class="questionnaire-form__label">Время для звонков</div>
                                            <div class="questionnaire-form__clock">
                                                <input class="js-input-hidden" type="hidden" name="time">
                                                <div class="questionnaire-form__clock-group js-time">
                                                    <label>с</label>
                                                    <input name="call_time_from" id="callTimeFromHidden" type="hidden" value="">
                                                    <input
                                                        class="questionnaire-form__input-time questionnaire-form__input-time--hours js-time-input js-time-hours-with"
                                                        type="number"
                                                        id="callTimeFromHour"
                                                        value="10"
                                                        max="23"
                                                        onchange="document.getElementById('callTimeFromHidden').value = (document.getElementById('callTimeFromHour').value ?? '00') + ':' + (document.getElementById('callTimeFromMinute').value ?? '00');"
                                                    >
                                                    <input
                                                        class="questionnaire-form__input-time questionnaire-form__input-time--minutes js-time-input js-time-minutes-with"
                                                        type="number"
                                                        id="callTimeFromMinute"
                                                        value="00"
                                                        max="59"
                                                        onchange="document.getElementById('callTimeFromHidden').value = (document.getElementById('callTimeFromHour').value ?? '00') + ':' + (document.getElementById('callTimeFromMinute').value ?? '00');"
                                                    >
                                                </div>
                                                <div class="questionnaire-form__clock-group js-time">
                                                    <label>до</label>
                                                    <input name="call_time_to" id="callTimeToHidden" type="hidden" value="">
                                                    <input
                                                        class="questionnaire-form__input-time questionnaire-form__input-time--hours js-time-input js-time-hours-to"
                                                        type="number"
                                                        id="callTimeToHour"
                                                        value="20"
                                                        max="23"
                                                        oninput="document.getElementById('callTimeToHidden').value = (document.getElementById('callTimeToHour').value ?? '00') + ':' + (document.getElementById('callTimeToMinute').value ?? '00');"
                                                    >
                                                    <input
                                                        class="questionnaire-form__input-time questionnaire-form__input-time--minutes js-time-input js-time-minutes-to"
                                                        type="number"
                                                        id="callTimeToMinute"
                                                        value="00"
                                                        max="59"
                                                        oninput="document.getElementById('callTimeToHidden').value = (document.getElementById('callTimeToHour').value ?? '00') + ':' + (document.getElementById('callTimeToMinute').value ?? '00');"
                                                    >
                                                </div>
                                                <div class="questionnaire-form__checkbox checkbox js-time-disabled">
                                                    <input class="checkbox__input" id="around-clock" type="checkbox" value="1" name="is_around_clock">
                                                    <label class="checkbox__label" for="around-clock">Круглосуточно</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="questionnaire-form__part">
                                    <div class="questionnaire-form__name">Расположение</div>
                                    <div class="questionnaire-form__selects">
                                        <div class="questionnaire-form__place questionnaire-form__place--width">
                                            <div class="questionnaire-form__label">Страна</div>
                                            <select class="js-select-form questionnaire-form__select" data-placeholder="Страна" name="country_id" required>
                                                <option value=""></option>
                                                <option value="1">Украина</option>
                                                <option value="2">Россия</option>
                                            </select>
                                        </div>
                                        <div class="questionnaire-form__place questionnaire-form__place--width">
                                            <div class="questionnaire-form__label">Область</div>
                                            <select class="js-select-form questionnaire-form__select" data-placeholder="Область" name="region_id" required>
                                                <option value=""></option>
                                                <option value="1">Московская</option>
                                                <option value="2">Ростовская</option>
                                            </select>
                                        </div>
                                        <div class="questionnaire-form__place questionnaire-form__place--width">
                                            <div class="questionnaire-form__label">Город</div>
                                            <select class="js-select-form questionnaire-form__select" data-placeholder="Город" name="city_id" required>
                                                <option value=""></option>
                                                <option value="1">Москва</option>
                                                <option value="2">Ростов</option>
                                            </select>
                                        </div>
                                        <div class="questionnaire-form__place questionnaire-form__place--width">
                                            <div class="questionnaire-form__label">Район</div>
                                            <select class="js-select-form questionnaire-form__select" data-placeholder="Район" name="district_id">
                                                <option value=""></option>
                                                <option value="1">Вешняки</option>
                                                <option value="2">Вешняки</option>
                                            </select>
                                        </div>
                                        <div class="questionnaire-form__place questionnaire-form__place--width">
                                            <div class="questionnaire-form__label">Станция метро</div>
                                            <select class="js-select-form questionnaire-form__select" data-placeholder="Станция метро" name="subway_id">
                                                <option value=""></option>
                                                <option value="1">Выхино</option>
                                                <option value="2">Выхино</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="questionnaire-form__part">
                                    <div class="questionnaire-form__name">Параметры</div>
                                    <div class="questionnaire-form__params">
                                        <div class="questionnaire-form__parameter">
                                            <div class="questionnaire-form__label">Пол</div>
                                            <div class="questionnaire-form__radios">
                                                <div class="questionnaire-form__radio">
                                                    <input class="questionnaire-form__radio-input" id="female" type="radio" name="sex" value="2" required>
                                                    <label class="questionnaire-form__radio-label" for="female">Женский</label>
                                                </div>
                                                <div class="questionnaire-form__radio">
                                                    <input class="questionnaire-form__radio-input" id="male" type="radio" name="sex" value="1">
                                                    <label class="questionnaire-form__radio-label" for="male">Мужской</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="questionnaire-form__parameter questionnaire-form__parameter--inputs">
                                            <div class="questionnaire-form__place">
                                                <div class="input">
                                                    <label class="input__label" for="age">Возраст
                                                    </label>
                                                    <input class="input__control" id="age" type="number" name="age" placeholder="" onblur="placeholder=''" onfocus="placeholder=''" autocomplete="on"/>
                                                </div>
                                            </div>
                                            <div class="questionnaire-form__place">
                                                <div class="input">
                                                    <label class="input__label" for="height">Рост, см
                                                    </label>
                                                    <input class="input__control" id="height" type="number" name="height" placeholder="" onblur="placeholder=''" onfocus="placeholder=''" autocomplete="on"/>
                                                </div>
                                            </div>
                                            <div class="questionnaire-form__place">
                                                <div class="input">
                                                    <label class="input__label" for="weight">Вес, кг
                                                    </label>
                                                    <input class="input__control" id="weight" type="number" name="weight" placeholder="" onblur="placeholder=''" onfocus="placeholder=''" autocomplete="on"/>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="questionnaire-form__parameter">
                                            <div class="questionnaire-form__label">Размер груди</div>
                                            <div class="questionnaire-form__radios questionnaire-form__radios--size">
                                                <div class="questionnaire-form__radio">
                                                    <input class="questionnaire-form__radio-input" id="1" type="radio" name="breast_size" value="1">
                                                    <label class="questionnaire-form__radio-label" for="1">1</label>
                                                </div>
                                                <div class="questionnaire-form__radio">
                                                    <input class="questionnaire-form__radio-input" id="2" type="radio" name="breast_size" value="2">
                                                    <label class="questionnaire-form__radio-label" for="2">2</label>
                                                </div>
                                                <div class="questionnaire-form__radio">
                                                    <input class="questionnaire-form__radio-input" id="3" type="radio" name="breast_size" value="3">
                                                    <label class="questionnaire-form__radio-label" for="3">3</label>
                                                </div>
                                                <div class="questionnaire-form__radio">
                                                    <input class="questionnaire-form__radio-input" id="4" type="radio" name="breast_size" value="4">
                                                    <label class="questionnaire-form__radio-label" for="4">4</label>
                                                </div>
                                                <div class="questionnaire-form__radio">
                                                    <input class="questionnaire-form__radio-input" id="5" type="radio" name="breast_size" value="5+">
                                                    <label class="questionnaire-form__radio-label" for="5">5+</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="questionnaire-form__selects">
                                        <div class="questionnaire-form__place questionnaire-form__place--width">
                                            <div class="questionnaire-form__label">Ориентация</div>
                                            <select class="js-select-form-search questionnaire-form__select" required data-placeholder="" name="orientation_id">
                                                <option value=""></option>
                                                <option value="1">Гетеро</option>
                                                <option value="2">Би</option>
                                                <option value="3">Лесби</option>
                                            </select>
                                        </div>
                                        <div class="questionnaire-form__place questionnaire-form__place--width">
                                            <div class="questionnaire-form__label">Национальность</div>
                                            <select class="js-select-form-search questionnaire-form__select" required data-placeholder="" name="nationality_id">
                                                <option value=""></option>
                                                <option value="1">Русская</option>
                                                <option value="2">Украинская</option>
                                            </select>
                                        </div>
                                        <div class="questionnaire-form__place questionnaire-form__place--width">
                                            <div class="questionnaire-form__label">Владение языками</div>
                                            <select class="js-select-form-search questionnaire-form__select" data-placeholder="" name="languages[]" multiple="multiple">
                                                <option value=""></option>
                                                <option value="1">Русский</option>
                                                <option value="2">Украинский</option>
                                            </select>
                                        </div>
                                        <div class="questionnaire-form__place questionnaire-form__place--width">
                                            <div class="questionnaire-form__label">Цвет волос</div>
                                            <select class="js-select-form-search questionnaire-form__select" data-placeholder="" name="hair_color_id">
                                                <option value=""></option>
                                                <option value="1">Брюнетка</option>
                                                <option value="2">Блондинка</option>
                                            </select>
                                        </div>
                                        <div class="questionnaire-form__place questionnaire-form__place--width">
                                            <div class="questionnaire-form__label">Интимная стрижка</div>
                                            <select class="js-select-form-search questionnaire-form__select" data-placeholder="" name="intimate_haircut_id">
                                                <option value=""></option>
                                                <option value="1">Бабочка</option>
                                                <option value="2">Стрелочка</option>
                                            </select>
                                        </div>
                                        <div class="questionnaire-form__place questionnaire-form__place--width questionnaire-form__place--checkboxes">
                                            <div class="questionnaire-form__label">На теле</div>
                                            <div class="questionnaire-form__checkbox">
                                                <div class="checkbox">
                                                    <input class="checkbox__input" id="scars" type="checkbox" value="1" name="is_scars">
                                                    <label class="checkbox__label" for="scars">Шрамы</label>
                                                </div>
                                            </div>
                                            <div class="questionnaire-form__checkbox">
                                                <div class="checkbox">
                                                    <input class="checkbox__input" id="piercing" type="checkbox" value="1" name="is_piercing">
                                                    <label class="checkbox__label" for="piercing">Пирсинг</label>
                                                </div>
                                            </div>
                                            <div class="questionnaire-form__checkbox">
                                                <div class="checkbox">
                                                    <input class="checkbox__input" id="tattoos" type="checkbox" value="1" name="is_tattoos">
                                                    <label class="checkbox__label" for="tattoos">Татуировки</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="questionnaire-form__part">
                                    <div class="questionnaire-form__name">Стоимость и условия</div>
                                    <div class="questionnaire-form__place questionnaire-form__place--width">
                                        <div class="questionnaire-form__label">Выберите валюту</div>
                                        <select class="js-select-form questionnaire-form__select" data-placeholder="Выберите валюту" name="Price[currency_id]">
                                            <option value=""></option>
                                            <option value="1" selected>USD</option>
                                            <option value="3">UAH</option>
                                            <option value="4">RUB</option>
                                            <option value="2">EUR</option>
                                        </select>
                                    </div>
                                    <div class="questionnaire-form__list">
                                        <div class="questionnaire-form__item">
                                            <div class="questionnaire-form__label">Цена в апартаментах</div>
                                            <div class="questionnaire-form__elem">
                                                <div class="input questionnaire-form__elem-input">
                                                    <label class="input__label" for="price-apartments-one-hour">1 час
                                                    </label>
                                                    <input class="input__control" id="price-apartments-one-hour" type="number" name="Price[hour_in_apartments]" value="0" />
                                                </div>
                                                <div class="questionnaire-form__checkbox js-checkbox">
                                                    <div class="checkbox">
                                                        <input class="checkbox__input" id="apartments-one-hour" type="checkbox" value="1" name="Price[hour_in_apartments_negotiated]">
                                                        <label class="checkbox__label" for="apartments-one-hour">Договорная</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="questionnaire-form__elem">
                                                <div class="input questionnaire-form__elem-input">
                                                    <label class="input__label" for="price-apartments-two-hour">2 часа
                                                    </label>
                                                    <input class="input__control" id="price-apartments-two-hour" type="number" name="Price[two_hours_in_apartments]" value="0" />
                                                </div>
                                                <div class="questionnaire-form__checkbox js-checkbox">
                                                    <div class="checkbox">
                                                        <input class="checkbox__input" id="apartments-two-hour" type="checkbox" value="1" name="Price[two_hours_in_apartments_negotiated]">
                                                        <label class="checkbox__label" for="apartments-two-hour">Договорная</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="questionnaire-form__elem">
                                                <div class="input questionnaire-form__elem-input">
                                                    <label class="input__label" for="price-apartments-night">Ночь
                                                    </label>
                                                    <input class="input__control" id="price-apartments-night" type="number" name="Price[night_in_apartments]" value="0" />
                                                </div>
                                                <div class="questionnaire-form__checkbox js-checkbox">
                                                    <div class="checkbox">
                                                        <input class="checkbox__input" id="apartments-night" type="checkbox" value="1" name="Price[night_in_apartments_negotiated]">
                                                        <label class="checkbox__label" for="apartments-night">Договорная</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="questionnaire-form__item">
                                            <div class="questionnaire-form__label">Цена на выезд</div>
                                            <div class="questionnaire-form__elem">
                                                <div class="input questionnaire-form__elem-input">
                                                    <label class="input__label" for="price-departure-one-hour">1 час
                                                    </label>
                                                    <input class="input__control" id="price-departure-one-hour" type="number" name="Price[hour_in_departures]" value="0" />
                                                </div>
                                                <div class="questionnaire-form__checkbox js-checkbox">
                                                    <div class="checkbox">
                                                        <input class="checkbox__input" id="departure-one-hour" type="checkbox" value="1" name="Price[hour_in_departures_negotiated]">
                                                        <label class="checkbox__label" for="departure-one-hour">Договорная</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="questionnaire-form__elem">
                                                <div class="input questionnaire-form__elem-input">
                                                    <label class="input__label" for="price-departure-two-hour">2 часа
                                                    </label>
                                                    <input class="input__control" id="price-departure-two-hour" type="number" name="Price[two_hours_in_departures]" value="0" />
                                                </div>
                                                <div class="questionnaire-form__checkbox js-checkbox">
                                                    <div class="checkbox">
                                                        <input class="checkbox__input" id="departure-two-hour" type="checkbox" value="1" name="Price[two_hours_in_departures_negotiated]">
                                                        <label class="checkbox__label" for="departure-two-hour">Договорная</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="questionnaire-form__elem">
                                                <div class="input questionnaire-form__elem-input">
                                                    <label class="input__label" for="price-departure-night">Ночь
                                                    </label>
                                                    <input class="input__control" id="price-departure-night" type="number" name="Price[night_in_departures]" value="0" />
                                                </div>
                                                <div class="questionnaire-form__checkbox js-checkbox">
                                                    <div class="checkbox">
                                                        <input class="checkbox__input" id="departure-night" type="checkbox" value="1" name="Price[night_in_departures_negotiated]">
                                                        <label class="checkbox__label" for="departure-night">Договорная</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="questionnaire-form__item">
                                            <div class="questionnaire-form__label">Выезд</div>
                                            <div class="questionnaire-form__checkboxes">
                                                <div class="questionnaire-form__checkbox">
                                                    <div class="checkbox">
                                                        <input class="checkbox__input" id="office" type="checkbox" value="1" name="Price[is_departure_to_office]">
                                                        <label class="checkbox__label" for="office">В офис</label>
                                                    </div>
                                                </div>
                                                <div class="questionnaire-form__checkbox">
                                                    <div class="checkbox">
                                                        <input class="checkbox__input" id="sauna" type="checkbox" value="1" name="Price[is_departure_to_sauna]">
                                                        <label class="checkbox__label" for="sauna">В баню/сауну</label>
                                                    </div>
                                                </div>
                                                <div class="questionnaire-form__checkbox">
                                                    <div class="checkbox">
                                                        <input class="checkbox__input" id="hotel" type="checkbox" value="1" name="Price[is_departure_to_hotel]">
                                                        <label class="checkbox__label" for="hotel">В гостиницу</label>
                                                    </div>
                                                </div>
                                                <div class="questionnaire-form__checkbox">
                                                    <div class="checkbox">
                                                        <input class="checkbox__input" id="apartment" type="checkbox" value="1" name="Price[is_departure_to_apartment]">
                                                        <label class="checkbox__label" for="apartment">На квартиру</label>
                                                    </div>
                                                </div>
                                                <div class="questionnaire-form__checkbox">
                                                    <div class="checkbox">
                                                        <input class="checkbox__input" id="house" type="checkbox" value="1" name="Price[is_departure_to_country_house]">
                                                        <label class="checkbox__label" for="house">В загородный дом</label>
                                                    </div>
                                                </div>
                                                <div class="questionnaire-form__checkbox">
                                                    <div class="checkbox">
                                                        <input class="checkbox__input" id="anywhere" type="checkbox" value="1" name="Price[is_departure_to_any_place]">
                                                        <label class="checkbox__label" for="anywhere">В любое место</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="questionnaire-form__part">
                                    <div class="questionnaire-form__name">О себе в произвольной форме</div>
                                    <div class="questionnaire-form__textarea-wrap">
                                        <textarea class="questionnaire-form__textarea" name="about" maxlength="1000"></textarea>
                                        <div class="questionnaire-form__text">не более 1000 символов</div>
                                    </div>
                                </div>
                                <div class="questionnaire-form__part">
                                    <div class="questionnaire-form__name">Предоставляемые услуги</div>
                                    <div class="questionnaire-form__services">
                                        <div class="services">
                                            <div class="services__column">
                                                <div class="services__item">
                                                    <div class="services__name">Основное</div>
                                                    <div class="services__elem">
                                                        <div class="services__checkbox js-checkbox">
                                                            <div class="checkbox">
                                                                <input class="checkbox__input" id="vaginal" type="checkbox" value="1" name="Service[vaginal]">
                                                                <label class="checkbox__label" for="vaginal">Вагинальный</label>
                                                            </div>
                                                        </div>
                                                        <div class="services__select">
                                                            <select class="js-select-services" data-placeholder="" name="Service[vaginal_price]">
                                                                <option value="0">Входит в стоимость часа</option>
                                                                <option value="1000">+1000 ₽</option>
                                                                <option value="2000">+2000 ₽</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="services__elem">
                                                        <div class="services__checkbox js-checkbox">
                                                            <div class="checkbox">
                                                                <input class="checkbox__input" id="anal" type="checkbox" value="1" name="Service[anal]">
                                                                <label class="checkbox__label" for="anal">Анальный</label>
                                                            </div>
                                                        </div>
                                                        <div class="services__select">
                                                            <select class="js-select-services" data-placeholder="" name="Service[anal_price]">
                                                                <option value="0">Входит в стоимость часа</option>
                                                                <option value="1000">+1000 ₽</option>
                                                                <option value="2000">+2000 ₽</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="services__elem">
                                                        <div class="services__checkbox js-checkbox">
                                                            <div class="checkbox">
                                                                <input class="checkbox__input" id="group" type="checkbox" value="1" name="Service[group]">
                                                                <label class="checkbox__label" for="group">Групповой</label>
                                                            </div>
                                                        </div>
                                                        <div class="services__select">
                                                            <select class="js-select-services" data-placeholder="" name="Service[group_price]">
                                                                <option value="0">Входит в стоимость часа</option>
                                                                <option value="1000">+1000 ₽</option>
                                                                <option value="2000">+2000 ₽</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="services__elem">
                                                        <div class="services__checkbox js-checkbox">
                                                            <div class="checkbox">
                                                                <input class="checkbox__input" id="lesbian" type="checkbox" value="1" name="Service[lesbian]">
                                                                <label class="checkbox__label" for="lesbian">Лесбийский</label>
                                                            </div>
                                                        </div>
                                                        <div class="services__select">
                                                            <select class="js-select-services" data-placeholder="" name="Service[lesbian_price]">
                                                                <option value="0">Входит в стоимость часа</option>
                                                                <option value="1000">+1000 ₽</option>
                                                                <option value="2000">+2000 ₽</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="services__item">
                                                    <div class="services__name">Финиш</div>
                                                    <div class="services__elem">
                                                        <div class="services__checkbox js-checkbox">
                                                            <div class="checkbox">
                                                                <input class="checkbox__input" id="mouth" type="checkbox" value="1" name="Service[mouth]">
                                                                <label class="checkbox__label" for="mouth">В рот</label>
                                                            </div>
                                                        </div>
                                                        <div class="services__select">
                                                            <select class="js-select-services" data-placeholder="" name="Service[mouth_price]">
                                                                <option value="0">Входит в стоимость часа</option>
                                                                <option value="1000">+1000 ₽</option>
                                                                <option value="2000">+2000 ₽</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="services__elem">
                                                        <div class="services__checkbox js-checkbox">
                                                            <div class="checkbox">
                                                                <input class="checkbox__input" id="face" type="checkbox" value="1" name="Service[face]">
                                                                <label class="checkbox__label" for="face">На лицо</label>
                                                            </div>
                                                        </div>
                                                        <div class="services__select">
                                                            <select class="js-select-services" data-placeholder="" name="Service[face_price]">
                                                                <option value="0">Входит в стоимость часа</option>
                                                                <option value="1000">+1000 ₽</option>
                                                                <option value="2000">+2000 ₽</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="services__elem">
                                                        <div class="services__checkbox js-checkbox">
                                                            <div class="checkbox">
                                                                <input class="checkbox__input" id="chest" type="checkbox" value="1" name="Service[chest]">
                                                                <label class="checkbox__label" for="chest">На грудь</label>
                                                            </div>
                                                        </div>
                                                        <div class="services__select">
                                                            <select class="js-select-services" data-placeholder="" name="Service[chest_price]">
                                                                <option value="0">Входит в стоимость часа</option>
                                                                <option value="1000">+1000 ₽</option>
                                                                <option value="2000">+2000 ₽</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="services__item">
                                                    <div class="services__name">Лесби-шоу</div>
                                                    <div class="services__elem">
                                                        <div class="services__checkbox js-checkbox">
                                                            <div class="checkbox">
                                                                <input class="checkbox__input" id="frank" type="checkbox" value="1" name="Service[frank]">
                                                                <label class="checkbox__label" for="frank">Откровенное</label>
                                                            </div>
                                                        </div>
                                                        <div class="services__select">
                                                            <select class="js-select-services" data-placeholder="" name="Service[frank_price]">
                                                                <option value="0">Входит в стоимость часа</option>
                                                                <option value="1000">+1000 ₽</option>
                                                                <option value="2000">+2000 ₽</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="services__elem">
                                                        <div class="services__checkbox js-checkbox">
                                                            <div class="checkbox">
                                                                <input class="checkbox__input" id="lung" type="checkbox" value="1" name="Service[lung]">
                                                                <label class="checkbox__label" for="lung">Лёгкое</label>
                                                            </div>
                                                        </div>
                                                        <div class="services__select">
                                                            <select class="js-select-services" data-placeholder="" name="Service[lung_price]">
                                                                <option value="0">Входит в стоимость часа</option>
                                                                <option value="1000">+1000 ₽</option>
                                                                <option value="2000">+2000 ₽</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="services__item">
                                                    <div class="services__name">Дополнительно</div>
                                                    <div class="services__elem">
                                                        <div class="services__checkbox js-checkbox">
                                                            <div class="checkbox">
                                                                <input class="checkbox__input" id="escort" type="checkbox" value="1" name="Service[escort]">
                                                                <label class="checkbox__label" for="escort">Эскорт</label>
                                                            </div>
                                                        </div>
                                                        <div class="services__select">
                                                            <select class="js-select-services" data-placeholder="" name="Service[escort_price]">
                                                                <option value="0">Входит в стоимость часа</option>
                                                                <option value="1000">+1000 ₽</option>
                                                                <option value="2000">+2000 ₽</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="services__elem">
                                                        <div class="services__checkbox js-checkbox">
                                                            <div class="checkbox">
                                                                <input class="checkbox__input" id="photo-video" type="checkbox" value="1" name="Service[photo_video]">
                                                                <label class="checkbox__label" for="photo-video">Фото/видео</label>
                                                            </div>
                                                        </div>
                                                        <div class="services__select">
                                                            <select class="js-select-services" data-placeholder="" name="Service[photo_video_price]">
                                                                <option value="0">Входит в стоимость часа</option>
                                                                <option value="1000">+1000 ₽</option>
                                                                <option value="2000">+2000 ₽</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="services__elem">
                                                        <div class="services__checkbox js-checkbox">
                                                            <div class="checkbox">
                                                                <input class="checkbox__input" id="family-services" type="checkbox" value="1" name="Service[family_services]">
                                                                <label class="checkbox__label" for="family-services">Услуги семейной паре</label>
                                                            </div>
                                                        </div>
                                                        <div class="services__select">
                                                            <select class="js-select-services" data-placeholder="" name="Service[family_services_price]">
                                                                <option value="0">Входит в стоимость часа</option>
                                                                <option value="1000">+1000 ₽</option>
                                                                <option value="2000">+2000 ₽</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="services__elem">
                                                    <div class="services__checkbox js-checkbox">
                                                        <div class="checkbox">
                                                            <input class="checkbox__input" id="virtual" type="checkbox" value="1" name="Service[virtual]">
                                                            <label class="checkbox__label" for="virtual">Виртуальный секс</label>
                                                        </div>
                                                    </div>
                                                    <div class="services__select">
                                                        <select class="js-select-services" data-placeholder="" name="Service[virtual_price]">
                                                            <option value="0">Входит в стоимость часа</option>
                                                            <option value="1000">+1000 ₽</option>
                                                            <option value="2000">+2000 ₽</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="services__item">
                                                    <div class="services__name">Садо-мазо</div>
                                                    <div class="services__elem">
                                                        <div class="services__checkbox js-checkbox">
                                                            <div class="checkbox">
                                                                <input class="checkbox__input" id="banjo" type="checkbox" value="1" name="Service[banjo]">
                                                                <label class="checkbox__label" for="banjo">Бандож</label>
                                                            </div>
                                                        </div>
                                                        <div class="services__select">
                                                            <select class="js-select-services" data-placeholder="" name="Service[banjo_price]">
                                                                <option value="0">Входит в стоимость часа</option>
                                                                <option value="1000">+1000 ₽</option>
                                                                <option value="2000">+2000 ₽</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="services__elem">
                                                        <div class="services__checkbox js-checkbox">
                                                            <div class="checkbox">
                                                                <input class="checkbox__input" id="madam" type="checkbox" value="1" name="Service[madam]">
                                                                <label class="checkbox__label" for="madam">Госпожа</label>
                                                            </div>
                                                        </div>
                                                        <div class="services__select">
                                                            <select class="js-select-services" data-placeholder="" name="Service[madam_price]">
                                                                <option value="0">Входит в стоимость часа</option>
                                                                <option value="1000">+1000 ₽</option>
                                                                <option value="2000">+2000 ₽</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="services__elem">
                                                        <div class="services__checkbox js-checkbox">
                                                            <div class="checkbox">
                                                                <input class="checkbox__input" id="slave" type="checkbox" value="1" name="Service[slave]">
                                                                <label class="checkbox__label" for="slave">Рабыня</label>
                                                            </div>
                                                        </div>
                                                        <div class="services__select">
                                                            <select class="js-select-services" data-placeholder="" name="Service[slave_price]">
                                                                <option value="0">Входит в стоимость часа</option>
                                                                <option value="1000">+1000 ₽</option>
                                                                <option value="2000">+2000 ₽</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="services__elem">
                                                        <div class="services__checkbox js-checkbox">
                                                            <div class="checkbox">
                                                                <input class="checkbox__input" id="light-domination" type="checkbox" value="1" name="Service[light_domination]">
                                                                <label class="checkbox__label" for="light-domination">Лёгкая доминация</label>
                                                            </div>
                                                        </div>
                                                        <div class="services__select">
                                                            <select class="js-select-services" data-placeholder="" name="Service[light_domination_price]">
                                                                <option value="0">Входит в стоимость часа</option>
                                                                <option value="1000">+1000 ₽</option>
                                                                <option value="2000">+2000 ₽</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="services__elem">
                                                        <div class="services__checkbox js-checkbox">
                                                            <div class="checkbox">
                                                                <input class="checkbox__input" id="erotic-games" type="checkbox" value="1" name="Service[erotic_games]">
                                                                <label class="checkbox__label" for="erotic-games">Эротические игры</label>
                                                            </div>
                                                        </div>
                                                        <div class="services__select">
                                                            <select class="js-select-services" data-placeholder="" name="Service[erotic_games_price]">
                                                                <option value="0">Входит в стоимость часа</option>
                                                                <option value="1000">+1000 ₽</option>
                                                                <option value="2000">+2000 ₽</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="services__elem">
                                                        <div class="services__checkbox js-checkbox">
                                                            <div class="checkbox">
                                                                <input class="checkbox__input" id="flogging" type="checkbox" value="1" name="Service[flogging]">
                                                                <label class="checkbox__label" for="flogging">Порка</label>
                                                            </div>
                                                        </div>
                                                        <div class="services__select">
                                                            <select class="js-select-services" data-placeholder="" name="Service[flogging_price]">
                                                                <option value="0">Входит в стоимость часа</option>
                                                                <option value="1000">+1000 ₽</option>
                                                                <option value="2000">+2000 ₽</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="services__elem">
                                                        <div class="services__checkbox js-checkbox">
                                                            <div class="checkbox">
                                                                <input class="checkbox__input" id="fetish" type="checkbox" value="1" name="Service[fetish]">
                                                                <label class="checkbox__label" for="fetish">Фетиш</label>
                                                            </div>
                                                        </div>
                                                        <div class="services__select">
                                                            <select class="js-select-services" data-placeholder="" name="Service[fetish_price]">
                                                                <option value="0">Входит в стоимость часа</option>
                                                                <option value="1000">+1000 ₽</option>
                                                                <option value="2000">+2000 ₽</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="services__elem">
                                                        <div class="services__checkbox js-checkbox">
                                                            <div class="checkbox">
                                                                <input class="checkbox__input" id="foot-fetish" type="checkbox" value="1" name="Service[foot_fetish]">
                                                                <label class="checkbox__label" for="foot-fetish">Фут-фетиш</label>
                                                            </div>
                                                        </div>
                                                        <div class="services__select">
                                                            <select class="js-select-services" data-placeholder="" name="Service[foot_fetish_price">
                                                                <option value="0">Входит в стоимость часа</option>
                                                                <option value="1000">+1000 ₽</option>
                                                                <option value="2000">+2000 ₽</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="services__elem">
                                                        <div class="services__checkbox js-checkbox">
                                                            <div class="checkbox">
                                                                <input class="checkbox__input" id="trampling" type="checkbox" value="1" name="Service[trampling]">
                                                                <label class="checkbox__label" for="trampling">Трамплинг</label>
                                                            </div>
                                                        </div>
                                                        <div class="services__select">
                                                            <select class="js-select-services" data-placeholder="" name="Service[trampling_price]">
                                                                <option value="0">Входит в стоимость часа</option>
                                                                <option value="1000">+1000 ₽</option>
                                                                <option value="2000">+2000 ₽</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="services__column">
                                                <div class="services__item">
                                                    <div class="services__name">Ласка</div>
                                                    <div class="services__elem">
                                                        <div class="services__checkbox js-checkbox">
                                                            <div class="checkbox">
                                                                <input class="checkbox__input" id="condom" type="checkbox" value="1" name="Service[condom]">
                                                                <label class="checkbox__label" for="condom">В презервативе</label>
                                                            </div>
                                                        </div>
                                                        <div class="services__select">
                                                            <select class="js-select-services" data-placeholder="" name="Service[condom_price]">
                                                                <option value="0">Входит в стоимость часа</option>
                                                                <option value="1000">+1000 ₽</option>
                                                                <option value="2000">+2000 ₽</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="services__elem">
                                                        <div class="services__checkbox js-checkbox">
                                                            <div class="checkbox">
                                                                <input class="checkbox__input" id="dont-condom" type="checkbox" value="1" name="Service[dont_condom]">
                                                                <label class="checkbox__label" for="dont-condom">Без презерватива</label>
                                                            </div>
                                                        </div>
                                                        <div class="services__select">
                                                            <select class="js-select-services" data-placeholder="" name="Service[dont_condom_price]">
                                                                <option value="0">Входит в стоимость часа</option>
                                                                <option value="1000">+1000 ₽</option>
                                                                <option value="2000">+2000 ₽</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="services__elem">
                                                        <div class="services__checkbox js-checkbox">
                                                            <div class="checkbox">
                                                                <input class="checkbox__input" id="deep" type="checkbox" value="1" name="Service[deep]">
                                                                <label class="checkbox__label" for="deep">Глубокий</label>
                                                            </div>
                                                        </div>
                                                        <div class="services__select">
                                                            <select class="js-select-services" data-placeholder="" name="Service[deep_price]">
                                                                <option value="0">Входит в стоимость часа</option>
                                                                <option value="1000">+1000 ₽</option>
                                                                <option value="2000">+2000 ₽</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="services__elem">
                                                        <div class="services__checkbox js-checkbox">
                                                            <div class="checkbox">
                                                                <input class="checkbox__input" id="in-car" type="checkbox" value="1" name="Service[in_car]">
                                                                <label class="checkbox__label" for="in-car">В машине</label>
                                                            </div>
                                                        </div>
                                                        <div class="services__select">
                                                            <select class="js-select-services" data-placeholder="" name="Service[in_car_price]">
                                                                <option value="0">Входит в стоимость часа</option>
                                                                <option value="1000">+1000 ₽</option>
                                                                <option value="2000">+2000 ₽</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="services__elem">
                                                        <div class="services__checkbox js-checkbox">
                                                            <div class="checkbox">
                                                                <input class="checkbox__input" id="cunnilingus" type="checkbox" value="1" name="Service[cunnilingus]">
                                                                <label class="checkbox__label" for="cunnilingus">Кунилингус</label>
                                                            </div>
                                                        </div>
                                                        <div class="services__select">
                                                            <select class="js-select-services" data-placeholder="" name="Service[cunnilingus_price]">
                                                                <option value="0">Входит в стоимость часа</option>
                                                                <option value="1000">+1000 ₽</option>
                                                                <option value="2000">+2000 ₽</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="services__elem">
                                                        <div class="services__checkbox js-checkbox">
                                                            <div class="checkbox">
                                                                <input class="checkbox__input" id="anilingus" type="checkbox" value="1" name="Service[anilingus]">
                                                                <label class="checkbox__label" for="anilingus">Анилингус</label>
                                                            </div>
                                                        </div>
                                                        <div class="services__select">
                                                            <select class="js-select-services" data-placeholder="" name="Service[anilingus_price]">
                                                                <option value="0">Входит в стоимость часа</option>
                                                                <option value="1000">+1000 ₽</option>
                                                                <option value="2000">+2000 ₽</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="services__item">
                                                    <div class="services__name">Стриптиз</div>
                                                    <div class="services__elem">
                                                        <div class="services__checkbox js-checkbox">
                                                            <div class="checkbox">
                                                                <input class="checkbox__input" id="pro" type="checkbox" value="1" name="Service[pro]">
                                                                <label class="checkbox__label" for="pro">Профи</label>
                                                            </div>
                                                        </div>
                                                        <div class="services__select">
                                                            <select class="js-select-services" data-placeholder="" name="Service[pro_price]">
                                                                <option value="0">Входит в стоимость часа</option>
                                                                <option value="1000">+1000 ₽</option>
                                                                <option value="2000">+2000 ₽</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="services__elem">
                                                        <div class="services__checkbox js-checkbox">
                                                            <div class="checkbox">
                                                                <input class="checkbox__input" id="not-pro" type="checkbox" value="1" name="Service[not_pro]">
                                                                <label class="checkbox__label" for="not-pro">Не профи</label>
                                                            </div>
                                                        </div>
                                                        <div class="services__select">
                                                            <select class="js-select-services" data-placeholder="" name="Service[not_pro_price]">
                                                                <option value="0">Входит в стоимость часа</option>
                                                                <option value="1000">+1000 ₽</option>
                                                                <option value="2000">+2000 ₽</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="services__item">
                                                    <div class="services__name">Массаж</div>
                                                    <div class="services__elem">
                                                        <div class="services__checkbox js-checkbox">
                                                            <div class="checkbox">
                                                                <input class="checkbox__input" id="classical" type="checkbox" value="1" name="Service[classical]">
                                                                <label class="checkbox__label" for="classical">Классический</label>
                                                            </div>
                                                        </div>
                                                        <div class="services__select">
                                                            <select class="js-select-services" data-placeholder="" name="Service[classical_price]">
                                                                <option value="0">Входит в стоимость часа</option>
                                                                <option value="1000">+1000 ₽</option>
                                                                <option value="2000">+2000 ₽</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="services__elem">
                                                        <div class="services__checkbox js-checkbox">
                                                            <div class="checkbox">
                                                                <input class="checkbox__input" id="professional" type="checkbox" value="1" name="Service[professional]">
                                                                <label class="checkbox__label" for="professional">Профессиональный</label>
                                                            </div>
                                                        </div>
                                                        <div class="services__select">
                                                            <select class="js-select-services" data-placeholder="" name="Service[professional_price]">
                                                                <option value="0">Входит в стоимость часа</option>
                                                                <option value="1000">+1000 ₽</option>
                                                                <option value="2000">+2000 ₽</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="services__elem">
                                                        <div class="services__checkbox js-checkbox">
                                                            <div class="checkbox">
                                                                <input class="checkbox__input" id="relaxing" type="checkbox" value="1" name="Service[relaxing]">
                                                                <label class="checkbox__label" for="relaxing">Расслабляющий</label>
                                                            </div>
                                                        </div>
                                                        <div class="services__select">
                                                            <select class="js-select-services" data-placeholder="" name="Service[relaxing_price]">
                                                                <option value="0">Входит в стоимость часа</option>
                                                                <option value="1000">+1000 ₽</option>
                                                                <option value="2000">+2000 ₽</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="services__elem">
                                                        <div class="services__checkbox js-checkbox">
                                                            <div class="checkbox">
                                                                <input class="checkbox__input" id="thai" type="checkbox" value="1" name="Service[thai]">
                                                                <label class="checkbox__label" for="thai">Тайский</label>
                                                            </div>
                                                        </div>
                                                        <div class="services__select">
                                                            <select class="js-select-services" data-placeholder="" name="Service[thai_price]">
                                                                <option value="0">Входит в стоимость часа</option>
                                                                <option value="1000">+1000 ₽</option>
                                                                <option value="2000">+2000 ₽</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="services__elem">
                                                        <div class="services__checkbox js-checkbox">
                                                            <div class="checkbox">
                                                                <input class="checkbox__input" id="urological" type="checkbox" value="1" name="Service[urological]">
                                                                <label class="checkbox__label" for="urological">Урологический</label>
                                                            </div>
                                                        </div>
                                                        <div class="services__select">
                                                            <select class="js-select-services" data-placeholder="" name="Service[urological_price]">
                                                                <option value="0">Входит в стоимость часа</option>
                                                                <option value="1000">+1000 ₽</option>
                                                                <option value="2000">+2000 ₽</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="services__elem">
                                                        <div class="services__checkbox js-checkbox">
                                                            <div class="checkbox">
                                                                <input class="checkbox__input" id="point" type="checkbox" value="1" name="Service[point]">
                                                                <label class="checkbox__label" for="point">Точечный</label>
                                                            </div>
                                                        </div>
                                                        <div class="services__select">
                                                            <select class="js-select-services" data-placeholder="" name="Service[point_price]">
                                                                <option value="0">Входит в стоимость часа</option>
                                                                <option value="1000">+1000 ₽</option>
                                                                <option value="2000">+2000 ₽</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="services__elem">
                                                        <div class="services__checkbox js-checkbox">
                                                            <div class="checkbox">
                                                                <input class="checkbox__input" id="erotic" type="checkbox" value="1" name="Service[erotic]">
                                                                <label class="checkbox__label" for="erotic">Эротический</label>
                                                            </div>
                                                        </div>
                                                        <div class="services__select">
                                                            <select class="js-select-services" data-placeholder="" name="Service[erotic_price]">
                                                                <option value="0">Входит в стоимость часа</option>
                                                                <option value="1000">+1000 ₽</option>
                                                                <option value="2000">+2000 ₽</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="services__elem">
                                                        <div class="services__checkbox js-checkbox">
                                                            <div class="checkbox">
                                                                <input class="checkbox__input" id="sakura-branch" type="checkbox" value="1" name="Service[sakura_branch]">
                                                                <label class="checkbox__label" for="sakura-branch">Ветка сакуры</label>
                                                            </div>
                                                        </div>
                                                        <div class="services__select">
                                                            <select class="js-select-services" data-placeholder="" name="Service[sakura_branch_price]">
                                                                <option value="0">Входит в стоимость часа</option>
                                                                <option value="1000">+1000 ₽</option>
                                                                <option value="2000">+2000 ₽</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="services__item">
                                                    <div class="services__name">Экстрим</div>
                                                    <div class="services__elem">
                                                        <div class="services__checkbox js-checkbox">
                                                            <div class="checkbox">
                                                                <input class="checkbox__input" id="strapon" type="checkbox" value="1" name="Service[strapon]">
                                                                <label class="checkbox__label" for="strapon">Страпон</label>
                                                            </div>
                                                        </div>
                                                        <div class="services__select">
                                                            <select class="js-select-services" data-placeholder="" name="Service[strapon_price]">
                                                                <option value="0">Входит в стоимость часа</option>
                                                                <option value="1000">+1000 ₽</option>
                                                                <option value="2000">+2000 ₽</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="services__elem">
                                                        <div class="services__checkbox js-checkbox">
                                                            <div class="checkbox">
                                                                <input class="checkbox__input" id="golden-shower-issue" type="checkbox" value="1" name="Service[golden_shower_issue]">
                                                                <label class="checkbox__label" for="golden-shower-issue">Золотой дождь выдача</label>
                                                            </div>
                                                        </div>
                                                        <div class="services__select">
                                                            <select class="js-select-services" data-placeholder="" name="Service[golden_shower_issue_price]">
                                                                <option value="0">Входит в стоимость часа</option>
                                                                <option value="1000">+1000 ₽</option>
                                                                <option value="2000">+2000 ₽</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="services__elem">
                                                        <div class="services__checkbox js-checkbox">
                                                            <div class="checkbox">
                                                                <input class="checkbox__input" id="golden-shower-reception" type="checkbox" value="1" name="Service[golden_shower_reception]">
                                                                <label class="checkbox__label" for="golden-shower-reception">Золотой дождь приём</label>
                                                            </div>
                                                        </div>
                                                        <div class="services__select">
                                                            <select class="js-select-services" data-placeholder="" name="Service[golden_shower_reception_price]">
                                                                <option value="0">Входит в стоимость часа</option>
                                                                <option value="1000">+1000 ₽</option>
                                                                <option value="2000">+2000 ₽</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="services__elem">
                                                        <div class="services__checkbox js-checkbox">
                                                            <div class="checkbox">
                                                                <input class="checkbox__input" id="copro-issue" type="checkbox" value="1" name="Service[copro_issue]">
                                                                <label class="checkbox__label" for="copro-issue">Копро выдача</label>
                                                            </div>
                                                        </div>
                                                        <div class="services__select">
                                                            <select class="js-select-services" data-placeholder="" name="Service[copro_issue_price]">
                                                                <option value="0">Входит в стоимость часа</option>
                                                                <option value="1000">+1000 ₽</option>
                                                                <option value="2000">+2000 ₽</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="services__elem">
                                                        <div class="services__checkbox js-checkbox">
                                                            <div class="checkbox">
                                                                <input class="checkbox__input" id="copro-reception" type="checkbox" value="1" name="Service[сopro_reception]">
                                                                <label class="checkbox__label" for="copro-reception">Копро приём</label>
                                                            </div>
                                                        </div>
                                                        <div class="services__select">
                                                            <select class="js-select-services" data-placeholder="" name="Service[сopro_reception_price]">
                                                                <option value="0">Входит в стоимость часа</option>
                                                                <option value="1000">+1000 ₽</option>
                                                                <option value="2000">+2000 ₽</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="services__elem">
                                                        <div class="services__checkbox js-checkbox">
                                                            <div class="checkbox">
                                                                <input class="checkbox__input" id="fisting-anal" type="checkbox" value="1" name="Service[fisting_anal]">
                                                                <label class="checkbox__label" for="fisting-anal">Фистинг анальный</label>
                                                            </div>
                                                        </div>
                                                        <div class="services__select">
                                                            <select class="js-select-services" data-placeholder="" name="Service[fisting_anal_price]">
                                                                <option value="0">Входит в стоимость часа</option>
                                                                <option value="1000">+1000 ₽</option>
                                                                <option value="2000">+2000 ₽</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="services__elem">
                                                        <div class="services__checkbox js-checkbox">
                                                            <div class="checkbox">
                                                                <input class="checkbox__input" id="fisting-vaginal" type="checkbox" value="1" name="Service[fisting_vaginal]">
                                                                <label class="checkbox__label" for="fisting-vaginal">Фистинг вагинальный</label>
                                                            </div>
                                                        </div>
                                                        <div class="services__select">
                                                            <select class="js-select-services" data-placeholder="" name="Service[fisting_vaginal_price]">
                                                                <option value="0">Входит в стоимость часа</option>
                                                                <option value="1000">+1000 ₽</option>
                                                                <option value="2000">+2000 ₽</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="services__elem">
                                                        <div class="services__checkbox js-checkbox">
                                                            <div class="checkbox">
                                                                <input class="checkbox__input" id="sex-toys" type="checkbox" value="1" name="Service[sex_toys]">
                                                                <label class="checkbox__label" for="sex-toys">Секс игрушки</label>
                                                            </div>
                                                        </div>
                                                        <div class="services__select">
                                                            <select class="js-select-services" data-placeholder="" name="Service[sex_toys_price]">
                                                                <option value="0">Входит в стоимость часа</option>
                                                                <option value="1000">+1000 ₽</option>
                                                                <option value="2000">+2000 ₽</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="questionnaire-form__part">
                                    <div class="questionnaire-form__name">Фотографии и видео</div>
                                    <div class="questionnaire-form__media">
                                        <div class="media">
                                            <div class="media__info">
                                                <div class="media__text"><span>Требования к фото</span><span>Размер файла не более 10 мб.</span><span>Должно быть добавленно минимум 3 фотографии</span>
                                                    <ul>
                                                        <li>- Фотографии должные быть с расширением *.jpg</li>
                                                        <li>- Минимальное разрешение фото — 250x250 пикселей;</li>
                                                    </ul>
                                                </div>
                                                <div class="media__text"><span>Требования к видео</span>
                                                    <ul>
                                                        <li>- Максимальная продолжительность видео до 5 мин(300 сек)</li>
                                                        <li>
                                                            - Поддерживаемые форматы видеороликов: AVI, MP4, 3GP, MPEG, MOV, FLV, F4V, WMV, MKV, WEBM, VOB, RM, RMVB, M4V, MPG, OGV, TS, M2TS, MTS.</li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="media__inputs">
                                                <div class="media__wrapper">
                                                    <div class="media__place js-input-img">
                                                        <input id="img" type="file" multiple accept="image/jpeg, image/jpg">
                                                        <label class="media__place-label" for="img"><img src="/images/upload-image-b070da18.svg" alt=""><span class="media__place-text">Перетащите изображения или нажмите на область</span><span class="media__place-text media__place-text--mobile">Загрузить фото</span></label>
                                                    </div>
                                                    <div class="media__items media__items--img">
                                                        <div class="media__previews dropzone-previews"></div>
                                                        <div class="media__loaded">
                                                            <div class="media__count"><span class="media__count-img"></span><span class="media__all">/10&nbsp;</span><span>загружено</span></div>
                                                        </div>
                                                        <div class="media__buttons">
                                                            <button class="media__button media__button--dashed js-btn-add" type="button">Добавить ещё</button>
                                                            <button class="media__button media__button--clear js-btn-remove" type="button">Очистить всё</button>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="media__wrapper">
                                                    <label class="media__place media__place--video">
                                                        <input class="js-input-video" id="video" name="video" type="file" accept="video/avi,  video/mp4,  video/3gp,  video/mpeg,  video/mov,  video/flv,  video/f4v,  video/wmv,  video/mkv,  video/webm,  video/vob,  video/rm,  video/rmvb,  video/m4v,  video/mpg,  video/ogv,  video/ts,  video/m2ts,  video/mts">
                                                        <div class="media__place-label"><img src="/images/upload-video-29112f42.svg" alt=""><span class="media__place-text">Перетащите видео или нажмите на область</span><span class="media__place-text media__place-text--mobile">Загрузить фото</span></div>
                                                    </label>
                                                    <div class="media__items media__items--video">
                                                        <div class="media__previews"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="media__info">
                                                <div class="media__desc">
                                                    <div class="media__desc-title">К размещению не принимаются:</div>
                                                    <ul>
                                                        <li>Фотографии лиц, не достигших 18-ти лет;</li>
                                                        <li>Откровенно порнографические фотографии, а также фотографии с обнаженными половыми органами;</li>
                                                        <li>Фотографии плохого качества или сильно видоизмененные графическими редакторами;</li>
                                                        <li>Фотографии содержащие любые логотипы, надписи, слоганы, тексты, и т.д., а так же фотографии не относящиеся к теме сайта;</li>
                                                        <li>Фотографии в рамке, а также прочие украшения</li>
                                                    </ul>
                                                </div>
                                                <div class="media__desc media__desc--bottom">
                                                    <div class="media__desc-title">Получить статус «Проверенные фото»</div><span>
                                                                Для подтверждения реальности ваших фотографий в анкете, Вам необходимо выслать дополнительное фото, на котором Вы держите в руках листок, где написано ваше имя, адрес сайта Don.com и надпись «100%». После
                                                                 проверки подлинности фотографии, Вы получаете статус «Проверенных фото», который подтверждает соответсвие
                                                                 Вашего фото действительности.</span>
                                                    <div class="media__desc-bottom">Данное фото видит только администратор и оно на сайте не размещается.</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="questionnaire-form__part">
                                    <div class="questionnaire-form__name">Дополнительные опции</div>
                                    <div class="questionnaire-form__options">
                                        <div class="questionnaire-form__option">
                                            <div class="checkbox">
                                                <input class="checkbox__input" id="automatic-show" type="checkbox" value="1" name="automatic-show">
                                                <label class="checkbox__label" for="automatic-show">Автоматически включить показ анкеты на сайтах после проверки ее администратором</label>
                                            </div>
                                        </div>
                                        <div class="questionnaire-form__option questionnaire-form__option--margin">
                                            <div class="checkbox">
                                                <input class="checkbox__input" id="vip" type="checkbox" value="1" name="vip">
                                                <label class="checkbox__label" for="vip">Активировать статус VIP</label>
                                            </div>
                                            <div class="questionnaire-form__option-text">
                                                <p>
                                                    Данный сервис значительно расширяет ваши возможности на сайте, а именно:<br> Ваша анкета всегда будет выше обычных анкет.<br>
                                                    Это дает неоспоримое преимущество перед другими в быстроте поиска Вас самыми выгодными клиентами.<br>
                                                    Ваша анкета будет иметь значок VIP, который даст возможность понять пользователям, что Вы лучшая.<br>
                                                </p>
                                            </div>
                                            <div class="questionnaire-form__option-text">
                                                <p>
                                                    Стоимость данной услуги составляет дополнительно<span> 2 токена в сутки. </span><br> Если денег на вашем счету недостаточно, пополните его в разделе «счет».<br>
                                                    В любой момент вы можете отключить эту функцию и перейти в обычный режим показа анкеты.<br>
                                                </p>
                                            </div>
                                        </div>
                                        <div class="questionnaire-form__option questionnaire-form__option--width">
                                            <div class="questionnaire-form__label">Период размещения VIP</div>
                                            <select class="js-select-form questionnaire-form__select" data-placeholder="Выберите период" name="period">
                                                <option value=""></option>
                                                <option value="Test1">1 день</option>
                                                <option value="Test2">2 день</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="questionnaire-form__part">
                                    <div class="questionnaire-form__name">График публикации анкеты</div>
                                    <div class="questionnaire-form__inner">
                                        <div class="questionnaire-form__radios">
                                            <div class="questionnaire-form__radio">
                                                <input class="questionnaire-form__radio-input js-radio" id="monday" type="checkbox" value="1" name="monday">
                                                <label class="questionnaire-form__radio-label" for="monday">пн</label>
                                            </div>
                                            <div class="questionnaire-form__radio">
                                                <input class="questionnaire-form__radio-input js-radio" id="tuesday" type="checkbox" value="1" name="tuesday">
                                                <label class="questionnaire-form__radio-label" for="tuesday">вт</label>
                                            </div>
                                            <div class="questionnaire-form__radio">
                                                <input class="questionnaire-form__radio-input js-radio" id="wednesday" type="checkbox" value="1" name="wednesday">
                                                <label class="questionnaire-form__radio-label" for="wednesday">ср</label>
                                            </div>
                                            <div class="questionnaire-form__radio">
                                                <input class="questionnaire-form__radio-input js-radio" id="thursday" type="checkbox" value="1" name="thursday">
                                                <label class="questionnaire-form__radio-label" for="thursday">чт</label>
                                            </div>
                                            <div class="questionnaire-form__radio">
                                                <input class="questionnaire-form__radio-input js-radio" id="friday" type="checkbox" value="1" name="friday">
                                                <label class="questionnaire-form__radio-label" for="friday">пт</label>
                                            </div>
                                            <div class="questionnaire-form__radio">
                                                <input class="questionnaire-form__radio-input js-radio" id="saturday" type="checkbox" value="1" name="saturday">
                                                <label class="questionnaire-form__radio-label" for="saturday">сб</label>
                                            </div>
                                            <div class="questionnaire-form__radio">
                                                <input class="questionnaire-form__radio-input js-radio" id="sunday" type="checkbox" value="1" name="sunday">
                                                <label class="questionnaire-form__radio-label" for="sunday">вс</label>
                                            </div>
                                        </div>
                                        <div class="questionnaire-form__status">
                                            <div class="checkbox">
                                                <input class="checkbox__input js-checkbox-all-day" id="all-day" type="checkbox" value="1" name="all-day">
                                                <label class="checkbox__label" for="all-day">Все дни</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="questionnaire-form__part">
                                    <div class="questionnaire-form__bottom">
                                        <div class="questionnaire-form__bottom-part">
                                            <div class="questionnaire-form__bottom-text">Общая стоимость показа этой анкеты</div>
                                            <div class="questionnaire-form__bottom-count">
                                                <div class="questionnaire-form__bottom-price"> 0 токенов<span> /сутки</span></div>
                                            </div>
                                        </div>
                                        <div class="questionnaire-form__bottom-part">
                                            <div class="questionnaire-form__bottom-btns">
                                                <button class="questionnaire-form__btn-clear js-clear-form" type="button">Очистить данные</button>
                                                <button class="questionnaire-form__btn-submit button">Добавить анкету</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="new-questionnaire__preloader"><svg version="1.1" id="L3" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                       viewBox="0 0 100 100" width="64" height="64" enable-background="new 0 0 0 0" xml:space="preserve">
<circle fill="none" stroke="#fff" stroke-width="4" cx="50" cy="50" r="44" style="opacity:0.5;"/>
                <circle fill="#fff" stroke="#FF9900" stroke-width="3" cx="8" cy="54" r="6" >
                    <animateTransform
                        attributeName="transform"
                        dur="1s"
                        type="rotate"
                        from="0 50 48"
                        to="360 50 52"
                        repeatCount="indefinite" />
                </circle>
</svg>

        </div>
    </div>
    @include('includes._bottom-banner', ['wrapperClass' => 'bottom-banner--transparent'])
@endsection
