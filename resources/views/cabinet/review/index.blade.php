@extends('layouts.cabinet')

@section('title')
    Отзывы
@endsection

@section('content')
    @include('cabinet._header', ['title' => 'Отзывы'])
    <div class="template">
        <div class="template__container container">
            <div class="template__body">
                <div class="template__sidebar">
                    <aside class="sidebar">
                        <div class="sidebar__wrap">
                            <ul class="sidebar__list">
                                <li class="sidebar__item sidebar__item--active"><a class="sidebar__link js-btn-screen"
                                                                                   href="/reviews-get.html">Полученные<span class="sidebar__value">20</span></a></li>
                                <li class="sidebar__item"><a class="sidebar__link js-btn-screen"
                                                             href="/reviews-send.html">Отправленные<span class="sidebar__value">20</span></a></li>
                            </ul>
                        </div>
                    </aside>
                </div>
                <div class="template__content">
                    <div class="reviews-get">
                        <div class="reviews-get__items">
                            <div class="reviews-get__item">
                                <div class="reviews-item reviews-item--get reviews-item--like">
                                    <div class="reviews-item__head">
                                        <div class="reviews-item__title">Название анкеты/объявления</div>
                                        <div class="reviews-item__head-wrap">
                                            <div class="reviews-item__button-wrap">
                                                <button class="reviews-item__button"></button>
                                            </div>
                                            <div class="reviews-item__date">06 мая 2020 14:14</div>
                                        </div>
                                    </div>
                                    <div class="reviews-item__content">
                                        <div class="reviews-item__desc">
                                            <div class="reviews-item__user">
                                                <div class="reviews-item__user-wrap">
                                                    <div class="reviews-item__icon"><img src="./images/user-icon-d74ee624.jpg"
                                                                                         alt="Константин">
                                                        <div class="reviews-item__status"></div>
                                                    </div>
                                                </div>
                                                <div class="reviews-item__name">Константин</div>
                                            </div>
                                            <div class="reviews-item__text">Повседневная практика показывает, что консультация с широким
                                                активом представляет собой интересный эксперимент проверки систем массового участия. Идейные
                                                соображения высшего порядка, а также сложившаяся структура организации представляет собой
                                                интересный эксперимент проверки позиций, занимаемых участниками в отношении поставленных
                                                задач.</div>
                                        </div>
                                        <div class="reviews-item__side">
                                            <div class="reviews-item__list">
                                                <div class="reviews-item__line">
                                                    <div class="reviews-item__line-title">Общая оценка</div>
                                                    <div class="reviews-item__line-rating">
                                                        <div class="rating-static rating-static--three rating-static--size">
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="reviews-item__line">
                                                    <div class="reviews-item__line-title">Фото соответствует</div>
                                                    <div class="reviews-item__line-rating">
                                                        <div class="rating-static">нет</div>
                                                    </div>
                                                </div>
                                                <div class="reviews-item__line">
                                                    <div class="reviews-item__line-title">Апартаменты</div>
                                                    <div class="reviews-item__line-rating">
                                                        <div class="rating-static rating-static--three">
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="reviews-item__line">
                                                    <div class="reviews-item__line-title">Сервис</div>
                                                    <div class="reviews-item__line-rating">
                                                        <div class="rating-static rating-static--two">
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="reviews-item__line">
                                                    <div class="reviews-item__line-title">Классический секс</div>
                                                    <div class="reviews-item__line-rating">
                                                        <div class="rating-static rating-static--one">
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="reviews-item__line">
                                                    <div class="reviews-item__line-title">Минет</div>
                                                    <div class="reviews-item__line-rating">
                                                        <div class="rating-static rating-static--four">
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="reviews-item__line">
                                                    <div class="reviews-item__line-title">Окончание</div>
                                                    <div class="reviews-item__line-rating">
                                                        <div class="rating-static rating-static--two">
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="reviews-item__bottom reviews-item__bottom--mobile">
                                                <div class="reviews-item__like like">Рекомендую</div>
                                                <div class="reviews-item__place">
                                                    <button class="reviews-item__btn">Ответить</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="reviews-item__bottom">
                                        <div class="reviews-item__like like">Рекомендую</div><a class="reviews-item__answer button"
                                                                                                href="#">Ответить</a>
                                    </div>
                                </div>
                            </div>
                            <div class="reviews-get__item">
                                <div class="reviews-item reviews-item--get reviews-item--like">
                                    <div class="reviews-item__head">
                                        <div class="reviews-item__title">Название анкеты/объявления</div>
                                        <div class="reviews-item__head-wrap">
                                            <div class="reviews-item__button-wrap">
                                                <button class="reviews-item__button"></button>
                                            </div>
                                            <div class="reviews-item__date">06 мая 2020 14:14</div>
                                        </div>
                                    </div>
                                    <div class="reviews-item__content">
                                        <div class="reviews-item__desc">
                                            <div class="reviews-item__user">
                                                <div class="reviews-item__user-wrap">
                                                    <div class="reviews-item__icon"><img src="./images/user-icon-d74ee624.jpg"
                                                                                         alt="Константин">
                                                        <div class="reviews-item__status"></div>
                                                    </div>
                                                </div>
                                                <div class="reviews-item__name">Константин</div>
                                            </div>
                                            <div class="reviews-item__text">Повседневная практика показывает, что консультация с широким
                                                активом представляет собой интересный эксперимент проверки систем массового участия. Идейные
                                                соображения высшего порядка, а также сложившаяся структура организации представляет собой
                                                интересный эксперимент проверки позиций, занимаемых участниками в отношении поставленных
                                                задач.</div>
                                        </div>
                                        <div class="reviews-item__side">
                                            <div class="reviews-item__list">
                                                <div class="reviews-item__line">
                                                    <div class="reviews-item__line-title">Общая оценка</div>
                                                    <div class="reviews-item__line-rating">
                                                        <div class="rating-static rating-static--three rating-static--size">
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="reviews-item__line">
                                                    <div class="reviews-item__line-title">Фото соответствует</div>
                                                    <div class="reviews-item__line-rating">
                                                        <div class="rating-static">нет</div>
                                                    </div>
                                                </div>
                                                <div class="reviews-item__line">
                                                    <div class="reviews-item__line-title">Апартаменты</div>
                                                    <div class="reviews-item__line-rating">
                                                        <div class="rating-static rating-static--three">
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="reviews-item__line">
                                                    <div class="reviews-item__line-title">Сервис</div>
                                                    <div class="reviews-item__line-rating">
                                                        <div class="rating-static rating-static--two">
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="reviews-item__line">
                                                    <div class="reviews-item__line-title">Классический секс</div>
                                                    <div class="reviews-item__line-rating">
                                                        <div class="rating-static rating-static--one">
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="reviews-item__line">
                                                    <div class="reviews-item__line-title">Минет</div>
                                                    <div class="reviews-item__line-rating">
                                                        <div class="rating-static rating-static--four">
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="reviews-item__line">
                                                    <div class="reviews-item__line-title">Окончание</div>
                                                    <div class="reviews-item__line-rating">
                                                        <div class="rating-static rating-static--two">
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="reviews-item__bottom reviews-item__bottom--mobile">
                                                <div class="reviews-item__like like">Рекомендую</div>
                                                <div class="reviews-item__place">
                                                    <button class="reviews-item__btn">Ответить</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="reviews-item__bottom">
                                        <div class="reviews-item__like like">Рекомендую</div><a class="reviews-item__answer button"
                                                                                                href="#">Ответить</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="template__pagination">
                            <div class="pagination">
                                <div class="pagination__wrap">
                                    <button class="pagination__button pagination__button--back pagination__button--disabled">
                                        <svg width="7" height="12" viewBox="0 0 7 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M6 1L1 6L6 11" stroke="#5C5C5C" stroke-width="1.8" stroke-linecap="round"
                                                  stroke-linejoin="round" />
                                        </svg> Назад </button>
                                    <ul class="pagination__list">
                                        <li class="pagination__item pagination__item--active"><a class="pagination__link" href="#">1</a>
                                        </li>
                                        <li class="pagination__item"><a class="pagination__link" href="#">2</a></li>
                                        <li class="pagination__item"><a class="pagination__link pagination__link--dot" href="#">...</a>
                                        </li>
                                    </ul>
                                    <button class="pagination__button"> Далее<svg width="7" height="12" viewBox="0 0 7 12" fill="none"
                                                                                  xmlns="http://www.w3.org/2000/svg">
                                            <path d="M1 11L6 6L1 1" stroke="#5C5C5C" stroke-width="1.8" stroke-linecap="round"
                                                  stroke-linejoin="round" />
                                        </svg>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('includes._bottom-banner', ['wrapperClass' => 'bottom-banner--transparent'])
@endsection
