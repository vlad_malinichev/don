<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="UTF-8">
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title', 'Welcome to the site')</title>
    <meta name="keywords" content="">
    <meta name="description" content="">
    <link rel="shortcut icon" href="{{ asset('images/favicon-338abbb5.ico') }}" type="image/x-icon">
    <link rel="apple-touch-icon" href="{{ asset('images/apple-touch-icon-2805113e.png') }}" type="image/png">
    <!-- Styles -->
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
</head>
<body>
<div class="wrapper" data-locale="{{ app()->getLocale() }}">
    @include('layouts.parts._header')
    <main class="cabinet">
        @include('includes.flash-message')
        @yield('content')
    </main>
    @include('layouts.parts._footer')
</div>
<!-- Scripts -->
<script src="{{ asset('libs/jquery/jquery-3.2.1.min.js') }}"></script>
<script src="{{ asset('libs/autocomplete/autocomplete.min.js') }}"></script>
<script src="{{ asset('libs/select2/select2.full.min.js') }}"></script>
<script src="{{ asset('libs/swiper/swiper-bundle.min.js') }}"></script>
<script src="{{ asset('libs/ion/ion.rangeSlider.min.js') }}"></script>
<script src="{{ asset('libs/magnific-popup/jquery.magnific-popup.min.js') }}"></script>
<script src="{{ asset('libs/jquery-countdown/jquery.countdown.min.js') }}"></script>
<script src="{{ asset('libs/dropzone/dropzone.min.js') }}"></script>
<script src="{{ asset('libs/tippy/popper.min.js') }}"></script>
<script src="{{ asset('libs/tippy/tippy.min.js') }}"></script>
<script src="{{ asset('js/media.js') }}"></script>
<script src="{{ asset('js/chat.js') }}"></script>
<script src="{{ asset('js/main.js') }}"></script>
</body>
</html>
