<footer class="footer">
    <div class="container footer__container">
        <div class="footer__top">
            <div class="footer__holder">
                <div class="footer__app-store">
                    <div class="app-store">
                        <a class="app-store__item" href="//www.apple.com/ru/ios/app-store/" target="_blank">
                            <img class="app-store__img" src="/images/app-store-cb893930.jpg" alt="app store">
                        </a>
                        <a class="app-store__item" href="//play.google.com/store" target="_blank">
                            <img class="app-store__img" src="/images/google-play-943c7416.jpg" alt="google play">
                        </a>
                    </div>
                </div>
                <ul class="footer__menu">
                    <li class="footer__menu-item"><a class="footer__menu-link" href="/">Главная</a></li>
                    <li class="footer__menu-item"><a class="footer__menu-link" href="/feedback.html">Обратная связь</a></li>
                    <li class="footer__menu-item"><a class="footer__menu-link" href="/privacy.html">Правила портала</a></li>
                    <li class="footer__menu-item"><a class="footer__menu-link" href="price.html">Цены</a></li>
                    <li class="footer__menu-item"><a class="footer__menu-link" href="/articles.html">Статьи</a></li>
                    <li class="footer__menu-item"><a class="footer__menu-link" href="/model-instructions.html">Инструкции для
                            моделей</a></li>
                    <li class="footer__menu-item"><a class="footer__menu-link" href="/customer-instructions.html">Инструкции
                            для клиентов</a></li>
                </ul>
            </div>
            <div class="footer__laptop">
                <div class="footer__copyright">
                    <p class="footer__copyright-text">© 2019-2020. Все права защищены.</p>
                    <p class="footer__copyright-text">Копирование материалов запрещено.</p>
                </div>
                <div class="footer__app-store">
                    <div class="app-store">
                        <a class="app-store__item" href="//www.apple.com/ru/ios/app-store/" target="_blank">
                            <img class="app-store__img" src="/images/app-store-cb893930.jpg" alt="app store">
                        </a>
                        <a class="app-store__item" href="//play.google.com/store" target="_blank">
                            <img class="app-store__img" src="/images/google-play-943c7416.jpg" alt="google play">
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer__bottom">
            <div class="footer__copyright">
                <p class="footer__copyright-text">© 2019-2020. Все права защищены.</p>
                <p class="footer__copyright-text">Копирование материалов запрещено.</p>
            </div>
            <div class="footer__content">
                <div class="footer__descr">
                    <p class="footer__text">Администрация сайта, исключительно, предоставляет площадку для размещения рекламы
                        и ответственности за ее содержимое не несет.</p>
                    <p class="footer__text">В свою очередь, все посетители сайта а также лица, связанные с этим видом
                        деятельности, обязуются, что достигли 18-тилетнего возраста.</p>
                </div><img class="footer__age-limit" src="/images/age-limit-b7004eb2.jpg" alt="age limit">
            </div>
        </div>
    </div>
</footer>

@include('auth.modal')
