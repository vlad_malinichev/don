<header class="header header--basic">
    <div class="header__wrapper">
        <div class="header__main">
            <div class="header__container container">
                <div class="header__inner">
                    <a class="header__logo logo" href="{{ url('/') }}">
                        <img class="logo__img" src="{{ asset('images/logo-31ac340c.svg') }}" alt="logo">
                    </a>
                    <div class="header__info">
                        <a class="header__add-profile button button--transparent" href="{{ route('questionnaire.create') }}">
                            <svg width="18" height="18" viewbox="0 0 18 18" fill="none"
                                 xmlns="http://www.w3.org/2000/svg">
                                <path d="M9 13.9091V4.09091V9.14876" stroke="#ff9900" stroke-width="2"
                                      stroke-linecap="round"
                                      stroke-linejoin="round"></path>
                                <path d="M4.09082 9H13.909H8.99991" stroke="#ff9900" stroke-width="2"
                                      stroke-linecap="round"
                                      stroke-linejoin="round"></path>
                            </svg>
                            Добавить анкету
                        </a>
                        <div class="header__language">
                            <div class="header__language-placeholder placeholder"></div>
                            <div class="header__language-select">
                                <select class="js-select-lang" name="language"></select>
                            </div>
                        </div>
                        <div class="header__favorites">
                            <img class="header__favorites-icon"
                                 src="./images/favorites-ccf9eb9c.svg"
                                 alt="icon favorites">
                            <span class="header__favorites-amount">0</span>
                        </div>
                        <a
                            class="header__cabinet button button--white js-login-modal" href="#modal-login"
                            style="display: none;">
                            <svg width="14" height="16" viewBox="0 0 14 16" fill="none"
                                 xmlns="http://www.w3.org/2000/svg">
                                <g clip-path="url(#clip0)">
                                    <path
                                        d="M7.00024 8.17195C8.93324 8.17195 10.5002 6.49504 10.5002 4.42647C10.5002 2.3579 8.93324 0.680996 7.00024 0.680996C5.06725 0.680996 3.50024 2.3579 3.50024 4.42647C3.50024 6.49504 5.06725 8.17195 7.00024 8.17195Z"
                                        stroke="white" stroke-width="1.2" stroke-miterlimit="10"
                                        stroke-linecap="round"/>
                                    <path
                                        d="M13.364 12.5099V14.5767C13.364 14.7742 13.3004 15.319 13.1158 15.319H1.04399C0.862628 15.3224 0.636719 14.7742 0.636719 14.5767V12.3396C0.636719 10.0821 3.50036 8.25367 7.00036 8.25367C10.5004 8.25367 13.364 10.0821 13.364 12.3396V12.5099Z"
                                        stroke="white" stroke-width="1.2" stroke-miterlimit="10"
                                        stroke-linecap="round"/>
                                </g>
                                <defs>
                                    <clipPath id="clip0">
                                        <rect width="14" height="16" fill="white" transform="translate(0.000244141)"/>
                                    </clipPath>
                                </defs>
                            </svg>
                            <span>Личный кабинет</span>
                        </a>
                        <div class="header__account" style="display: block;">
                            <div class="account-dropdown account-dropdown--convert">
                                <div class="account-dropdown__selection js-account-dropdown"><img
                                        class="account-dropdown__avatar"
                                        src="./images/account-dropdown-353a15a0.jpg" alt="avatar">
                                    <div class="account-dropdown__name">Анжелика</div>
                                    <div class="account-dropdown__arrow"></div>
                                </div>
                                <ul class="account-dropdown__list">
                                    <li class="account-dropdown__item"><a class="account-dropdown__link" href="#"><span
                                                class="account-dropdown__label">Анкеты</span><span
                                                class="account-dropdown__value">4</span></a></li>
                                    <li class="account-dropdown__item"><a class="account-dropdown__link" href="#"><span
                                                class="account-dropdown__label">Рекламные баннеры</span><span
                                                class="account-dropdown__value">4</span></a></li>
                                    <li class="account-dropdown__item"><a class="account-dropdown__link" href="#"><span
                                                class="account-dropdown__label">Сообщения</span><span
                                                class="account-dropdown__value">568</span></a></li>
                                    <li class="account-dropdown__item"><a class="account-dropdown__link" href="#"><span
                                                class="account-dropdown__label">Избранное</span><span
                                                class="account-dropdown__value">38</span></a></li>
                                    <li class="account-dropdown__item"><a class="account-dropdown__link" href="#"><span
                                                class="account-dropdown__label">Подарки</span><span
                                                class="account-dropdown__value">35</span></a></li>
                                    <li class="account-dropdown__item"><a class="account-dropdown__link" href="#"><span
                                                class="account-dropdown__label">Отзывы</span><span
                                                class="account-dropdown__value">14</span></a></li>
                                    <li class="account-dropdown__item"><a class="account-dropdown__link" href="#"><span
                                                class="account-dropdown__label">Плохие клиенты</span></a></li>
                                    <li class="account-dropdown__item"><a class="account-dropdown__link" href="#"><span
                                                class="account-dropdown__label">Счёт и платежи</span></a></li>
                                    <li class="account-dropdown__item"><a class="account-dropdown__link" href="#"><span
                                                class="account-dropdown__label">Настройки</span></a></li>
                                    <li class="account-dropdown__item"><a class="account-dropdown__link" href="#"><span
                                                class="account-dropdown__label">Выйти</span></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="header__actions">
                        <img class="header__action js-search-menu" src="{{ asset('images/search-yellow-46b36f68.svg') }}" alt="icon search">
                        <img class="header__action js-burger" src="{{ asset('images/burger-522238e2.svg') }}" alt="icon burger">
                    </div>
                    <div class="header__pannel">
                        <div class="header__pannel-container">

                            @guest
                                <a class="header__pannel-button button button--white js-login-modal" href="#modal-login">
                                    <svg
                                        width="14" height="16" viewBox="0 0 14 16" fill="none"
                                        xmlns="http://www.w3.org/2000/svg">
                                        <g clip-path="url(#clip0)">
                                            <path
                                                d="M7.00024 8.17195C8.93324 8.17195 10.5002 6.49504 10.5002 4.42647C10.5002 2.3579 8.93324 0.680996 7.00024 0.680996C5.06725 0.680996 3.50024 2.3579 3.50024 4.42647C3.50024 6.49504 5.06725 8.17195 7.00024 8.17195Z"
                                                stroke="white" stroke-width="1.2" stroke-miterlimit="10"
                                                stroke-linecap="round"/>
                                            <path
                                                d="M13.364 12.5099V14.5767C13.364 14.7742 13.3004 15.319 13.1158 15.319H1.04399C0.862628 15.3224 0.636719 14.7742 0.636719 14.5767V12.3396C0.636719 10.0821 3.50036 8.25367 7.00036 8.25367C10.5004 8.25367 13.364 10.0821 13.364 12.3396V12.5099Z"
                                                stroke="white" stroke-width="1.2" stroke-miterlimit="10"
                                                stroke-linecap="round"/>
                                        </g>
                                        <defs>
                                            <clipPath id="clip0">
                                                <rect width="14" height="16" fill="white"
                                                      transform="translate(0.000244141)"/>
                                            </clipPath>
                                        </defs>
                                    </svg>
                                    <span>{{ __('auth.login') }}</span>
                                </a>
                            @else
                                <div class="header__pannel-account" style="display: block">
                                    <div class="account-dropdown">
                                        <div class="account-dropdown__selection js-account-dropdown"><img
                                                class="account-dropdown__avatar"
                                                src="{{ asset('images/account-dropdown-353a15a0.jpg') }}" alt="avatar">
                                            <div class="account-dropdown__name">{{ Auth::user()->name }}</div>
                                            <div class="account-dropdown__arrow"></div>
                                        </div>
                                        <ul class="account-dropdown__list">
                                            <li class="account-dropdown__item">
                                                <a class="account-dropdown__link" href="#">
                                                    <span class="account-dropdown__label">Анкеты</span>
                                                    <span class="account-dropdown__value">4</span>
                                                </a>
                                            </li>
                                            <li class="account-dropdown__item">
                                                <a class="account-dropdown__link" href="#">
                                                    <span class="account-dropdown__label">Рекламные баннеры</span>
                                                    <span class="account-dropdown__value">4</span>
                                                </a>
                                            </li>
                                            <li class="account-dropdown__item"><a class="account-dropdown__link"
                                                                                  href="#"><span
                                                        class="account-dropdown__label">Сообщения</span><span
                                                        class="account-dropdown__value">568</span></a></li>
                                            <li class="account-dropdown__item"><a class="account-dropdown__link"
                                                                                  href="#"><span
                                                        class="account-dropdown__label">Избранное</span><span
                                                        class="account-dropdown__value">38</span></a></li>
                                            <li class="account-dropdown__item"><a class="account-dropdown__link"
                                                                                  href="#"><span
                                                        class="account-dropdown__label">Подарки</span><span
                                                        class="account-dropdown__value">35</span></a></li>
                                            <li class="account-dropdown__item"><a class="account-dropdown__link"
                                                                                  href="#"><span
                                                        class="account-dropdown__label">Отзывы</span><span
                                                        class="account-dropdown__value">14</span></a></li>
                                            <li class="account-dropdown__item"><a class="account-dropdown__link"
                                                                                  href="#"><span
                                                        class="account-dropdown__label">Плохие клиенты</span></a></li>
                                            <li class="account-dropdown__item"><a class="account-dropdown__link"
                                                                                  href="#"><span
                                                        class="account-dropdown__label">Счёт и платежи</span></a></li>
                                            <li class="account-dropdown__item">
                                                <a class="account-dropdown__link" href="#"><span
                                                        class="account-dropdown__label">Настройки</span></a></li>
                                            <li class="account-dropdown__item">
                                                <a class="account-dropdown__link"
                                                   href="{{ route('logout') }}"
                                                   onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                                    <span class="account-dropdown__label">{{ __('auth.logout') }}</span>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            @endguest

                            <a class="header__pannel-button button button--transparent" href="#">
                                <svg width="18" height="18" viewbox="0 0 18 18" fill="none"
                                     xmlns="http://www.w3.org/2000/svg">
                                    <path d="M9 13.9091V4.09091V9.14876" stroke="#ff9900" stroke-width="2"
                                          stroke-linecap="round"
                                          stroke-linejoin="round"></path>
                                    <path d="M4.09082 9H13.909H8.99991" stroke="#ff9900" stroke-width="2"
                                          stroke-linecap="round"
                                          stroke-linejoin="round"></path>
                                </svg>
                                Добавить анкету
                            </a>
                            <div class="header__pannel-select">
                                <select class="js-select-lang" name="language"></select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="header__body">
            <div class="header__container container">
                <form class="header__search search js-search" action="/">
                    <div class="search__by">
                        <input class="search__by-control" type="text" name="search"
                               placeholder="Поиск по id анкеты / номеру телефона / имени" autocomplete="off">
                    </div>
                    <div class="search__region">
                        <input class="search__region-control js-search-region" id="autoComplete" type="text"
                               name="country"
                               placeholder="Вся Россия" autocomplete="off">
                    </div>
                    <button class="search__button button button--grey" type="submit">
                        <svg width="15" height="16" viewBox="0 0 15 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M10.4714 11.2764L14.2857 15.2727" stroke="white" stroke-width="1.4"
                                  stroke-miterlimit="10"
                                  stroke-linecap="round"/>
                            <path
                                d="M6.53221 12.4364C9.74533 12.4364 12.3501 9.8152 12.3501 6.58183C12.3501 3.34845 9.74533 0.72728 6.53221 0.72728C3.3191 0.72728 0.714355 3.34845 0.714355 6.58183C0.714355 9.8152 3.3191 12.4364 6.53221 12.4364Z"
                                stroke="white" stroke-width="1.4" stroke-miterlimit="10" stroke-linecap="round"/>
                        </svg>
                        Найти
                    </button>
                    <div class="search__form js-search-form">
                        <div class="form-search js-form-search" action="/" data-location="[
  {&quot;id&quot;: 7,&quot;name&quot;:&quot;Россия&quot;,&quot;regions&quot;:[{&quot;id&quot;:28,&quot;name&quot;:&quot;Амурская область&quot;,&quot;cities&quot;:[{&quot;id&quot;:41658,&quot;name&quot;:&quot;Москва&quot;},{&quot;id&quot;:41643,&quot;name&quot;:&quot;Балашиха&quot;},{&quot;id&quot;:41658,&quot;name&quot;:&quot;Бронницы&quot;},{&quot;id&quot;:41643,&quot;name&quot;:&quot;Видное (Ленинский)&quot;},{&quot;id&quot;:41658,&quot;name&quot;:&quot;Волоколамск&quot;},{&quot;id&quot;:41643,&quot;name&quot;:&quot;Воскресенск&quot;},{&quot;id&quot;:41658,&quot;name&quot;:&quot;Восход&quot;},{&quot;id&quot;:41643,&quot;name&quot;:&quot;Дзержинский&quot;},{&quot;id&quot;:41658,&quot;name&quot;:&quot;Дмитров&quot;},{&quot;id&quot;:41643,&quot;name&quot;:&quot;Долгопрудный&quot;},{&quot;id&quot;:41658,&quot;name&quot;:&quot;Домодедово&quot;},{&quot;id&quot;:41643,&quot;name&quot;:&quot;Дубна&quot;},{&quot;id&quot;:41658,&quot;name&quot;:&quot;Егорьевск&quot;},{&quot;id&quot;:41643,&quot;name&quot;:&quot;Зарайск&quot;},{&quot;id&quot;:41658,&quot;name&quot;:&quot;Звёздный городок&quot;},{&quot;id&quot;:41643,&quot;name&quot;:&quot;Ивантеевка&quot;},{&quot;id&quot;:41658,&quot;name&quot;:&quot;Истра&quot;},{&quot;id&quot;:41643,&quot;name&quot;:&quot;Кашира&quot;},{&quot;id&quot;:41658,&quot;name&quot;:&quot;Клин&quot;},{&quot;id&quot;:41643,&quot;name&quot;:&quot;Коломна&quot;},{&quot;id&quot;:41658,&quot;name&quot;:&quot;Королёв&quot;},{&quot;id&quot;:41643,&quot;name&quot;:&quot;Котельники&quot;},{&quot;id&quot;:41658,&quot;name&quot;:&quot;Красноармейск&quot;},{&quot;id&quot;:41643,&quot;name&quot;:&quot;Краснознаменск&quot;},{&quot;id&quot;:41658,&quot;name&quot;:&quot;Красногорск&quot;},{&quot;id&quot;:41643,&quot;name&quot;:&quot;Лобня&quot;},{&quot;id&quot;:41658,&quot;name&quot;:&quot;Лосино-Петровский&quot;},{&quot;id&quot;:41643,&quot;name&quot;:&quot;Луховицы&quot;},{&quot;id&quot;:41658,&quot;name&quot;:&quot;Лыткарино&quot;},{&quot;id&quot;:41643,&quot;name&quot;:&quot;Люберцы&quot;},{&quot;id&quot;:41658,&quot;name&quot;:&quot;Можайск&quot;},{&quot;id&quot;:41643,&quot;name&quot;:&quot;Молодёжный&quot;},{&quot;id&quot;:41658,&quot;name&quot;:&quot;Мытищи&quot;},{&quot;id&quot;:41643,&quot;name&quot;:&quot;Наро-Фоминск&quot;},{&quot;id&quot;:41658,&quot;name&quot;:&quot;Ногинск (Богородский)&quot;},{&quot;id&quot;:41643,&quot;name&quot;:&quot;Одинцово&quot;},{&quot;id&quot;:41658,&quot;name&quot;:&quot;Озёры&quot;},{&quot;id&quot;:41643,&quot;name&quot;:&quot;Орехово-Зуево&quot;},{&quot;id&quot;:41658,&quot;name&quot;:&quot;Павловский Посад&quot;},{&quot;id&quot;:41643,&quot;name&quot;:&quot;Подольск&quot;},{&quot;id&quot;:41658,&quot;name&quot;:&quot;Протвино&quot;},{&quot;id&quot;:41643,&quot;name&quot;:&quot;Пушкино&quot;},{&quot;id&quot;:41658,&quot;name&quot;:&quot;Пущино&quot;},{&quot;id&quot;:41643,&quot;name&quot;:&quot;Раменское&quot;},{&quot;id&quot;:41658,&quot;name&quot;:&quot;Реутов&quot;},{&quot;id&quot;:41643,&quot;name&quot;:&quot;Руза&quot;},{&quot;id&quot;:41658,&quot;name&quot;:&quot;Сергиев Посад&quot;},{&quot;id&quot;:41643,&quot;name&quot;:&quot;Серебряные Пруды&quot;},{&quot;id&quot;:41658,&quot;name&quot;:&quot;Серпухов&quot;},{&quot;id&quot;:41643,&quot;name&quot;:&quot;Солнечногорск&quot;},{&quot;id&quot;:41658,&quot;name&quot;:&quot;Ступино&quot;},{&quot;id&quot;:41643,&quot;name&quot;:&quot;Талдом&quot;},{&quot;id&quot;:41658,&quot;name&quot;:&quot;Фрязино&quot;},{&quot;id&quot;:41643,&quot;name&quot;:&quot;Химки&quot;},{&quot;id&quot;:41658,&quot;name&quot;:&quot;Черноголовка&quot;},{&quot;id&quot;:41643,&quot;name&quot;:&quot;Чехов&quot;},{&quot;id&quot;:41658,&quot;name&quot;:&quot;Шатура&quot;},{&quot;id&quot;:41643,&quot;name&quot;:&quot;Шаховская&quot;},{&quot;id&quot;:41658,&quot;name&quot;:&quot;Щёлково&quot;},{&quot;id&quot;:41643,&quot;name&quot;:&quot;Электрогорск&quot;},{&quot;id&quot;:41658,&quot;name&quot;:&quot;Электросталь&quot;}]},{&quot;id&quot;:29,&quot;name&quot;:&quot;Архангельская область&quot;,&quot;cities&quot;:[{&quot;id&quot;:81834,&quot;name&quot;:&quot;Мирный&quot;},{&quot;id&quot;:81839,&quot;name&quot;:&quot;Онега&quot;}]},{&quot;id&quot;:30,&quot;name&quot;:&quot;Астраханская область&quot;,&quot;cities&quot;:[{&quot;id&quot;:81834,&quot;name&quot;:&quot;Мирный&quot;},{&quot;id&quot;:81839,&quot;name&quot;:&quot;Онега&quot;}]},{&quot;id&quot;:31,&quot;name&quot;:&quot;Белгородская область&quot;,&quot;cities&quot;:[{&quot;id&quot;:81834,&quot;name&quot;:&quot;Мирный&quot;},{&quot;id&quot;:81839,&quot;name&quot;:&quot;Онега&quot;}]},{&quot;id&quot;:32,&quot;name&quot;:&quot;Брянская область&quot;,&quot;cities&quot;:[{&quot;id&quot;:81834,&quot;name&quot;:&quot;Мирный&quot;},{&quot;id&quot;:81839,&quot;name&quot;:&quot;Онега&quot;}]},{&quot;id&quot;:33,&quot;name&quot;:&quot;Владимирская область&quot;,&quot;cities&quot;:[{&quot;id&quot;:81834,&quot;name&quot;:&quot;Мирный&quot;},{&quot;id&quot;:81839,&quot;name&quot;:&quot;Онега&quot;}]},{&quot;id&quot;:34,&quot;name&quot;:&quot;Волгоградская область&quot;,&quot;cities&quot;:[{&quot;id&quot;:81834,&quot;name&quot;:&quot;Мирный&quot;},{&quot;id&quot;:81839,&quot;name&quot;:&quot;Онега&quot;}]},{&quot;id&quot;:35,&quot;name&quot;:&quot;Вологодская область&quot;,&quot;cities&quot;:[{&quot;id&quot;:81834,&quot;name&quot;:&quot;Мирный&quot;},{&quot;id&quot;:81839,&quot;name&quot;:&quot;Онега&quot;}]},{&quot;id&quot;:36,&quot;name&quot;:&quot;Воронежская область&quot;,&quot;cities&quot;:[{&quot;id&quot;:81834,&quot;name&quot;:&quot;Мирный&quot;},{&quot;id&quot;:81839,&quot;name&quot;:&quot;Онега&quot;}]},{&quot;id&quot;:37,&quot;name&quot;:&quot;Иркутская область&quot;,&quot;cities&quot;:[{&quot;id&quot;:81834,&quot;name&quot;:&quot;Мирный&quot;},{&quot;id&quot;:81839,&quot;name&quot;:&quot;Онега&quot;}]},{&quot;id&quot;:38,&quot;name&quot;:&quot;Ивановская область&quot;,&quot;cities&quot;:[{&quot;id&quot;:81834,&quot;name&quot;:&quot;Мирный&quot;},{&quot;id&quot;:81839,&quot;name&quot;:&quot;Онега&quot;}]},{&quot;id&quot;:39,&quot;name&quot;:&quot;Калининградская область&quot;,&quot;cities&quot;:[{&quot;id&quot;:81834,&quot;name&quot;:&quot;Мирный&quot;},{&quot;id&quot;:81839,&quot;name&quot;:&quot;Онега&quot;}]},{&quot;id&quot;:40,&quot;name&quot;:&quot;Калужская область&quot;,&quot;cities&quot;:[{&quot;id&quot;:81834,&quot;name&quot;:&quot;Мирный&quot;},{&quot;id&quot;:81839,&quot;name&quot;:&quot;Онега&quot;}]},{&quot;id&quot;:29,&quot;name&quot;:&quot;Кемеровская область&quot;,&quot;cities&quot;:[{&quot;id&quot;:81834,&quot;name&quot;:&quot;Мирный&quot;},{&quot;id&quot;:81839,&quot;name&quot;:&quot;Онега&quot;}]},{&quot;id&quot;:30,&quot;name&quot;:&quot;Кировская область&quot;,&quot;cities&quot;:[{&quot;id&quot;:81834,&quot;name&quot;:&quot;Мирный&quot;},{&quot;id&quot;:81839,&quot;name&quot;:&quot;Онега&quot;}]},{&quot;id&quot;:29,&quot;name&quot;:&quot;Костромская область&quot;,&quot;cities&quot;:[{&quot;id&quot;:81834,&quot;name&quot;:&quot;Мирный&quot;},{&quot;id&quot;:81839,&quot;name&quot;:&quot;Онега&quot;}]},{&quot;id&quot;:30,&quot;name&quot;:&quot;Курганская область&quot;,&quot;cities&quot;:[{&quot;id&quot;:81834,&quot;name&quot;:&quot;Мирный&quot;},{&quot;id&quot;:81839,&quot;name&quot;:&quot;Онега&quot;}]},{&quot;id&quot;:29,&quot;name&quot;:&quot;Курская область&quot;,&quot;cities&quot;:[{&quot;id&quot;:81834,&quot;name&quot;:&quot;Мирный&quot;},{&quot;id&quot;:81839,&quot;name&quot;:&quot;Онега&quot;}]},{&quot;id&quot;:30,&quot;name&quot;:&quot;Ленинградская область&quot;,&quot;cities&quot;:[{&quot;id&quot;:81834,&quot;name&quot;:&quot;Мирный&quot;},{&quot;id&quot;:81839,&quot;name&quot;:&quot;Онега&quot;}]},{&quot;id&quot;:29,&quot;name&quot;:&quot;Липецкая область&quot;,&quot;cities&quot;:[{&quot;id&quot;:81834,&quot;name&quot;:&quot;Мирный&quot;},{&quot;id&quot;:81839,&quot;name&quot;:&quot;Онега&quot;}]},{&quot;id&quot;:30,&quot;name&quot;:&quot;Магаданская область&quot;,&quot;cities&quot;:[{&quot;id&quot;:81834,&quot;name&quot;:&quot;Мирный&quot;},{&quot;id&quot;:81839,&quot;name&quot;:&quot;Онега&quot;}]},{&quot;id&quot;:31,&quot;name&quot;:&quot;Московская область&quot;,&quot;cities&quot;:[{&quot;id&quot;:81834,&quot;name&quot;:&quot;Мирный&quot;},{&quot;id&quot;:81839,&quot;name&quot;:&quot;Онега&quot;}]},{&quot;id&quot;:32,&quot;name&quot;:&quot;Мурманская область&quot;,&quot;cities&quot;:[{&quot;id&quot;:81834,&quot;name&quot;:&quot;Мирный&quot;},{&quot;id&quot;:81839,&quot;name&quot;:&quot;Онега&quot;}]},{&quot;id&quot;:33,&quot;name&quot;:&quot;Нижегородская область&quot;,&quot;cities&quot;:[{&quot;id&quot;:81834,&quot;name&quot;:&quot;Мирный&quot;},{&quot;id&quot;:81839,&quot;name&quot;:&quot;Онега&quot;}]},{&quot;id&quot;:34,&quot;name&quot;:&quot;Новгородская область&quot;,&quot;cities&quot;:[{&quot;id&quot;:81834,&quot;name&quot;:&quot;Мирный&quot;},{&quot;id&quot;:81839,&quot;name&quot;:&quot;Онега&quot;}]},{&quot;id&quot;:35,&quot;name&quot;:&quot;Новосибирская область&quot;,&quot;cities&quot;:[{&quot;id&quot;:81834,&quot;name&quot;:&quot;Мирный&quot;},{&quot;id&quot;:81839,&quot;name&quot;:&quot;Онега&quot;}]},{&quot;id&quot;:36,&quot;name&quot;:&quot;Омская область&quot;,&quot;cities&quot;:[{&quot;id&quot;:81834,&quot;name&quot;:&quot;Мирный&quot;},{&quot;id&quot;:81839,&quot;name&quot;:&quot;Онега&quot;}]},{&quot;id&quot;:37,&quot;name&quot;:&quot;Оренбургская область&quot;,&quot;cities&quot;:[{&quot;id&quot;:81834,&quot;name&quot;:&quot;Мирный&quot;},{&quot;id&quot;:81839,&quot;name&quot;:&quot;Онега&quot;}]},{&quot;id&quot;:38,&quot;name&quot;:&quot;Орловская область&quot;,&quot;cities&quot;:[{&quot;id&quot;:81834,&quot;name&quot;:&quot;Мирный&quot;},{&quot;id&quot;:81839,&quot;name&quot;:&quot;Онега&quot;}]},{&quot;id&quot;:39,&quot;name&quot;:&quot;Пензенская область&quot;,&quot;cities&quot;:[{&quot;id&quot;:81834,&quot;name&quot;:&quot;Мирный&quot;},{&quot;id&quot;:81839,&quot;name&quot;:&quot;Онега&quot;}]},{&quot;id&quot;:40,&quot;name&quot;:&quot;Псковская область&quot;,&quot;cities&quot;:[{&quot;id&quot;:81834,&quot;name&quot;:&quot;Мирный&quot;},{&quot;id&quot;:81839,&quot;name&quot;:&quot;Онега&quot;}]},{&quot;id&quot;:29,&quot;name&quot;:&quot;Ростовская область&quot;,&quot;cities&quot;:[{&quot;id&quot;:81834,&quot;name&quot;:&quot;Мирный&quot;},{&quot;id&quot;:81839,&quot;name&quot;:&quot;Онега&quot;}]},{&quot;id&quot;:30,&quot;name&quot;:&quot;Рязанская область&quot;,&quot;cities&quot;:[{&quot;id&quot;:81834,&quot;name&quot;:&quot;Мирный&quot;},{&quot;id&quot;:81839,&quot;name&quot;:&quot;Онега&quot;}]},{&quot;id&quot;:29,&quot;name&quot;:&quot;Сахалинская область&quot;,&quot;cities&quot;:[{&quot;id&quot;:81834,&quot;name&quot;:&quot;Мирный&quot;},{&quot;id&quot;:81839,&quot;name&quot;:&quot;Онега&quot;}]},{&quot;id&quot;:30,&quot;name&quot;:&quot;Самарская область&quot;,&quot;cities&quot;:[{&quot;id&quot;:81834,&quot;name&quot;:&quot;Мирный&quot;},{&quot;id&quot;:81839,&quot;name&quot;:&quot;Онега&quot;}]},{&quot;id&quot;:29,&quot;name&quot;:&quot;Саратовская область&quot;,&quot;cities&quot;:[{&quot;id&quot;:81834,&quot;name&quot;:&quot;Мирный&quot;},{&quot;id&quot;:81839,&quot;name&quot;:&quot;Онега&quot;}]},{&quot;id&quot;:30,&quot;name&quot;:&quot;Смоленская область&quot;,&quot;cities&quot;:[{&quot;id&quot;:81834,&quot;name&quot;:&quot;Мирный&quot;},{&quot;id&quot;:81839,&quot;name&quot;:&quot;Онега&quot;}]},{&quot;id&quot;:38,&quot;name&quot;:&quot;Свердловская область&quot;,&quot;cities&quot;:[{&quot;id&quot;:81834,&quot;name&quot;:&quot;Мирный&quot;},{&quot;id&quot;:81839,&quot;name&quot;:&quot;Онега&quot;}]},{&quot;id&quot;:39,&quot;name&quot;:&quot;Тамбовская область&quot;,&quot;cities&quot;:[{&quot;id&quot;:81834,&quot;name&quot;:&quot;Мирный&quot;},{&quot;id&quot;:81839,&quot;name&quot;:&quot;Онега&quot;}]},{&quot;id&quot;:40,&quot;name&quot;:&quot;Томская область&quot;,&quot;cities&quot;:[{&quot;id&quot;:81834,&quot;name&quot;:&quot;Мирный&quot;},{&quot;id&quot;:81839,&quot;name&quot;:&quot;Онега&quot;}]},{&quot;id&quot;:29,&quot;name&quot;:&quot;Тверская область&quot;,&quot;cities&quot;:[{&quot;id&quot;:81834,&quot;name&quot;:&quot;Мирный&quot;},{&quot;id&quot;:81839,&quot;name&quot;:&quot;Онега&quot;}]},{&quot;id&quot;:30,&quot;name&quot;:&quot;Тульская область&quot;,&quot;cities&quot;:[{&quot;id&quot;:81834,&quot;name&quot;:&quot;Мирный&quot;},{&quot;id&quot;:81839,&quot;name&quot;:&quot;Онега&quot;}]},{&quot;id&quot;:29,&quot;name&quot;:&quot;Тюменская область&quot;,&quot;cities&quot;:[{&quot;id&quot;:81834,&quot;name&quot;:&quot;Мирный&quot;},{&quot;id&quot;:81839,&quot;name&quot;:&quot;Онега&quot;}]},{&quot;id&quot;:30,&quot;name&quot;:&quot;Ульяновская область&quot;,&quot;cities&quot;:[{&quot;id&quot;:81834,&quot;name&quot;:&quot;Мирный&quot;},{&quot;id&quot;:81839,&quot;name&quot;:&quot;Онега&quot;}]},{&quot;id&quot;:29,&quot;name&quot;:&quot;Челябинская область&quot;,&quot;cities&quot;:[{&quot;id&quot;:81834,&quot;name&quot;:&quot;Мирный&quot;},{&quot;id&quot;:81839,&quot;name&quot;:&quot;Онега&quot;}]},{&quot;id&quot;:30,&quot;name&quot;:&quot;Ярославская область&quot;,&quot;cities&quot;:[{&quot;id&quot;:81834,&quot;name&quot;:&quot;Мирный&quot;},{&quot;id&quot;:81839,&quot;name&quot;:&quot;Онега&quot;}]}]},
  {&quot;id&quot;: 3,&quot;name&quot;:&quot;Украина&quot;,&quot;regions&quot;:[{&quot;id&quot;:83,&quot;name&quot;:&quot;Донецкая область&quot;,&quot;cities&quot;:[{&quot;id&quot;:6232,&quot;name&quot;:&quot;Макеевка&quot;},{&quot;id&quot;:6242,&quot;name&quot;:&quot;Горловка&quot;}]},{&quot;id&quot;:2,&quot;name&quot;:&quot;Луганская область&quot;,&quot;cities&quot;:[{&quot;id&quot;:94100,&quot;name&quot;:&quot;Брянка&quot;},{&quot;id&quot;:6442,&quot;name&quot;:&quot;Алчевск&quot;}]}]}
]">
                            <div class="form-search__back js-search-back">Местоположение</div>
                            <div class="form-search__wrapper">
                                <div class="form-search__header">
                                    <div class="form-search__header-container">
                                        <div class="form-search__title">Выберите страну</div>
                                        <div class="form-search__cols">
                                            <div class="form-search__col">
                                                <div class="select form-search__select">
                                                    <select
                                                        class="select__control js-select-country js-country-selection"
                                                        name="country"></select>
                                                </div>
                                                <div class="form-search__region"><span class="form-search__region-name">Московская
																область</span><span
                                                        class="form-search__region-cancel"></span></div>
                                            </div>
                                            <div class="form-search__col">
                                                <div class="form-search__whole-area">Искать по всей области</div>
                                                <div class="input form-search__input">
                                                    <label class="input__label" for="search">Укажите город или выберите
                                                        из списка </label>
                                                    <input class="input__control" id="search" type="text" name="search"
                                                           placeholder="Название города"
                                                           onblur="placeholder='Название города'"
                                                           onfocus="placeholder=''" autocomplete="off"/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-search__body">
                                    <ul class="form-search__list"></ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                <ul class="header__menu menu">
                    <li class="menu__item"><a class="menu__link menu__link--active" href="#"><span
                                class="menu__value">878</span><span class="menu__name">Индивидуалки</span></a></li>
                    <li class="menu__item"><a class="menu__link " href="#"><span class="menu__value">325</span><span
                                class="menu__name">Салоны</span></a></li>
                    <li class="menu__item"><a class="menu__link " href="#"><span class="menu__value">215</span><span
                                class="menu__name">Массажистки</span></a></li>
                    <li class="menu__item"><a class="menu__link " href="#"><span class="menu__value">78</span><span
                                class="menu__name">Массажные салоны</span></a></li>
                    <li class="menu__item"><a class="menu__link " href="#"><span class="menu__value">36</span><span
                                class="menu__name">Виртуалки</span></a></li>
                    <li class="menu__item"><a class="menu__link " href="#"><span class="menu__value">50</span><span
                                class="menu__name">Стрип-клубы</span></a></li>
                    <li class="menu__item"><a class="menu__link " href="/articles.html"><span
                                class="menu__value">16</span><span class="menu__name">Статьи</span></a></li>
                </ul>
                <div class="header__menu-mobile">
                    <select class="js-select-menu" name="menu"></select>
                </div>
            </div>
        </div>
    </div>
</header>
