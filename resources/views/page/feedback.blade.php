@extends('layouts.main')

@section('title')
    Обратная связь
@endsection

@section('content')
    <div class="feedback">
        <div class="template">
            <div class="template__container container">
                @include('page._breadcrumbs', ['url' => route('page.feedback'), 'name' => 'Feedback'])
                <div class="template__body">
                    @include('page._sidebar', ['url' => route('page.feedback')])
                    <div class="template__content">
                        <div class="content">
                            <h1 class="content__title">Обратная связь</h1>
                            <div class="content__info">
                                <p>Мы делаем все возможное, чтобы наш проект был максимально удобным и эффективным. Администрация
                                    сайта будет очень признательна всем, кто сообщит о любых недоработках, ошибках и неточностях,
                                    выявленных в процессе работы. Также, мы будем благодарны за ваши предложения и пожелания по
                                    улучшению качества обслуживания.</p>
                                <p>Если вы желаете задать вопрос или сделать комментарий, напишите письмо на адрес <a
                                        href='/support'>support@don.com</a> или воспользуйтесь нижеприведенной формой.</p>
                            </div>
                        </div>
                        <form class="feedback-form">
                            <div class="feedback-form__column">
                                <div class="feedback-form__place">
                                    <div class="input">
                                        <label class="input__label" for="name">Ваше имя </label>
                                        <input class="input__control" id="name" type="text" name="name"
                                               placeholder="Например, Константин" onblur="placeholder='Например, Константин'"
                                               onfocus="placeholder=''" autocomplete="on" />
                                    </div>
                                </div>
                                <div class="feedback-form__place">
                                    <div class="input">
                                        <label class="input__label" for="email">Ваша почта </label>
                                        <input class="input__control" id="email" type="email" name="email" placeholder="hallo@mail.ru"
                                               onblur="placeholder='hallo@mail.ru'" onfocus="placeholder=''" autocomplete="on"
                                               required="required" />
                                    </div>
                                </div>
                                <div class="feedback-form__place feedback-form__place--width">
                                    <label>Тема письма</label>
                                    <select class="select__control js-select-topic" name="topic"
                                            data-topics="[{&quot;topic&quot;:&quot;Предложение о сотрудничестве&quot;},{&quot;topic&quot;:&quot;Предложение 1&quot;},{&quot;topic&quot;:&quot;Предложение 2&quot;}]">
                                        <option value="Предложение о сотрудничестве" id="0">Предложение о сотрудничестве</option>
                                        <option value="Предложение 1" id="1">Предложение 1</option>
                                        <option value="Предложение 2" id="2">Предложение 2</option>
                                    </select>
                                </div>
                            </div>
                            <div class="feedback-form__column feedback-form__column--textarea">
                                <div class="feedback-form__place feedback-form__place--width">
                                    <label class="feedback-form__textarea-label" for="letter">Текст письма</label>
                                    <textarea class="feedback-form__textarea" id="letter" name="letter"></textarea>
                                </div>
                                <div class="feedback-form__inner">
                                    <div class="feedback-form__grecaptcha">
                                        <div class="feedback-form__grecaptcha-content"><img src="/images/image-cf83fbf1.jpg"
                                                                                            alt="age limit"></div>
                                    </div>
                                    <div class="feedback-form__submit">
                                        <button class="feedback-form__button button button--transparent">Отправить</button>
                                        <div class="feedback-form__agree"><span>Все поля обязательны к заполнению.</span>
                                            <div class="feedback-form__agree-text">Нажимая «Отправить», Вы соглашаетесь с <a
                                                    href='/policy'>политикой конфиденциальности.</a></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        @include('includes._tabs')
    </div>
    @include('includes._bottom-banner')
@endsection
