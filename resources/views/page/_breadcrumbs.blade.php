<div class="template__breadcrumbs">
    <div class="breadcrumbs">
        <div class="breadcrumbs__wrap">
            <ul class="breadcrumbs__list">
                <li class="breadcrumbs__item breadcrumbs__item--back">
                    <a class="breadcrumbs__link" href="{{ route('home') }}">
                        <img src="/images/arrow-left-34b34338.svg">
                        <span>Назад</span>
                    </a>
                </li>
                <li class="breadcrumbs__item breadcrumbs__item--active">
                    <a class="breadcrumbs__link" href="{{ $url ?? '' }}">{{ $name ?? '' }}</a>
                </li>
            </ul>
        </div>
    </div>
</div>
