<div class="template__sidebar">
    <aside class="sidebar">
        <div class="sidebar__wrap">
            <ul class="sidebar__list">
                <li class="sidebar__item @if ($url === route('page.privacy')) sidebar__item--active @endif">
                    <a class="sidebar__link js-btn-screen" href="{{ route('page.privacy') }}">
                        Правила портала
                    </a>
                </li>
                <li class="sidebar__item @if ($url === route('page.model-instructions')) sidebar__item--active @endif">
                    <a class="sidebar__link js-btn-screen" href="{{ route('page.model-instructions') }}">
                        Инструкции для моделей
                    </a>
                </li>
                <li class="sidebar__item @if ($url === route('page.customer-instructions')) sidebar__item--active @endif">
                    <a class="sidebar__link js-btn-screen" href="{{ route('page.customer-instructions') }}">
                        Инструкции для клиентов
                    </a>
                </li>
                <li class="sidebar__item @if ($url === route('page.feedback')) sidebar__item--active @endif">
                    <a class="sidebar__link js-btn-screen" href="{{ route('page.feedback') }}">
                        Обратная связь
                    </a>
                </li>
            </ul>
        </div>
    </aside>
</div>
