@extends('layouts.main')

@section('content')
    <div class="login">
        <div class="login__wrapper">
            <ul class="login__tabs js-login-tabs">
                <li class="login__tab login__tab--active">{{ __('auth.login') }}</li>
                <li class="login__tab">{{ __('auth.register') }}</li>
            </ul>
            <div class="login__items">
                <div class="login__item login__item--active">
                    <div class="login__content">
                        <form id="login-form" method="post" action="{{ route('login') }}">
                            @csrf
                            <div class="login__inputs">
                                <div class="input login__input">
                                    <label class="input__label" for="login_email">{{ __('auth.email') }}</label>
                                    <input class="input__control @error('email') is-invalid @enderror" id="login_email" type="email" name="email" placeholder="" onblur="placeholder=''" onfocus="placeholder=''" value="{{ old('email') }}" required autocomplete="email" />
                                    @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                <div class="input login__input">
                                    <label class="input__label" for="login_password">{{ __('auth.password') }}</label>
                                    <div class="input__field">
                                        <input class="input__control @error('password') is-invalid @enderror" id="login_password" type="password" name="password" placeholder="" onblur="placeholder=''" onfocus="placeholder=''" required autocomplete="current-password" />
                                        <div class="input__eye js-show-password"></div>
                                    </div>
                                    @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="login__actions">
                                <label class="login__checkbox checkbox">
                                    <input class="checkbox__input" type="checkbox" name="remember" id="login_remember" {{ old('remember') ? 'checked' : '' }}>
                                    <span class="checkbox__label">{{ __('auth.remember_me') }}</span>
                                </label>
                                <a
                                    class="login__remind js-forgot-password"
                                    href="{{ route('password.request') }}"
                                >{{ __('auth.forgot_your_password') }}</a>
                            </div>
                            <button class="login__button button" type="submit" name="enter">{{ __('auth.login') }}</button>
                        </form>
                    </div>
                    <div class="login__bottom">
                        <div class="login__agree">При входе вы подтверждаете согласие с <a class="login__privacy" href="#">политикой конфиденциальности</a>.</div>
                    </div>
                </div>
                <div class="login__item">
                    <div class="login__content">
                        <form id="register-form" method="post" action="{{ route('register') }}">
                            @csrf
                            <div class="login__inputs">
                                <div class="input login__input">
                                    <label class="input__label" for="name">{{ __('auth.name') }}</label>
                                    <input class="input__control @error('name') is-invalid @enderror" id="register_name" type="text" name="name" value="{{ old('name') }}" required placeholder="" onblur="placeholder=''" onfocus="placeholder=''" autocomplete="name" />
                                    @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                <div class="input login__input">
                                    <label class="input__label" for="email">{{ __('auth.email') }}</label>
                                    <input class="input__control @error('email') is-invalid @enderror" id="register_email" type="email" name="email" value="{{ old('email') }}" required placeholder="" onblur="placeholder=''" onfocus="placeholder=''" autocomplete="email" />
                                    @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                <div class="input login__input">
                                    <label class="input__label" for="password">{{ __('auth.password') }}</label>
                                    <div class="input__field">
                                        <input class="input__control @error('password') is-invalid @enderror" id="register_password" type="password" name="password" required placeholder="" onblur="placeholder=''" onfocus="placeholder=''" autocomplete="password" />
                                        <div class="input__eye js-show-password"></div>
                                    </div>
                                    @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                <div class="input login__input">
                                    <label class="input__label" for="password_confirmation">{{ __('auth.confirm_password') }}</label>
                                    <div class="input__field">
                                        <input class="input__control @error('password_confirmation') is-invalid @enderror" id="password_confirmation" type="password" name="password_confirmation" required placeholder="" onblur="placeholder=''" onfocus="placeholder=''" autocomplete="confirm_password"/>
                                        <div class="input__eye js-show-password"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="login__captcha"> <img src="./images/captcha-e1f4a6f5.jpg" alt="captcha"></div>
                            <button class="login__button button" type="submit" name="check_in">{{ __('auth.register') }}</button>
                        </form>
                    </div>
                    <div class="login__bottom">
                        <div class="login__agree">При регистрации вы подтверждаете согласие с<a class="login__privacy" href="#">политикой конфиденциальности</a>.</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="login__recovery">
            <div class="login-recovery">
                <div class="login-recovery__header">
                    <div class="login-recovery__back js-recovery-back">
                        <div
                            class="login-recovery__arrow"
                            data-action="{{ route('login') }}"
                            onclick="document.getElementById('login-form').action = this.getAttribute('data-action')"></div>
                        <div class="login-recovery__title">{{ __('auth.reset_password') }}</div>
                    </div>
                </div>
                <div class="login-recovery__body">
                    <form id="reset-password-form" method="post" action="{{ route('password.request') }}">
                        @csrf
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        <div class="input login-recovery__input">
                            <label class="input__label" for="reset_email">{{ __('auth.email') }}</label>
                            <input class="input__control @error('email') is-invalid @enderror" id="reset_email" type="email" name="email" value="{{ old('email') }}" required placeholder="" autocomplete="email" />
                        </div>
                        @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                        <button class="login-recovery__button button" type="reset" name="reset">{{ __('auth.send_password_reset_link') }}</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="bottom-banner bottom-banner--transparent">
        <div class="bottom-banner__wrap">
            <div class="bottom-banner__title">Заблокировали доступ к сайту?<a class="bottom-banner__link" href="#" title="link">Инструкция по обходу блокировки</a></div>
        </div>
    </div>
@endsection
