@extends('layouts.main')

@section('content')
    <form class="login" action="{{ route('password.email') }}">
        <div class="login__recovery" style="display: block">
            <div class="login-recovery">
                <div class="login-recovery__header">
                    <div class="login-recovery__back">
                        <div class="login-recovery__title">{{ __('Reset Password') }}</div>
                    </div>
                </div>
                <div class="login-recovery__body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <div class="input login-recovery__input">
                        <label class="input__label" for="email">{{ __('E-Mail Address') }}</label>
                        <input class="input__control @error('email') is-invalid @enderror" id="email" type="text" name="email" value="{{ old('email') }}" required placeholder="" onblur="placeholder=''" onfocus="placeholder=''" autocomplete="email" autofocus />
                    </div>
                    @error('email')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                    <button class="login-recovery__button button" type="reset" name="reset">{{ __('Send Password Reset Link') }}</button>
                </div>
            </div>
        </div>
    </form>
    <div class="bottom-banner bottom-banner--transparent">
        <div class="bottom-banner__wrap">
            <div class="bottom-banner__title">Заблокировали доступ к сайту?<a class="bottom-banner__link" href="#" title="link">Инструкция по обходу блокировки</a></div>
        </div>
    </div>
@endsection

