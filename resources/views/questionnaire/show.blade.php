@extends('layouts.main')

@section('content')
    <div class="questionnaire">
        <div class="container">
            <div class="questionnaire__breadcrumbs questionnaire__breadcrumbs--hidden">
                <div class="breadcrumbs">
                    <div class="breadcrumbs__wrap">
                        <ul class="breadcrumbs__list">
                            <li class="breadcrumbs__item breadcrumbs__item--back"><a class="breadcrumbs__link" href="#"><img
                                        src="/images/arrow-left-34b34338.svg"><span>Назад</span></a></li>
                            <li class="breadcrumbs__item"><a class="breadcrumbs__link" href="">Объявления Москва</a></li>
                            <li class="breadcrumbs__item"><a class="breadcrumbs__link" href="">Индивидуалки Москва</a></li>
                            <li class="breadcrumbs__item breadcrumbs__item--active"><a class="breadcrumbs__link"
                                                                                       href="">{{ $questionnaire->name }}</a></li>
                        </ul>
                    </div>
                </div>
                <div class="questionnaire__navigation questionnaire__navigation--mobile">
                    <div class="questionnaire__navigation-prev"><svg xmlns="http://www.w3.org/2000/svg" width="28" height="28"
                                                                     viewBox="0 0 28 28" fill="none">
                            <circle cx="14" cy="14" r="13.5" stroke="white" />
                            <g>
                                <path
                                    d="M7.1604 14.3871C7.16056 14.3873 7.1607 14.3875 7.16089 14.3876L10.0184 17.2314C10.2325 17.4444 10.5787 17.4436 10.7918 17.2295C11.0049 17.0154 11.0041 16.6692 10.79 16.4561L8.87146 14.5469L20.4531 14.5469C20.7552 14.5469 21 14.302 21 14C21 13.698 20.7552 13.4531 20.4531 13.4531L8.87149 13.4531L10.79 11.5439C11.0041 11.3308 11.0049 10.9846 10.7918 10.7705C10.5787 10.5564 10.2324 10.5556 10.0184 10.7686L7.16086 13.6124C7.1607 13.6125 7.16056 13.6127 7.16037 13.6129C6.94619 13.8267 6.94687 14.174 7.1604 14.3871Z"
                                    fill="white" />
                            </g>
                            <defs>
                                <clipPath id="clip0">
                                    <rect width="14" height="14" fill="white" transform="translate(21 21) rotate(-180)" />
                                </clipPath>
                            </defs>
                        </svg>
                    </div>
                    <div class="questionnaire__navigation-next"><svg xmlns="http://www.w3.org/2000/svg" width="28" height="28"
                                                                     viewBox="0 0 28 28" fill="none">
                            <circle cx="14" cy="14" r="13.5" stroke="white" />
                            <g>
                                <path
                                    d="M20.8396 13.6129C20.8394 13.6127 20.8393 13.6125 20.8391 13.6124L17.9816 10.7686C17.7675 10.5556 17.4213 10.5564 17.2082 10.7705C16.9951 10.9846 16.9959 11.3308 17.21 11.5439L19.1285 13.4531H7.54688C7.24484 13.4531 7 13.698 7 14C7 14.302 7.24484 14.5469 7.54688 14.5469H19.1285L17.21 16.4561C16.9959 16.6692 16.9951 17.0154 17.2082 17.2295C17.4213 17.4436 17.7676 17.4444 17.9816 17.2314L20.8391 14.3876C20.8393 14.3875 20.8394 14.3873 20.8396 14.3871C21.0538 14.1733 21.0531 13.8259 20.8396 13.6129Z"
                                    fill="white" />
                            </g>
                            <defs>
                                <clipPath id="clip0">
                                    <rect width="14" height="14" fill="white" transform="translate(7 7)" />
                                </clipPath>
                            </defs>
                        </svg>
                    </div>
                </div>
            </div>
            <div class="questionnaire__tiles questionnaire__tiles-hidden">
                <div class="questionnaire__tile">
                    <div class="questionnaire__tile-title">Рекомендация</div>
                    <div class="questionnaire__tile-info"> Перед тем, как начать общение с девушкой, пожалуйста, не забудьте
                        сообщить ей, что нашли ее телефон на сайте. Желаем приятного времяпровождения. </div>
                </div>
                <div class="questionnaire__tile">
                    <div class="questionnaire__tile-title questionnaire__tile-title--red">Предупреждение</div>
                    <div class="questionnaire__tile-info"> Остерегайтесь мошенников! Не переводите деньги на счёт мобильного,
                        банковскую карту или WebMoney если Вас об этом просят. О фактах мошенничества и других неправомерных
                        действиях рекламодателя сообщите администратору. </div>
                </div>
            </div>
            <div class="questionnaire__content">
                <div class="questionnaire__column questionnaire__column--padding">
                    <div class="questionnaire__inner js-sticky">
                        <div class="questionnaire__breadcrumbs">
                            <div class="breadcrumbs">
                                <div class="breadcrumbs__wrap">
                                    <ul class="breadcrumbs__list">
                                        <li class="breadcrumbs__item breadcrumbs__item--back"><a class="breadcrumbs__link" href="#"><img
                                                    src="/images/arrow-left-34b34338.svg"><span>Назад</span></a></li>
                                        <li class="breadcrumbs__item"><a class="breadcrumbs__link" href="">Объявления Москва</a></li>
                                        <li class="breadcrumbs__item"><a class="breadcrumbs__link" href="">Индивидуалки Москва</a></li>
                                        <li class="breadcrumbs__item breadcrumbs__item--active"><a class="breadcrumbs__link"
                                                                                                   href="">{{ $questionnaire->name }}</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="questionnaire__tile">
                            <div class="questionnaire__tile-title">Рекомендация</div>
                            <div class="questionnaire__tile-info"> Перед тем, как начать общение с девушкой, пожалуйста, не
                                забудьте сообщить ей, что нашли ее телефон на сайте. Желаем приятного времяпровождения. </div>
                        </div>
                        <div class="questionnaire__column-wrap">
                            <div class="questionnaire__personal">
                                <div class="questionnaire__personal-top">
                                    <div class="questionnaire__header-info">
                                        <div class="questionnaire__header-text questionnaire__header-text--date">06 мая 2020 14:14 {{ $questionnaire->created_at }}</div>
                                        <div class="questionnaire__header-text">12011</div>
                                    </div>
                                    <div class="questionnaire__header-rating">
                                        <div class="rating-static rating-static--three">
                                            <div class="rating-static__star"></div>
                                            <div class="rating-static__star"></div>
                                            <div class="rating-static__star"></div>
                                            <div class="rating-static__star"></div>
                                            <div class="rating-static__star"></div>
                                        </div>
                                        <div class="questionnaire__header-count">Проголосовало людей: 15</div>
                                    </div>
                                </div>
                                <div class="questionnaire__personal-heading">
                                    <h1 class="questionnaire__personal-name">{{ $questionnaire->name }}</h1>
                                    <div class="questionnaire__personal-status">Индивидуалка</div>
                                    <div class="questionnaire__header">
                                        <div class="questionnaire__header-left">
                                            <div class="questionnaire__header-icon"><img src="/images/vip-88eaf260.svg" alt=""></div>
                                            <div class="questionnaire__header-icon"><img src="/images/new-123c0122.svg" alt=""></div>
                                        </div>
                                        <div class="questionnaire__header-inform"><a class="questionnaire__personal-complain" href="#">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="10" height="11" viewbox="0 0 10 11"
                                                     fill="none">
                                                    <path
                                                        d="M9.79704 8.08263L6.26059 1.57213C5.99368 1.12279 5.52227 0.854431 4.99962 0.854431C4.47696 0.854431 4.00556 1.12279 3.73864 1.57213C3.73663 1.57556 3.73493 1.579 3.73292 1.58244L0.208231 8.07234C-0.0644058 8.53115 -0.069562 9.08215 0.194188 9.54611C0.458543 10.0103 0.934793 10.2873 1.46891 10.2873H8.51079C9.04491 10.2873 9.54073 10.0103 9.80507 9.54611C10.0688 9.08217 10.0637 8.53113 9.79704 8.08263ZM4.41309 3.83547C4.41309 3.51152 4.67569 3.24894 4.99962 3.24894C5.32356 3.24894 5.58614 3.51154 5.58614 3.83547V6.18158C5.58614 6.50549 5.32354 6.7681 4.99962 6.7681C4.67569 6.7681 4.41309 6.50547 4.41309 6.18158V3.83547ZM4.99962 9.11424C4.51448 9.11424 4.11981 8.71959 4.11981 8.23443C4.11981 7.74929 4.51446 7.35465 4.99962 7.35465C5.48475 7.35465 5.8794 7.74929 5.8794 8.23443C5.87942 8.71957 5.48477 9.11424 4.99962 9.11424Z"
                                                        fill="#5C5C5C"></path>
                                                </svg>Пожаловаться</a>
                                            <div class="questionnaire__personal-id">ID #{{ $questionnaire->id }}</div>
                                        </div>
                                        <div class="questionnaire__header-right">
                                            <div class="questionnaire__header-info">
                                                <div class="questionnaire__header-text">12011</div>
                                                <div class="questionnaire__header-text questionnaire__header-text--date">06 мая 2020 14:14 {{ $questionnaire->create_at }}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="questionnaire__personal-inner"><a class="questionnaire__personal-phone"
                                                                              href="tel:+79681234567">{{ $questionnaire->phone }}</a>
                                    <div class="questionnaire__personal-disabled">
                                        <div class="questionnaire__personal-massage button">Девушка временно не работает</div>
                                    </div>
                                    <div class="questionnaire__header-rating">
                                        <div class="rating-static rating-static--three">
                                            <div class="rating-static__star"></div>
                                            <div class="rating-static__star"></div>
                                            <div class="rating-static__star"></div>
                                            <div class="rating-static__star"></div>
                                            <div class="rating-static__star"></div>
                                        </div>
                                        <div class="questionnaire__header-count">Проголосовало людей: 15</div>
                                        <div class="questionnaire__personal-id">ID #55632</div>
                                    </div>
                                </div>
                                <div class="questionnaire__personal-socials"><a
                                        class="questionnaire__personal-social questionnaire__personal-social--viber" title="Viber"
                                        href="viber://chat?number=+79681234567">Viber</a><a
                                        class="questionnaire__personal-social questionnaire__personal-social--viber questionnaire__personal-social--mobile"
                                        href="viber://add?number=79681234567">Viber</a><a
                                        class="questionnaire__personal-social questionnaire__personal-social--whatsapp" title="WhatsApp"
                                        href="https://wa.me/79681234567" target="_blank">WhatsApp</a><a
                                        class="questionnaire__personal-social questionnaire__personal-social--telegram" title="Telegram"
                                        href="tg://resolve?domain=nikname">Telegram</a></div>
                                <div class="questionnaire__personal-time">Часы работы:<span> 09:00 — 21:00</span>
                                    <div class="questionnaire__personal-inform"><a class="questionnaire__personal-complain" href="#">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="10" height="11" viewbox="0 0 10 11"
                                                 fill="none">
                                                <path
                                                    d="M9.79704 8.08263L6.26059 1.57213C5.99368 1.12279 5.52227 0.854431 4.99962 0.854431C4.47696 0.854431 4.00556 1.12279 3.73864 1.57213C3.73663 1.57556 3.73493 1.579 3.73292 1.58244L0.208231 8.07234C-0.0644058 8.53115 -0.069562 9.08215 0.194188 9.54611C0.458543 10.0103 0.934793 10.2873 1.46891 10.2873H8.51079C9.04491 10.2873 9.54073 10.0103 9.80507 9.54611C10.0688 9.08217 10.0637 8.53113 9.79704 8.08263ZM4.41309 3.83547C4.41309 3.51152 4.67569 3.24894 4.99962 3.24894C5.32356 3.24894 5.58614 3.51154 5.58614 3.83547V6.18158C5.58614 6.50549 5.32354 6.7681 4.99962 6.7681C4.67569 6.7681 4.41309 6.50547 4.41309 6.18158V3.83547ZM4.99962 9.11424C4.51448 9.11424 4.11981 8.71959 4.11981 8.23443C4.11981 7.74929 4.51446 7.35465 4.99962 7.35465C5.48475 7.35465 5.8794 7.74929 5.8794 8.23443C5.87942 8.71957 5.48477 9.11424 4.99962 9.11424Z"
                                                    fill="#5C5C5C"></path>
                                            </svg>Пожаловаться</a>
                                        <div class="questionnaire__personal-id">ID #55632</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="questionnaire__sliders">
                            <div class="slider">
                                <div class="slider__thumbs swiper-container">
                                    <div class="slider__thumbs-wrapper swiper-wrapper">
                                        <div class="slider__thumbs-slide swiper-slide"><img src="/images/img-1-small-0654ff20.jpg"
                                                                                            alt=""></div>
                                        <div class="slider__thumbs-slide swiper-slide"><img src="/images/img-1-small-0654ff20.jpg"
                                                                                            alt=""></div>
                                        <div class="slider__thumbs-slide swiper-slide"><img src="/images/img-1-small-0654ff20.jpg"
                                                                                            alt=""></div>
                                        <div class="slider__thumbs-slide swiper-slide"><img src="/images/img-1-small-0654ff20.jpg"
                                                                                            alt=""></div>
                                        <div class="slider__thumbs-slide swiper-slide">
                                            <video poster="/images/img-1-db8e7456.jpg">
                                                <source src="">
                                            </video>
                                        </div>
                                        <div class="slider__thumbs-slide swiper-slide"><img src="/images/img-1-small-0654ff20.jpg"
                                                                                            alt=""></div>
                                        <div class="slider__thumbs-slide swiper-slide"><img src="/images/img-1-small-0654ff20.jpg"
                                                                                            alt=""></div>
                                        <div class="slider__thumbs-slide swiper-slide"><img src="/images/img-1-small-0654ff20.jpg"
                                                                                            alt=""></div>
                                        <div class="slider__thumbs-slide swiper-slide"><img src="/images/img-1-small-0654ff20.jpg"
                                                                                            alt=""></div>
                                        <div class="slider__thumbs-slide swiper-slide">
                                            <video poster="/images/img-1-db8e7456.jpg">
                                                <source src="">
                                            </video>
                                        </div>
                                    </div>
                                </div>
                                <div class="slider__gallery swiper-container">
                                    <div class="slider__gallery-wrapper swiper-wrapper"><a
                                            class="slider__gallery-slide swiper-slide js-gallery" href="/images/img-1-db8e7456.jpg"><img
                                                src="/images/img-1-db8e7456.jpg" alt=""></a><a
                                            class="slider__gallery-slide swiper-slide js-gallery" href="/images/img-1-db8e7456.jpg"><img
                                                src="/images/img-1-db8e7456.jpg" alt=""></a><a
                                            class="slider__gallery-slide swiper-slide js-gallery" href="/images/img-1-db8e7456.jpg"><img
                                                src="/images/img-1-db8e7456.jpg" alt=""></a><a
                                            class="slider__gallery-slide swiper-slide js-gallery" href="/images/img-1-db8e7456.jpg"><img
                                                src="/images/img-1-db8e7456.jpg" alt=""></a><a
                                            class="slider__gallery-slide swiper-slide js-gallery js-gallery-video" href="videos/test.mp4">
                                            <video class="js-video" poster="/images/img-1-db8e7456.jpg">
                                                <source src="videos/test.mp4" type="video/mp4">
                                            </video></a><a class="slider__gallery-slide swiper-slide js-gallery"
                                                           href="/images/img-1-db8e7456.jpg"><img src="/images/img-1-db8e7456.jpg" alt=""></a><a
                                            class="slider__gallery-slide swiper-slide js-gallery" href="/images/img-1-db8e7456.jpg"><img
                                                src="/images/img-1-db8e7456.jpg" alt=""></a><a
                                            class="slider__gallery-slide swiper-slide js-gallery" href="/images/img-1-db8e7456.jpg"><img
                                                src="/images/img-1-db8e7456.jpg" alt=""></a><a
                                            class="slider__gallery-slide swiper-slide js-gallery" href="/images/img-1-db8e7456.jpg"><img
                                                src="/images/img-1-db8e7456.jpg" alt=""></a><a
                                            class="slider__gallery-slide swiper-slide js-gallery js-gallery-video" href="videos/test.mp4">
                                            <video class="js-video" poster="/images/img-1-db8e7456.jpg">
                                                <source src="videos/test.mp4" type="video/mp4">
                                            </video></a></div>
                                    <div class="slider__gallery-bottom">
                                        <div class="slider__gallery-numbers">
                                            <div class="slider__gallery-count"></div><span>&nbsp/&nbsp</span>
                                            <div class="slider__gallery-max"></div>
                                        </div>
                                    </div>
                                    <div class="slider__gallery-pagination slider-pagination swiper-pagination"></div>
                                    <!-- Add Arrows-->
                                    <div class="slider__gallery-next slider-next swiper-button-next"></div>
                                    <div class="slider__gallery-prev slider-prev swiper-button-prev"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="questionnaire__column questionnaire__column--padding questionnaire__column--mobile">
                    <div class="questionnaire__navigation">
                        <div class="questionnaire__navigation-prev"><svg xmlns="http://www.w3.org/2000/svg" width="28"
                                                                         height="28" viewBox="0 0 28 28" fill="none">
                                <circle cx="14" cy="14" r="13.5" stroke="white" />
                                <g>
                                    <path
                                        d="M7.1604 14.3871C7.16056 14.3873 7.1607 14.3875 7.16089 14.3876L10.0184 17.2314C10.2325 17.4444 10.5787 17.4436 10.7918 17.2295C11.0049 17.0154 11.0041 16.6692 10.79 16.4561L8.87146 14.5469L20.4531 14.5469C20.7552 14.5469 21 14.302 21 14C21 13.698 20.7552 13.4531 20.4531 13.4531L8.87149 13.4531L10.79 11.5439C11.0041 11.3308 11.0049 10.9846 10.7918 10.7705C10.5787 10.5564 10.2324 10.5556 10.0184 10.7686L7.16086 13.6124C7.1607 13.6125 7.16056 13.6127 7.16037 13.6129C6.94619 13.8267 6.94687 14.174 7.1604 14.3871Z"
                                        fill="white" />
                                </g>
                                <defs>
                                    <clipPath id="clip0">
                                        <rect width="14" height="14" fill="white" transform="translate(21 21) rotate(-180)" />
                                    </clipPath>
                                </defs>
                            </svg>
                        </div>
                        <div class="questionnaire__navigation-next"><svg xmlns="http://www.w3.org/2000/svg" width="28"
                                                                         height="28" viewBox="0 0 28 28" fill="none">
                                <circle cx="14" cy="14" r="13.5" stroke="white" />
                                <g>
                                    <path
                                        d="M20.8396 13.6129C20.8394 13.6127 20.8393 13.6125 20.8391 13.6124L17.9816 10.7686C17.7675 10.5556 17.4213 10.5564 17.2082 10.7705C16.9951 10.9846 16.9959 11.3308 17.21 11.5439L19.1285 13.4531H7.54688C7.24484 13.4531 7 13.698 7 14C7 14.302 7.24484 14.5469 7.54688 14.5469H19.1285L17.21 16.4561C16.9959 16.6692 16.9951 17.0154 17.2082 17.2295C17.4213 17.4436 17.7676 17.4444 17.9816 17.2314L20.8391 14.3876C20.8393 14.3875 20.8394 14.3873 20.8396 14.3871C21.0538 14.1733 21.0531 13.8259 20.8396 13.6129Z"
                                        fill="white" />
                                </g>
                                <defs>
                                    <clipPath id="clip0">
                                        <rect width="14" height="14" fill="white" transform="translate(7 7)" />
                                    </clipPath>
                                </defs>
                            </svg>
                        </div>
                    </div>
                    <div class="questionnaire__tile">
                        <div class="questionnaire__tile-title questionnaire__tile-title--red">Предупреждение</div>
                        <div class="questionnaire__tile-info"> Остерегайтесь мошенников! Не переводите деньги на счёт
                            мобильного, банковскую карту или WebMoney если Вас об этом просят. О фактах мошенничества и других
                            неправомерных действиях рекламодателя сообщите администратору. </div>
                    </div>
                    <header class="questionnaire__header">
                        <div class="questionnaire__header-left">
                            <div class="questionnaire__header-icon"><img src="/images/vip-88eaf260.svg" alt=""></div>
                            <div class="questionnaire__header-icon"><img src="/images/new-123c0122.svg" alt=""></div>
                        </div>
                        <div class="questionnaire__header-right">
                            <div class="questionnaire__header-info">
                                <div class="questionnaire__header-text">12011</div>
                                <div class="questionnaire__header-text questionnaire__header-text--date">06 мая 2020 14:14 {{ $questionnaire->created_at }}</div>
                            </div>
                            <div class="questionnaire__header-rating">
                                <div class="rating-static rating-static--three">
                                    <div class="rating-static__star"></div>
                                    <div class="rating-static__star"></div>
                                    <div class="rating-static__star"></div>
                                    <div class="rating-static__star"></div>
                                    <div class="rating-static__star"></div>
                                </div>
                            </div>
                            <div class="questionnaire__header-count">Проголосовало людей: 15</div>
                        </div>
                    </header>
                    <div class="questionnaire__wrap questionnaire__wrap--hidden">
                        <div class="questionnaire__personal">
                            <div class="questionnaire__personal-heading">
                                <h1 class="questionnaire__personal-name">{{ $questionnaire->name }}</h1>
                                <div class="questionnaire__personal-status">Индивидуалка</div>
                                <div class="questionnaire__personal-block"><a class="questionnaire__personal-complain" href="#">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="10" height="11" viewbox="0 0 10 11" fill="none">
                                            <path
                                                d="M9.79704 8.08263L6.26059 1.57213C5.99368 1.12279 5.52227 0.854431 4.99962 0.854431C4.47696 0.854431 4.00556 1.12279 3.73864 1.57213C3.73663 1.57556 3.73493 1.579 3.73292 1.58244L0.208231 8.07234C-0.0644058 8.53115 -0.069562 9.08215 0.194188 9.54611C0.458543 10.0103 0.934793 10.2873 1.46891 10.2873H8.51079C9.04491 10.2873 9.54073 10.0103 9.80507 9.54611C10.0688 9.08217 10.0637 8.53113 9.79704 8.08263ZM4.41309 3.83547C4.41309 3.51152 4.67569 3.24894 4.99962 3.24894C5.32356 3.24894 5.58614 3.51154 5.58614 3.83547V6.18158C5.58614 6.50549 5.32354 6.7681 4.99962 6.7681C4.67569 6.7681 4.41309 6.50547 4.41309 6.18158V3.83547ZM4.99962 9.11424C4.51448 9.11424 4.11981 8.71959 4.11981 8.23443C4.11981 7.74929 4.51446 7.35465 4.99962 7.35465C5.48475 7.35465 5.8794 7.74929 5.8794 8.23443C5.87942 8.71957 5.48477 9.11424 4.99962 9.11424Z"
                                                fill="#5C5C5C"></path>
                                        </svg>Пожаловаться</a>
                                    <div class="questionnaire__personal-id">ID #{{ $questionnaire->id }}</div>
                                </div>
                            </div>
                            <div class="questionnaire__personal-disabled">
                                <div class="questionnaire__personal-massage button">Девушка временно не работает</div>
                            </div><a class="questionnaire__personal-phone" href="tel:+79681234567">{{ $questionnaire->phone }}</a>
                            <div class="questionnaire__personal-socials"><a
                                    class="questionnaire__personal-social questionnaire__personal-social--viber" title="Viber"
                                    href="viber://chat?number=+380662958892">Viber</a><a
                                    class="questionnaire__personal-social questionnaire__personal-social--viber questionnaire__personal-social--mobile"
                                    title="Viber" href="viber://add?number=380662958892">Viber</a><a
                                    class="questionnaire__personal-social questionnaire__personal-social--whatsapp" title="WhatsApp"
                                    href="https://wa.me/380662958892" target="_blank">WhatsApp</a><a
                                    class="questionnaire__personal-social questionnaire__personal-social--telegram" title="Telegram"
                                    href="tg://resolve?domain=ArtemSergienko">Telegram</a></div>
                            <div class="questionnaire__personal-time">Часы работы:<span> 09:00 — 21:00</span></div>
                        </div>
                    </div>
                    <div class="questionnaire__wrap questionnaire__wrap--mobile">
                        <div class="questionnaire__buttons">
                            <div class="questionnaire__buttons-wrap">
                                <!--.questionnaire__fans Поклонников:-->
                                <!--  span &nbsp;873-->
                                <a href="#popup-message"
                                   class="questionnaire__button questionnaire__button--message button button--transparent button--icon-right js-message-popup">
                                    Написать сообщение<svg width="18" height="18" viewBox="0 0 20 20" fill="none"
                                                           xmlns="http://www.w3.org/2000/svg">
                                        <g>
                                            <path
                                                d="M19.2862 2.85718H0.714787C0.320284 2.85718 0.000488281 3.17697 0.000488281 3.57144V16.4286C0.000488281 16.8231 0.320284 17.1429 0.714787 17.1429H19.2862C19.6807 17.1429 20.0005 16.8231 20.0005 16.4286V3.57144C20.0005 3.17697 19.6807 2.85718 19.2862 2.85718ZM18.1148 4.28573L10.0005 10.5272L1.8862 4.28573H18.1148ZM18.5719 15.7143H1.42904V5.73643L9.56549 11.995C9.82205 12.192 10.1789 12.192 10.4355 11.995L18.5719 5.73643V15.7143Z"
                                                fill="#FF9900" />
                                        </g>
                                        <defs>
                                            <clipPath id="clip0">
                                                <rect width="20" height="20" fill="white" transform="translate(0.000488281)" />
                                            </clipPath>
                                        </defs>
                                    </svg>
                                </a>
                                <a href="#popup-not-auth"
                                   class="questionnaire__button questionnaire__button--favorites button button--transparent button--icon-right js-not-auth-popup">
                                    В избранное <svg width="20" height="18" viewBox="0 0 18 18" fill="none"
                                                     xmlns="http://www.w3.org/2000/svg">
                                        <path
                                            d="M16.528 2.20919C15.597 1.20846 14.2923 0.639056 12.9255 0.636946C11.5576 0.638529 10.2516 1.20763 9.31911 2.20854L9.00132 2.54436L8.68353 2.20854C6.83326 0.217151 3.71893 0.102789 1.72758 1.95306C1.63933 2.03508 1.55411 2.12027 1.47209 2.20854C-0.490696 4.32565 -0.490696 7.59753 1.47209 9.71463L8.5343 17.1622C8.77863 17.4201 9.18579 17.4312 9.44373 17.1868C9.45217 17.1788 9.46039 17.1706 9.46838 17.1622L16.528 9.71463C18.4907 7.59776 18.4907 4.32606 16.528 2.20919ZM15.5971 8.82879H15.5965L9.00132 15.7849L2.40553 8.82879C0.90608 7.21113 0.90608 4.7114 2.40553 3.09374C3.76722 1.61789 6.06755 1.52535 7.5434 2.88703C7.61506 2.95315 7.684 3.02209 7.75012 3.09374L8.5343 3.92104C8.79272 4.17781 9.20995 4.17781 9.46838 3.92104L10.2526 3.09438C11.6142 1.61853 13.9146 1.52599 15.3904 2.88767C15.4621 2.95379 15.531 3.02273 15.5971 3.09438C17.1096 4.71461 17.1207 7.2189 15.5971 8.82879Z"
                                            fill="#FF9900" />
                                    </svg>
                                </a>
                            </div>
                            <div class="questionnaire__fans">Поклонников:<span>&nbsp;873</span></div>
                        </div>
                    </div>
                    <div class="questionnaire__wrap questionnaire__wrap--mobile">
                        <div class="questionnaire__gift-slider">
                            <div class="slider-gift">
                                <div class="slider-gift__cover">
                                    <div class="slider-gift__container swiper-container">
                                        <div class="slider-gift__wrapper swiper-wrapper">
                                            <div class="slider-gift__slide swiper-slide"><a class="" href="#popup-gifts"
                                                                                            data-type="gifts"
                                                                                            data-gift="{&quot;count&quot;:41,&quot;gifts&quot;:[{&quot;name&quot;:&quot;Анонимный пользователь&quot;,&quot;date&quot;:&quot;06 мая 2020 14:14&quot;,&quot;img&quot;:&quot;/images/popup/bear.svg&quot;},{&quot;name&quot;:&quot;Петя&quot;,&quot;date&quot;:&quot;06 мая 2020 20:00&quot;,&quot;img&quot;:&quot;/images/sliders/hearts.png&quot;},{&quot;name&quot;:&quot;Гоша&quot;,&quot;date&quot;:&quot;06 мая 2020 19:14&quot;,&quot;img&quot;:&quot;/images/popup/bear.svg&quot;},{&quot;name&quot;:&quot;Влад&quot;,&quot;date&quot;:&quot;06 мая 2020 07:14&quot;,&quot;img&quot;:&quot;/images/sliders/hearts.png&quot;}]}"><img
                                                        src="/images/bear-d1f15151.png" alt=""></a></div>
                                            <div class="slider-gift__slide swiper-slide"><a class="" href="#popup-gifts"
                                                                                            data-type="gifts"
                                                                                            data-gift="{&quot;count&quot;:41,&quot;gifts&quot;:[{&quot;name&quot;:&quot;Анонимный пользователь&quot;,&quot;date&quot;:&quot;06 мая 2020 14:14&quot;,&quot;img&quot;:&quot;/images/popup/bear.svg&quot;},{&quot;name&quot;:&quot;Петя&quot;,&quot;date&quot;:&quot;06 мая 2020 20:00&quot;,&quot;img&quot;:&quot;/images/sliders/hearts.png&quot;},{&quot;name&quot;:&quot;Гоша&quot;,&quot;date&quot;:&quot;06 мая 2020 19:14&quot;,&quot;img&quot;:&quot;/images/popup/bear.svg&quot;},{&quot;name&quot;:&quot;Влад&quot;,&quot;date&quot;:&quot;06 мая 2020 07:14&quot;,&quot;img&quot;:&quot;/images/sliders/hearts.png&quot;}]}"><img
                                                        src="/images/hearts-f99762c1.png" alt=""></a></div>
                                            <div class="slider-gift__slide swiper-slide"><a class="" href="#popup-gifts"
                                                                                            data-type="gifts"
                                                                                            data-gift="{&quot;count&quot;:41,&quot;gifts&quot;:[{&quot;name&quot;:&quot;Анонимный пользователь&quot;,&quot;date&quot;:&quot;06 мая 2020 14:14&quot;,&quot;img&quot;:&quot;/images/popup/bear.svg&quot;},{&quot;name&quot;:&quot;Петя&quot;,&quot;date&quot;:&quot;06 мая 2020 20:00&quot;,&quot;img&quot;:&quot;/images/sliders/hearts.png&quot;},{&quot;name&quot;:&quot;Гоша&quot;,&quot;date&quot;:&quot;06 мая 2020 19:14&quot;,&quot;img&quot;:&quot;/images/popup/bear.svg&quot;},{&quot;name&quot;:&quot;Влад&quot;,&quot;date&quot;:&quot;06 мая 2020 07:14&quot;,&quot;img&quot;:&quot;/images/sliders/hearts.png&quot;}]}"><img
                                                        src="/images/bear-d1f15151.png" alt=""></a></div>
                                            <div class="slider-gift__slide swiper-slide"><a class="" href="#popup-gifts"
                                                                                            data-type="gifts"
                                                                                            data-gift="{&quot;count&quot;:41,&quot;gifts&quot;:[{&quot;name&quot;:&quot;Анонимный пользователь&quot;,&quot;date&quot;:&quot;06 мая 2020 14:14&quot;,&quot;img&quot;:&quot;/images/popup/bear.svg&quot;},{&quot;name&quot;:&quot;Петя&quot;,&quot;date&quot;:&quot;06 мая 2020 20:00&quot;,&quot;img&quot;:&quot;/images/sliders/hearts.png&quot;},{&quot;name&quot;:&quot;Гоша&quot;,&quot;date&quot;:&quot;06 мая 2020 19:14&quot;,&quot;img&quot;:&quot;/images/popup/bear.svg&quot;},{&quot;name&quot;:&quot;Влад&quot;,&quot;date&quot;:&quot;06 мая 2020 07:14&quot;,&quot;img&quot;:&quot;/images/sliders/hearts.png&quot;}]}"><img
                                                        src="/images/hearts-f99762c1.png" alt=""></a></div>
                                            <div class="slider-gift__slide swiper-slide"><a class="" href="#popup-gifts"
                                                                                            data-type="gifts"
                                                                                            data-gift="{&quot;count&quot;:41,&quot;gifts&quot;:[{&quot;name&quot;:&quot;Анонимный пользователь&quot;,&quot;date&quot;:&quot;06 мая 2020 14:14&quot;,&quot;img&quot;:&quot;/images/popup/bear.svg&quot;},{&quot;name&quot;:&quot;Петя&quot;,&quot;date&quot;:&quot;06 мая 2020 20:00&quot;,&quot;img&quot;:&quot;/images/sliders/hearts.png&quot;},{&quot;name&quot;:&quot;Гоша&quot;,&quot;date&quot;:&quot;06 мая 2020 19:14&quot;,&quot;img&quot;:&quot;/images/popup/bear.svg&quot;},{&quot;name&quot;:&quot;Влад&quot;,&quot;date&quot;:&quot;06 мая 2020 07:14&quot;,&quot;img&quot;:&quot;/images/sliders/hearts.png&quot;}]}"><img
                                                        src="/images/bear-d1f15151.png" alt=""></a></div>
                                            <div class="slider-gift__slide swiper-slide"><a class="" href="#popup-gifts"
                                                                                            data-type="gifts"
                                                                                            data-gift="{&quot;count&quot;:41,&quot;gifts&quot;:[{&quot;name&quot;:&quot;Анонимный пользователь&quot;,&quot;date&quot;:&quot;06 мая 2020 14:14&quot;,&quot;img&quot;:&quot;/images/popup/bear.svg&quot;},{&quot;name&quot;:&quot;Петя&quot;,&quot;date&quot;:&quot;06 мая 2020 20:00&quot;,&quot;img&quot;:&quot;/images/sliders/hearts.png&quot;},{&quot;name&quot;:&quot;Гоша&quot;,&quot;date&quot;:&quot;06 мая 2020 19:14&quot;,&quot;img&quot;:&quot;/images/popup/bear.svg&quot;},{&quot;name&quot;:&quot;Влад&quot;,&quot;date&quot;:&quot;06 мая 2020 07:14&quot;,&quot;img&quot;:&quot;/images/sliders/hearts.png&quot;}]}"><img
                                                        src="/images/hearts-f99762c1.png" alt=""></a></div>
                                        </div>
                                    </div>
                                    <!-- Add Arrows-->
                                    <div class="slider-gift__next"></div>
                                    <div class="slider-gift__prev"></div>
                                </div>
                            </div>
                            <a class="questionnaire__btn-present button " data-type="dispatch" href="#popup-dispatch"
                               data-gift="{&quot;user&quot;:&quot;Толик&quot;,&quot;name&quot;:&quot;Анжелики&quot;,&quot;items&quot;:[{&quot;price&quot;:&quot;1 токен&quot;,&quot;presents&quot;:[{&quot;img&quot;:&quot;/images/popup/bear.svg&quot;,&quot;id&quot;:1},{&quot;img&quot;:&quot;/images/sliders/hearts.png&quot;,&quot;id&quot;:2},{&quot;img&quot;:&quot;/images/popup/bear.svg&quot;,&quot;id&quot;:3},{&quot;img&quot;:&quot;/images/sliders/hearts.png&quot;,&quot;id&quot;:4},{&quot;img&quot;:&quot;/images/popup/bear.svg&quot;,&quot;id&quot;:5},{&quot;img&quot;:&quot;/images/sliders/hearts.png&quot;,&quot;id&quot;:6},{&quot;img&quot;:&quot;/images/popup/bear.svg&quot;,&quot;id&quot;:7}]},{&quot;price&quot;:&quot;2 токена&quot;,&quot;presents&quot;:[{&quot;img&quot;:&quot;/images/popup/bear.svg&quot;,&quot;id&quot;:8},{&quot;img&quot;:&quot;/images/sliders/hearts.png&quot;,&quot;id&quot;:9},{&quot;img&quot;:&quot;/images/popup/bear.svg&quot;,&quot;id&quot;:10},{&quot;img&quot;:&quot;/images/sliders/hearts.png&quot;,&quot;id&quot;:11},{&quot;img&quot;:&quot;/images/popup/bear.svg&quot;,&quot;id&quot;:12},{&quot;img&quot;:&quot;/images/sliders/hearts.png&quot;,&quot;id&quot;:13},{&quot;img&quot;:&quot;/images/sliders/hearts.png&quot;,&quot;id&quot;:14}]},{&quot;price&quot;:&quot;3 токена&quot;,&quot;presents&quot;:[{&quot;img&quot;:&quot;/images/popup/bear.svg&quot;,&quot;id&quot;:8},{&quot;img&quot;:&quot;/images/sliders/hearts.png&quot;,&quot;id&quot;:15},{&quot;img&quot;:&quot;/images/popup/bear.svg&quot;,&quot;id&quot;:16},{&quot;img&quot;:&quot;/images/sliders/hearts.png&quot;,&quot;id&quot;:17},{&quot;img&quot;:&quot;/images/popup/bear.svg&quot;,&quot;id&quot;:18},{&quot;img&quot;:&quot;/images/sliders/hearts.png&quot;,&quot;id&quot;:19},{&quot;img&quot;:&quot;/images/sliders/hearts.png&quot;,&quot;id&quot;:20}]},{&quot;price&quot;:&quot;4 токена&quot;,&quot;presents&quot;:[{&quot;img&quot;:&quot;/images/popup/bear.svg&quot;,&quot;id&quot;:8},{&quot;img&quot;:&quot;/images/sliders/hearts.png&quot;,&quot;id&quot;:21},{&quot;img&quot;:&quot;/images/popup/bear.svg&quot;,&quot;id&quot;:22},{&quot;img&quot;:&quot;/images/sliders/hearts.png&quot;,&quot;id&quot;:23},{&quot;img&quot;:&quot;/images/popup/bear.svg&quot;,&quot;id&quot;:23},{&quot;img&quot;:&quot;/images/sliders/hearts.png&quot;,&quot;id&quot;:25},{&quot;img&quot;:&quot;/images/sliders/hearts.png&quot;,&quot;id&quot;:26}]}]}">
                                <svg width="28" height="28" viewBox="0 0 28 28" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path
                                        d="M26.25 7.85775H20.713C21.1103 7.5865 21.4515 7.317 21.6895 7.0755C23.1 5.65625 23.1 3.34625 21.6895 1.927C20.3193 0.547996 17.9288 0.546246 16.5568 1.927C15.799 2.68825 13.7847 5.7875 14.063 7.85775H13.937C14.2135 5.7875 12.201 2.68825 11.4432 1.927C10.0712 0.546246 7.68075 0.547996 6.3105 1.927C4.9 3.34625 4.9 5.65625 6.30875 7.0755C6.5485 7.317 6.88975 7.5865 7.287 7.85775H1.75C0.78575 7.85775 0 8.6435 0 9.60775V13.9827C0 14.4657 0.392 14.8577 0.875 14.8577H1.75V25.3577C1.75 26.322 2.53575 27.1077 3.5 27.1077H24.5C25.4643 27.1077 26.25 26.322 26.25 25.3577V14.8577H27.125C27.608 14.8577 28 14.4657 28 13.9827V9.60775C28 8.6435 27.2143 7.85775 26.25 7.85775ZM7.55125 3.16075C7.9065 2.80375 8.37725 2.60775 8.87775 2.60775C9.3765 2.60775 9.84725 2.80375 10.2025 3.16075C11.382 4.34725 12.5545 7.37825 12.1467 7.8C12.1467 7.8 12.0732 7.85775 11.823 7.85775C10.6138 7.85775 8.3055 6.60125 7.55125 5.84175C6.818 5.10325 6.818 3.89925 7.55125 3.16075ZM13.125 25.3577H3.5V14.8577H13.125V25.3577ZM13.125 13.1077H1.75V9.60775H11.823H13.125V13.1077ZM17.7975 3.16075C18.508 2.4485 19.74 2.45025 20.4488 3.16075C21.182 3.89925 21.182 5.10325 20.4488 5.84175C19.6945 6.60125 17.3863 7.85775 16.177 7.85775C15.9268 7.85775 15.8532 7.80175 15.8515 7.8C15.4455 7.37825 16.618 4.34725 17.7975 3.16075ZM24.5 25.3577H14.875V14.8577H24.5V25.3577ZM26.25 13.1077H14.875V9.60775H16.177H26.25V13.1077Z"
                                        fill="#171717" />
                                </svg> Отправить подарок </a>
                        </div>
                    </div>
                    <div class="questionnaire__wrap questionnaire__wrap--border questionnaire__wrap--mobile">
                        <div class="questionnaire__params">
                            <div class="params">
                                <div class="params__tables">
                                    <div class="params__part">
                                        <div class="params__table">
                                            <table>
                                                <caption class="params__heading heading">Параметры</caption>
                                                <tr>
                                                    <td>Пол</td>
                                                    <td>Женский</td>
                                                </tr>
                                                <tr>
                                                    <td>Ориентация</td>
                                                    <td>Гетеро</td>
                                                </tr>
                                                <tr>
                                                    <td>Возраст</td>
                                                    <td>25</td>
                                                </tr>
                                                <tr>
                                                    <td>Рост, см</td>
                                                    <td>165</td>
                                                </tr>
                                                <tr>
                                                    <td>Вес, кг</td>
                                                    <td>52</td>
                                                </tr>
                                                <tr>
                                                    <td>Размер груди</td>
                                                    <td>2</td>
                                                </tr>
                                                <tr>
                                                    <td>Цвет волос</td>
                                                    <td>Блондинка</td>
                                                </tr>
                                                <tr>
                                                    <td>Национальность</td>
                                                    <td>Русская</td>
                                                </tr>
                                                <tr>
                                                    <td>Фото</td>
                                                    <td>Типажные</td>
                                                </tr>
                                                <tr>
                                                    <td>Владение языками</td>
                                                    <td>Русский, Английский, Немецкий</td>
                                                </tr>
                                                <tr>
                                                    <td>На теле</td>
                                                    <td>Тату, пирсинг</td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="params__part">
                                        <div class="params__table">
                                            <table>
                                                <caption class="params__heading heading heading--marker">Местоположение</caption>
                                                <tr>
                                                    <td>Страна</td>
                                                    <td>Россия</td>
                                                </tr>
                                                <tr>
                                                    <td>Область</td>
                                                    <td>Московская</td>
                                                </tr>
                                                <tr>
                                                    <td>Город</td>
                                                    <td>Москва</td>
                                                </tr>
                                                <tr>
                                                    <td>Район</td>
                                                    <td>Вешняки</td>
                                                </tr>
                                                <tr>
                                                    <td>Метро</td>
                                                    <td>Выхино</td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="params__about">
                                    <div class="params__about-wrap">
                                        <h2 class="params__heading heading heading--about">О себе</h2>
                                        <div class="params__about-text"> Эффектная красотка для состоятельных мужчин! Красивая,
                                            ухоженная, темпераментная девушка, без вредных привычек! ТОЛЬКО для состоятельных мужчин!
                                            Убедительная просьба малолеток, пьяных и малоимущих не звонить! Отдых на высшем уровне!
                                            Приезжай или пригласи, и ты убедишься в этом сам! Качественно! Дорого! Есть Viber и Whats App.
                                            I speak English very well. Не салон, не терминал! </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="questionnaire__wrap questionnaire__wrap--border questionnaire__wrap--mobile">
                        <div class="questionnaire-price">
                            <div class="questionnaire-price__tiles">
                                <div class="questionnaire-price__tile questionnaire-price__tile--title">
                                    Апартаменты<span>стоимость</span></div>
                                <div class="questionnaire-price__tile">
                                    <div class="questionnaire-price__time">1 час</div>
                                    <div class="questionnaire-price__money">2 000&nbsp₽</div>
                                </div>
                                <div class="questionnaire-price__tile">
                                    <div class="questionnaire-price__time">2 час</div>
                                    <div class="questionnaire-price__money">4 000&nbsp₽</div>
                                </div>
                                <div class="questionnaire-price__tile">
                                    <div class="questionnaire-price__time">Ночь</div>
                                    <div class="questionnaire-price__money">10 000&nbsp₽</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="questionnaire__wrap questionnaire__wrap--border questionnaire__wrap--mobile">
                        <div class="questionnaire-price">
                            <div class="questionnaire-price__tiles">
                                <div class="questionnaire-price__tile questionnaire-price__tile--title">
                                    Апартаменты<span>стоимость</span></div>
                                <div class="questionnaire-price__tile">
                                    <div class="questionnaire-price__time">1 час</div>
                                    <div class="questionnaire-price__money">2 000&nbsp₽</div>
                                </div>
                                <div class="questionnaire-price__tile">
                                    <div class="questionnaire-price__time">2 час</div>
                                    <div class="questionnaire-price__money">4 000&nbsp₽</div>
                                </div>
                                <div class="questionnaire-price__tile">
                                    <div class="questionnaire-price__time">Ночь</div>
                                    <div class="questionnaire-price__money">10 000&nbsp₽</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="questionnaire__wrap questionnaire__wrap--border questionnaire__wrap--mobile">
                        <div class="questionnaire__services">
                            <div class="questionnaire-services">
                                <h2 class="questionnaire-services__heading heading heading--background">Интим-услуги</h2>
                                <div class="questionnaire-services__tables">
                                    <div class="questionnaire-services__table">
                                        <table>
                                            <caption>Основное</caption>
                                            <tr>
                                                <td>Классический</td>
                                                <td>Входит в стоимость часа</td>
                                            </tr>
                                            <tr>
                                                <td class="questionnaire-services__text-disabled">Анальный</td>
                                                <td class="questionnaire-services__text-price">+1000 ₽</td>
                                            </tr>
                                            <tr>
                                                <td class="questionnaire-services__text-disabled">Групповой</td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td class="questionnaire-services__text-disabled">Лесбийский</td>
                                                <td></td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div class="questionnaire-services__table">
                                        <table>
                                            <caption>Ласка</caption>
                                            <tr>
                                                <td>В презервативе</td>
                                                <td>Входит в стоимость часа</td>
                                            </tr>
                                            <tr>
                                                <td class="questionnaire-services__text-disabled">Без презерватива</td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td>Глубокий</td>
                                                <td class="questionnaire-services__text-price">+500 ₽</td>
                                            </tr>
                                            <tr>
                                                <td class="questionnaire-services__text-disabled">В машине</td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td>Кунилингус</td>
                                                <td>Входит в стоимость часа</td>
                                            </tr>
                                            <tr>
                                                <td class="questionnaire-services__text-disabled">Анилингус</td>
                                                <td></td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div class="questionnaire-services__table">
                                        <table>
                                            <caption>Финиш</caption>
                                            <tr>
                                                <td class="questionnaire-services__text-disabled">В рот</td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td>На лицо</td>
                                                <td class="questionnaire-services__text-price">+500 ₽</td>
                                            </tr>
                                            <tr>
                                                <td>На грудь</td>
                                                <td>Входит в стоимость часа</td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div class="questionnaire-services__table">
                                        <table>
                                            <caption>Лесби-шоу</caption>
                                            <tr>
                                                <td>Откровенное</td>
                                                <td>Входит в стоимость часа</td>
                                            </tr>
                                            <tr>
                                                <td>Лёгкое</td>
                                                <td>Входит в стоимость часа</td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div class="questionnaire-services__table">
                                        <table>
                                            <caption>Стриптиз</caption>
                                            <tr>
                                                <td class="questionnaire-services__text-disabled">Профи</td>
                                                <td class="questionnaire-services__text-price"></td>
                                            </tr>
                                            <tr>
                                                <td>Не профи</td>
                                                <td class="questionnaire-services__text-price">+300 ₽</td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div class="questionnaire-services__table">
                                        <table>
                                            <caption>Массаж</caption>
                                            <tr>
                                                <td>Классический</td>
                                                <td>Входит в стоимость часа</td>
                                            </tr>
                                            <tr>
                                                <td class="questionnaire-services__text-disabled">Профессиональный</td>
                                                <td class="questionnaire-services__text-price"></td>
                                            </tr>
                                            <tr>
                                                <td>Расслабляющий</td>
                                                <td>Входит в стоимость часа</td>
                                            </tr>
                                            <tr>
                                                <td class="questionnaire-services__text-disabled">Тайский</td>
                                                <td class="questionnaire-services__text-price"></td>
                                            </tr>
                                            <tr>
                                                <td class="questionnaire-services__text-disabled">Урологический</td>
                                                <td class="questionnaire-services__text-price"></td>
                                            </tr>
                                            <tr>
                                                <td class="questionnaire-services__text-disabled">Точечный</td>
                                                <td class="questionnaire-services__text-price"></td>
                                            </tr>
                                            <tr>
                                                <td>Эротический</td>
                                                <td>Входит в стоимость часа</td>
                                            </tr>
                                            <tr>
                                                <td class="questionnaire-services__text-disabled">Ветка сакуры</td>
                                                <td class="questionnaire-services__text-price"></td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div class="questionnaire-services__table">
                                        <table>
                                            <caption>Садо-мазо</caption>
                                            <tr>
                                                <td class="questionnaire-services__text-disabled">Бандаж</td>
                                                <td class="questionnaire-services__text-price"></td>
                                            </tr>
                                            <tr>
                                                <td class="questionnaire-services__text-disabled">Госпожа</td>
                                                <td class="questionnaire-services__text-price"></td>
                                            </tr>
                                            <tr>
                                                <td class="questionnaire-services__text-disabled">Рабыня</td>
                                                <td class="questionnaire-services__text-price"></td>
                                            </tr>
                                            <tr>
                                                <td class="questionnaire-services__text-disabled">Лёгкая доминация</td>
                                                <td class="questionnaire-services__text-price"></td>
                                            </tr>
                                            <tr>
                                                <td class="questionnaire-services__text-disabled">Эротические игры</td>
                                                <td class="questionnaire-services__text-price"></td>
                                            </tr>
                                            <tr>
                                                <td class="questionnaire-services__text-disabled">Порка</td>
                                                <td class="questionnaire-services__text-price"></td>
                                            </tr>
                                            <tr>
                                                <td class="questionnaire-services__text-disabled">Фетиш</td>
                                                <td class="questionnaire-services__text-price"></td>
                                            </tr>
                                            <tr>
                                                <td class="questionnaire-services__text-disabled">Фут-фетиш</td>
                                                <td class="questionnaire-services__text-price"></td>
                                            </tr>
                                            <tr>
                                                <td class="questionnaire-services__text-disabled">Трамплинг</td>
                                                <td class="questionnaire-services__text-price"></td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div class="questionnaire-services__table">
                                        <table>
                                            <caption>Экстрим</caption>
                                            <tr>
                                                <td class="questionnaire-services__text-disabled">Страпон</td>
                                                <td class="questionnaire-services__text-price"></td>
                                            </tr>
                                            <tr>
                                                <td class="questionnaire-services__text-disabled">Золотой дождь выдача</td>
                                                <td class="questionnaire-services__text-price"></td>
                                            </tr>
                                            <tr>
                                                <td class="questionnaire-services__text-disabled">Золотой дождь приём</td>
                                                <td class="questionnaire-services__text-price"></td>
                                            </tr>
                                            <tr>
                                                <td class="questionnaire-services__text-disabled">Копро выдачая</td>
                                                <td class="questionnaire-services__text-price"></td>
                                            </tr>
                                            <tr>
                                                <td class="questionnaire-services__text-disabled">Копро приём</td>
                                                <td class="questionnaire-services__text-price"></td>
                                            </tr>
                                            <tr>
                                                <td class="questionnaire-services__text-disabled">Фистинг анальный</td>
                                                <td class="questionnaire-services__text-price"></td>
                                            </tr>
                                            <tr>
                                                <td class="questionnaire-services__text-disabled">Фистинг вагинальный</td>
                                                <td class="questionnaire-services__text-price"></td>
                                            </tr>
                                            <tr>
                                                <td class="questionnaire-services__text-disabled">Секс игрушки</td>
                                                <td class="questionnaire-services__text-price"></td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div class="questionnaire-services__table">
                                        <table>
                                            <caption>Экстрим</caption>
                                            <tr>
                                                <td>Эскорт</td>
                                                <td class="questionnaire-services__text-price">+1500 ₽</td>
                                            </tr>
                                            <tr>
                                                <td class="questionnaire-services__text-disabled">Фото/видео</td>
                                                <td class="questionnaire-services__text-price"></td>
                                            </tr>
                                            <tr>
                                                <td class="questionnaire-services__text-disabled">Услуги семейной паре</td>
                                                <td class="questionnaire-services__text-price"></td>
                                            </tr>
                                            <tr>
                                                <td class="questionnaire-services__text-disabled">Виртуальный секс</td>
                                                <td class="questionnaire-services__text-price"></td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="questionnaire__btn">
                        <a href="#popup-tip" class="questionnaire__btn-report button button--transparent js-tip-popup">
                            <svg xmlns="http://www.w3.org/2000/svg" width="10" height="18" viewBox="0 0 10 18" fill="none">
                                <path
                                    d="M4.99971 8.1C3.75906 8.1 2.74971 7.09065 2.74971 5.85C2.74971 4.60935 3.75906 3.6 4.99971 3.6C6.24036 3.6 7.24971 4.60935 7.24971 5.85C7.24971 6.34702 7.65263 6.75 8.14971 6.75C8.64679 6.75 9.04971 6.34702 9.04971 5.85C9.04971 3.92619 7.70094 2.31255 5.89971 1.90204V0.9C5.89971 0.402975 5.49679 0 4.99971 0C4.50263 0 4.09971 0.402975 4.09971 0.9V1.90204C2.29847 2.31255 0.949707 3.92619 0.949707 5.85C0.949707 8.08318 2.76653 9.9 4.99971 9.9C6.24036 9.9 7.24971 10.9093 7.24971 12.15C7.24971 13.3906 6.24036 14.4 4.99971 14.4C3.75906 14.4 2.74971 13.3906 2.74971 12.15C2.74971 11.653 2.34679 11.25 1.84971 11.25C1.35263 11.25 0.949707 11.653 0.949707 12.15C0.949707 14.0738 2.29847 15.6874 4.09971 16.098V17.1C4.09971 17.597 4.50263 18 4.99971 18C5.49679 18 5.89971 17.597 5.89971 17.1V16.098C7.70094 15.6874 9.04971 14.0738 9.04971 12.15C9.04971 9.91682 7.23289 8.1 4.99971 8.1Z"
                                    fill="#FF9900" />
                            </svg>
                            <span>Отправить чаевые</span>
                        </a>
                    </div>
                </div>
            </div>
            <div class="questionnaire__reviews">
                <div class="reviews">
                    <h2 class="reviews__title">Добавить отзыв</h2>
                    <form class="reviews__form" action="" method="post">
                        <div class="reviews__inputs">
                            <div class="reviews__place">
                                <div class="input">
                                    <label class="input__label" for="name">Представьтесь </label>
                                    <input class="input__control" id="name" type="text" name="name" placeholder=""
                                           onblur="placeholder=''" onfocus="placeholder=''" autocomplete="on" />
                                </div>
                            </div>
                            <div class="reviews__place">
                                <div class="input">
                                    <label class="input__label" for="email">Ваш E-mail </label>
                                    <input class="input__control" id="email" type="email" name="email" placeholder=""
                                           onblur="placeholder=''" onfocus="placeholder=''" autocomplete="on" required="required" />
                                </div>
                            </div>
                            <div class="reviews__place reviews__place--position">
                                <div class="checkbox">
                                    <input class="checkbox__input" id="show-name" type="checkbox" name="show-name">
                                    <label class="checkbox__label" for="show-name">Не показывать своё имя</label>
                                </div>
                            </div>
                        </div>
                        <div class="reviews__cover">
                            <div class="reviews__wrap">
                                <div class="reviews__textarea-wrap">
                                    <label class="reviews__textarea-label">Отзыв</label>
                                    <textarea class="reviews__textarea" name="review"></textarea>
                                </div>
                                <div class="reviews__buttons js-radio-container">
                                    <input class="js-reviews-radio" type="hidden" name="recommended" value="0">
                                    <div class="reviews__buttons-wrap">
                                        <div class="reviews__field">
                                            <input class="reviews__radio js-radio" id="recommended" type="radio" value="true">
                                            <label class="reviews__radio-label like" for="recommended">Рекомендую</label>
                                        </div>
                                        <div class="reviews__field">
                                            <input class="reviews__radio js-radio" id="not-recommended" type="radio" value="false">
                                            <label class="reviews__radio-label like like--dislike" for="not-recommended">Не
                                                рекомендую</label>
                                        </div>
                                    </div>
                                    <button class="reviews__sumbit button button--transparent"> Оставить отзыв</button>
                                </div>
                            </div>
                        </div>
                        <div class="reviews__rating">
                            <div class="reviews__rating-title">Оценки</div>
                            <div class="reviews__rating-wrap">
                                <div class="reviews__rating-line js-radio-container">
                                    <input class="js-reviews-radio" type="hidden" name="photo" value="yes">
                                    <div class="reviews__rating-name">Фото соответствует</div>
                                    <div class="reviews__rating-group">
                                        <div class="reviews__rating-field">
                                            <input class="reviews__rating-radio js-radio" id="yes" type="radio" value="yes" checked>
                                            <label class="reviews__rating-label" for="yes">да</label>
                                        </div>
                                        <div class="reviews__rating-field">
                                            <input class="reviews__rating-radio js-radio" id="no" type="radio" value="no">
                                            <label class="reviews__rating-label" for="no">нет</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="reviews__rating-line js-checkbox-container">
                                    <div class="reviews__rating-name">Апартаменты</div>
                                    <input class="js-reviews-input" type="hidden" name="apartments" value="1">
                                    <div class="reviews__rating-places">
                                        <div class="reviews__rating-place">
                                            <input class="reviews__rating-checkbox js-checkbox" id="apartments1" type="checkbox" value="1"
                                                   data-type="apartments">
                                            <label class="reviews__checkbox-label" for="apartments1"></label>
                                        </div>
                                        <div class="reviews__rating-place">
                                            <input class="reviews__rating-checkbox js-checkbox" id="apartments2" type="checkbox" value="2"
                                                   data-type="apartments">
                                            <label class="reviews__checkbox-label" for="apartments2"></label>
                                        </div>
                                        <div class="reviews__rating-place">
                                            <input class="reviews__rating-checkbox js-checkbox" id="apartments3" type="checkbox" value="3"
                                                   data-type="apartments">
                                            <label class="reviews__checkbox-label" for="apartments3"></label>
                                        </div>
                                        <div class="reviews__rating-place">
                                            <input class="reviews__rating-checkbox js-checkbox" id="apartments4" type="checkbox" value="4"
                                                   data-type="apartments">
                                            <label class="reviews__checkbox-label" for="apartments4"></label>
                                        </div>
                                        <div class="reviews__rating-place">
                                            <input class="reviews__rating-checkbox js-checkbox" id="apartments5" type="checkbox" value="5"
                                                   data-type="apartments">
                                            <label class="reviews__checkbox-label" for="apartments5"></label>
                                        </div>
                                    </div>
                                </div>
                                <div class="reviews__rating-line js-checkbox-container">
                                    <div class="reviews__rating-name">Сервис</div>
                                    <input class="js-reviews-input" type="hidden" name="service" value="1">
                                    <div class="reviews__rating-places">
                                        <div class="reviews__rating-place">
                                            <input class="reviews__rating-checkbox js-checkbox" id="service1" type="checkbox" value="1"
                                                   data-type="service">
                                            <label class="reviews__checkbox-label" for="service1"></label>
                                        </div>
                                        <div class="reviews__rating-place">
                                            <input class="reviews__rating-checkbox js-checkbox" id="service2" type="checkbox" value="2"
                                                   data-type="service">
                                            <label class="reviews__checkbox-label" for="service2"></label>
                                        </div>
                                        <div class="reviews__rating-place">
                                            <input class="reviews__rating-checkbox js-checkbox" id="service3" type="checkbox" value="3"
                                                   data-type="service">
                                            <label class="reviews__checkbox-label" for="service3"></label>
                                        </div>
                                        <div class="reviews__rating-place">
                                            <input class="reviews__rating-checkbox js-checkbox" id="service4" type="checkbox" value="4"
                                                   data-type="service">
                                            <label class="reviews__checkbox-label" for="service4"></label>
                                        </div>
                                        <div class="reviews__rating-place">
                                            <input class="reviews__rating-checkbox js-checkbox" id="service5" type="checkbox" value="5"
                                                   data-type="service">
                                            <label class="reviews__checkbox-label" for="service5"></label>
                                        </div>
                                    </div>
                                </div>
                                <div class="reviews__rating-line js-checkbox-container">
                                    <div class="reviews__rating-name">Классический секс</div>
                                    <input class="js-reviews-input" type="hidden" name="sex" value="1">
                                    <div class="reviews__rating-places">
                                        <div class="reviews__rating-place">
                                            <input class="reviews__rating-checkbox js-checkbox" id="sex1" type="checkbox" value="1"
                                                   data-type="sex">
                                            <label class="reviews__checkbox-label" for="sex1"></label>
                                        </div>
                                        <div class="reviews__rating-place">
                                            <input class="reviews__rating-checkbox js-checkbox" id="sex2" type="checkbox" value="2"
                                                   data-type="sex">
                                            <label class="reviews__checkbox-label" for="sex2"></label>
                                        </div>
                                        <div class="reviews__rating-place">
                                            <input class="reviews__rating-checkbox js-checkbox" id="sex3" type="checkbox" value="3"
                                                   data-type="sex">
                                            <label class="reviews__checkbox-label" for="sex3"></label>
                                        </div>
                                        <div class="reviews__rating-place">
                                            <input class="reviews__rating-checkbox js-checkbox" id="sex4" type="checkbox" value="4"
                                                   data-type="sex">
                                            <label class="reviews__checkbox-label" for="sex4"></label>
                                        </div>
                                        <div class="reviews__rating-place">
                                            <input class="reviews__rating-checkbox js-checkbox" id="sex5" type="checkbox" value="5"
                                                   data-type="sex">
                                            <label class="reviews__checkbox-label" for="sex5"></label>
                                        </div>
                                    </div>
                                </div>
                                <div class="reviews__rating-line js-checkbox-container">
                                    <div class="reviews__rating-name">Минет</div>
                                    <input class="js-reviews-input" type="hidden" name="blow" value="1">
                                    <div class="reviews__rating-places">
                                        <div class="reviews__rating-place">
                                            <input class="reviews__rating-checkbox js-checkbox" id="blow1" type="checkbox" value="1"
                                                   data-type="blow">
                                            <label class="reviews__checkbox-label" for="blow1"></label>
                                        </div>
                                        <div class="reviews__rating-place">
                                            <input class="reviews__rating-checkbox js-checkbox" id="blow2" type="checkbox" value="2"
                                                   data-type="blow">
                                            <label class="reviews__checkbox-label" for="blow2"></label>
                                        </div>
                                        <div class="reviews__rating-place">
                                            <input class="reviews__rating-checkbox js-checkbox" id="blow3" type="checkbox" value="3"
                                                   data-type="blow">
                                            <label class="reviews__checkbox-label" for="blow3"></label>
                                        </div>
                                        <div class="reviews__rating-place">
                                            <input class="reviews__rating-checkbox js-checkbox" id="blow4" type="checkbox" value="4"
                                                   data-type="blow">
                                            <label class="reviews__checkbox-label" for="blow4"></label>
                                        </div>
                                        <div class="reviews__rating-place">
                                            <input class="reviews__rating-checkbox js-checkbox" id="blow5" type="checkbox" value="5"
                                                   data-type="blow">
                                            <label class="reviews__checkbox-label" for="blow5"></label>
                                        </div>
                                    </div>
                                </div>
                                <div class="reviews__rating-line js-checkbox-container">
                                    <div class="reviews__rating-name">Окончание</div>
                                    <input class="js-reviews-input" type="hidden" name="end" value="1">
                                    <div class="reviews__rating-places">
                                        <div class="reviews__rating-place">
                                            <input class="reviews__rating-checkbox js-checkbox" id="end1" type="checkbox" value="1"
                                                   data-type="end">
                                            <label class="reviews__checkbox-label" for="end1"></label>
                                        </div>
                                        <div class="reviews__rating-place">
                                            <input class="reviews__rating-checkbox js-checkbox" id="end2" type="checkbox" value="2"
                                                   data-type="end">
                                            <label class="reviews__checkbox-label" for="end2"></label>
                                        </div>
                                        <div class="reviews__rating-place">
                                            <input class="reviews__rating-checkbox js-checkbox" id="end3" type="checkbox" value="3"
                                                   data-type="end">
                                            <label class="reviews__checkbox-label" for="end3"></label>
                                        </div>
                                        <div class="reviews__rating-place">
                                            <input class="reviews__rating-checkbox js-checkbox" id="end4" type="checkbox" value="4"
                                                   data-type="end">
                                            <label class="reviews__checkbox-label" for="end4"></label>
                                        </div>
                                        <div class="reviews__rating-place">
                                            <input class="reviews__rating-checkbox js-checkbox" id="end5" type="checkbox" value="5"
                                                   data-type="end">
                                            <label class="reviews__checkbox-label" for="end5"></label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <button class="reviews__sumbit reviews__sumbit--mobile button button--transparent"> Оставить
                                отзыв</button>
                        </div>
                    </form>
                    <div class="reviews__list">
                        <div class="reviews__list-head">
                            <div class="reviews__title reviews__title--second">Отзывы посетителей<sup>10</sup></div>
                            <div class="reviews__list-likes">
                                <div class="reviews__list-like">7</div>
                                <div class="reviews__list-like reviews__list--dislike">3</div>
                            </div>
                        </div>
                        <div class="reviews__items">
                            <div class="reviews__item">
                                <div class="reviews-item reviews-item--dislike">
                                    <div class="reviews-item__head">
                                        <div class="reviews-item__user">
                                            <div class="reviews-item__user-wrap">
                                                <div class="reviews-item__icon"><img src="/images/user-icon-d74ee624.jpg" alt="Константин">
                                                    <div class="reviews-item__status"></div>
                                                </div>
                                            </div>
                                            <div class="reviews-item__name">Константин</div>
                                        </div>
                                        <div class="reviews-item__date">06 мая 2020 14:14</div>
                                    </div>
                                    <div class="reviews-item__content">
                                        <div class="reviews-item__desc">
                                            <div class="reviews-item__text">Повседневная практика показывает, что консультация с широким
                                                активом представляет собой интересный эксперимент проверки систем массового участия. Идейные
                                                соображения высшего порядка, а также сложившаяся структура организации представляет собой
                                                интересный эксперимент проверки позиций, занимаемых участниками в отношении поставленных
                                                задач.</div>
                                            <div class="reviews-item__bottom">
                                                <div class="reviews-item__like like like--dislike">Не рекомендую</div>
                                                <button class="reviews-item__btn">Ответить</button>
                                            </div>
                                        </div>
                                        <div class="reviews-item__side">
                                            <div class="reviews-item__list">
                                                <div class="reviews-item__line">
                                                    <div class="reviews-item__line-title">Общая оценка</div>
                                                    <div class="reviews-item__line-rating">
                                                        <div class="rating-static rating-static--three rating-static--size">
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="reviews-item__line">
                                                    <div class="reviews-item__line-title">Фото соответствует</div>
                                                    <div class="reviews-item__line-rating">
                                                        <div class="rating-static">нет</div>
                                                    </div>
                                                </div>
                                                <div class="reviews-item__line">
                                                    <div class="reviews-item__line-title">Апартаменты</div>
                                                    <div class="reviews-item__line-rating">
                                                        <div class="rating-static rating-static--three">
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="reviews-item__line">
                                                    <div class="reviews-item__line-title">Сервис</div>
                                                    <div class="reviews-item__line-rating">
                                                        <div class="rating-static rating-static--two">
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="reviews-item__line">
                                                    <div class="reviews-item__line-title">Классический секс</div>
                                                    <div class="reviews-item__line-rating">
                                                        <div class="rating-static rating-static--one">
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="reviews-item__line">
                                                    <div class="reviews-item__line-title">Минет</div>
                                                    <div class="reviews-item__line-rating">
                                                        <div class="rating-static rating-static--four">
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="reviews-item__line">
                                                    <div class="reviews-item__line-title">Окончание</div>
                                                    <div class="reviews-item__line-rating">
                                                        <div class="rating-static rating-static--two">
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="reviews-item__bottom reviews-item__bottom--mobile">
                                                <div class="reviews-item__like">
                                                    <div class="like like--dislike">Не рекомендую</div>
                                                </div>
                                                <button class="reviews-item__btn">Ответить</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="reviews__item">
                                <div class="reviews-item reviews-item--like">
                                    <div class="reviews-item__head">
                                        <div class="reviews-item__user">
                                            <div class="reviews-item__user-wrap">
                                                <div class="reviews-item__icon"><img src="/images/user-icon-d74ee624.jpg" alt="Константин">
                                                    <div class="reviews-item__status reviews-item__status--offline"></div>
                                                </div>
                                            </div>
                                            <div class="reviews-item__name">Константин</div>
                                        </div>
                                        <div class="reviews-item__date">06 мая 2020 14:14</div>
                                    </div>
                                    <div class="reviews-item__content">
                                        <div class="reviews-item__desc">
                                            <div class="reviews-item__text">Повседневная практика показывает, что консультация с широким
                                                активом представляет собой интересный эксперимент проверки систем массового участия. Идейные
                                                соображения высшего порядка, а также сложившаяся структура организации представляет собой
                                                интересный эксперимент проверки позиций, занимаемых участниками в отношении поставленных
                                                задач.</div>
                                            <div class="reviews-item__bottom">
                                                <div class="reviews-item__like like">Рекомендую</div>
                                                <button class="reviews-item__btn">Ответить</button>
                                            </div>
                                        </div>
                                        <div class="reviews-item__side">
                                            <div class="reviews-item__list">
                                                <div class="reviews-item__line">
                                                    <div class="reviews-item__line-title">Общая оценка</div>
                                                    <div class="reviews-item__line-rating">
                                                        <div class="rating-static rating-static--three rating-static--size">
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="reviews-item__line">
                                                    <div class="reviews-item__line-title">Фото соответствует</div>
                                                    <div class="reviews-item__line-rating">
                                                        <div class="rating-static">да</div>
                                                    </div>
                                                </div>
                                                <div class="reviews-item__line">
                                                    <div class="reviews-item__line-title">Апартаменты</div>
                                                    <div class="reviews-item__line-rating">
                                                        <div class="rating-static rating-static--two">
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="reviews-item__line">
                                                    <div class="reviews-item__line-title">Сервис</div>
                                                    <div class="reviews-item__line-rating">
                                                        <div class="rating-static rating-static--five">
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="reviews-item__line">
                                                    <div class="reviews-item__line-title">Классический секс</div>
                                                    <div class="reviews-item__line-rating">
                                                        <div class="rating-static rating-static--two">
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="reviews-item__line">
                                                    <div class="reviews-item__line-title">Минет</div>
                                                    <div class="reviews-item__line-rating">
                                                        <div class="rating-static rating-static--one">
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="reviews-item__line">
                                                    <div class="reviews-item__line-title">Окончание</div>
                                                    <div class="reviews-item__line-rating">
                                                        <div class="rating-static rating-static--three">
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="reviews-item__bottom reviews-item__bottom--mobile">
                                                <div class="reviews-item__like">
                                                    <div class="like">Рекомендую</div>
                                                </div>
                                                <button class="reviews-item__btn">Ответить</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="reviews__response">
                                <div class="reviews-response">
                                    <div class="reviews-response__wrap">
                                        <div class="reviews-response__left">
                                            <div class="reviews-response__user">
                                                <div class="reviews-response__inner">
                                                    <div class="reviews-response__icon"><img src="/images/user-icon-d74ee624.jpg"
                                                                                             alt="Константин">
                                                        <div class="reviews-response__status"></div>
                                                    </div>
                                                </div>
                                                <div class="reviews-response-name">Анжелика</div>
                                            </div>
                                            <div class="reviews-response__massage">Спасибо большое!</div>
                                        </div>
                                        <div class="reviews-response__date">06 мая 2020 14:14</div>
                                    </div>
                                    <div class="reviews-response__body">
                                        <div class="reviews-response__massage"> Спасибо большое!</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="questionnaire__pagination">
                <div class="pagination">
                    <div class="pagination__wrap">
                        <button class="pagination__button pagination__button--back pagination__button--disabled">
                            <svg width="7" height="12" viewBox="0 0 7 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M6 1L1 6L6 11" stroke="#5C5C5C" stroke-width="1.8" stroke-linecap="round"
                                      stroke-linejoin="round" />
                            </svg> Назад </button>
                        <ul class="pagination__list">
                            <li class="pagination__item pagination__item--active"><a class="pagination__link" href="#">1</a></li>
                            <li class="pagination__item"><a class="pagination__link" href="#">2</a></li>
                            <li class="pagination__item"><a class="pagination__link pagination__link--dot" href="#">...</a></li>
                        </ul>
                        <button class="pagination__button"> Далее<svg width="7" height="12" viewBox="0 0 7 12" fill="none"
                                                                      xmlns="http://www.w3.org/2000/svg">
                                <path d="M1 11L6 6L1 1" stroke="#5C5C5C" stroke-width="1.8" stroke-linecap="round"
                                      stroke-linejoin="round" />
                            </svg>
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <div class="questionnaire__tabs">
            <div class="tabs">
                <div class="tabs__head">
                    <div class="tabs__btns">
                        <button class="tabs__btn tabs__btn--active" data-type="questionnaire"><span class="tabs__count"></span>
                            <div class="tabs__btn-text">Похожие анкеты</div>
                        </button>
                        <button class="tabs__btn" data-type="viewed"><span class="tabs__count">18</span>
                            <div class="tabs__btn-text">Недавно просмотренные</div>
                        </button>
                        <button class="tabs__btn" data-type="Favorites"><span class="tabs__count">3</span>
                            <div class="tabs__btn-text">Избранные анкеты</div>
                        </button>
                    </div>
                </div>
                <div class="tabs__inner">
                    <div class="tabs__container">
                        <div class="tabs__content" data-type="questionnaire">
                            <div class="tabs__slider swiper-container">
                                <div class="tabs__wrapper swiper-wrapper">
                                    <div class="tabs__slide swiper-slide">
                                        <div class="tabs__slide-head">
                                            <div class="tabs__slide-name">Оля</div>
                                            <button class="tabs__slide-favorites"><svg class="add-favorites js-favorites"
                                                                                       xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24">
                                                    <path class="path-empty"
                                                          d="M6.28 3c3.236.001 4.973 3.491 5.72 5.031.75-1.547 2.469-5.021 5.726-5.021 2.058 0 4.274 1.309 4.274 4.182 0 3.442-4.744 7.851-10 13-5.258-5.151-10-9.559-10-13 0-2.676 1.965-4.193 4.28-4.192zm.001-2c-3.183 0-6.281 2.187-6.281 6.192 0 4.661 5.57 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-4.011-3.097-6.182-6.274-6.182-2.204 0-4.446 1.042-5.726 3.238-1.285-2.206-3.522-3.248-5.719-3.248z">
                                                    </path>
                                                    <path class="path-full" xmlns="http://www.w3.org/2000/svg"
                                                          d="M12 4.248c-3.148-5.402-12-3.825-12 2.944 0 4.661 5.571 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-6.792-8.875-8.306-12-2.944z">
                                                    </path>
                                                </svg></button>
                                        </div>
                                        <div class="tabs__slide-body"><img src="/images/img-1-db8e7456.jpg" alt="Оля">
                                            <div class="tabs__slide-stars">
                                                <div class="tabs__slide-title">Массажный салон</div>
                                                <div class="tabs__slide-rating">
                                                    <div class="rating-static rating-static--five">
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tabs__slide-bottom">
                                            <div class="tabs__slide-line">
                                                <div class="tabs__slide-title">Апартаменты</div>
                                                <div class="tabs__slide-price">от&nbsp<span>2 500 ₽</span> /час</div>
                                            </div>
                                            <div class="tabs__slide-line">
                                                <div class="tabs__slide-title">Выезд</div>
                                                <div class="tabs__slide-price">от&nbsp<span>4 500 ₽</span> /час</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tabs__slide swiper-slide">
                                        <div class="tabs__slide-head">
                                            <div class="tabs__slide-name">Катя</div>
                                            <button class="tabs__slide-favorites"><svg class="add-favorites js-favorites"
                                                                                       xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24">
                                                    <path class="path-empty"
                                                          d="M6.28 3c3.236.001 4.973 3.491 5.72 5.031.75-1.547 2.469-5.021 5.726-5.021 2.058 0 4.274 1.309 4.274 4.182 0 3.442-4.744 7.851-10 13-5.258-5.151-10-9.559-10-13 0-2.676 1.965-4.193 4.28-4.192zm.001-2c-3.183 0-6.281 2.187-6.281 6.192 0 4.661 5.57 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-4.011-3.097-6.182-6.274-6.182-2.204 0-4.446 1.042-5.726 3.238-1.285-2.206-3.522-3.248-5.719-3.248z">
                                                    </path>
                                                    <path class="path-full" xmlns="http://www.w3.org/2000/svg"
                                                          d="M12 4.248c-3.148-5.402-12-3.825-12 2.944 0 4.661 5.571 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-6.792-8.875-8.306-12-2.944z">
                                                    </path>
                                                </svg></button>
                                        </div>
                                        <div class="tabs__slide-body"><img src="/images/img-1-db8e7456.jpg" alt="Катя">
                                            <div class="tabs__slide-stars">
                                                <div class="tabs__slide-title">Массажный салон</div>
                                                <div class="tabs__slide-rating">
                                                    <div class="rating-static rating-static--two">
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tabs__slide-bottom">
                                            <div class="tabs__slide-line">
                                                <div class="tabs__slide-title">Апартаменты</div>
                                                <div class="tabs__slide-price">от&nbsp<span>2 500 ₽</span> /час</div>
                                            </div>
                                            <div class="tabs__slide-line">
                                                <div class="tabs__slide-title">Выезд</div>
                                                <div class="tabs__slide-price">от&nbsp<span>4 500 ₽</span> /час</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tabs__slide swiper-slide">
                                        <div class="tabs__slide-head">
                                            <div class="tabs__slide-name">Настя</div>
                                            <button class="tabs__slide-favorites"><svg class="add-favorites js-favorites"
                                                                                       xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24">
                                                    <path class="path-empty"
                                                          d="M6.28 3c3.236.001 4.973 3.491 5.72 5.031.75-1.547 2.469-5.021 5.726-5.021 2.058 0 4.274 1.309 4.274 4.182 0 3.442-4.744 7.851-10 13-5.258-5.151-10-9.559-10-13 0-2.676 1.965-4.193 4.28-4.192zm.001-2c-3.183 0-6.281 2.187-6.281 6.192 0 4.661 5.57 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-4.011-3.097-6.182-6.274-6.182-2.204 0-4.446 1.042-5.726 3.238-1.285-2.206-3.522-3.248-5.719-3.248z">
                                                    </path>
                                                    <path class="path-full" xmlns="http://www.w3.org/2000/svg"
                                                          d="M12 4.248c-3.148-5.402-12-3.825-12 2.944 0 4.661 5.571 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-6.792-8.875-8.306-12-2.944z">
                                                    </path>
                                                </svg></button>
                                        </div>
                                        <div class="tabs__slide-body"><img src="/images/img-1-db8e7456.jpg" alt="Настя">
                                            <div class="tabs__slide-stars">
                                                <div class="tabs__slide-title">Массажный салон</div>
                                                <div class="tabs__slide-rating">
                                                    <div class="rating-static rating-static--three">
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tabs__slide-bottom">
                                            <div class="tabs__slide-line">
                                                <div class="tabs__slide-title">Апартаменты</div>
                                                <div class="tabs__slide-price">от&nbsp<span>2 500 ₽</span> /час</div>
                                            </div>
                                            <div class="tabs__slide-line">
                                                <div class="tabs__slide-title">Выезд</div>
                                                <div class="tabs__slide-price">от&nbsp<span>4 500 ₽</span> /час</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tabs__slide swiper-slide">
                                        <div class="tabs__slide-head">
                                            <div class="tabs__slide-name">Алена</div>
                                            <button class="tabs__slide-favorites"><svg class="add-favorites js-favorites"
                                                                                       xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24">
                                                    <path class="path-empty"
                                                          d="M6.28 3c3.236.001 4.973 3.491 5.72 5.031.75-1.547 2.469-5.021 5.726-5.021 2.058 0 4.274 1.309 4.274 4.182 0 3.442-4.744 7.851-10 13-5.258-5.151-10-9.559-10-13 0-2.676 1.965-4.193 4.28-4.192zm.001-2c-3.183 0-6.281 2.187-6.281 6.192 0 4.661 5.57 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-4.011-3.097-6.182-6.274-6.182-2.204 0-4.446 1.042-5.726 3.238-1.285-2.206-3.522-3.248-5.719-3.248z">
                                                    </path>
                                                    <path class="path-full" xmlns="http://www.w3.org/2000/svg"
                                                          d="M12 4.248c-3.148-5.402-12-3.825-12 2.944 0 4.661 5.571 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-6.792-8.875-8.306-12-2.944z">
                                                    </path>
                                                </svg></button>
                                        </div>
                                        <div class="tabs__slide-body"><img src="/images/img-1-db8e7456.jpg" alt="Алена">
                                            <div class="tabs__slide-stars">
                                                <div class="tabs__slide-title">Массажный салон</div>
                                                <div class="tabs__slide-rating">
                                                    <div class="rating-static rating-static--one">
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tabs__slide-bottom">
                                            <div class="tabs__slide-line">
                                                <div class="tabs__slide-title">Апартаменты</div>
                                                <div class="tabs__slide-price">от&nbsp<span>2 500 ₽</span> /час</div>
                                            </div>
                                            <div class="tabs__slide-line">
                                                <div class="tabs__slide-title">Выезд</div>
                                                <div class="tabs__slide-price">от&nbsp<span>4 500 ₽</span> /час</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tabs__slide swiper-slide">
                                        <div class="tabs__slide-head">
                                            <div class="tabs__slide-name">Оля</div>
                                            <button class="tabs__slide-favorites"><svg class="add-favorites js-favorites"
                                                                                       xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24">
                                                    <path class="path-empty"
                                                          d="M6.28 3c3.236.001 4.973 3.491 5.72 5.031.75-1.547 2.469-5.021 5.726-5.021 2.058 0 4.274 1.309 4.274 4.182 0 3.442-4.744 7.851-10 13-5.258-5.151-10-9.559-10-13 0-2.676 1.965-4.193 4.28-4.192zm.001-2c-3.183 0-6.281 2.187-6.281 6.192 0 4.661 5.57 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-4.011-3.097-6.182-6.274-6.182-2.204 0-4.446 1.042-5.726 3.238-1.285-2.206-3.522-3.248-5.719-3.248z">
                                                    </path>
                                                    <path class="path-full" xmlns="http://www.w3.org/2000/svg"
                                                          d="M12 4.248c-3.148-5.402-12-3.825-12 2.944 0 4.661 5.571 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-6.792-8.875-8.306-12-2.944z">
                                                    </path>
                                                </svg></button>
                                        </div>
                                        <div class="tabs__slide-body"><img src="/images/img-1-db8e7456.jpg" alt="Оля">
                                            <div class="tabs__slide-stars">
                                                <div class="tabs__slide-title">Массажный салон</div>
                                                <div class="tabs__slide-rating">
                                                    <div class="rating-static rating-static--five">
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tabs__slide-bottom">
                                            <div class="tabs__slide-line">
                                                <div class="tabs__slide-title">Апартаменты</div>
                                                <div class="tabs__slide-price">от&nbsp<span>2 500 ₽</span> /час</div>
                                            </div>
                                            <div class="tabs__slide-line">
                                                <div class="tabs__slide-title">Выезд</div>
                                                <div class="tabs__slide-price">от&nbsp<span>4 500 ₽</span> /час</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tabs__slide swiper-slide">
                                        <div class="tabs__slide-head">
                                            <div class="tabs__slide-name">Катя</div>
                                            <button class="tabs__slide-favorites"><svg class="add-favorites js-favorites"
                                                                                       xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24">
                                                    <path class="path-empty"
                                                          d="M6.28 3c3.236.001 4.973 3.491 5.72 5.031.75-1.547 2.469-5.021 5.726-5.021 2.058 0 4.274 1.309 4.274 4.182 0 3.442-4.744 7.851-10 13-5.258-5.151-10-9.559-10-13 0-2.676 1.965-4.193 4.28-4.192zm.001-2c-3.183 0-6.281 2.187-6.281 6.192 0 4.661 5.57 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-4.011-3.097-6.182-6.274-6.182-2.204 0-4.446 1.042-5.726 3.238-1.285-2.206-3.522-3.248-5.719-3.248z">
                                                    </path>
                                                    <path class="path-full" xmlns="http://www.w3.org/2000/svg"
                                                          d="M12 4.248c-3.148-5.402-12-3.825-12 2.944 0 4.661 5.571 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-6.792-8.875-8.306-12-2.944z">
                                                    </path>
                                                </svg></button>
                                        </div>
                                        <div class="tabs__slide-body"><img src="/images/img-1-db8e7456.jpg" alt="Катя">
                                            <div class="tabs__slide-stars">
                                                <div class="tabs__slide-title">Массажный салон</div>
                                                <div class="tabs__slide-rating">
                                                    <div class="rating-static rating-static--two">
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tabs__slide-bottom">
                                            <div class="tabs__slide-line">
                                                <div class="tabs__slide-title">Апартаменты</div>
                                                <div class="tabs__slide-price">от&nbsp<span>2 500 ₽</span> /час</div>
                                            </div>
                                            <div class="tabs__slide-line">
                                                <div class="tabs__slide-title">Выезд</div>
                                                <div class="tabs__slide-price">от&nbsp<span>4 500 ₽</span> /час</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tabs__slide swiper-slide">
                                        <div class="tabs__slide-head">
                                            <div class="tabs__slide-name">Настя</div>
                                            <button class="tabs__slide-favorites"><svg class="add-favorites js-favorites"
                                                                                       xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24">
                                                    <path class="path-empty"
                                                          d="M6.28 3c3.236.001 4.973 3.491 5.72 5.031.75-1.547 2.469-5.021 5.726-5.021 2.058 0 4.274 1.309 4.274 4.182 0 3.442-4.744 7.851-10 13-5.258-5.151-10-9.559-10-13 0-2.676 1.965-4.193 4.28-4.192zm.001-2c-3.183 0-6.281 2.187-6.281 6.192 0 4.661 5.57 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-4.011-3.097-6.182-6.274-6.182-2.204 0-4.446 1.042-5.726 3.238-1.285-2.206-3.522-3.248-5.719-3.248z">
                                                    </path>
                                                    <path class="path-full" xmlns="http://www.w3.org/2000/svg"
                                                          d="M12 4.248c-3.148-5.402-12-3.825-12 2.944 0 4.661 5.571 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-6.792-8.875-8.306-12-2.944z">
                                                    </path>
                                                </svg></button>
                                        </div>
                                        <div class="tabs__slide-body"><img src="/images/img-1-db8e7456.jpg" alt="Настя">
                                            <div class="tabs__slide-stars">
                                                <div class="tabs__slide-title">Массажный салон</div>
                                                <div class="tabs__slide-rating">
                                                    <div class="rating-static rating-static--three">
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tabs__slide-bottom">
                                            <div class="tabs__slide-line">
                                                <div class="tabs__slide-title">Апартаменты</div>
                                                <div class="tabs__slide-price">от&nbsp<span>2 500 ₽</span> /час</div>
                                            </div>
                                            <div class="tabs__slide-line">
                                                <div class="tabs__slide-title">Выезд</div>
                                                <div class="tabs__slide-price">от&nbsp<span>4 500 ₽</span> /час</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tabs__slide swiper-slide">
                                        <div class="tabs__slide-head">
                                            <div class="tabs__slide-name">Алена</div>
                                            <button class="tabs__slide-favorites"><svg class="add-favorites js-favorites"
                                                                                       xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24">
                                                    <path class="path-empty"
                                                          d="M6.28 3c3.236.001 4.973 3.491 5.72 5.031.75-1.547 2.469-5.021 5.726-5.021 2.058 0 4.274 1.309 4.274 4.182 0 3.442-4.744 7.851-10 13-5.258-5.151-10-9.559-10-13 0-2.676 1.965-4.193 4.28-4.192zm.001-2c-3.183 0-6.281 2.187-6.281 6.192 0 4.661 5.57 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-4.011-3.097-6.182-6.274-6.182-2.204 0-4.446 1.042-5.726 3.238-1.285-2.206-3.522-3.248-5.719-3.248z">
                                                    </path>
                                                    <path class="path-full" xmlns="http://www.w3.org/2000/svg"
                                                          d="M12 4.248c-3.148-5.402-12-3.825-12 2.944 0 4.661 5.571 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-6.792-8.875-8.306-12-2.944z">
                                                    </path>
                                                </svg></button>
                                        </div>
                                        <div class="tabs__slide-body"><img src="/images/img-1-db8e7456.jpg" alt="Алена">
                                            <div class="tabs__slide-stars">
                                                <div class="tabs__slide-title">Массажный салон</div>
                                                <div class="tabs__slide-rating">
                                                    <div class="rating-static rating-static--one">
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tabs__slide-bottom">
                                            <div class="tabs__slide-line">
                                                <div class="tabs__slide-title">Апартаменты</div>
                                                <div class="tabs__slide-price">от&nbsp<span>2 500 ₽</span> /час</div>
                                            </div>
                                            <div class="tabs__slide-line">
                                                <div class="tabs__slide-title">Выезд</div>
                                                <div class="tabs__slide-price">от&nbsp<span>4 500 ₽</span> /час</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tabs__slider-pagination slider-pagination swiper-pagination">
                                    <!-- Add Arrows-->
                                </div>
                                <div class="tabs__slider-next slider-next swiper-button-next"></div>
                                <div class="tabs__slider-prev slider-prev swiper-button-prev"></div>
                            </div>
                        </div>
                        <div class="tabs__content tabs__content--hidden" data-type="viewed">
                            <div class="tabs__slider swiper-container">
                                <div class="tabs__wrapper swiper-wrapper">
                                    <div class="tabs__slide swiper-slide">
                                        <div class="tabs__slide-head">
                                            <div class="tabs__slide-name">Оля</div>
                                            <button class="tabs__slide-favorites"><svg class="add-favorites js-favorites"
                                                                                       xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24">
                                                    <path class="path-empty"
                                                          d="M6.28 3c3.236.001 4.973 3.491 5.72 5.031.75-1.547 2.469-5.021 5.726-5.021 2.058 0 4.274 1.309 4.274 4.182 0 3.442-4.744 7.851-10 13-5.258-5.151-10-9.559-10-13 0-2.676 1.965-4.193 4.28-4.192zm.001-2c-3.183 0-6.281 2.187-6.281 6.192 0 4.661 5.57 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-4.011-3.097-6.182-6.274-6.182-2.204 0-4.446 1.042-5.726 3.238-1.285-2.206-3.522-3.248-5.719-3.248z">
                                                    </path>
                                                    <path class="path-full" xmlns="http://www.w3.org/2000/svg"
                                                          d="M12 4.248c-3.148-5.402-12-3.825-12 2.944 0 4.661 5.571 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-6.792-8.875-8.306-12-2.944z">
                                                    </path>
                                                </svg></button>
                                        </div>
                                        <div class="tabs__slide-body"><img src="/images/img-1-db8e7456.jpg" alt="Оля">
                                            <div class="tabs__slide-stars">
                                                <div class="tabs__slide-title">Массажный салон</div>
                                                <div class="tabs__slide-rating">
                                                    <div class="rating-static rating-static--five">
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tabs__slide-bottom">
                                            <div class="tabs__slide-line">
                                                <div class="tabs__slide-title">Апартаменты</div>
                                                <div class="tabs__slide-price">от&nbsp<span>2 500 ₽</span> /час</div>
                                            </div>
                                            <div class="tabs__slide-line">
                                                <div class="tabs__slide-title">Выезд</div>
                                                <div class="tabs__slide-price">от&nbsp<span>4 500 ₽</span> /час</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tabs__slide swiper-slide">
                                        <div class="tabs__slide-head">
                                            <div class="tabs__slide-name">Катя</div>
                                            <button class="tabs__slide-favorites"><svg class="add-favorites js-favorites"
                                                                                       xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24">
                                                    <path class="path-empty"
                                                          d="M6.28 3c3.236.001 4.973 3.491 5.72 5.031.75-1.547 2.469-5.021 5.726-5.021 2.058 0 4.274 1.309 4.274 4.182 0 3.442-4.744 7.851-10 13-5.258-5.151-10-9.559-10-13 0-2.676 1.965-4.193 4.28-4.192zm.001-2c-3.183 0-6.281 2.187-6.281 6.192 0 4.661 5.57 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-4.011-3.097-6.182-6.274-6.182-2.204 0-4.446 1.042-5.726 3.238-1.285-2.206-3.522-3.248-5.719-3.248z">
                                                    </path>
                                                    <path class="path-full" xmlns="http://www.w3.org/2000/svg"
                                                          d="M12 4.248c-3.148-5.402-12-3.825-12 2.944 0 4.661 5.571 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-6.792-8.875-8.306-12-2.944z">
                                                    </path>
                                                </svg></button>
                                        </div>
                                        <div class="tabs__slide-body"><img src="/images/img-1-db8e7456.jpg" alt="Катя">
                                            <div class="tabs__slide-stars">
                                                <div class="tabs__slide-title">Массажный салон</div>
                                                <div class="tabs__slide-rating">
                                                    <div class="rating-static rating-static--two">
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tabs__slide-bottom">
                                            <div class="tabs__slide-line">
                                                <div class="tabs__slide-title">Апартаменты</div>
                                                <div class="tabs__slide-price">от&nbsp<span>2 500 ₽</span> /час</div>
                                            </div>
                                            <div class="tabs__slide-line">
                                                <div class="tabs__slide-title">Выезд</div>
                                                <div class="tabs__slide-price">от&nbsp<span>4 500 ₽</span> /час</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tabs__slide swiper-slide">
                                        <div class="tabs__slide-head">
                                            <div class="tabs__slide-name">Настя</div>
                                            <button class="tabs__slide-favorites"><svg class="add-favorites js-favorites"
                                                                                       xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24">
                                                    <path class="path-empty"
                                                          d="M6.28 3c3.236.001 4.973 3.491 5.72 5.031.75-1.547 2.469-5.021 5.726-5.021 2.058 0 4.274 1.309 4.274 4.182 0 3.442-4.744 7.851-10 13-5.258-5.151-10-9.559-10-13 0-2.676 1.965-4.193 4.28-4.192zm.001-2c-3.183 0-6.281 2.187-6.281 6.192 0 4.661 5.57 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-4.011-3.097-6.182-6.274-6.182-2.204 0-4.446 1.042-5.726 3.238-1.285-2.206-3.522-3.248-5.719-3.248z">
                                                    </path>
                                                    <path class="path-full" xmlns="http://www.w3.org/2000/svg"
                                                          d="M12 4.248c-3.148-5.402-12-3.825-12 2.944 0 4.661 5.571 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-6.792-8.875-8.306-12-2.944z">
                                                    </path>
                                                </svg></button>
                                        </div>
                                        <div class="tabs__slide-body"><img src="/images/img-1-db8e7456.jpg" alt="Настя">
                                            <div class="tabs__slide-stars">
                                                <div class="tabs__slide-title">Массажный салон</div>
                                                <div class="tabs__slide-rating">
                                                    <div class="rating-static rating-static--three">
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tabs__slide-bottom">
                                            <div class="tabs__slide-line">
                                                <div class="tabs__slide-title">Апартаменты</div>
                                                <div class="tabs__slide-price">от&nbsp<span>2 500 ₽</span> /час</div>
                                            </div>
                                            <div class="tabs__slide-line">
                                                <div class="tabs__slide-title">Выезд</div>
                                                <div class="tabs__slide-price">от&nbsp<span>4 500 ₽</span> /час</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tabs__slide swiper-slide">
                                        <div class="tabs__slide-head">
                                            <div class="tabs__slide-name">Алена</div>
                                            <button class="tabs__slide-favorites"><svg class="add-favorites js-favorites"
                                                                                       xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24">
                                                    <path class="path-empty"
                                                          d="M6.28 3c3.236.001 4.973 3.491 5.72 5.031.75-1.547 2.469-5.021 5.726-5.021 2.058 0 4.274 1.309 4.274 4.182 0 3.442-4.744 7.851-10 13-5.258-5.151-10-9.559-10-13 0-2.676 1.965-4.193 4.28-4.192zm.001-2c-3.183 0-6.281 2.187-6.281 6.192 0 4.661 5.57 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-4.011-3.097-6.182-6.274-6.182-2.204 0-4.446 1.042-5.726 3.238-1.285-2.206-3.522-3.248-5.719-3.248z">
                                                    </path>
                                                    <path class="path-full" xmlns="http://www.w3.org/2000/svg"
                                                          d="M12 4.248c-3.148-5.402-12-3.825-12 2.944 0 4.661 5.571 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-6.792-8.875-8.306-12-2.944z">
                                                    </path>
                                                </svg></button>
                                        </div>
                                        <div class="tabs__slide-body"><img src="/images/img-1-db8e7456.jpg" alt="Алена">
                                            <div class="tabs__slide-stars">
                                                <div class="tabs__slide-title">Массажный салон</div>
                                                <div class="tabs__slide-rating">
                                                    <div class="rating-static rating-static--one">
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tabs__slide-bottom">
                                            <div class="tabs__slide-line">
                                                <div class="tabs__slide-title">Апартаменты</div>
                                                <div class="tabs__slide-price">от&nbsp<span>2 500 ₽</span> /час</div>
                                            </div>
                                            <div class="tabs__slide-line">
                                                <div class="tabs__slide-title">Выезд</div>
                                                <div class="tabs__slide-price">от&nbsp<span>4 500 ₽</span> /час</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tabs__slide swiper-slide">
                                        <div class="tabs__slide-head">
                                            <div class="tabs__slide-name">Оля</div>
                                            <button class="tabs__slide-favorites"><svg class="add-favorites js-favorites"
                                                                                       xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24">
                                                    <path class="path-empty"
                                                          d="M6.28 3c3.236.001 4.973 3.491 5.72 5.031.75-1.547 2.469-5.021 5.726-5.021 2.058 0 4.274 1.309 4.274 4.182 0 3.442-4.744 7.851-10 13-5.258-5.151-10-9.559-10-13 0-2.676 1.965-4.193 4.28-4.192zm.001-2c-3.183 0-6.281 2.187-6.281 6.192 0 4.661 5.57 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-4.011-3.097-6.182-6.274-6.182-2.204 0-4.446 1.042-5.726 3.238-1.285-2.206-3.522-3.248-5.719-3.248z">
                                                    </path>
                                                    <path class="path-full" xmlns="http://www.w3.org/2000/svg"
                                                          d="M12 4.248c-3.148-5.402-12-3.825-12 2.944 0 4.661 5.571 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-6.792-8.875-8.306-12-2.944z">
                                                    </path>
                                                </svg></button>
                                        </div>
                                        <div class="tabs__slide-body"><img src="/images/img-1-db8e7456.jpg" alt="Оля">
                                            <div class="tabs__slide-stars">
                                                <div class="tabs__slide-title">Массажный салон</div>
                                                <div class="tabs__slide-rating">
                                                    <div class="rating-static rating-static--five">
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tabs__slide-bottom">
                                            <div class="tabs__slide-line">
                                                <div class="tabs__slide-title">Апартаменты</div>
                                                <div class="tabs__slide-price">от&nbsp<span>2 500 ₽</span> /час</div>
                                            </div>
                                            <div class="tabs__slide-line">
                                                <div class="tabs__slide-title">Выезд</div>
                                                <div class="tabs__slide-price">от&nbsp<span>4 500 ₽</span> /час</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tabs__slide swiper-slide">
                                        <div class="tabs__slide-head">
                                            <div class="tabs__slide-name">Катя</div>
                                            <button class="tabs__slide-favorites"><svg class="add-favorites js-favorites"
                                                                                       xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24">
                                                    <path class="path-empty"
                                                          d="M6.28 3c3.236.001 4.973 3.491 5.72 5.031.75-1.547 2.469-5.021 5.726-5.021 2.058 0 4.274 1.309 4.274 4.182 0 3.442-4.744 7.851-10 13-5.258-5.151-10-9.559-10-13 0-2.676 1.965-4.193 4.28-4.192zm.001-2c-3.183 0-6.281 2.187-6.281 6.192 0 4.661 5.57 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-4.011-3.097-6.182-6.274-6.182-2.204 0-4.446 1.042-5.726 3.238-1.285-2.206-3.522-3.248-5.719-3.248z">
                                                    </path>
                                                    <path class="path-full" xmlns="http://www.w3.org/2000/svg"
                                                          d="M12 4.248c-3.148-5.402-12-3.825-12 2.944 0 4.661 5.571 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-6.792-8.875-8.306-12-2.944z">
                                                    </path>
                                                </svg></button>
                                        </div>
                                        <div class="tabs__slide-body"><img src="/images/img-1-db8e7456.jpg" alt="Катя">
                                            <div class="tabs__slide-stars">
                                                <div class="tabs__slide-title">Массажный салон</div>
                                                <div class="tabs__slide-rating">
                                                    <div class="rating-static rating-static--two">
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tabs__slide-bottom">
                                            <div class="tabs__slide-line">
                                                <div class="tabs__slide-title">Апартаменты</div>
                                                <div class="tabs__slide-price">от&nbsp<span>2 500 ₽</span> /час</div>
                                            </div>
                                            <div class="tabs__slide-line">
                                                <div class="tabs__slide-title">Выезд</div>
                                                <div class="tabs__slide-price">от&nbsp<span>4 500 ₽</span> /час</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tabs__slide swiper-slide">
                                        <div class="tabs__slide-head">
                                            <div class="tabs__slide-name">Настя</div>
                                            <button class="tabs__slide-favorites"><svg class="add-favorites js-favorites"
                                                                                       xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24">
                                                    <path class="path-empty"
                                                          d="M6.28 3c3.236.001 4.973 3.491 5.72 5.031.75-1.547 2.469-5.021 5.726-5.021 2.058 0 4.274 1.309 4.274 4.182 0 3.442-4.744 7.851-10 13-5.258-5.151-10-9.559-10-13 0-2.676 1.965-4.193 4.28-4.192zm.001-2c-3.183 0-6.281 2.187-6.281 6.192 0 4.661 5.57 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-4.011-3.097-6.182-6.274-6.182-2.204 0-4.446 1.042-5.726 3.238-1.285-2.206-3.522-3.248-5.719-3.248z">
                                                    </path>
                                                    <path class="path-full" xmlns="http://www.w3.org/2000/svg"
                                                          d="M12 4.248c-3.148-5.402-12-3.825-12 2.944 0 4.661 5.571 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-6.792-8.875-8.306-12-2.944z">
                                                    </path>
                                                </svg></button>
                                        </div>
                                        <div class="tabs__slide-body"><img src="/images/img-1-db8e7456.jpg" alt="Настя">
                                            <div class="tabs__slide-stars">
                                                <div class="tabs__slide-title">Массажный салон</div>
                                                <div class="tabs__slide-rating">
                                                    <div class="rating-static rating-static--three">
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tabs__slide-bottom">
                                            <div class="tabs__slide-line">
                                                <div class="tabs__slide-title">Апартаменты</div>
                                                <div class="tabs__slide-price">от&nbsp<span>2 500 ₽</span> /час</div>
                                            </div>
                                            <div class="tabs__slide-line">
                                                <div class="tabs__slide-title">Выезд</div>
                                                <div class="tabs__slide-price">от&nbsp<span>4 500 ₽</span> /час</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tabs__slide swiper-slide">
                                        <div class="tabs__slide-head">
                                            <div class="tabs__slide-name">Алена</div>
                                            <button class="tabs__slide-favorites"><svg class="add-favorites js-favorites"
                                                                                       xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24">
                                                    <path class="path-empty"
                                                          d="M6.28 3c3.236.001 4.973 3.491 5.72 5.031.75-1.547 2.469-5.021 5.726-5.021 2.058 0 4.274 1.309 4.274 4.182 0 3.442-4.744 7.851-10 13-5.258-5.151-10-9.559-10-13 0-2.676 1.965-4.193 4.28-4.192zm.001-2c-3.183 0-6.281 2.187-6.281 6.192 0 4.661 5.57 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-4.011-3.097-6.182-6.274-6.182-2.204 0-4.446 1.042-5.726 3.238-1.285-2.206-3.522-3.248-5.719-3.248z">
                                                    </path>
                                                    <path class="path-full" xmlns="http://www.w3.org/2000/svg"
                                                          d="M12 4.248c-3.148-5.402-12-3.825-12 2.944 0 4.661 5.571 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-6.792-8.875-8.306-12-2.944z">
                                                    </path>
                                                </svg></button>
                                        </div>
                                        <div class="tabs__slide-body"><img src="/images/img-1-db8e7456.jpg" alt="Алена">
                                            <div class="tabs__slide-stars">
                                                <div class="tabs__slide-title">Массажный салон</div>
                                                <div class="tabs__slide-rating">
                                                    <div class="rating-static rating-static--one">
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tabs__slide-bottom">
                                            <div class="tabs__slide-line">
                                                <div class="tabs__slide-title">Апартаменты</div>
                                                <div class="tabs__slide-price">от&nbsp<span>2 500 ₽</span> /час</div>
                                            </div>
                                            <div class="tabs__slide-line">
                                                <div class="tabs__slide-title">Выезд</div>
                                                <div class="tabs__slide-price">от&nbsp<span>4 500 ₽</span> /час</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tabs__slider-pagination slider-pagination swiper-pagination">
                                    <!-- Add Arrows-->
                                </div>
                                <div class="tabs__slider-next slider-next swiper-button-next"></div>
                                <div class="tabs__slider-prev slider-prev swiper-button-prev"></div>
                            </div>
                        </div>
                        <div class="tabs__content tabs__content--hidden" data-type="Favorites">
                            <div class="tabs__slider swiper-container">
                                <div class="tabs__wrapper swiper-wrapper">
                                    <div class="tabs__slide swiper-slide">
                                        <div class="tabs__slide-head">
                                            <div class="tabs__slide-name">Оля</div>
                                            <button class="tabs__slide-favorites"><svg class="add-favorites js-favorites"
                                                                                       xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24">
                                                    <path class="path-empty"
                                                          d="M6.28 3c3.236.001 4.973 3.491 5.72 5.031.75-1.547 2.469-5.021 5.726-5.021 2.058 0 4.274 1.309 4.274 4.182 0 3.442-4.744 7.851-10 13-5.258-5.151-10-9.559-10-13 0-2.676 1.965-4.193 4.28-4.192zm.001-2c-3.183 0-6.281 2.187-6.281 6.192 0 4.661 5.57 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-4.011-3.097-6.182-6.274-6.182-2.204 0-4.446 1.042-5.726 3.238-1.285-2.206-3.522-3.248-5.719-3.248z">
                                                    </path>
                                                    <path class="path-full" xmlns="http://www.w3.org/2000/svg"
                                                          d="M12 4.248c-3.148-5.402-12-3.825-12 2.944 0 4.661 5.571 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-6.792-8.875-8.306-12-2.944z">
                                                    </path>
                                                </svg></button>
                                        </div>
                                        <div class="tabs__slide-body"><img src="/images/img-1-db8e7456.jpg" alt="Оля">
                                            <div class="tabs__slide-stars">
                                                <div class="tabs__slide-title">Массажный салон</div>
                                                <div class="tabs__slide-rating">
                                                    <div class="rating-static rating-static--five">
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tabs__slide-bottom">
                                            <div class="tabs__slide-line">
                                                <div class="tabs__slide-title">Апартаменты</div>
                                                <div class="tabs__slide-price">от&nbsp<span>2 500 ₽</span> /час</div>
                                            </div>
                                            <div class="tabs__slide-line">
                                                <div class="tabs__slide-title">Выезд</div>
                                                <div class="tabs__slide-price">от&nbsp<span>4 500 ₽</span> /час</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tabs__slide swiper-slide">
                                        <div class="tabs__slide-head">
                                            <div class="tabs__slide-name">Катя</div>
                                            <button class="tabs__slide-favorites"><svg class="add-favorites js-favorites"
                                                                                       xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24">
                                                    <path class="path-empty"
                                                          d="M6.28 3c3.236.001 4.973 3.491 5.72 5.031.75-1.547 2.469-5.021 5.726-5.021 2.058 0 4.274 1.309 4.274 4.182 0 3.442-4.744 7.851-10 13-5.258-5.151-10-9.559-10-13 0-2.676 1.965-4.193 4.28-4.192zm.001-2c-3.183 0-6.281 2.187-6.281 6.192 0 4.661 5.57 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-4.011-3.097-6.182-6.274-6.182-2.204 0-4.446 1.042-5.726 3.238-1.285-2.206-3.522-3.248-5.719-3.248z">
                                                    </path>
                                                    <path class="path-full" xmlns="http://www.w3.org/2000/svg"
                                                          d="M12 4.248c-3.148-5.402-12-3.825-12 2.944 0 4.661 5.571 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-6.792-8.875-8.306-12-2.944z">
                                                    </path>
                                                </svg></button>
                                        </div>
                                        <div class="tabs__slide-body"><img src="/images/img-1-db8e7456.jpg" alt="Катя">
                                            <div class="tabs__slide-stars">
                                                <div class="tabs__slide-title">Массажный салон</div>
                                                <div class="tabs__slide-rating">
                                                    <div class="rating-static rating-static--two">
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tabs__slide-bottom">
                                            <div class="tabs__slide-line">
                                                <div class="tabs__slide-title">Апартаменты</div>
                                                <div class="tabs__slide-price">от&nbsp<span>2 500 ₽</span> /час</div>
                                            </div>
                                            <div class="tabs__slide-line">
                                                <div class="tabs__slide-title">Выезд</div>
                                                <div class="tabs__slide-price">от&nbsp<span>4 500 ₽</span> /час</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tabs__slide swiper-slide">
                                        <div class="tabs__slide-head">
                                            <div class="tabs__slide-name">Настя</div>
                                            <button class="tabs__slide-favorites"><svg class="add-favorites js-favorites"
                                                                                       xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24">
                                                    <path class="path-empty"
                                                          d="M6.28 3c3.236.001 4.973 3.491 5.72 5.031.75-1.547 2.469-5.021 5.726-5.021 2.058 0 4.274 1.309 4.274 4.182 0 3.442-4.744 7.851-10 13-5.258-5.151-10-9.559-10-13 0-2.676 1.965-4.193 4.28-4.192zm.001-2c-3.183 0-6.281 2.187-6.281 6.192 0 4.661 5.57 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-4.011-3.097-6.182-6.274-6.182-2.204 0-4.446 1.042-5.726 3.238-1.285-2.206-3.522-3.248-5.719-3.248z">
                                                    </path>
                                                    <path class="path-full" xmlns="http://www.w3.org/2000/svg"
                                                          d="M12 4.248c-3.148-5.402-12-3.825-12 2.944 0 4.661 5.571 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-6.792-8.875-8.306-12-2.944z">
                                                    </path>
                                                </svg></button>
                                        </div>
                                        <div class="tabs__slide-body"><img src="/images/img-1-db8e7456.jpg" alt="Настя">
                                            <div class="tabs__slide-stars">
                                                <div class="tabs__slide-title">Массажный салон</div>
                                                <div class="tabs__slide-rating">
                                                    <div class="rating-static rating-static--three">
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tabs__slide-bottom">
                                            <div class="tabs__slide-line">
                                                <div class="tabs__slide-title">Апартаменты</div>
                                                <div class="tabs__slide-price">от&nbsp<span>2 500 ₽</span> /час</div>
                                            </div>
                                            <div class="tabs__slide-line">
                                                <div class="tabs__slide-title">Выезд</div>
                                                <div class="tabs__slide-price">от&nbsp<span>4 500 ₽</span> /час</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tabs__slide swiper-slide">
                                        <div class="tabs__slide-head">
                                            <div class="tabs__slide-name">Алена</div>
                                            <button class="tabs__slide-favorites"><svg class="add-favorites js-favorites"
                                                                                       xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24">
                                                    <path class="path-empty"
                                                          d="M6.28 3c3.236.001 4.973 3.491 5.72 5.031.75-1.547 2.469-5.021 5.726-5.021 2.058 0 4.274 1.309 4.274 4.182 0 3.442-4.744 7.851-10 13-5.258-5.151-10-9.559-10-13 0-2.676 1.965-4.193 4.28-4.192zm.001-2c-3.183 0-6.281 2.187-6.281 6.192 0 4.661 5.57 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-4.011-3.097-6.182-6.274-6.182-2.204 0-4.446 1.042-5.726 3.238-1.285-2.206-3.522-3.248-5.719-3.248z">
                                                    </path>
                                                    <path class="path-full" xmlns="http://www.w3.org/2000/svg"
                                                          d="M12 4.248c-3.148-5.402-12-3.825-12 2.944 0 4.661 5.571 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-6.792-8.875-8.306-12-2.944z">
                                                    </path>
                                                </svg></button>
                                        </div>
                                        <div class="tabs__slide-body"><img src="/images/img-1-db8e7456.jpg" alt="Алена">
                                            <div class="tabs__slide-stars">
                                                <div class="tabs__slide-title">Массажный салон</div>
                                                <div class="tabs__slide-rating">
                                                    <div class="rating-static rating-static--one">
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tabs__slide-bottom">
                                            <div class="tabs__slide-line">
                                                <div class="tabs__slide-title">Апартаменты</div>
                                                <div class="tabs__slide-price">от&nbsp<span>2 500 ₽</span> /час</div>
                                            </div>
                                            <div class="tabs__slide-line">
                                                <div class="tabs__slide-title">Выезд</div>
                                                <div class="tabs__slide-price">от&nbsp<span>4 500 ₽</span> /час</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tabs__slide swiper-slide">
                                        <div class="tabs__slide-head">
                                            <div class="tabs__slide-name">Оля</div>
                                            <button class="tabs__slide-favorites"><svg class="add-favorites js-favorites"
                                                                                       xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24">
                                                    <path class="path-empty"
                                                          d="M6.28 3c3.236.001 4.973 3.491 5.72 5.031.75-1.547 2.469-5.021 5.726-5.021 2.058 0 4.274 1.309 4.274 4.182 0 3.442-4.744 7.851-10 13-5.258-5.151-10-9.559-10-13 0-2.676 1.965-4.193 4.28-4.192zm.001-2c-3.183 0-6.281 2.187-6.281 6.192 0 4.661 5.57 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-4.011-3.097-6.182-6.274-6.182-2.204 0-4.446 1.042-5.726 3.238-1.285-2.206-3.522-3.248-5.719-3.248z">
                                                    </path>
                                                    <path class="path-full" xmlns="http://www.w3.org/2000/svg"
                                                          d="M12 4.248c-3.148-5.402-12-3.825-12 2.944 0 4.661 5.571 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-6.792-8.875-8.306-12-2.944z">
                                                    </path>
                                                </svg></button>
                                        </div>
                                        <div class="tabs__slide-body"><img src="/images/img-1-db8e7456.jpg" alt="Оля">
                                            <div class="tabs__slide-stars">
                                                <div class="tabs__slide-title">Массажный салон</div>
                                                <div class="tabs__slide-rating">
                                                    <div class="rating-static rating-static--five">
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tabs__slide-bottom">
                                            <div class="tabs__slide-line">
                                                <div class="tabs__slide-title">Апартаменты</div>
                                                <div class="tabs__slide-price">от&nbsp<span>2 500 ₽</span> /час</div>
                                            </div>
                                            <div class="tabs__slide-line">
                                                <div class="tabs__slide-title">Выезд</div>
                                                <div class="tabs__slide-price">от&nbsp<span>4 500 ₽</span> /час</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tabs__slide swiper-slide">
                                        <div class="tabs__slide-head">
                                            <div class="tabs__slide-name">Катя</div>
                                            <button class="tabs__slide-favorites"><svg class="add-favorites js-favorites"
                                                                                       xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24">
                                                    <path class="path-empty"
                                                          d="M6.28 3c3.236.001 4.973 3.491 5.72 5.031.75-1.547 2.469-5.021 5.726-5.021 2.058 0 4.274 1.309 4.274 4.182 0 3.442-4.744 7.851-10 13-5.258-5.151-10-9.559-10-13 0-2.676 1.965-4.193 4.28-4.192zm.001-2c-3.183 0-6.281 2.187-6.281 6.192 0 4.661 5.57 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-4.011-3.097-6.182-6.274-6.182-2.204 0-4.446 1.042-5.726 3.238-1.285-2.206-3.522-3.248-5.719-3.248z">
                                                    </path>
                                                    <path class="path-full" xmlns="http://www.w3.org/2000/svg"
                                                          d="M12 4.248c-3.148-5.402-12-3.825-12 2.944 0 4.661 5.571 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-6.792-8.875-8.306-12-2.944z">
                                                    </path>
                                                </svg></button>
                                        </div>
                                        <div class="tabs__slide-body"><img src="/images/img-1-db8e7456.jpg" alt="Катя">
                                            <div class="tabs__slide-stars">
                                                <div class="tabs__slide-title">Массажный салон</div>
                                                <div class="tabs__slide-rating">
                                                    <div class="rating-static rating-static--two">
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tabs__slide-bottom">
                                            <div class="tabs__slide-line">
                                                <div class="tabs__slide-title">Апартаменты</div>
                                                <div class="tabs__slide-price">от&nbsp<span>2 500 ₽</span> /час</div>
                                            </div>
                                            <div class="tabs__slide-line">
                                                <div class="tabs__slide-title">Выезд</div>
                                                <div class="tabs__slide-price">от&nbsp<span>4 500 ₽</span> /час</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tabs__slide swiper-slide">
                                        <div class="tabs__slide-head">
                                            <div class="tabs__slide-name">Настя</div>
                                            <button class="tabs__slide-favorites"><svg class="add-favorites js-favorites"
                                                                                       xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24">
                                                    <path class="path-empty"
                                                          d="M6.28 3c3.236.001 4.973 3.491 5.72 5.031.75-1.547 2.469-5.021 5.726-5.021 2.058 0 4.274 1.309 4.274 4.182 0 3.442-4.744 7.851-10 13-5.258-5.151-10-9.559-10-13 0-2.676 1.965-4.193 4.28-4.192zm.001-2c-3.183 0-6.281 2.187-6.281 6.192 0 4.661 5.57 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-4.011-3.097-6.182-6.274-6.182-2.204 0-4.446 1.042-5.726 3.238-1.285-2.206-3.522-3.248-5.719-3.248z">
                                                    </path>
                                                    <path class="path-full" xmlns="http://www.w3.org/2000/svg"
                                                          d="M12 4.248c-3.148-5.402-12-3.825-12 2.944 0 4.661 5.571 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-6.792-8.875-8.306-12-2.944z">
                                                    </path>
                                                </svg></button>
                                        </div>
                                        <div class="tabs__slide-body"><img src="/images/img-1-db8e7456.jpg" alt="Настя">
                                            <div class="tabs__slide-stars">
                                                <div class="tabs__slide-title">Массажный салон</div>
                                                <div class="tabs__slide-rating">
                                                    <div class="rating-static rating-static--three">
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tabs__slide-bottom">
                                            <div class="tabs__slide-line">
                                                <div class="tabs__slide-title">Апартаменты</div>
                                                <div class="tabs__slide-price">от&nbsp<span>2 500 ₽</span> /час</div>
                                            </div>
                                            <div class="tabs__slide-line">
                                                <div class="tabs__slide-title">Выезд</div>
                                                <div class="tabs__slide-price">от&nbsp<span>4 500 ₽</span> /час</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tabs__slide swiper-slide">
                                        <div class="tabs__slide-head">
                                            <div class="tabs__slide-name">Алена</div>
                                            <button class="tabs__slide-favorites"><svg class="add-favorites js-favorites"
                                                                                       xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24">
                                                    <path class="path-empty"
                                                          d="M6.28 3c3.236.001 4.973 3.491 5.72 5.031.75-1.547 2.469-5.021 5.726-5.021 2.058 0 4.274 1.309 4.274 4.182 0 3.442-4.744 7.851-10 13-5.258-5.151-10-9.559-10-13 0-2.676 1.965-4.193 4.28-4.192zm.001-2c-3.183 0-6.281 2.187-6.281 6.192 0 4.661 5.57 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-4.011-3.097-6.182-6.274-6.182-2.204 0-4.446 1.042-5.726 3.238-1.285-2.206-3.522-3.248-5.719-3.248z">
                                                    </path>
                                                    <path class="path-full" xmlns="http://www.w3.org/2000/svg"
                                                          d="M12 4.248c-3.148-5.402-12-3.825-12 2.944 0 4.661 5.571 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-6.792-8.875-8.306-12-2.944z">
                                                    </path>
                                                </svg></button>
                                        </div>
                                        <div class="tabs__slide-body"><img src="/images/img-1-db8e7456.jpg" alt="Алена">
                                            <div class="tabs__slide-stars">
                                                <div class="tabs__slide-title">Массажный салон</div>
                                                <div class="tabs__slide-rating">
                                                    <div class="rating-static rating-static--one">
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                        <div class="rating-static__star"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tabs__slide-bottom">
                                            <div class="tabs__slide-line">
                                                <div class="tabs__slide-title">Апартаменты</div>
                                                <div class="tabs__slide-price">от&nbsp<span>2 500 ₽</span> /час</div>
                                            </div>
                                            <div class="tabs__slide-line">
                                                <div class="tabs__slide-title">Выезд</div>
                                                <div class="tabs__slide-price">от&nbsp<span>4 500 ₽</span> /час</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tabs__slider-pagination slider-pagination swiper-pagination">
                                    <!-- Add Arrows-->
                                </div>
                                <div class="tabs__slider-next slider-next swiper-button-next"></div>
                                <div class="tabs__slider-prev slider-prev swiper-button-prev"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="questionnaire__bars">
            <div class="questionnaire__bar questionnaire__bar--top">
                <div class="questionnaire__bar-back"></div>
                <div class="questionnaire__bar-name"> Анжелика<svg class="add-favorites js-favorites"
                                                                   xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24">
                        <path class="path-empty"
                              d="M6.28 3c3.236.001 4.973 3.491 5.72 5.031.75-1.547 2.469-5.021 5.726-5.021 2.058 0 4.274 1.309 4.274 4.182 0 3.442-4.744 7.851-10 13-5.258-5.151-10-9.559-10-13 0-2.676 1.965-4.193 4.28-4.192zm.001-2c-3.183 0-6.281 2.187-6.281 6.192 0 4.661 5.57 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-4.011-3.097-6.182-6.274-6.182-2.204 0-4.446 1.042-5.726 3.238-1.285-2.206-3.522-3.248-5.719-3.248z">
                        </path>
                        <path class="path-full" xmlns="http://www.w3.org/2000/svg"
                              d="M12 4.248c-3.148-5.402-12-3.825-12 2.944 0 4.661 5.571 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-6.792-8.875-8.306-12-2.944z">
                        </path>
                    </svg></div>
            </div>
            <div class="questionnaire__bar questionnaire__bar--bottom">
                <button class="questionnaire__bar-btn button button--transparent">Сообщение</button><a
                    class="questionnaire__bar-btn button" href="tel:+700000000">Позвонить</a>
            </div>
        </div>
    </div>
    @include('includes._bottom-banner')
@endsection
