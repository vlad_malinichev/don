@extends('layouts.main')

@section('content')
    <div class="page">
        <div class="page__wrapper">
            <div class="container page__container">
                <div class="page__body">
                    <div class="page__content">
<!--                        <div class="page__tabs js-tabs">
                            <div class="page__tab page__tab&#45;&#45;active">Девушки</div>
                            <div class="page__tab">Парни</div>
                        </div>-->
                        <div class="page__filter">
                            <form class="filter js-filter" action="{{ route('search.search')  }}">
                                <div class="filter__header">
                                    <div class="filter__title">
                                        <span class="filter__title-expand">Развернуть фильтр поиска</span>
                                        <span class="filter__title-collapse">Свернуть фильтр поиска</span>
                                    </div>
                                    <div class="filter__sort">
                                        <div class="filter__choice filter__choice--price">
                                            <select class="filter__choice-control js-choice-price" name="currency"></select>
                                        </div>
                                        <div class="filter__choice filter__choice--sort">
                                            <select class="filter__choice-control js-choice-sort" name="select-sort"></select>
                                        </div>
                                    </div>
                                </div>
                                <div class="filter__body">
                                    <div class="filter__rows">
                                        <div class="filter__row">
                                            <div class="filter__col">
                                                <div class="filter__head">
                                                    Апартаменты&nbsp;
                                                    <span class="filter__label">стоимость</span>
                                                </div>
                                            </div>
                                            <div class="filter__col">
                                                <div class="filter__head">
                                                    <span class="filter__label">1 час</span>
                                                </div>
                                                <div class="range-slider filter__range">
                                                    <div class="range-slider__input">
                                                        <input class="js-range-slider" name="prices[hour_in_apartments]" type="text" value="" data-min="1" data-max="20000" data-from="1" data-to="20000"/>
                                                    </div>
                                                    <div class="range-slider__controls">
                                                        <div class="range-slider__control">
                                                            <div class="range-slider__from">от</div>
                                                            <div class="range-slider__input">
                                                                <input class="js-range-from" type="text" value="0"/>
                                                            </div>
                                                        </div>
                                                        <div class="range-slider__control">
                                                            <div class="range-slider__to">до</div>
                                                            <div class="range-slider__input">
                                                                <input class="js-range-to" type="text" value="0"/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="filter__col">
                                                <div class="filter__head"><span class="filter__label">2 часа</span></div>
                                                <div class="range-slider filter__range">
                                                    <div class="range-slider__input">
                                                        <input class="js-range-slider" type="text" name="prices[two_hours_in_apartments]" value="" data-min="4000" data-max="36000" data-from="4000" data-to="36000"/>
                                                    </div>
                                                    <div class="range-slider__controls">
                                                        <div class="range-slider__control">
                                                            <div class="range-slider__from">от</div>
                                                            <div class="range-slider__input">
                                                                <input class="js-range-from" type="text" value="0"/>
                                                            </div>
                                                        </div>
                                                        <div class="range-slider__control">
                                                            <div class="range-slider__to">до</div>
                                                            <div class="range-slider__input">
                                                                <input class="js-range-to" type="text" value="0"/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="filter__col">
                                                <div class="filter__head"><span class="filter__label">Ночь</span></div>
                                                <div class="range-slider filter__range">
                                                    <div class="range-slider__input">
                                                        <input class="js-range-slider" type="text" name="prices[night_in_apartments]" value="" data-min="1" data-max="60000" data-from="1" data-to="60000"/>
                                                    </div>
                                                    <div class="range-slider__controls">
                                                        <div class="range-slider__control">
                                                            <div class="range-slider__from">от</div>
                                                            <div class="range-slider__input">
                                                                <input class="js-range-from" type="text" value="0"/>
                                                            </div>
                                                        </div>
                                                        <div class="range-slider__control">
                                                            <div class="range-slider__to">до</div>
                                                            <div class="range-slider__input">
                                                                <input class="js-range-to" type="text" value="0"/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="filter__row">
                                            <div class="filter__col">
                                                <div class="filter__head">Выезд&nbsp;<span class="filter__label">стоимость</span></div>
                                            </div>
                                            <div class="filter__col">
                                                <div class="filter__head"><span class="filter__label">1 час</span></div>
                                                <div class="range-slider filter__range">
                                                    <div class="range-slider__input">
                                                        <input class="js-range-slider" type="text" name="prices[hour_in_departures]" value="" data-min="2000" data-max="20000" data-from="2000" data-to="19000"/>
                                                    </div>
                                                    <div class="range-slider__controls">
                                                        <div class="range-slider__control">
                                                            <div class="range-slider__from">от</div>
                                                            <div class="range-slider__input">
                                                                <input class="js-range-from" type="text" value="0"/>
                                                            </div>
                                                        </div>
                                                        <div class="range-slider__control">
                                                            <div class="range-slider__to">до</div>
                                                            <div class="range-slider__input">
                                                                <input class="js-range-to" type="text" value="0"/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="filter__col">
                                                <div class="filter__head"><span class="filter__label">2 часа</span></div>
                                                <div class="range-slider filter__range">
                                                    <div class="range-slider__input">
                                                        <input class="js-range-slider" type="text" name="prices[two_hours_in_departures]" value="" data-min="4000" data-max="36000" data-from="4000" data-to="30000"/>
                                                    </div>
                                                    <div class="range-slider__controls">
                                                        <div class="range-slider__control">
                                                            <div class="range-slider__from">от</div>
                                                            <div class="range-slider__input">
                                                                <input class="js-range-from" type="text" value="0"/>
                                                            </div>
                                                        </div>
                                                        <div class="range-slider__control">
                                                            <div class="range-slider__to">до</div>
                                                            <div class="range-slider__input">
                                                                <input class="js-range-to" type="text" value="0"/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="filter__col">
                                                <div class="filter__head"><span class="filter__label">Ночь</span></div>
                                                <div class="range-slider filter__range">
                                                    <div class="range-slider__input">
                                                        <input class="js-range-slider" type="text" name="prices[night_in_departures]" value="" data-min="1" data-max="60000" data-from="1" data-to="60000"/>
                                                    </div>
                                                    <div class="range-slider__controls">
                                                        <div class="range-slider__control">
                                                            <div class="range-slider__from">от</div>
                                                            <div class="range-slider__input">
                                                                <input class="js-range-from" type="text" value="0"/>
                                                            </div>
                                                        </div>
                                                        <div class="range-slider__control">
                                                            <div class="range-slider__to">до</div>
                                                            <div class="range-slider__input">
                                                                <input class="js-range-to" type="text" value="0"/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="filter__row">
                                            <div class="filter__col">
                                                <div class="filter__head">Возраст&nbsp;<span class="filter__label">лет</span></div>
                                                <div class="range-slider filter__range">
                                                    <div class="range-slider__input">
                                                        <input class="js-range-slider" type="text" name="filters[age]" value="" data-min="18" data-max="100" data-from="18" data-to="30"/>
                                                    </div>
                                                    <div class="range-slider__controls">
                                                        <div class="range-slider__control">
                                                            <div class="range-slider__from">от</div>
                                                            <div class="range-slider__input">
                                                                <input class="js-range-from" type="text" value="0"/>
                                                            </div>
                                                        </div>
                                                        <div class="range-slider__control">
                                                            <div class="range-slider__to">до</div>
                                                            <div class="range-slider__input">
                                                                <input class="js-range-to" type="text" value="0"/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="filter__col">
                                                <div class="filter__head">
                                                    Грудь&nbsp;<span class="filter__label">размер</span></div>
                                                <div class="range-slider filter__range">
                                                    <div class="range-slider__input">
                                                        <input class="js-range-slider" type="text" name="filters[breast_size]" value="" data-min="0" data-max="10" data-from="0" data-to="4"/>
                                                    </div>
                                                    <div class="range-slider__controls">
                                                        <div class="range-slider__control">
                                                            <div class="range-slider__from">от</div>
                                                            <div class="range-slider__input">
                                                                <input class="js-range-from" type="text" value="0"/>
                                                            </div>
                                                        </div>
                                                        <div class="range-slider__control">
                                                            <div class="range-slider__to">до</div>
                                                            <div class="range-slider__input">
                                                                <input class="js-range-to" type="text" value="0"/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="filter__col">
                                                <div class="filter__head">Рост&nbsp;<span class="filter__label">см</span></div>
                                                <div class="range-slider filter__range">
                                                    <div class="range-slider__input">
                                                        <input class="js-range-slider" type="text" name="filters[height]" value="" data-min="140" data-max="400" data-from="140" data-to="210"/>
                                                    </div>
                                                    <div class="range-slider__controls">
                                                        <div class="range-slider__control">
                                                            <div class="range-slider__from">от</div>
                                                            <div class="range-slider__input">
                                                                <input class="js-range-from" type="text" value="0"/>
                                                            </div>
                                                        </div>
                                                        <div class="range-slider__control">
                                                            <div class="range-slider__to">до</div>
                                                            <div class="range-slider__input">
                                                                <input class="js-range-to" type="text" value="0"/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="filter__col">
                                                <div class="filter__head">Вес&nbsp;<span class="filter__label">кг</span></div>
                                                <div class="range-slider filter__range">
                                                    <div class="range-slider__input">
                                                        <input class="js-range-slider" type="text" name="filters[weight]" value="" data-min="35" data-max="110" data-from="35" data-to="110"/>
                                                    </div>
                                                    <div class="range-slider__controls">
                                                        <div class="range-slider__control">
                                                            <div class="range-slider__from">от</div>
                                                            <div class="range-slider__input">
                                                                <input class="js-range-from" type="text" value="0"/>
                                                            </div>
                                                        </div>
                                                        <div class="range-slider__control">
                                                            <div class="range-slider__to">до</div>
                                                            <div class="range-slider__input">
                                                                <input class="js-range-to" type="text" value="0"/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="filter__row">
                                            <div class="filter__col">
                                                <div class="filter__head">Пол</div>
                                                <div class="filter__select">
                                                    <select class="js-select-filter" name="filters[sex]" data-placeholder="Ничего не выбрано">
                                                        <option value=""></option>
                                                        <option value="1">Мужской</option>
                                                        <option value="2">Женский</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="filter__col">
                                                <div class="filter__head">Ориентация</div>
                                                <div class="filter__select">
                                                    <select class="js-select-filter" name="filters[orientationId]" data-placeholder="Ничего не выбрано">
                                                        <option value=""></option>
                                                        <option value="1">Мужской</option>
                                                        <option value="2">Женский</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="filter__col">
                                                <div class="filter__head">Национальность</div>
                                                <div class="filter__select">
                                                    <select class="js-select-filter" name="filters[nationalityId]" data-placeholder="Ничего не выбрано">
                                                        <option value=""></option>
                                                        <option value="1">Мужской</option>
                                                        <option value="2">Женский</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="filter__col">
                                                <div class="filter__head">На теле</div>
                                                <div class="filter__checkboxs">
                                                    <label class="filter__checkbox checkbox">
                                                        <input class="checkbox__input" type="checkbox" name="filters[isScars]"><span class="checkbox__label">Шрамы</span>
                                                    </label>
                                                    <label class="filter__checkbox checkbox">
                                                        <input class="checkbox__input" type="checkbox" name="filters[isPiercing]"><span class="checkbox__label">Пирсинг</span>
                                                    </label>
                                                    <label class="filter__checkbox checkbox">
                                                        <input class="checkbox__input" type="checkbox" name="filters[isTattoos]"><span class="checkbox__label">Татуировки</span>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="filter__col">
                                                <div class="filter__head">Интим стрижка</div>
                                                <div class="filter__select">
                                                    <select class="js-select-filter" name="filters[intimateHaircutId]" data-placeholder="Ничего не выбрано">
                                                        <option value=""></option>
                                                        <option value="1">Бабочка</option>
                                                        <option value="2">Стрелочка</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="filter__col">
                                                <div class="filter__head">Цвет волос</div>
                                                <div class="filter__select">
                                                    <select class="js-select-filter" name="filters[hairColorId]" data-placeholder="Ничего не выбрано">
                                                        <option value=""></option>
                                                        <option value="1">Брюнетка</option>
                                                        <option value="2">Блондинка</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="filter__col">
                                                <div class="filter__head">Язык общения</div>
                                                <div class="filter__select">
                                                    <select class="js-select-filter" data-placeholder="Ничего не выбрано">
                                                        <option value=""></option>
                                                        <option value="Мужской">Мужской</option>
                                                        <option value="Женский">Женский</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="filter__col">
                                                <div class="filter__head">Материалы</div>
                                                <div class="filter__checkboxs">
                                                    <label class="filter__checkbox checkbox">
                                                        <input class="checkbox__input" type="checkbox" name="extra[with_photos]">
                                                        <span class="checkbox__label">С фото</span>
                                                    </label>
                                                    <label class="filter__checkbox checkbox">
                                                        <input class="checkbox__input" type="checkbox" name="extra[with_videos]">
                                                        <span class="checkbox__label">С видео</span>
                                                    </label>
                                                    <label class="filter__checkbox checkbox">
                                                        <input class="checkbox__input" type="checkbox" name="extra[with_comments]">
                                                        <span class="checkbox__label">С комментариями</span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="filter__options">
                                        <div class="filter__row">
                                            <div class="filter__col">
                                                <div class="filter__head">Основное</div>
                                                <div class="filter__checkboxs">
                                                    <label class="filter__checkbox checkbox">
                                                        <input class="checkbox__input" type="checkbox" name="classical"><span class="checkbox__label">Классический</span>
                                                    </label>
                                                    <label class="filter__checkbox checkbox">
                                                        <input class="checkbox__input" type="checkbox" name="anal"><span class="checkbox__label">Анальный</span>
                                                    </label>
                                                    <label class="filter__checkbox checkbox">
                                                        <input class="checkbox__input" type="checkbox" name="group"><span class="checkbox__label">Групповой</span>
                                                    </label>
                                                    <label class="filter__checkbox checkbox">
                                                        <input class="checkbox__input" type="checkbox" name="lesbian"><span class="checkbox__label">Лесбийский</span>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="filter__col">
                                                <div class="filter__head">Место</div>
                                                <div class="filter__checkboxs">
                                                    <label class="filter__checkbox checkbox">
                                                        <input class="checkbox__input" type="checkbox" name="flat"><span class="checkbox__label">Квартира</span>
                                                    </label>
                                                    <label class="filter__checkbox checkbox">
                                                        <input class="checkbox__input" type="checkbox" name="bath"><span class="checkbox__label">Баня/сауна</span>
                                                    </label>
                                                    <label class="filter__checkbox checkbox">
                                                        <input class="checkbox__input" type="checkbox" name="hotel"><span class="checkbox__label">Гостиница</span>
                                                    </label>
                                                    <label class="filter__checkbox checkbox">
                                                        <input class="checkbox__input" type="checkbox" name="office"><span class="checkbox__label">Офис</span>
                                                    </label>
                                                    <label class="filter__checkbox checkbox">
                                                        <input class="checkbox__input" type="checkbox" name="apartments"><span class="checkbox__label">Апартаменты</span>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="filter__col">
                                                <div class="filter__head">Ласка</div>
                                                <div class="filter__checkboxs">
                                                    <label class="filter__checkbox checkbox">
                                                        <input class="checkbox__input" type="checkbox" name="with-elastic"><span class="checkbox__label">С резинкой</span>
                                                    </label>
                                                    <label class="filter__checkbox checkbox">
                                                        <input class="checkbox__input" type="checkbox" name="without-elastic"><span class="checkbox__label">Без резинки</span>
                                                    </label>
                                                    <label class="filter__checkbox checkbox">
                                                        <input class="checkbox__input" type="checkbox" name="deep"><span class="checkbox__label">Глубокий</span>
                                                    </label>
                                                    <label class="filter__checkbox checkbox">
                                                        <input class="checkbox__input" type="checkbox" name="in-car"><span class="checkbox__label">В машине</span>
                                                    </label>
                                                    <label class="filter__checkbox checkbox">
                                                        <input class="checkbox__input" type="checkbox" name="cunnilingus"><span class="checkbox__label">Кунилингус</span>
                                                    </label>
                                                    <label class="filter__checkbox checkbox">
                                                        <input class="checkbox__input" type="checkbox" name="anilingus"><span class="checkbox__label">Анилингус</span>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="filter__col">
                                                <div class="filter__head">Финиш</div>
                                                <div class="filter__checkboxs">
                                                    <label class="filter__checkbox checkbox">
                                                        <input class="checkbox__input" type="checkbox" name="in-mouth"><span class="checkbox__label">В рот</span>
                                                    </label>
                                                    <label class="filter__checkbox checkbox">
                                                        <input class="checkbox__input" type="checkbox" name="on-face"><span class="checkbox__label">На лицо</span>
                                                    </label>
                                                    <label class="filter__checkbox checkbox">
                                                        <input class="checkbox__input" type="checkbox" name="on-chest"><span class="checkbox__label">На грудь</span>
                                                    </label>
                                                </div>
                                            </div>
<!--                                            <div class="filter__col filter__col&#45;&#45;extra">
                                                <label class="filter__checkbox checkbox">
                                                    <input class="checkbox__input" type="checkbox" name="with-video"><span class="checkbox__label">С видео</span>
                                                </label>
                                                <label class="filter__checkbox checkbox">
                                                    <input class="checkbox__input" type="checkbox" name="verified"><span class="checkbox__label">Проверенные пользователями (с комментариями)</span>
                                                </label>
                                            </div>-->
                                        </div>
                                        <div class="filter__row">
                                            <div class="filter__col">
                                                <div class="filter__head">Массаж</div>
                                                <div class="filter__checkboxs">
                                                    <label class="filter__checkbox checkbox">
                                                        <input class="checkbox__input" type="checkbox" name="classic"><span class="checkbox__label">Классический</span>
                                                    </label>
                                                    <label class="filter__checkbox checkbox">
                                                        <input class="checkbox__input" type="checkbox" name="professional"><span class="checkbox__label">Профессиональный</span>
                                                    </label>
                                                    <label class="filter__checkbox checkbox">
                                                        <input class="checkbox__input" type="checkbox" name="relaxing"><span class="checkbox__label">Расслабляющий</span>
                                                    </label>
                                                    <label class="filter__checkbox checkbox">
                                                        <input class="checkbox__input" type="checkbox" name="thai"><span class="checkbox__label">Тайский</span>
                                                    </label>
                                                    <label class="filter__checkbox checkbox">
                                                        <input class="checkbox__input" type="checkbox" name="urological"><span class="checkbox__label">Урологический</span>
                                                    </label>
                                                    <label class="filter__checkbox checkbox">
                                                        <input class="checkbox__input" type="checkbox" name="dotty"><span class="checkbox__label">Точечный</span>
                                                    </label>
                                                    <label class="filter__checkbox checkbox">
                                                        <input class="checkbox__input" type="checkbox" name="erotic"><span class="checkbox__label">Эротический</span>
                                                    </label>
                                                    <label class="filter__checkbox checkbox">
                                                        <input class="checkbox__input" type="checkbox" name="sakura-branch"><span class="checkbox__label">Ветка сакуры</span>
                                                    </label>
                                                    <label class="filter__checkbox checkbox">
                                                        <input class="checkbox__input" type="checkbox" name="bodysuit"><span class="checkbox__label">Боди</span>
                                                    </label>
                                                    <label class="filter__checkbox checkbox">
                                                        <input class="checkbox__input" type="checkbox" name="lingam"><span class="checkbox__label">Лингам</span>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="filter__col">
                                                <div class="filter__head">Садо-мазо</div>
                                                <div class="filter__checkboxs">
                                                    <label class="filter__checkbox checkbox">
                                                        <input class="checkbox__input" type="checkbox" name="bandage"><span class="checkbox__label">Бандаж</span>
                                                    </label>
                                                    <label class="filter__checkbox checkbox">
                                                        <input class="checkbox__input" type="checkbox" name="madam"><span class="checkbox__label">Госпожа</span>
                                                    </label>
                                                    <label class="filter__checkbox checkbox">
                                                        <input class="checkbox__input" type="checkbox" name="role-game"><span class="checkbox__label">Ролевые игры</span>
                                                    </label>
                                                    <label class="filter__checkbox checkbox">
                                                        <input class="checkbox__input" type="checkbox" name="light-domination"><span class="checkbox__label">Лёгкая доминация</span>
                                                    </label>
                                                    <label class="filter__checkbox checkbox">
                                                        <input class="checkbox__input" type="checkbox" name="flogging"><span class="checkbox__label">Порка</span>
                                                    </label>
                                                    <label class="filter__checkbox checkbox">
                                                        <input class="checkbox__input" type="checkbox" name="slave"><span class="checkbox__label">Рабыня</span>
                                                    </label>
                                                    <label class="filter__checkbox checkbox">
                                                        <input class="checkbox__input" type="checkbox" name="fetish"><span class="checkbox__label">Фетиш</span>
                                                    </label>
                                                    <label class="filter__checkbox checkbox">
                                                        <input class="checkbox__input" type="checkbox" name="foot-fetish"><span class="checkbox__label">Фут-фетиш</span>
                                                    </label>
                                                    <label class="filter__checkbox checkbox">
                                                        <input class="checkbox__input" type="checkbox" name="trampling"><span class="checkbox__label">Трамплинг</span>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="filter__col">
                                                <div class="filter__head">Лесби-шоу</div>
                                                <div class="filter__checkboxs">
                                                    <label class="filter__checkbox checkbox">
                                                        <input class="checkbox__input" type="checkbox" name="frank"><span class="checkbox__label">Откровенное</span>
                                                    </label>
                                                    <label class="filter__checkbox checkbox">
                                                        <input class="checkbox__input" type="checkbox" name="lung"><span class="checkbox__label">Лёгкое</span>
                                                    </label>
                                                </div>
                                                <div class="filter__head">Стриптиз</div>
                                                <div class="filter__checkboxs">
                                                    <label class="filter__checkbox checkbox">
                                                        <input class="checkbox__input" type="checkbox" name="profi"><span class="checkbox__label">Профи</span>
                                                    </label>
                                                    <label class="filter__checkbox checkbox">
                                                        <input class="checkbox__input" type="checkbox" name="not-profi"><span class="checkbox__label">Не профи</span>
                                                    </label>
                                                </div>
                                                <div class="filter__head">Дополнительно</div>
                                                <div class="filter__checkboxs">
                                                    <label class="filter__checkbox checkbox">
                                                        <input class="checkbox__input" type="checkbox" name="escort"><span class="checkbox__label">Эскорт</span>
                                                    </label>
                                                    <label class="filter__checkbox checkbox">
                                                        <input class="checkbox__input" type="checkbox" name="foto"><span class="checkbox__label">Фото/видео</span>
                                                    </label>
                                                    <label class="filter__checkbox checkbox">
                                                        <input class="checkbox__input" type="checkbox" name="services-couples"><span class="checkbox__label">Услуги семейной паре</span>
                                                    </label>
                                                    <label class="filter__checkbox checkbox">
                                                        <input class="checkbox__input" type="checkbox" name="virtual-sex"><span class="checkbox__label">Виртуальный секс</span>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="filter__col">
                                                <div class="filter__head">Золотой дождь</div>
                                                <div class="filter__checkboxs">
                                                    <label class="filter__checkbox checkbox">
                                                        <input class="checkbox__input" type="checkbox" name="issue"><span class="checkbox__label">Выдача</span>
                                                    </label>
                                                    <label class="filter__checkbox checkbox">
                                                        <input class="checkbox__input" type="checkbox" name="acceptance"><span class="checkbox__label">Приём</span>
                                                    </label>
                                                </div>
                                                <div class="filter__head">Копро</div>
                                                <div class="filter__checkboxs">
                                                    <label class="filter__checkbox checkbox">
                                                        <input class="checkbox__input" type="checkbox" name="issue"><span class="checkbox__label">Выдача</span>
                                                    </label>
                                                    <label class="filter__checkbox checkbox">
                                                        <input class="checkbox__input" type="checkbox" name="acceptance"><span class="checkbox__label">Приём</span>
                                                    </label>
                                                </div>
                                                <div class="filter__head">Экстрим</div>
                                                <div class="filter__checkboxs">
                                                    <label class="filter__checkbox checkbox">
                                                        <input class="checkbox__input" type="checkbox" name="strapon"><span class="checkbox__label">Страпон</span>
                                                    </label>
                                                    <label class="filter__checkbox checkbox">
                                                        <input class="checkbox__input" type="checkbox" name="toys"><span class="checkbox__label">Игрушки</span>
                                                    </label>
                                                </div>
                                                <div class="filter__head">Фистинг</div>
                                                <div class="filter__checkboxs">
                                                    <label class="filter__checkbox checkbox">
                                                        <input class="checkbox__input" type="checkbox" name="anal"><span class="checkbox__label">Анальный</span>
                                                    </label>
                                                    <label class="filter__checkbox checkbox">
                                                        <input class="checkbox__input" type="checkbox" name="classic"><span class="checkbox__label">Классический</span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="filter__footer">
                                        <div class="filter__controls"><a class="button button--transparent button--icon-right filter__button-favorites" href="#">
                                                Добавить в избранное<svg width="20" height="18" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                    <path d="M16.528 2.20919C15.597 1.20846 14.2923 0.639056 12.9255 0.636946C11.5576 0.638529 10.2516 1.20763 9.31911 2.20854L9.00132 2.54436L8.68353 2.20854C6.83326 0.217151 3.71893 0.102789 1.72758 1.95306C1.63933 2.03508 1.55411 2.12027 1.47209 2.20854C-0.490696 4.32565 -0.490696 7.59753 1.47209 9.71463L8.5343 17.1622C8.77863 17.4201 9.18579 17.4312 9.44373 17.1868C9.45217 17.1788 9.46039 17.1706 9.46838 17.1622L16.528 9.71463C18.4907 7.59776 18.4907 4.32606 16.528 2.20919ZM15.5971 8.82879H15.5965L9.00132 15.7849L2.40553 8.82879C0.90608 7.21113 0.90608 4.7114 2.40553 3.09374C3.76722 1.61789 6.06755 1.52535 7.5434 2.88703C7.61506 2.95315 7.684 3.02209 7.75012 3.09374L8.5343 3.92104C8.79272 4.17781 9.20995 4.17781 9.46838 3.92104L10.2526 3.09438C11.6142 1.61853 13.9146 1.52599 15.3904 2.88767C15.4621 2.95379 15.531 3.02273 15.5971 3.09438C17.1096 4.71461 17.1207 7.2189 15.5971 8.82879Z" fill="#FF9900"/>
                                                </svg>
                                            </a>
                                            <button class="filter__reset button button--transparent" type="reset"><svg width="19" height="18" viewBox="0 0 19 18" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                    <g >
                                                        <path d="M16.3117 4.02333L15.9143 2.832C15.763 2.37827 15.3398 2.07339 14.8617 2.07339H11.5214V0.985886C11.5214 0.442337 11.0795 0 10.5361 0H7.47721C6.93393 0 6.49187 0.442337 6.49187 0.985886V2.07339H3.15175C2.67344 2.07339 2.25032 2.37827 2.09899 2.832L1.70156 4.02333C1.61106 4.29456 1.65693 4.59489 1.82405 4.82684C1.99118 5.05879 2.26159 5.19736 2.5475 5.19736H2.96293L3.87726 16.5038C3.94524 17.3427 4.65715 18 5.4983 18H12.7005C13.5415 18 14.2536 17.3427 14.3214 16.5037L15.2358 5.19736H15.4658C15.7517 5.19736 16.0221 5.05879 16.1892 4.82698C16.3564 4.59503 16.4022 4.29456 16.3117 4.02333ZM7.54656 1.05469H10.4667V2.07339H7.54656V1.05469ZM13.2702 16.4187C13.2463 16.7141 12.9961 16.9453 12.7005 16.9453H5.4983C5.20276 16.9453 4.95255 16.7141 4.92865 16.4187L4.02105 5.19736H14.1776L13.2702 16.4187ZM2.77369 4.14267L3.09957 3.16571C3.10698 3.14319 3.12799 3.12808 3.15175 3.12808H14.8617C14.8854 3.12808 14.9063 3.14319 14.9139 3.16571L15.2397 4.14267H2.77369Z" fill="#FF9900"/>
                                                        <path d="M11.5876 16.3813C11.5969 16.3819 11.6061 16.382 11.6154 16.382C11.8941 16.382 12.127 16.1636 12.1416 15.8821L12.6368 6.37593C12.6519 6.08507 12.4283 5.83691 12.1376 5.82181C11.846 5.80629 11.5987 6.03013 11.5834 6.321L11.0884 15.8272C11.0733 16.1181 11.2967 16.3662 11.5876 16.3813Z" fill="#FF9900"/>
                                                        <path d="M5.89627 15.8833C5.91165 16.1644 6.14428 16.3819 6.42237 16.3819C6.43198 16.3819 6.44187 16.3816 6.45162 16.3811C6.74235 16.3653 6.96523 16.1167 6.94944 15.8259L6.43075 6.31967C6.41496 6.0288 6.16639 5.80592 5.87553 5.82185C5.5848 5.83764 5.36192 6.08621 5.37771 6.37707L5.89627 15.8833Z" fill="#FF9900"/>
                                                        <path d="M9.01263 16.3819C9.30391 16.3819 9.53998 16.1459 9.53998 15.8546V6.34839C9.53998 6.05711 9.30391 5.82104 9.01263 5.82104C8.72136 5.82104 8.48529 6.05711 8.48529 6.34839V15.8546C8.48529 16.1459 8.72136 16.3819 9.01263 16.3819Z" fill="#FF9900"/>
                                                    </g>
                                                    <defs>
                                                        <clipPath id="clip0">
                                                            <rect width="18" height="18" fill="white" transform="translate(0.00415039)"/>
                                                        </clipPath>
                                                    </defs>
                                                </svg>

                                            </button>
                                        </div>
                                        <div class="filter__footer-actions"><a class="filter__button js-show-options" href="#"><span class="filter__button-show">Показать дополнительные опции</span><span class="filter__button-hidden">Скрыть дополнительные опции </span></a>
                                            <button class="button filter__button-submit" type="submit">Применить фильтр</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="page__items">
                            <div class="page__item page__item--active">
                                <div class="page__main-cards">
                                    @foreach($vipQuestionnaires as $questionnaire)
                                        @include('includes/questionnaires/vip', ['questionnaire' => $questionnaire])
                                    @endforeach
                                </div>
                                <div class="page__section">
                                    <div class="page__cards">
                                        @foreach($questionnaires as $questionnaire)
                                            @include('includes/questionnaires/card', ['questionnaire' => $questionnaire])
                                        @endforeach
                                    </div>
                                    <div class="page__banners">
                                        <div class="page__banner-sm banner-sm"><img src="/images/banner-65ce4c4c.png" alt="banner">
                                        </div>
                                        <div class="page__headline headline">Магазины интимных товаров</div>
                                        <div class="page__banner-lg banner-lg"><img src="/images/banner-2-a94b3fc1.jpg" alt="banner">
                                        </div>
                                        <div class="page__banner-md banner-md"></div>
                                        <div class="page__banner-xs banner-xs"></div>
                                        <div class="page__headline headline">Недвижимость</div>
                                        <div class="page__banner-lg banner-lg"><img src="/images/banner-1-71fdccf6.jpg" alt="banner">
                                        </div>
                                        <div class="page__banner-md banner-md"></div>
                                        <div class="page__banner-xs banner-xs"></div>
                                        <div class="page__headline headline">Работа</div>
                                        <div class="page__banner-lg banner-lg"><img src="/images/banner-1-71fdccf6.jpg" alt="banner">
                                        </div>
                                        <div class="page__banner-md banner-md"></div>
                                        <div class="page__banner-xs banner-xs"></div>
                                        <div class="page__headline headline">Транспорт</div>
                                        <div class="page__banner-lg banner-lg"><img src="/images/banner-1-71fdccf6.jpg" alt="banner">
                                        </div>
                                        <div class="page__banner-md banner-md"></div>
                                        <div class="page__banner-xs banner-xs"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="page__pagination">
                            {{ $questionnaires->links() }}
                            {{--<div class="pagination">
                                <div class="pagination__wrap">
                                    <button class="pagination__button pagination__button--back pagination__button--disabled">
                                        <svg width="7" height="12" viewBox="0 0 7 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M6 1L1 6L6 11" stroke="#5C5C5C" stroke-width="1.8" stroke-linecap="round"
                                                  stroke-linejoin="round" />
                                        </svg>
                                        Назад
                                    </button>
                                    <ul class="pagination__list">
                                        <li class="pagination__item pagination__item--active">
                                            <a class="pagination__link" href="#">1</a>
                                        </li>
                                        <li class="pagination__item">
                                            <a class="pagination__link" href="#">2</a>
                                        </li>
                                        <li class="pagination__item">
                                            <a class="pagination__link pagination__link--dot" href="#">...</a>
                                        </li>
                                    </ul>
                                    <button class="pagination__button"> Далее<svg width="7" height="12" viewBox="0 0 7 12" fill="none"
                                                                                  xmlns="http://www.w3.org/2000/svg">
                                            <path d="M1 11L6 6L1 1" stroke="#5C5C5C" stroke-width="1.8" stroke-linecap="round"
                                                  stroke-linejoin="round" />
                                        </svg>
                                    </button>
                                </div>
                            </div>--}}
                        </div>
                    </div>
                    {{--<div class="page__sidebar">
                        <div class="page__banner-sm banner-sm"><img src="/images/banner-65ce4c4c.png" alt="banner"></div>
                        <div class="page__headline headline">Магазины интимных товаров</div>
                        <div class="page__banner-lg banner-lg"><img src="/images/banner-2-a94b3fc1.jpg" alt="banner"></div>
                        <div class="page__banner-md banner-md"></div>
                        <div class="page__banner-xs banner-xs"></div>
                        <div class="page__headline headline">Недвижимость</div>
                        <div class="page__banner-lg banner-lg"><img src="/images/banner-1-71fdccf6.jpg" alt="banner"></div>
                        <div class="page__banner-md banner-md"></div>
                        <div class="page__banner-xs banner-xs"></div>
                        <div class="page__headline headline">Работа</div>
                        <div class="page__banner-lg banner-lg"><img src="/images/banner-1-71fdccf6.jpg" alt="banner"></div>
                        <div class="page__banner-md banner-md"></div>
                        <div class="page__banner-xs banner-xs"></div>
                        <div class="page__headline headline">Транспорт</div>
                        <div class="page__banner-lg banner-lg"><img src="/images/banner-1-71fdccf6.jpg" alt="banner"></div>
                        <div class="page__banner-md banner-md"></div>
                        <div class="page__banner-xs banner-xs"></div>
                    </div>--}}
                </div>
            </div>
            <div class="page__addition">
                <div class="tabs">
                    <div class="tabs__head">
                        <div class="tabs__btns">
                            <button class="tabs__btn tabs__btn--active" data-type="questionnaire"><span class="tabs__count"></span>
                                <div class="tabs__btn-text">Похожие анкеты</div>
                            </button>
                            <button class="tabs__btn" data-type="viewed"><span class="tabs__count">18</span>
                                <div class="tabs__btn-text">Недавно просмотренные</div>
                            </button>
                            <button class="tabs__btn" data-type="Favorites"><span class="tabs__count">3</span>
                                <div class="tabs__btn-text">Избранные анкеты</div>
                            </button>
                        </div>
                    </div>
                    <div class="tabs__inner">
                        <div class="tabs__container">
                            <div class="tabs__content" data-type="questionnaire">
                                <div class="tabs__slider swiper-container">
                                    <div class="tabs__wrapper swiper-wrapper">
                                        <div class="tabs__slide swiper-slide">
                                            <div class="tabs__slide-head">
                                                <div class="tabs__slide-name">Оля</div>
                                                <button class="tabs__slide-favorites"><svg class="add-favorites js-favorites" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24"><path class="path-empty" d="M6.28 3c3.236.001 4.973 3.491 5.72 5.031.75-1.547 2.469-5.021 5.726-5.021 2.058 0 4.274 1.309 4.274 4.182 0 3.442-4.744 7.851-10 13-5.258-5.151-10-9.559-10-13 0-2.676 1.965-4.193 4.28-4.192zm.001-2c-3.183 0-6.281 2.187-6.281 6.192 0 4.661 5.57 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-4.011-3.097-6.182-6.274-6.182-2.204 0-4.446 1.042-5.726 3.238-1.285-2.206-3.522-3.248-5.719-3.248z"></path><path class="path-full" xmlns="http://www.w3.org/2000/svg" d="M12 4.248c-3.148-5.402-12-3.825-12 2.944 0 4.661 5.571 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-6.792-8.875-8.306-12-2.944z"></path></svg></button>
                                            </div>
                                            <div class="tabs__slide-body"><img src="/images/img-1-db8e7456.jpg" alt="Оля">
                                                <div class="tabs__slide-stars">
                                                    <div class="tabs__slide-title">Массажный салон</div>
                                                    <div class="tabs__slide-rating">
                                                        <div class="rating-static rating-static--five">
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tabs__slide-bottom">
                                                <div class="tabs__slide-line">
                                                    <div class="tabs__slide-title">Апартаменты</div>
                                                    <div class="tabs__slide-price">от&nbsp<span>2 500 ₽</span> /час</div>
                                                </div>
                                                <div class="tabs__slide-line">
                                                    <div class="tabs__slide-title">Выезд</div>
                                                    <div class="tabs__slide-price">от&nbsp<span>4 500 ₽</span> /час</div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tabs__slide swiper-slide">
                                            <div class="tabs__slide-head">
                                                <div class="tabs__slide-name">Катя</div>
                                                <button class="tabs__slide-favorites"><svg class="add-favorites js-favorites" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24"><path class="path-empty" d="M6.28 3c3.236.001 4.973 3.491 5.72 5.031.75-1.547 2.469-5.021 5.726-5.021 2.058 0 4.274 1.309 4.274 4.182 0 3.442-4.744 7.851-10 13-5.258-5.151-10-9.559-10-13 0-2.676 1.965-4.193 4.28-4.192zm.001-2c-3.183 0-6.281 2.187-6.281 6.192 0 4.661 5.57 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-4.011-3.097-6.182-6.274-6.182-2.204 0-4.446 1.042-5.726 3.238-1.285-2.206-3.522-3.248-5.719-3.248z"></path><path class="path-full" xmlns="http://www.w3.org/2000/svg" d="M12 4.248c-3.148-5.402-12-3.825-12 2.944 0 4.661 5.571 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-6.792-8.875-8.306-12-2.944z"></path></svg></button>
                                            </div>
                                            <div class="tabs__slide-body"><img src="/images/img-1-db8e7456.jpg" alt="Катя">
                                                <div class="tabs__slide-stars">
                                                    <div class="tabs__slide-title">Массажный салон</div>
                                                    <div class="tabs__slide-rating">
                                                        <div class="rating-static rating-static--two">
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tabs__slide-bottom">
                                                <div class="tabs__slide-line">
                                                    <div class="tabs__slide-title">Апартаменты</div>
                                                    <div class="tabs__slide-price">от&nbsp<span>2 500 ₽</span> /час</div>
                                                </div>
                                                <div class="tabs__slide-line">
                                                    <div class="tabs__slide-title">Выезд</div>
                                                    <div class="tabs__slide-price">от&nbsp<span>4 500 ₽</span> /час</div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tabs__slide swiper-slide">
                                            <div class="tabs__slide-head">
                                                <div class="tabs__slide-name">Настя</div>
                                                <button class="tabs__slide-favorites"><svg class="add-favorites js-favorites" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24"><path class="path-empty" d="M6.28 3c3.236.001 4.973 3.491 5.72 5.031.75-1.547 2.469-5.021 5.726-5.021 2.058 0 4.274 1.309 4.274 4.182 0 3.442-4.744 7.851-10 13-5.258-5.151-10-9.559-10-13 0-2.676 1.965-4.193 4.28-4.192zm.001-2c-3.183 0-6.281 2.187-6.281 6.192 0 4.661 5.57 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-4.011-3.097-6.182-6.274-6.182-2.204 0-4.446 1.042-5.726 3.238-1.285-2.206-3.522-3.248-5.719-3.248z"></path><path class="path-full" xmlns="http://www.w3.org/2000/svg" d="M12 4.248c-3.148-5.402-12-3.825-12 2.944 0 4.661 5.571 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-6.792-8.875-8.306-12-2.944z"></path></svg></button>
                                            </div>
                                            <div class="tabs__slide-body"><img src="/images/img-1-db8e7456.jpg" alt="Настя">
                                                <div class="tabs__slide-stars">
                                                    <div class="tabs__slide-title">Массажный салон</div>
                                                    <div class="tabs__slide-rating">
                                                        <div class="rating-static rating-static--three">
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tabs__slide-bottom">
                                                <div class="tabs__slide-line">
                                                    <div class="tabs__slide-title">Апартаменты</div>
                                                    <div class="tabs__slide-price">от&nbsp<span>2 500 ₽</span> /час</div>
                                                </div>
                                                <div class="tabs__slide-line">
                                                    <div class="tabs__slide-title">Выезд</div>
                                                    <div class="tabs__slide-price">от&nbsp<span>4 500 ₽</span> /час</div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tabs__slide swiper-slide">
                                            <div class="tabs__slide-head">
                                                <div class="tabs__slide-name">Алена</div>
                                                <button class="tabs__slide-favorites"><svg class="add-favorites js-favorites" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24"><path class="path-empty" d="M6.28 3c3.236.001 4.973 3.491 5.72 5.031.75-1.547 2.469-5.021 5.726-5.021 2.058 0 4.274 1.309 4.274 4.182 0 3.442-4.744 7.851-10 13-5.258-5.151-10-9.559-10-13 0-2.676 1.965-4.193 4.28-4.192zm.001-2c-3.183 0-6.281 2.187-6.281 6.192 0 4.661 5.57 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-4.011-3.097-6.182-6.274-6.182-2.204 0-4.446 1.042-5.726 3.238-1.285-2.206-3.522-3.248-5.719-3.248z"></path><path class="path-full" xmlns="http://www.w3.org/2000/svg" d="M12 4.248c-3.148-5.402-12-3.825-12 2.944 0 4.661 5.571 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-6.792-8.875-8.306-12-2.944z"></path></svg></button>
                                            </div>
                                            <div class="tabs__slide-body"><img src="/images/img-1-db8e7456.jpg" alt="Алена">
                                                <div class="tabs__slide-stars">
                                                    <div class="tabs__slide-title">Массажный салон</div>
                                                    <div class="tabs__slide-rating">
                                                        <div class="rating-static rating-static--one">
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tabs__slide-bottom">
                                                <div class="tabs__slide-line">
                                                    <div class="tabs__slide-title">Апартаменты</div>
                                                    <div class="tabs__slide-price">от&nbsp<span>2 500 ₽</span> /час</div>
                                                </div>
                                                <div class="tabs__slide-line">
                                                    <div class="tabs__slide-title">Выезд</div>
                                                    <div class="tabs__slide-price">от&nbsp<span>4 500 ₽</span> /час</div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tabs__slide swiper-slide">
                                            <div class="tabs__slide-head">
                                                <div class="tabs__slide-name">Оля</div>
                                                <button class="tabs__slide-favorites"><svg class="add-favorites js-favorites" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24"><path class="path-empty" d="M6.28 3c3.236.001 4.973 3.491 5.72 5.031.75-1.547 2.469-5.021 5.726-5.021 2.058 0 4.274 1.309 4.274 4.182 0 3.442-4.744 7.851-10 13-5.258-5.151-10-9.559-10-13 0-2.676 1.965-4.193 4.28-4.192zm.001-2c-3.183 0-6.281 2.187-6.281 6.192 0 4.661 5.57 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-4.011-3.097-6.182-6.274-6.182-2.204 0-4.446 1.042-5.726 3.238-1.285-2.206-3.522-3.248-5.719-3.248z"></path><path class="path-full" xmlns="http://www.w3.org/2000/svg" d="M12 4.248c-3.148-5.402-12-3.825-12 2.944 0 4.661 5.571 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-6.792-8.875-8.306-12-2.944z"></path></svg></button>
                                            </div>
                                            <div class="tabs__slide-body"><img src="/images/img-1-db8e7456.jpg" alt="Оля">
                                                <div class="tabs__slide-stars">
                                                    <div class="tabs__slide-title">Массажный салон</div>
                                                    <div class="tabs__slide-rating">
                                                        <div class="rating-static rating-static--five">
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tabs__slide-bottom">
                                                <div class="tabs__slide-line">
                                                    <div class="tabs__slide-title">Апартаменты</div>
                                                    <div class="tabs__slide-price">от&nbsp<span>2 500 ₽</span> /час</div>
                                                </div>
                                                <div class="tabs__slide-line">
                                                    <div class="tabs__slide-title">Выезд</div>
                                                    <div class="tabs__slide-price">от&nbsp<span>4 500 ₽</span> /час</div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tabs__slide swiper-slide">
                                            <div class="tabs__slide-head">
                                                <div class="tabs__slide-name">Катя</div>
                                                <button class="tabs__slide-favorites"><svg class="add-favorites js-favorites" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24"><path class="path-empty" d="M6.28 3c3.236.001 4.973 3.491 5.72 5.031.75-1.547 2.469-5.021 5.726-5.021 2.058 0 4.274 1.309 4.274 4.182 0 3.442-4.744 7.851-10 13-5.258-5.151-10-9.559-10-13 0-2.676 1.965-4.193 4.28-4.192zm.001-2c-3.183 0-6.281 2.187-6.281 6.192 0 4.661 5.57 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-4.011-3.097-6.182-6.274-6.182-2.204 0-4.446 1.042-5.726 3.238-1.285-2.206-3.522-3.248-5.719-3.248z"></path><path class="path-full" xmlns="http://www.w3.org/2000/svg" d="M12 4.248c-3.148-5.402-12-3.825-12 2.944 0 4.661 5.571 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-6.792-8.875-8.306-12-2.944z"></path></svg></button>
                                            </div>
                                            <div class="tabs__slide-body"><img src="/images/img-1-db8e7456.jpg" alt="Катя">
                                                <div class="tabs__slide-stars">
                                                    <div class="tabs__slide-title">Массажный салон</div>
                                                    <div class="tabs__slide-rating">
                                                        <div class="rating-static rating-static--two">
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tabs__slide-bottom">
                                                <div class="tabs__slide-line">
                                                    <div class="tabs__slide-title">Апартаменты</div>
                                                    <div class="tabs__slide-price">от&nbsp<span>2 500 ₽</span> /час</div>
                                                </div>
                                                <div class="tabs__slide-line">
                                                    <div class="tabs__slide-title">Выезд</div>
                                                    <div class="tabs__slide-price">от&nbsp<span>4 500 ₽</span> /час</div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tabs__slide swiper-slide">
                                            <div class="tabs__slide-head">
                                                <div class="tabs__slide-name">Настя</div>
                                                <button class="tabs__slide-favorites"><svg class="add-favorites js-favorites" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24"><path class="path-empty" d="M6.28 3c3.236.001 4.973 3.491 5.72 5.031.75-1.547 2.469-5.021 5.726-5.021 2.058 0 4.274 1.309 4.274 4.182 0 3.442-4.744 7.851-10 13-5.258-5.151-10-9.559-10-13 0-2.676 1.965-4.193 4.28-4.192zm.001-2c-3.183 0-6.281 2.187-6.281 6.192 0 4.661 5.57 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-4.011-3.097-6.182-6.274-6.182-2.204 0-4.446 1.042-5.726 3.238-1.285-2.206-3.522-3.248-5.719-3.248z"></path><path class="path-full" xmlns="http://www.w3.org/2000/svg" d="M12 4.248c-3.148-5.402-12-3.825-12 2.944 0 4.661 5.571 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-6.792-8.875-8.306-12-2.944z"></path></svg></button>
                                            </div>
                                            <div class="tabs__slide-body"><img src="/images/img-1-db8e7456.jpg" alt="Настя">
                                                <div class="tabs__slide-stars">
                                                    <div class="tabs__slide-title">Массажный салон</div>
                                                    <div class="tabs__slide-rating">
                                                        <div class="rating-static rating-static--three">
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tabs__slide-bottom">
                                                <div class="tabs__slide-line">
                                                    <div class="tabs__slide-title">Апартаменты</div>
                                                    <div class="tabs__slide-price">от&nbsp<span>2 500 ₽</span> /час</div>
                                                </div>
                                                <div class="tabs__slide-line">
                                                    <div class="tabs__slide-title">Выезд</div>
                                                    <div class="tabs__slide-price">от&nbsp<span>4 500 ₽</span> /час</div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tabs__slide swiper-slide">
                                            <div class="tabs__slide-head">
                                                <div class="tabs__slide-name">Алена</div>
                                                <button class="tabs__slide-favorites"><svg class="add-favorites js-favorites" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24"><path class="path-empty" d="M6.28 3c3.236.001 4.973 3.491 5.72 5.031.75-1.547 2.469-5.021 5.726-5.021 2.058 0 4.274 1.309 4.274 4.182 0 3.442-4.744 7.851-10 13-5.258-5.151-10-9.559-10-13 0-2.676 1.965-4.193 4.28-4.192zm.001-2c-3.183 0-6.281 2.187-6.281 6.192 0 4.661 5.57 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-4.011-3.097-6.182-6.274-6.182-2.204 0-4.446 1.042-5.726 3.238-1.285-2.206-3.522-3.248-5.719-3.248z"></path><path class="path-full" xmlns="http://www.w3.org/2000/svg" d="M12 4.248c-3.148-5.402-12-3.825-12 2.944 0 4.661 5.571 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-6.792-8.875-8.306-12-2.944z"></path></svg></button>
                                            </div>
                                            <div class="tabs__slide-body"><img src="/images/img-1-db8e7456.jpg" alt="Алена">
                                                <div class="tabs__slide-stars">
                                                    <div class="tabs__slide-title">Массажный салон</div>
                                                    <div class="tabs__slide-rating">
                                                        <div class="rating-static rating-static--one">
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tabs__slide-bottom">
                                                <div class="tabs__slide-line">
                                                    <div class="tabs__slide-title">Апартаменты</div>
                                                    <div class="tabs__slide-price">от&nbsp<span>2 500 ₽</span> /час</div>
                                                </div>
                                                <div class="tabs__slide-line">
                                                    <div class="tabs__slide-title">Выезд</div>
                                                    <div class="tabs__slide-price">от&nbsp<span>4 500 ₽</span> /час</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tabs__slider-pagination slider-pagination swiper-pagination">
                                        <!-- Add Arrows-->
                                    </div>
                                    <div class="tabs__slider-next slider-next swiper-button-next"></div>
                                    <div class="tabs__slider-prev slider-prev swiper-button-prev"></div>
                                </div>
                            </div>
                            <div class="tabs__content tabs__content--hidden" data-type="viewed">
                                <div class="tabs__slider swiper-container">
                                    <div class="tabs__wrapper swiper-wrapper">
                                        <div class="tabs__slide swiper-slide">
                                            <div class="tabs__slide-head">
                                                <div class="tabs__slide-name">Оля</div>
                                                <button class="tabs__slide-favorites"><svg class="add-favorites js-favorites" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24"><path class="path-empty" d="M6.28 3c3.236.001 4.973 3.491 5.72 5.031.75-1.547 2.469-5.021 5.726-5.021 2.058 0 4.274 1.309 4.274 4.182 0 3.442-4.744 7.851-10 13-5.258-5.151-10-9.559-10-13 0-2.676 1.965-4.193 4.28-4.192zm.001-2c-3.183 0-6.281 2.187-6.281 6.192 0 4.661 5.57 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-4.011-3.097-6.182-6.274-6.182-2.204 0-4.446 1.042-5.726 3.238-1.285-2.206-3.522-3.248-5.719-3.248z"></path><path class="path-full" xmlns="http://www.w3.org/2000/svg" d="M12 4.248c-3.148-5.402-12-3.825-12 2.944 0 4.661 5.571 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-6.792-8.875-8.306-12-2.944z"></path></svg></button>
                                            </div>
                                            <div class="tabs__slide-body"><img src="/images/img-1-db8e7456.jpg" alt="Оля">
                                                <div class="tabs__slide-stars">
                                                    <div class="tabs__slide-title">Массажный салон</div>
                                                    <div class="tabs__slide-rating">
                                                        <div class="rating-static rating-static--five">
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tabs__slide-bottom">
                                                <div class="tabs__slide-line">
                                                    <div class="tabs__slide-title">Апартаменты</div>
                                                    <div class="tabs__slide-price">от&nbsp<span>2 500 ₽</span> /час</div>
                                                </div>
                                                <div class="tabs__slide-line">
                                                    <div class="tabs__slide-title">Выезд</div>
                                                    <div class="tabs__slide-price">от&nbsp<span>4 500 ₽</span> /час</div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tabs__slide swiper-slide">
                                            <div class="tabs__slide-head">
                                                <div class="tabs__slide-name">Катя</div>
                                                <button class="tabs__slide-favorites"><svg class="add-favorites js-favorites" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24"><path class="path-empty" d="M6.28 3c3.236.001 4.973 3.491 5.72 5.031.75-1.547 2.469-5.021 5.726-5.021 2.058 0 4.274 1.309 4.274 4.182 0 3.442-4.744 7.851-10 13-5.258-5.151-10-9.559-10-13 0-2.676 1.965-4.193 4.28-4.192zm.001-2c-3.183 0-6.281 2.187-6.281 6.192 0 4.661 5.57 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-4.011-3.097-6.182-6.274-6.182-2.204 0-4.446 1.042-5.726 3.238-1.285-2.206-3.522-3.248-5.719-3.248z"></path><path class="path-full" xmlns="http://www.w3.org/2000/svg" d="M12 4.248c-3.148-5.402-12-3.825-12 2.944 0 4.661 5.571 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-6.792-8.875-8.306-12-2.944z"></path></svg></button>
                                            </div>
                                            <div class="tabs__slide-body"><img src="/images/img-1-db8e7456.jpg" alt="Катя">
                                                <div class="tabs__slide-stars">
                                                    <div class="tabs__slide-title">Массажный салон</div>
                                                    <div class="tabs__slide-rating">
                                                        <div class="rating-static rating-static--two">
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tabs__slide-bottom">
                                                <div class="tabs__slide-line">
                                                    <div class="tabs__slide-title">Апартаменты</div>
                                                    <div class="tabs__slide-price">от&nbsp<span>2 500 ₽</span> /час</div>
                                                </div>
                                                <div class="tabs__slide-line">
                                                    <div class="tabs__slide-title">Выезд</div>
                                                    <div class="tabs__slide-price">от&nbsp<span>4 500 ₽</span> /час</div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tabs__slide swiper-slide">
                                            <div class="tabs__slide-head">
                                                <div class="tabs__slide-name">Настя</div>
                                                <button class="tabs__slide-favorites"><svg class="add-favorites js-favorites" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24"><path class="path-empty" d="M6.28 3c3.236.001 4.973 3.491 5.72 5.031.75-1.547 2.469-5.021 5.726-5.021 2.058 0 4.274 1.309 4.274 4.182 0 3.442-4.744 7.851-10 13-5.258-5.151-10-9.559-10-13 0-2.676 1.965-4.193 4.28-4.192zm.001-2c-3.183 0-6.281 2.187-6.281 6.192 0 4.661 5.57 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-4.011-3.097-6.182-6.274-6.182-2.204 0-4.446 1.042-5.726 3.238-1.285-2.206-3.522-3.248-5.719-3.248z"></path><path class="path-full" xmlns="http://www.w3.org/2000/svg" d="M12 4.248c-3.148-5.402-12-3.825-12 2.944 0 4.661 5.571 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-6.792-8.875-8.306-12-2.944z"></path></svg></button>
                                            </div>
                                            <div class="tabs__slide-body"><img src="/images/img-1-db8e7456.jpg" alt="Настя">
                                                <div class="tabs__slide-stars">
                                                    <div class="tabs__slide-title">Массажный салон</div>
                                                    <div class="tabs__slide-rating">
                                                        <div class="rating-static rating-static--three">
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tabs__slide-bottom">
                                                <div class="tabs__slide-line">
                                                    <div class="tabs__slide-title">Апартаменты</div>
                                                    <div class="tabs__slide-price">от&nbsp<span>2 500 ₽</span> /час</div>
                                                </div>
                                                <div class="tabs__slide-line">
                                                    <div class="tabs__slide-title">Выезд</div>
                                                    <div class="tabs__slide-price">от&nbsp<span>4 500 ₽</span> /час</div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tabs__slide swiper-slide">
                                            <div class="tabs__slide-head">
                                                <div class="tabs__slide-name">Алена</div>
                                                <button class="tabs__slide-favorites"><svg class="add-favorites js-favorites" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24"><path class="path-empty" d="M6.28 3c3.236.001 4.973 3.491 5.72 5.031.75-1.547 2.469-5.021 5.726-5.021 2.058 0 4.274 1.309 4.274 4.182 0 3.442-4.744 7.851-10 13-5.258-5.151-10-9.559-10-13 0-2.676 1.965-4.193 4.28-4.192zm.001-2c-3.183 0-6.281 2.187-6.281 6.192 0 4.661 5.57 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-4.011-3.097-6.182-6.274-6.182-2.204 0-4.446 1.042-5.726 3.238-1.285-2.206-3.522-3.248-5.719-3.248z"></path><path class="path-full" xmlns="http://www.w3.org/2000/svg" d="M12 4.248c-3.148-5.402-12-3.825-12 2.944 0 4.661 5.571 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-6.792-8.875-8.306-12-2.944z"></path></svg></button>
                                            </div>
                                            <div class="tabs__slide-body"><img src="/images/img-1-db8e7456.jpg" alt="Алена">
                                                <div class="tabs__slide-stars">
                                                    <div class="tabs__slide-title">Массажный салон</div>
                                                    <div class="tabs__slide-rating">
                                                        <div class="rating-static rating-static--one">
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tabs__slide-bottom">
                                                <div class="tabs__slide-line">
                                                    <div class="tabs__slide-title">Апартаменты</div>
                                                    <div class="tabs__slide-price">от&nbsp<span>2 500 ₽</span> /час</div>
                                                </div>
                                                <div class="tabs__slide-line">
                                                    <div class="tabs__slide-title">Выезд</div>
                                                    <div class="tabs__slide-price">от&nbsp<span>4 500 ₽</span> /час</div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tabs__slide swiper-slide">
                                            <div class="tabs__slide-head">
                                                <div class="tabs__slide-name">Оля</div>
                                                <button class="tabs__slide-favorites"><svg class="add-favorites js-favorites" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24"><path class="path-empty" d="M6.28 3c3.236.001 4.973 3.491 5.72 5.031.75-1.547 2.469-5.021 5.726-5.021 2.058 0 4.274 1.309 4.274 4.182 0 3.442-4.744 7.851-10 13-5.258-5.151-10-9.559-10-13 0-2.676 1.965-4.193 4.28-4.192zm.001-2c-3.183 0-6.281 2.187-6.281 6.192 0 4.661 5.57 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-4.011-3.097-6.182-6.274-6.182-2.204 0-4.446 1.042-5.726 3.238-1.285-2.206-3.522-3.248-5.719-3.248z"></path><path class="path-full" xmlns="http://www.w3.org/2000/svg" d="M12 4.248c-3.148-5.402-12-3.825-12 2.944 0 4.661 5.571 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-6.792-8.875-8.306-12-2.944z"></path></svg></button>
                                            </div>
                                            <div class="tabs__slide-body"><img src="/images/img-1-db8e7456.jpg" alt="Оля">
                                                <div class="tabs__slide-stars">
                                                    <div class="tabs__slide-title">Массажный салон</div>
                                                    <div class="tabs__slide-rating">
                                                        <div class="rating-static rating-static--five">
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tabs__slide-bottom">
                                                <div class="tabs__slide-line">
                                                    <div class="tabs__slide-title">Апартаменты</div>
                                                    <div class="tabs__slide-price">от&nbsp<span>2 500 ₽</span> /час</div>
                                                </div>
                                                <div class="tabs__slide-line">
                                                    <div class="tabs__slide-title">Выезд</div>
                                                    <div class="tabs__slide-price">от&nbsp<span>4 500 ₽</span> /час</div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tabs__slide swiper-slide">
                                            <div class="tabs__slide-head">
                                                <div class="tabs__slide-name">Катя</div>
                                                <button class="tabs__slide-favorites"><svg class="add-favorites js-favorites" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24"><path class="path-empty" d="M6.28 3c3.236.001 4.973 3.491 5.72 5.031.75-1.547 2.469-5.021 5.726-5.021 2.058 0 4.274 1.309 4.274 4.182 0 3.442-4.744 7.851-10 13-5.258-5.151-10-9.559-10-13 0-2.676 1.965-4.193 4.28-4.192zm.001-2c-3.183 0-6.281 2.187-6.281 6.192 0 4.661 5.57 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-4.011-3.097-6.182-6.274-6.182-2.204 0-4.446 1.042-5.726 3.238-1.285-2.206-3.522-3.248-5.719-3.248z"></path><path class="path-full" xmlns="http://www.w3.org/2000/svg" d="M12 4.248c-3.148-5.402-12-3.825-12 2.944 0 4.661 5.571 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-6.792-8.875-8.306-12-2.944z"></path></svg></button>
                                            </div>
                                            <div class="tabs__slide-body"><img src="/images/img-1-db8e7456.jpg" alt="Катя">
                                                <div class="tabs__slide-stars">
                                                    <div class="tabs__slide-title">Массажный салон</div>
                                                    <div class="tabs__slide-rating">
                                                        <div class="rating-static rating-static--two">
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tabs__slide-bottom">
                                                <div class="tabs__slide-line">
                                                    <div class="tabs__slide-title">Апартаменты</div>
                                                    <div class="tabs__slide-price">от&nbsp<span>2 500 ₽</span> /час</div>
                                                </div>
                                                <div class="tabs__slide-line">
                                                    <div class="tabs__slide-title">Выезд</div>
                                                    <div class="tabs__slide-price">от&nbsp<span>4 500 ₽</span> /час</div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tabs__slide swiper-slide">
                                            <div class="tabs__slide-head">
                                                <div class="tabs__slide-name">Настя</div>
                                                <button class="tabs__slide-favorites"><svg class="add-favorites js-favorites" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24"><path class="path-empty" d="M6.28 3c3.236.001 4.973 3.491 5.72 5.031.75-1.547 2.469-5.021 5.726-5.021 2.058 0 4.274 1.309 4.274 4.182 0 3.442-4.744 7.851-10 13-5.258-5.151-10-9.559-10-13 0-2.676 1.965-4.193 4.28-4.192zm.001-2c-3.183 0-6.281 2.187-6.281 6.192 0 4.661 5.57 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-4.011-3.097-6.182-6.274-6.182-2.204 0-4.446 1.042-5.726 3.238-1.285-2.206-3.522-3.248-5.719-3.248z"></path><path class="path-full" xmlns="http://www.w3.org/2000/svg" d="M12 4.248c-3.148-5.402-12-3.825-12 2.944 0 4.661 5.571 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-6.792-8.875-8.306-12-2.944z"></path></svg></button>
                                            </div>
                                            <div class="tabs__slide-body"><img src="/images/img-1-db8e7456.jpg" alt="Настя">
                                                <div class="tabs__slide-stars">
                                                    <div class="tabs__slide-title">Массажный салон</div>
                                                    <div class="tabs__slide-rating">
                                                        <div class="rating-static rating-static--three">
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tabs__slide-bottom">
                                                <div class="tabs__slide-line">
                                                    <div class="tabs__slide-title">Апартаменты</div>
                                                    <div class="tabs__slide-price">от&nbsp<span>2 500 ₽</span> /час</div>
                                                </div>
                                                <div class="tabs__slide-line">
                                                    <div class="tabs__slide-title">Выезд</div>
                                                    <div class="tabs__slide-price">от&nbsp<span>4 500 ₽</span> /час</div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tabs__slide swiper-slide">
                                            <div class="tabs__slide-head">
                                                <div class="tabs__slide-name">Алена</div>
                                                <button class="tabs__slide-favorites"><svg class="add-favorites js-favorites" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24"><path class="path-empty" d="M6.28 3c3.236.001 4.973 3.491 5.72 5.031.75-1.547 2.469-5.021 5.726-5.021 2.058 0 4.274 1.309 4.274 4.182 0 3.442-4.744 7.851-10 13-5.258-5.151-10-9.559-10-13 0-2.676 1.965-4.193 4.28-4.192zm.001-2c-3.183 0-6.281 2.187-6.281 6.192 0 4.661 5.57 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-4.011-3.097-6.182-6.274-6.182-2.204 0-4.446 1.042-5.726 3.238-1.285-2.206-3.522-3.248-5.719-3.248z"></path><path class="path-full" xmlns="http://www.w3.org/2000/svg" d="M12 4.248c-3.148-5.402-12-3.825-12 2.944 0 4.661 5.571 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-6.792-8.875-8.306-12-2.944z"></path></svg></button>
                                            </div>
                                            <div class="tabs__slide-body"><img src="/images/img-1-db8e7456.jpg" alt="Алена">
                                                <div class="tabs__slide-stars">
                                                    <div class="tabs__slide-title">Массажный салон</div>
                                                    <div class="tabs__slide-rating">
                                                        <div class="rating-static rating-static--one">
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tabs__slide-bottom">
                                                <div class="tabs__slide-line">
                                                    <div class="tabs__slide-title">Апартаменты</div>
                                                    <div class="tabs__slide-price">от&nbsp<span>2 500 ₽</span> /час</div>
                                                </div>
                                                <div class="tabs__slide-line">
                                                    <div class="tabs__slide-title">Выезд</div>
                                                    <div class="tabs__slide-price">от&nbsp<span>4 500 ₽</span> /час</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tabs__slider-pagination slider-pagination swiper-pagination">
                                        <!-- Add Arrows-->
                                    </div>
                                    <div class="tabs__slider-next slider-next swiper-button-next"></div>
                                    <div class="tabs__slider-prev slider-prev swiper-button-prev"></div>
                                </div>
                            </div>
                            <div class="tabs__content tabs__content--hidden" data-type="Favorites">
                                <div class="tabs__slider swiper-container">
                                    <div class="tabs__wrapper swiper-wrapper">
                                        <div class="tabs__slide swiper-slide">
                                            <div class="tabs__slide-head">
                                                <div class="tabs__slide-name">Оля</div>
                                                <button class="tabs__slide-favorites"><svg class="add-favorites js-favorites" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24"><path class="path-empty" d="M6.28 3c3.236.001 4.973 3.491 5.72 5.031.75-1.547 2.469-5.021 5.726-5.021 2.058 0 4.274 1.309 4.274 4.182 0 3.442-4.744 7.851-10 13-5.258-5.151-10-9.559-10-13 0-2.676 1.965-4.193 4.28-4.192zm.001-2c-3.183 0-6.281 2.187-6.281 6.192 0 4.661 5.57 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-4.011-3.097-6.182-6.274-6.182-2.204 0-4.446 1.042-5.726 3.238-1.285-2.206-3.522-3.248-5.719-3.248z"></path><path class="path-full" xmlns="http://www.w3.org/2000/svg" d="M12 4.248c-3.148-5.402-12-3.825-12 2.944 0 4.661 5.571 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-6.792-8.875-8.306-12-2.944z"></path></svg></button>
                                            </div>
                                            <div class="tabs__slide-body"><img src="/images/img-1-db8e7456.jpg" alt="Оля">
                                                <div class="tabs__slide-stars">
                                                    <div class="tabs__slide-title">Массажный салон</div>
                                                    <div class="tabs__slide-rating">
                                                        <div class="rating-static rating-static--five">
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tabs__slide-bottom">
                                                <div class="tabs__slide-line">
                                                    <div class="tabs__slide-title">Апартаменты</div>
                                                    <div class="tabs__slide-price">от&nbsp<span>2 500 ₽</span> /час</div>
                                                </div>
                                                <div class="tabs__slide-line">
                                                    <div class="tabs__slide-title">Выезд</div>
                                                    <div class="tabs__slide-price">от&nbsp<span>4 500 ₽</span> /час</div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tabs__slide swiper-slide">
                                            <div class="tabs__slide-head">
                                                <div class="tabs__slide-name">Катя</div>
                                                <button class="tabs__slide-favorites"><svg class="add-favorites js-favorites" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24"><path class="path-empty" d="M6.28 3c3.236.001 4.973 3.491 5.72 5.031.75-1.547 2.469-5.021 5.726-5.021 2.058 0 4.274 1.309 4.274 4.182 0 3.442-4.744 7.851-10 13-5.258-5.151-10-9.559-10-13 0-2.676 1.965-4.193 4.28-4.192zm.001-2c-3.183 0-6.281 2.187-6.281 6.192 0 4.661 5.57 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-4.011-3.097-6.182-6.274-6.182-2.204 0-4.446 1.042-5.726 3.238-1.285-2.206-3.522-3.248-5.719-3.248z"></path><path class="path-full" xmlns="http://www.w3.org/2000/svg" d="M12 4.248c-3.148-5.402-12-3.825-12 2.944 0 4.661 5.571 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-6.792-8.875-8.306-12-2.944z"></path></svg></button>
                                            </div>
                                            <div class="tabs__slide-body"><img src="/images/img-1-db8e7456.jpg" alt="Катя">
                                                <div class="tabs__slide-stars">
                                                    <div class="tabs__slide-title">Массажный салон</div>
                                                    <div class="tabs__slide-rating">
                                                        <div class="rating-static rating-static--two">
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tabs__slide-bottom">
                                                <div class="tabs__slide-line">
                                                    <div class="tabs__slide-title">Апартаменты</div>
                                                    <div class="tabs__slide-price">от&nbsp<span>2 500 ₽</span> /час</div>
                                                </div>
                                                <div class="tabs__slide-line">
                                                    <div class="tabs__slide-title">Выезд</div>
                                                    <div class="tabs__slide-price">от&nbsp<span>4 500 ₽</span> /час</div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tabs__slide swiper-slide">
                                            <div class="tabs__slide-head">
                                                <div class="tabs__slide-name">Настя</div>
                                                <button class="tabs__slide-favorites"><svg class="add-favorites js-favorites" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24"><path class="path-empty" d="M6.28 3c3.236.001 4.973 3.491 5.72 5.031.75-1.547 2.469-5.021 5.726-5.021 2.058 0 4.274 1.309 4.274 4.182 0 3.442-4.744 7.851-10 13-5.258-5.151-10-9.559-10-13 0-2.676 1.965-4.193 4.28-4.192zm.001-2c-3.183 0-6.281 2.187-6.281 6.192 0 4.661 5.57 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-4.011-3.097-6.182-6.274-6.182-2.204 0-4.446 1.042-5.726 3.238-1.285-2.206-3.522-3.248-5.719-3.248z"></path><path class="path-full" xmlns="http://www.w3.org/2000/svg" d="M12 4.248c-3.148-5.402-12-3.825-12 2.944 0 4.661 5.571 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-6.792-8.875-8.306-12-2.944z"></path></svg></button>
                                            </div>
                                            <div class="tabs__slide-body"><img src="/images/img-1-db8e7456.jpg" alt="Настя">
                                                <div class="tabs__slide-stars">
                                                    <div class="tabs__slide-title">Массажный салон</div>
                                                    <div class="tabs__slide-rating">
                                                        <div class="rating-static rating-static--three">
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tabs__slide-bottom">
                                                <div class="tabs__slide-line">
                                                    <div class="tabs__slide-title">Апартаменты</div>
                                                    <div class="tabs__slide-price">от&nbsp<span>2 500 ₽</span> /час</div>
                                                </div>
                                                <div class="tabs__slide-line">
                                                    <div class="tabs__slide-title">Выезд</div>
                                                    <div class="tabs__slide-price">от&nbsp<span>4 500 ₽</span> /час</div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tabs__slide swiper-slide">
                                            <div class="tabs__slide-head">
                                                <div class="tabs__slide-name">Алена</div>
                                                <button class="tabs__slide-favorites"><svg class="add-favorites js-favorites" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24"><path class="path-empty" d="M6.28 3c3.236.001 4.973 3.491 5.72 5.031.75-1.547 2.469-5.021 5.726-5.021 2.058 0 4.274 1.309 4.274 4.182 0 3.442-4.744 7.851-10 13-5.258-5.151-10-9.559-10-13 0-2.676 1.965-4.193 4.28-4.192zm.001-2c-3.183 0-6.281 2.187-6.281 6.192 0 4.661 5.57 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-4.011-3.097-6.182-6.274-6.182-2.204 0-4.446 1.042-5.726 3.238-1.285-2.206-3.522-3.248-5.719-3.248z"></path><path class="path-full" xmlns="http://www.w3.org/2000/svg" d="M12 4.248c-3.148-5.402-12-3.825-12 2.944 0 4.661 5.571 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-6.792-8.875-8.306-12-2.944z"></path></svg></button>
                                            </div>
                                            <div class="tabs__slide-body"><img src="/images/img-1-db8e7456.jpg" alt="Алена">
                                                <div class="tabs__slide-stars">
                                                    <div class="tabs__slide-title">Массажный салон</div>
                                                    <div class="tabs__slide-rating">
                                                        <div class="rating-static rating-static--one">
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tabs__slide-bottom">
                                                <div class="tabs__slide-line">
                                                    <div class="tabs__slide-title">Апартаменты</div>
                                                    <div class="tabs__slide-price">от&nbsp<span>2 500 ₽</span> /час</div>
                                                </div>
                                                <div class="tabs__slide-line">
                                                    <div class="tabs__slide-title">Выезд</div>
                                                    <div class="tabs__slide-price">от&nbsp<span>4 500 ₽</span> /час</div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tabs__slide swiper-slide">
                                            <div class="tabs__slide-head">
                                                <div class="tabs__slide-name">Оля</div>
                                                <button class="tabs__slide-favorites"><svg class="add-favorites js-favorites" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24"><path class="path-empty" d="M6.28 3c3.236.001 4.973 3.491 5.72 5.031.75-1.547 2.469-5.021 5.726-5.021 2.058 0 4.274 1.309 4.274 4.182 0 3.442-4.744 7.851-10 13-5.258-5.151-10-9.559-10-13 0-2.676 1.965-4.193 4.28-4.192zm.001-2c-3.183 0-6.281 2.187-6.281 6.192 0 4.661 5.57 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-4.011-3.097-6.182-6.274-6.182-2.204 0-4.446 1.042-5.726 3.238-1.285-2.206-3.522-3.248-5.719-3.248z"></path><path class="path-full" xmlns="http://www.w3.org/2000/svg" d="M12 4.248c-3.148-5.402-12-3.825-12 2.944 0 4.661 5.571 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-6.792-8.875-8.306-12-2.944z"></path></svg></button>
                                            </div>
                                            <div class="tabs__slide-body"><img src="/images/img-1-db8e7456.jpg" alt="Оля">
                                                <div class="tabs__slide-stars">
                                                    <div class="tabs__slide-title">Массажный салон</div>
                                                    <div class="tabs__slide-rating">
                                                        <div class="rating-static rating-static--five">
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tabs__slide-bottom">
                                                <div class="tabs__slide-line">
                                                    <div class="tabs__slide-title">Апартаменты</div>
                                                    <div class="tabs__slide-price">от&nbsp<span>2 500 ₽</span> /час</div>
                                                </div>
                                                <div class="tabs__slide-line">
                                                    <div class="tabs__slide-title">Выезд</div>
                                                    <div class="tabs__slide-price">от&nbsp<span>4 500 ₽</span> /час</div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tabs__slide swiper-slide">
                                            <div class="tabs__slide-head">
                                                <div class="tabs__slide-name">Катя</div>
                                                <button class="tabs__slide-favorites"><svg class="add-favorites js-favorites" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24"><path class="path-empty" d="M6.28 3c3.236.001 4.973 3.491 5.72 5.031.75-1.547 2.469-5.021 5.726-5.021 2.058 0 4.274 1.309 4.274 4.182 0 3.442-4.744 7.851-10 13-5.258-5.151-10-9.559-10-13 0-2.676 1.965-4.193 4.28-4.192zm.001-2c-3.183 0-6.281 2.187-6.281 6.192 0 4.661 5.57 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-4.011-3.097-6.182-6.274-6.182-2.204 0-4.446 1.042-5.726 3.238-1.285-2.206-3.522-3.248-5.719-3.248z"></path><path class="path-full" xmlns="http://www.w3.org/2000/svg" d="M12 4.248c-3.148-5.402-12-3.825-12 2.944 0 4.661 5.571 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-6.792-8.875-8.306-12-2.944z"></path></svg></button>
                                            </div>
                                            <div class="tabs__slide-body"><img src="/images/img-1-db8e7456.jpg" alt="Катя">
                                                <div class="tabs__slide-stars">
                                                    <div class="tabs__slide-title">Массажный салон</div>
                                                    <div class="tabs__slide-rating">
                                                        <div class="rating-static rating-static--two">
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tabs__slide-bottom">
                                                <div class="tabs__slide-line">
                                                    <div class="tabs__slide-title">Апартаменты</div>
                                                    <div class="tabs__slide-price">от&nbsp<span>2 500 ₽</span> /час</div>
                                                </div>
                                                <div class="tabs__slide-line">
                                                    <div class="tabs__slide-title">Выезд</div>
                                                    <div class="tabs__slide-price">от&nbsp<span>4 500 ₽</span> /час</div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tabs__slide swiper-slide">
                                            <div class="tabs__slide-head">
                                                <div class="tabs__slide-name">Настя</div>
                                                <button class="tabs__slide-favorites"><svg class="add-favorites js-favorites" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24"><path class="path-empty" d="M6.28 3c3.236.001 4.973 3.491 5.72 5.031.75-1.547 2.469-5.021 5.726-5.021 2.058 0 4.274 1.309 4.274 4.182 0 3.442-4.744 7.851-10 13-5.258-5.151-10-9.559-10-13 0-2.676 1.965-4.193 4.28-4.192zm.001-2c-3.183 0-6.281 2.187-6.281 6.192 0 4.661 5.57 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-4.011-3.097-6.182-6.274-6.182-2.204 0-4.446 1.042-5.726 3.238-1.285-2.206-3.522-3.248-5.719-3.248z"></path><path class="path-full" xmlns="http://www.w3.org/2000/svg" d="M12 4.248c-3.148-5.402-12-3.825-12 2.944 0 4.661 5.571 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-6.792-8.875-8.306-12-2.944z"></path></svg></button>
                                            </div>
                                            <div class="tabs__slide-body"><img src="/images/img-1-db8e7456.jpg" alt="Настя">
                                                <div class="tabs__slide-stars">
                                                    <div class="tabs__slide-title">Массажный салон</div>
                                                    <div class="tabs__slide-rating">
                                                        <div class="rating-static rating-static--three">
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tabs__slide-bottom">
                                                <div class="tabs__slide-line">
                                                    <div class="tabs__slide-title">Апартаменты</div>
                                                    <div class="tabs__slide-price">от&nbsp<span>2 500 ₽</span> /час</div>
                                                </div>
                                                <div class="tabs__slide-line">
                                                    <div class="tabs__slide-title">Выезд</div>
                                                    <div class="tabs__slide-price">от&nbsp<span>4 500 ₽</span> /час</div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tabs__slide swiper-slide">
                                            <div class="tabs__slide-head">
                                                <div class="tabs__slide-name">Алена</div>
                                                <button class="tabs__slide-favorites"><svg class="add-favorites js-favorites" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24"><path class="path-empty" d="M6.28 3c3.236.001 4.973 3.491 5.72 5.031.75-1.547 2.469-5.021 5.726-5.021 2.058 0 4.274 1.309 4.274 4.182 0 3.442-4.744 7.851-10 13-5.258-5.151-10-9.559-10-13 0-2.676 1.965-4.193 4.28-4.192zm.001-2c-3.183 0-6.281 2.187-6.281 6.192 0 4.661 5.57 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-4.011-3.097-6.182-6.274-6.182-2.204 0-4.446 1.042-5.726 3.238-1.285-2.206-3.522-3.248-5.719-3.248z"></path><path class="path-full" xmlns="http://www.w3.org/2000/svg" d="M12 4.248c-3.148-5.402-12-3.825-12 2.944 0 4.661 5.571 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-6.792-8.875-8.306-12-2.944z"></path></svg></button>
                                            </div>
                                            <div class="tabs__slide-body"><img src="/images/img-1-db8e7456.jpg" alt="Алена">
                                                <div class="tabs__slide-stars">
                                                    <div class="tabs__slide-title">Массажный салон</div>
                                                    <div class="tabs__slide-rating">
                                                        <div class="rating-static rating-static--one">
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                            <div class="rating-static__star"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tabs__slide-bottom">
                                                <div class="tabs__slide-line">
                                                    <div class="tabs__slide-title">Апартаменты</div>
                                                    <div class="tabs__slide-price">от&nbsp<span>2 500 ₽</span> /час</div>
                                                </div>
                                                <div class="tabs__slide-line">
                                                    <div class="tabs__slide-title">Выезд</div>
                                                    <div class="tabs__slide-price">от&nbsp<span>4 500 ₽</span> /час</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tabs__slider-pagination slider-pagination swiper-pagination">
                                        <!-- Add Arrows-->
                                    </div>
                                    <div class="tabs__slider-next slider-next swiper-button-next"></div>
                                    <div class="tabs__slider-prev slider-prev swiper-button-prev"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('includes._bottom-banner')
@endsection
