<?php

namespace Database\Seeders;

use App\Models\QuestionnairePrice;
use Illuminate\Database\Seeder;

class QuestionnairePriceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        QuestionnairePrice::factory()
            ->times(100)
            ->create();
    }
}
