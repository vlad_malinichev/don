<?php

namespace Database\Factories;

use App\Models\QuestionnairePrice;
use Illuminate\Database\Eloquent\Factories\Factory;

class QuestionnairePriceFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = QuestionnairePrice::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'questionnaire_id' => $this->faker->numberBetween(1, 1050),
            'currency_id' => 1,
            'hour_in_apartments' => $this->faker->randomFloat(2, 1000, 15000),
            'hour_in_apartments_negotiated' => $this->faker->boolean,
            'two_hours_in_apartments' => $this->faker->randomFloat(2, 5000, 50000),
            'two_hours_in_apartments_negotiated' => $this->faker->boolean,
            'night_in_apartments' => $this->faker->randomFloat(2, 1000, 50000),
            'night_in_apartments_negotiated' => $this->faker->boolean,
            'hour_in_departures' => $this->faker->randomFloat(2, 5000, 25000),
            'hour_in_departures_negotiated' => $this->faker->boolean,
            'two_hours_in_departures' => $this->faker->randomFloat(2, 10000, 50000),
            'two_hours_in_departures_negotiated' => $this->faker->boolean,
            'night_in_departures' => $this->faker->randomFloat(2, 100000, 75000),
            'night_in_departures_negotiated' => $this->faker->boolean,
            'is_departure_to_office' => $this->faker->boolean,
            'is_departure_to_sauna' => $this->faker->boolean,
            'is_departure_to_hotel' => $this->faker->boolean,
            'is_departure_to_apartment' => $this->faker->boolean,
            'is_departure_to_country_house' => $this->faker->boolean,
            'is_departure_to_any_place' => $this->faker->boolean,
        ];
    }
}
