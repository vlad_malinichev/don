<?php

namespace Database\Factories;

use App\Models\Questionnaire;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class QuestionnaireFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Questionnaire::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'user_id' => 0,
            'type_id' => 1,
            'nationality_id' => 1,
            'orientation_id' => 1,
            'country_id' => 1,
            'region_id' => 5,
            'city_id' => 2,
            'district_id' => 5,
            'subway_id' => null,
            'hair_color_id' => 1,
            'intimate_haircut_id' => 1,
            'name' => $this->faker->name,
            'phone' => $this->faker->e164PhoneNumber,
            'viber' => $this->faker->boolean,
            'whatsapp' => $this->faker->boolean,
            'telegram' => $this->faker->boolean,
            'call_time_from' => null,
            'call_time_to' => null,
            'is_around_clock' => $this->faker->boolean,
            'sex' => 2,
            'age' => date('Y') - $this->faker->year,
            'height' => $this->faker->numberBetween(145, 190),
            'weight' => $this->faker->numberBetween(35, 100),
            'breast_size' => $this->faker->numberBetween(1, 6),
            'is_scars' => $this->faker->boolean,
            'is_piercing' => $this->faker->boolean,
            'is_tattoos' => $this->faker->boolean,
            'about' => $this->faker->text,
            'is_vip' => $this->faker->boolean,
            'active' => $this->faker->boolean,
        ];
    }
}
