<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQuestionnairePricesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('questionnaire_prices', function (Blueprint $table) {
            $table->id();
            $table->integer('questionnaire_id');
            $table->integer('currency_id');
            $table->float('hour_in_apartments');
            $table->boolean('hour_in_apartments_negotiated');
            $table->float('two_hours_in_apartments');
            $table->boolean('two_hours_in_apartments_negotiated');
            $table->float('night_in_apartments');
            $table->boolean('night_in_apartments_negotiated');
            $table->float('hour_in_departures');
            $table->boolean('hour_in_departures_negotiated');
            $table->float('two_hours_in_departures');
            $table->boolean('two_hours_in_departures_negotiated');
            $table->float('night_in_departures');
            $table->boolean('night_in_departures_negotiated');
            $table->boolean('is_departure_to_office');
            $table->boolean('is_departure_to_sauna');
            $table->boolean('is_departure_to_hotel');
            $table->boolean('is_departure_to_apartment');
            $table->boolean('is_departure_to_country_house');
            $table->boolean('is_departure_to_any_place');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('questionnaire_prices');
    }
}
