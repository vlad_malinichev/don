<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQuestionnairesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('questionnaires', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id');
            $table->integer('type_id');
            $table->integer('nationality_id');
            $table->integer('orientation_id');
            $table->integer('country_id');
            $table->integer('region_id');
            $table->integer('city_id');
            $table->integer('district_id')->nullable();
            $table->integer('subway_id')->nullable();
            $table->integer('hair_color_id')->nullable();
            $table->integer('intimate_haircut_id')->nullable();
            $table->string('name');
            $table->string('phone');
            $table->boolean('viber');
            $table->boolean('whatsapp');
            $table->boolean('telegram');
            $table->string('call_time_from')->nullable();
            $table->string('call_time_to')->nullable();
            $table->boolean('is_around_clock');
            $table->integer('sex');
            $table->integer('age');
            $table->integer('height');
            $table->integer('weight');
            $table->string('breast_size');
            $table->boolean('is_scars');
            $table->boolean('is_piercing');
            $table->boolean('is_tattoos');
            $table->text('about')->nullable();
            $table->boolean('is_vip')->default(0);
            $table->boolean('active')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('questionnaires');
    }
}
