<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQuestionnaireServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('questionnaire_services', function (Blueprint $table) {
            $table->id();
            $table->integer('questionnaire_id');
            $table->boolean('vaginal');
            $table->integer('vaginal_price');
            $table->boolean('anal');
            $table->integer('anal_price');
            $table->boolean('group');
            $table->integer('group_price');
            $table->boolean('lesbian');
            $table->integer('lesbian_price');
            $table->boolean('mouth');
            $table->integer('mouth_price');
            $table->boolean('face');
            $table->integer('face_price');
            $table->boolean('chest');
            $table->integer('chest_price');
            $table->boolean('frank');
            $table->integer('frank_price');
            $table->boolean('lung');
            $table->integer('lung_price');
            $table->boolean('escort');
            $table->integer('escort_price');
            $table->boolean('photo_video');
            $table->integer('photo_video_price');
            $table->boolean('family_services');
            $table->integer('family_services_price');
            $table->boolean('virtual');
            $table->integer('virtual_price');
            $table->boolean('banjo');
            $table->integer('banjo_price');
            $table->boolean('madam');
            $table->integer('madam_price');
            $table->boolean('slave');
            $table->integer('slave_price');
            $table->boolean('light_domination');
            $table->integer('light_domination_price');
            $table->boolean('erotic_games');
            $table->integer('erotic_games_price');
            $table->boolean('flogging');
            $table->integer('flogging_price');
            $table->boolean('fetish');
            $table->integer('fetish_price');
            $table->boolean('foot_fetish');
            $table->integer('foot_fetish_price');
            $table->boolean('trampling');
            $table->integer('trampling_price');
            $table->boolean('condom');
            $table->integer('condom_price');
            $table->boolean('dont_condom');
            $table->integer('dont_condom_price');
            $table->boolean('deep');
            $table->integer('deep_price');
            $table->boolean('in_car');
            $table->integer('in_car_price');
            $table->boolean('cunnilingus');
            $table->integer('cunnilingus_price');
            $table->boolean('anilingus');
            $table->integer('anilingus_price');
            $table->boolean('pro');
            $table->integer('pro_price');
            $table->boolean('not_pro');
            $table->integer('not_pro_price');
            $table->boolean('classical');
            $table->integer('classical_price');
            $table->boolean('professional');
            $table->integer('professional_price');
            $table->boolean('relaxing');
            $table->integer('relaxing_price');
            $table->boolean('thai');
            $table->integer('thai_price');
            $table->boolean('urological');
            $table->integer('urological_price');
            $table->boolean('point');
            $table->integer('point_price');
            $table->boolean('erotic');
            $table->integer('erotic_price');
            $table->boolean('sakura_branch');
            $table->integer('sakura_branch_price');
            $table->boolean('strapon');
            $table->integer('strapon_price');
            $table->boolean('golden_shower_issue');
            $table->integer('golden_shower_issue_price');
            $table->boolean('golden_shower_reception');
            $table->integer('golden_shower_reception_price');
            $table->boolean('copro_issue');
            $table->integer('copro_issue_price');
            $table->boolean('copro_reception');
            $table->integer('copro_reception_price');
            $table->boolean('fisting_anal');
            $table->integer('fisting_anal_price');
            $table->boolean('fisting_vaginal');
            $table->integer('fisting_vaginal_price');
            $table->boolean('sex_toys');
            $table->integer('sex_toys_price');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('questionnaire_services');
    }
}
