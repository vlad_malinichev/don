class Media {
  constructor($form) {
    this.$form = $form;
    this.handlePreviewVideo();
  }

  initDropzoneImg() {
    const $count =  $('.media__count-img');
    const $dropImage =  $('.js-input-img');
    const $addBtn = $('.js-btn-add');

    const myDropzoneImage = new Dropzone('.js-input-img', {
      url: this.$form.attr('action'),
      autoProcessQueue: false,
      maxFilesize: 10,
      maxFiles: 10,
      acceptedFiles: '.jpg',
      previewsContainer: '.dropzone-previews',
     });

     myDropzoneImage.on("addedfile", function(file) {

       $(this.element).siblings('.media__items--img').addClass('media__items--show');
       $(file.previewElement).find('.dz-error-mark').on("click", function() {
         myDropzoneImage.removeFile(file);
       });

       $count.html($('.dz-preview').length);

       if(myDropzoneImage.files.length > 10) {
         $dropImage.addClass('media__place--disable');
         myDropzoneImage.files.pop();
         $('.dz-preview').slice(10).remove();
         $count.html($('.dz-preview').length);
       }

       if(myDropzoneImage.files.length >= 10){
         $addBtn.prop('disabled', true)
       }
     });

     myDropzoneImage.on("removedfile", function(file) {
       if(myDropzoneImage.files.length === 0) $('.media__items--img').removeClass('media__items--show');
       $count.html($('.dz-preview').length);

       if(myDropzoneImage.files.length < 10) {
         $dropImage.removeClass('media__place--disable');
         $addBtn.prop('disabled', false);
       }
     });

    $addBtn.on('click', function() {
       $('.js-input-img').trigger('click');
     });

    $('.js-btn-remove').on('click', function() {
      myDropzoneImage.removeAllFiles();
    });
    return myDropzoneImage;
  }

  handlePreviewVideo() {
    const $inputVideo = $('.js-input-video');

    $inputVideo.on('change', function(event) {
      const file = event.target.files[0];
      const fileReader = new FileReader();
      fileReader.onload = function() {
        const blob = new Blob([fileReader.result], {type: file.type});
        const url = URL.createObjectURL(blob);
        const video = document.createElement('video');

        const timeupdate = function() {
          if(video.duration <= 300) {
            if (snapImage()) {
              video.removeEventListener('timeupdate', timeupdate);
              video.pause();
            }
          } else {
            $inputVideo.val('');
            video.removeEventListener('timeupdate', timeupdate);
            video.pause();
          }
        };
        video.addEventListener('loadeddata', function() {
          if(video.duration <= 300) {

            if (snapImage()) {
              video.removeEventListener('timeupdate', timeupdate);
            }
          } else {
            $inputVideo.val('');
          }
        });
        const snapImage = function() {
          const canvas = document.createElement('canvas');
          const itemsWrap = document.querySelector('.media__items--video');
          canvas.width = video.videoWidth;
          canvas.height = video.videoHeight;
          canvas.getContext('2d').drawImage(video, 0, 0, canvas.width, canvas.height);
          const image = canvas.toDataURL();
          const success = image.length > 100000;
          if (success) {
            let wrapImg = document.createElement('div');
            const img = document.createElement('img');
            const btn =  document.createElement('span');
            wrapImg.classList.add('media__preview');
            img.src = image;
            wrapImg.appendChild(img);
            wrapImg.appendChild(btn);
            itemsWrap.classList.add('media__items--show');
            document.querySelector('.media__items--video .media__previews').appendChild(wrapImg).classList.add('media__preview');
            URL.revokeObjectURL(url);

            document.querySelector('.media__preview span').addEventListener('click', function() {
              wrapImg.remove();
              $inputVideo.val('');
              itemsWrap.classList.remove('media__items--show');
            })
          }
          return success;
        };
        video.addEventListener('timeupdate', timeupdate);
        video.preload = 'metadata';
        video.src = url;
        // Load video in Safari / IE11
        video.muted = true;
        video.playsInline = true;
        video.play();
      };
      fileReader.readAsArrayBuffer(file);
    });
  }
}
