$(function () {
    // $(name).select2({
    //   width: '100%',
    //   minimumResultsForSearch: Infinity,
    // })


    // Header

    $('.js-burger').on('click', function () {
        if ($('.header').find('.header__body').hasClass('header__body--open')) {
            $('.header').find('.header__body').fadeOut().removeClass('header__body--open');
        }
        $('.header').find('.header__pannel').slideToggle().toggleClass('header__pannel--open');
    });

    $('.js-search-menu').on('click', function () {
        if ($('.header').find('.header__pannel').hasClass('header__pannel--open')) {
            $('.header').find('.header__pannel').fadeOut().removeClass('header__pannel--open');
        }
        $('.header').find('.header__body').slideToggle().toggleClass('header__body--open');
    });

    $('.js-select-lang').select2({
        width: '100%',
        minimumResultsForSearch: Infinity,
        data: [
            {
                id: 'ru',
                iso: 'ru',
                title: 'russia',
                text: '<span data-lang="ru">Русский</span>',
                default: true,
                selected: $('.wrapper').data('locale') === 'ru'
            },
            {
                id: 'en',
                iso: 'en',
                title: 'english',
                text: '<span data-lang="en">English</span>',
                default: false,
                selected: $('.wrapper').data('locale') === 'en'
            },
        ],
        escapeMarkup: function (markup) {
            return markup;
        },
        theme: 'default select2-lang',
    });

    $('.js-select-lang').on('select2:select', function (e) {
        location.href = '/locale/' + e.params.data.iso;
    });

    $('.header__language').find('.header__language-placeholder').fadeOut(600, function () {
        $('.header__language').find('.header__language-select').fadeIn(500);
    });

    $('.js-select-menu').select2({
        width: '100%',
        minimumResultsForSearch: Infinity,
        data: [{
            id: '1',
            text: 'Индивидуалки<span>878</span>',
        },
            {
                id: '2',
                text: 'Салоны<span>325</span>',
            },
            {
                id: '3',
                text: 'Массажистки<span>215</span>',
            },
            {
                id: '4',
                text: 'Массажные салоны<span>78</span>',
            },
            {
                id: '5',
                text: 'Виртуалки<span>36</span>',
            },
            {
                id: '6',
                text: 'Стрип-клубы<span>50</span>',
            },
            {
                id: '7',
                text: 'Статьи <span>16</span>',
            },
        ],
        escapeMarkup: function (markup) {
            return markup;
        },
        theme: 'default select2-nav',
    });

    $(window).on('resize.menu-dropdown', function () {
        if ($(window).width() > 768) {
            $('.header').find('.header__body').removeAttr('style');
            $('.header').find('.header__pannel').removeAttr('style');
        }
    });

    $('.js-login-modal').magnificPopup({
        type: 'inline',
        midClick: true,
        showCloseBtn: false,
        closeOnBgClick: true,
    });

    // End Header

    // Login

    $('.js-login-tabs').on('click', '.login__tab', function (event) {
        var $tab = $(this);
        var $parent = $tab.closest('.login');
        var $tabs = $parent.find('.login__tab');
        var $items = $parent.find('.login__item');

        if (!$tab.hasClass('.login__tab--active')) {
            $tabs.removeClass('login__tab--active');
            $tab.addClass('login__tab--active');
            $items.hide();
            $items.eq($tab.index()).show();
        }
    });

    function handleClick(className, hideClass, showClass) {
        $(className).on('click', function (event) {
            event.preventDefault();
            var $this = $(this);
            var $parent = $this.closest('.login');
            $parent.find(hideClass).hide();
            $parent.find(showClass).fadeIn();
        });
    };

    handleClick('.js-forgot-password', '.login__wrapper', '.login__recovery');
    handleClick('.js-recovery-back', '.login__recovery', '.login__wrapper');

    // End Login

    // Change tabs for home page

    $('.js-tabs').on('click', '.page__tab', function () {
        var $this = $(this);
        if (!$this.hasClass('page__tab--active')) {
            $('.js-tabs').find('.page__tab').removeClass('page__tab--active');
            $this.addClass('page__tab--active');
            $('.page__item').removeClass('page__item--active');
            $('.page__item').eq($this.index()).addClass('page__item--active');
        }
    });

    // end tabs

    // Autocomplete

    if ($('#autoComplete').length) {
        new autoComplete({
            name: 'food & drinks',
            data: {
                async src() {
                    // Loading placeholder text
                    document
                        .querySelector('#autoComplete')
                        .setAttribute('placeholder', 'Loading...');
                    // Fetch External Data Source
                    var source = await fetch('https://tarekraafat.github.io/autoComplete.js/demo/db/generic.json');
                    var data = await source.json();
                    // Post loading placeholder text
                    document
                        .querySelector('#autoComplete')
                        .setAttribute('placeholder', 'Вся Россия');
                    // Returns Fetched data
                    return data;
                },
                key: ['food', 'cities', 'animals'],
            },
            trigger: {
                event: ['input', 'focus'],
            },
            placeHolder: 'Вся Россия',
            searchEngine: 'strict',
            highlight: true,
            maxResults: 5,
            resultItem: {
                content: function (data, element) {
                    // Prepare Value's Key
                    var key = Object.keys(data).find(function (key) {
                        return data[key] === element.innerText;
                    });
                    // Modify Results Item
                    element.innerHTML = `
            <span class="autoComplete_result_text">${element.innerHTML}</span>
            <span class="autoComplete_result_key">${key}</span>
          `;
                },
            },
            noResults: function (dataFeedback, generateList) {
                // Generate autoComplete List
                generateList(autoCompleteJS, dataFeedback, dataFeedback.results);
                // No Results List Item
                var result = document.createElement('li');
                result.setAttribute('class', 'no_result');
                result.setAttribute('tabindex', '1');
                result.innerHTML = `Не найдено результатов по запросу "${dataFeedback.query}"`;
                document
                    .querySelector(`#${autoCompleteJS.resultsList.idName}`)
                    .appendChild(result);
            },
            onSelection: function (feedback) {
                document.querySelector('#autoComplete').blur();
                // Prepare User's Selected Value
                var selection = feedback.selection.value[feedback.selection.key];
                // Replace Input value with the selected value
                document.querySelector('#autoComplete').value = selection;
            },
        });
    }

    // End Autocomplete

    // Account Dropdown

    $('.js-account-dropdown').on('click', function () {
        $(this)
            .closest('.account-dropdown')
            .toggleClass('account-dropdown--open')
            .find('.account-dropdown__list')
            .slideToggle();
    });

    $(document).off('click.dropdown').on('click.dropdown', function (event) {
        if ($(event.target).closest('.account-dropdown').length) return;
        $('.js-account-dropdown')
            .closest('.account-dropdown')
            .removeClass('account-dropdown--open')
            .find('.account-dropdown__list')
            .slideUp();
    });

    // End Account Dropdown

    // favorites

    $('.js-favorites').off('click.favorites').on('click.favorites', function () {
        $(this).toggleClass('add-favorites--active');
    });

    // End favorites

    // Filter

    $('.js-filter').find('.filter__title').on('click', function () {
        $(this).parent().next().slideToggle().closest('.filter').toggleClass('filter--open');
    });

    $(document).off('click.filter').on('click.filter', function (event) {
        if ($(event.target).closest('.filter').length) return;
        $('.js-filter').find('.filter__body').slideUp().parent().removeClass('filter--open');
    });

    $('.js-choice-price').select2({
        width: '100%',
        minimumResultsForSearch: Infinity,
        data: [
            {
                id: 'hryvnia',
                text: '<span data-price="hryvnia"></span> гривны',
            },
            {
                id: 'dollar',
                text: '<span data-price="dollar"></span> доллары',
            },
            {
                id: 'euro',
                text: '<span data-price="euro"></span> евро',
            },
            {
                id: 'rubles',
                text: '<span data-price="rubles"></span> рубли',
            },
        ],
        escapeMarkup: function (markup) {
            return markup;
        },
        theme: 'default select2-price',
    });

    $('.js-choice-sort').select2({
        width: '100%',
        minimumResultsForSearch: Infinity,
        data: [
            {
                id: 'default',
                text: '<span data-sort="default"></span> по умолчанию',
            },
            {
                id: 'ascending',
                text: '<span data-sort="ascending"></span> по возрастанию цены',
            },
            {
                id: 'descending',
                text: '<span data-sort="descending"></span> по убыванию цены',
            },
            {
                id: 'by-date',
                text: '<span data-sort="by-date"></span> по дате',
            },
        ],
        escapeMarkup: function (markup) {
            return markup;
        },
        theme: 'default select2-sort-by',
    });

    $('.filter__choice').find('.filter__choice-placeholder').fadeOut(600, function () {
        $('.filter__choice').find('.filter__choice-box').fadeIn(500);
    });

    $('.js-select-filter').select2({
        width: '100%',
        minimumResultsForSearch: Infinity,
        theme: 'default select2-filter'
    });

    $('.js-show-options').on('click', function (event) {
        event.preventDefault();
        var $this = $(this);
        $this.toggleClass('filter__button--show');
        $this.closest('.filter__body').find('.filter__options').slideToggle();
    });

    // End Filter


    // input

    $('.js-show-password').on('click', function () {
        var $this = $(this);
        var $parent = $this.closest('.input');
        var $input = $parent.find('.input__control');

        if ($input.prop('type') === 'password') {
            $input.prop('type', 'text');
        } else {
            $input.prop('type', 'password');
        }

        $this.toggleClass('input__eye--visible');
    });

    // end input

    // range

    $(".js-range-slider").each(function (idx, slider) {
        var $root = $(slider).closest('.range-slider');
        var $from = $root.find('.js-range-from');
        var $to = $root.find('.js-range-to');

        $(slider).ionRangeSlider({
            skin: "round",
            type: "double",
            hide_min_max: true,
            hide_from_to: true,
            min: slider.getAttribute('data-from'),
            max: slider.getAttribute('data-to'),
            onStart: function (data) {
                $from.prop("value", data.from);
                $to.prop("value", data.to);
            },
            onChange: function (data) {
                $from.prop("value", data.from);
                $to.prop("value", data.to);
            }
        });

        var instance = $(slider).data("ionRangeSlider");

        $from.on('input', function () {
            var val = $(event.target).prop("value");
            if (val < instance.options.min) {
                val = instance.options.min;
            } else if (val > instance.options.to) {
                val = instance.options.to;
            }
            instance.update({from: val});
        });

        $to.on('input', function () {
            var val = $(event.target).prop("value");
            if (val < instance.options.from) {
                val = instance.options.from;
            } else if (val > instance.options.max) {
                val = instance.options.max;
            }
            instance.update({to: val});
        });
    });

    // end range

    // main card

    var handleVideoOnSlideChange = function (index, video) {
        var $video = $(video);
        if (!$video.prop('paused')) {
            $video.parent().removeClass('swiper-slide__video--play');
            video.pause();
        }
    };

    $('.js-main-card-slider').each(function (index, slider) {
        var $videos = $(slider).find('.js-main-card-video');
        var instance = new Swiper(slider, {
            spaceBetween: 8,
            lazy: true,
            observeParents: true,
            observer: true,
            preloadImages: false,
            slidesPerView: 'auto',
            updateOnWindowResize: true,
            watchOverflow: true,
            watchSlidesVisibility: true,
            pagination: {
                el: '.swiper-pagination',
                type: 'progressbar',
            },
            navigation: {
                nextEl: slider.closest('.main-card').getElementsByClassName('swiper-button-next')[0],
                prevEl: slider.closest('.main-card').getElementsByClassName('swiper-button-prev')[0],
            },
        });
        instance.on('slideChange', function () {
            $videos.each(handleVideoOnSlideChange)
        });
    });

    $('.js-main-card-video')
        .on('click.main-card-video', function () {
            var $this = $(this);
            var isPaused = $this.prop('paused');
            var video = $this.get(0);
            var $parent = $this.parent();

            if (isPaused) {
                video.play();
                $parent.addClass('swiper-slide__video--play');
            } else {
                video.pause();
                $parent.removeClass('swiper-slide__video--play');
            }
        });

    // end main card

    // card

    new Swiper('.js-card-slider', {
        lazy: true,
        observeParents: true,
        observer: true,
        preloadImages: false,
        updateOnWindowResize: true,
        watchOverflow: true,
        watchSlidesVisibility: true,
        pagination: {
            el: '.swiper-pagination',
            type: 'bullets',
            clickable: true,
        },
    });

    // end card

    // profile card

    $('.js-profile-card-slider').each(function (index, slider) {
        new Swiper(slider, {
            spaceBetween: 8,
            lazy: true,
            observeParents: true,
            observer: true,
            preloadImages: false,
            slidesPerView: 'auto',
            updateOnWindowResize: true,
            watchOverflow: true,
            watchSlidesVisibility: true,
            pagination: {
                el: slider.closest('.profile-card__slider').getElementsByClassName('swiper-pagination')[0],
                type: 'bullets',
                clickable: true,
            },
        });
    });

    // end profile card

    // tabs

    new Swiper('.tabs__slider', {
        slidesPerView: 1.1,
        spaceBetween: 8,
        updateOnWindowResize: true,
        observer: true,
        observeParents: true,
        navigation: {
            nextEl: '.tabs__slider-next',
            prevEl: '.tabs__slider-prev',
        },
        pagination: {
            el: '.tabs__slider-pagination',
            type: 'progressbar',
        },
        breakpoints: {
            768: {
                slidesPerView: 2.4,
                spaceBetween: 40,
            },
            1024: {
                slidesPerView: 3.4,
                spaceBetween: 40,
            },
            1260: {
                slidesPerView: 4,
                spaceBetween: 40,
            },
        },
    });

    $('.tabs__btn').on('click', function () {
        var type = $(this).data('type');
        $('.tabs__btn').removeClass('tabs__btn--active');
        $(this).addClass('tabs__btn--active');
        $('.tabs__content').addClass('tabs__content--hidden');
        $('.tabs__content').filter(`[data-type='${type}']`).removeClass('tabs__content--hidden');
    });

    // end tabs

    //------------------------------------------------------------------------ form search

    if ($('.js-form-search').length) {
        // - Данные
        var $location = JSON.parse(JSON.stringify($('.js-form-search').data('location')));
        var $country = JSON.stringify($location.map(function (country) {
            return {id: country.id, name: country.name};
        }));

        // - Заполнитель для поиска
        function placeholderForSearch(placeholder) {
            $('.js-search-region').attr('placeholder', `Вся ${placeholder}`);
        };

        // - Инициализация выбора страны
        $('.js-select-country').select2({
            width: '100%',
            minimumResultsForSearch: Infinity,
            theme: 'default select2-country',
        });

        // - Отображения списка стран
        function renderCountry(country) {
            $('.js-country-selection').attr('data-cities', country);
            $('.js-country-selection')
                .data('cities')
                .forEach(function (country) {
                    var option = new Option(country.name, country.name);
                    $(option).attr('data-country-id', country.id);
                    $('.js-country-selection').append(option);
                });
            placeholderForSearch($('.js-country-selection').val());
        };
        renderCountry($country);

        // - Отображение списка Городов
        function renderCities(cities) {
            $('.form-search__list').empty();
            cities.forEach(function (city) {
                var $li = $('<li>', {
                    class: 'form-search__item',
                    text: city.name,
                    'data-city-id': city.id,
                    click() {
                        $('.js-search-region').val(`${city.name}, ${$('.js-form-search').find('.form-search__region').data('region-name')}, ${$('.js-country-selection').data('country-name')}`);
                        $('.js-search-form').fadeOut();
                    },
                });
                $('.form-search__list').append($li);
            });
        };

        // - Обработка отображения списка Регионов
        function renderRegions(id) {
            function handleClickRegion(region) {
                event.stopPropagation();
                $('.js-form-search').find('.form-search__region')
                    .attr('data-region-name', region.name)
                    .fadeIn()
                    .find('.form-search__region-name')
                    .text(region.name);
                $('.js-form-search').find('.form-search__whole-area')
                    .fadeIn()
                    .off('click')
                    .on('click', function () {
                        $('.js-search-region').val(`${region.name}, ${$('.js-country-selection').data('country-name')}`);
                        $('.js-search-form').fadeOut();
                    });
                renderCities(region.cities);
            };

            function createRegion(region) {
                return $('<li>', {
                    class: 'form-search__item',
                    text: region.name,
                    'data-region-id': region.id,
                    click() {
                        handleClickRegion(region);
                    },
                }).append($('<span>', {class: 'form-search__item-arrow'}));
            };

            $location.forEach(function (country) {
                if (parseInt(id, 10) === country.id) {
                    $('.js-country-selection').attr('data-country-name', country.name);
                    $('.form-search__list').empty();
                    country.regions.forEach(function (region) {
                        $('.form-search__list').append(createRegion(region));
                    });
                }
            });
        };

        // - Обработка выбора страны
        function handleSelectCountry() {
            $('.js-country-selection').on('change', function (event) {
                var idCountry = $(this.options[this.options.selectedIndex]).data('country-id');
                renderRegions(idCountry);
                $('.js-form-search').find('.form-search__region').hide().find('.form-search__region-name').text('');
                $('.js-form-search').find('.form-search__whole-area').hide();
                $('.js-search-region').val('');
                placeholderForSearch($(this).val());
            });
        };
        handleSelectCountry();

        // - Отображение регионов при заргузке страницы
        function initialRegions() {
            renderRegions($('.js-country-selection').get(0).options[$('.js-country-selection').get(0).options.selectedIndex].getAttribute('data-country-id'))
        };
        initialRegions();

        // - Сбросить форму к регионам
        function resetFormToRegion() {
            initialRegions();
            $('.js-form-search').find('.form-search__region').hide().find('.form-search__region-name').text('');
            $('.js-form-search').find('.form-search__whole-area').hide();
        };

        // - Обработка сброса региона (возврат к выбору региона)
        function handleCancelRegion() {
            $('.js-form-search').find('.form-search__region-cancel').off('click').on('click', function () {
                resetFormToRegion();
                $('.js-search-region').val('');
            })
        };
        handleCancelRegion();

        // - Сброс формы до начального состояния, после закрытия формы (потери фокуса)
        // $('.js-form-search').off('reset-form-search').on('reset-form-search', resetFormToRegion);

        // - Обработка поиска по всей области
        function handleSearchWholeArea() {
            $('.js-form-search').find('.form-search__whole-area').off('click').on('click', function () {
                $('.js-search-region').attr('value', $(this).data('region'));
                $('.js-search-form').fadeOut();
                resetFormToRegion();
            });
        };
    }

    //------------------------------------------------------------------------ end form search

    // Search

    $('.js-search-region').off('click').on('click', function () {
        if ($(this).val().length) return;
        $('.js-search-form').fadeIn(300);
    });

    $('.js-search-region').off('input').on('input', function () {
        if ($(this).val().length) $('.js-search-form').fadeOut(200);
        else $('.js-search-form').fadeIn(300);
    });

    $('.js-search-back').on('click', function () {
        $('.js-search-form').fadeOut();
    });

    $(document).off('click.form-search').on('click.form-search', function (event) {
        if ($(event.target).closest('.js-search-form, .js-search-region').length) return;
        // $('.js-form-search').trigger('reset-form-search');
        $('.js-search-form').fadeOut(200);
    });

    // end search


    // timer

    $('.js-timer').countdown('06/01/2021', function (event) {
        var template = `
      <ul class="timer">
        <li class="timer__item"><span class="timer__title">дней</span><div class="timer__value">%D</div></li>
        <li class="timer__item"><span class="timer__title">часов</span><div class="timer__value">%H</div></li>
        <li class="timer__item"><span class="timer__title">минут</span><div class="timer__value">%M</div></li>
        <li class="timer__item"><span class="timer__title">секунд</span><div class="timer__value">%S</div></li>
      </ul>
    `;
        $(this).html(event.strftime(template));
    });

    // end timer

    // check page

    $('.js-select-check').select2({
        width: '100%',
        minimumResultsForSearch: Infinity,
    });

    // end check page

    // feedback form

    $('.js-select-topic').select2({
        width: '100%',
        minimumResultsForSearch: Infinity,
        theme: 'default select2-country',
    })

    // end feedback form

    // price page

    $('.js-select').select2({
        width: '100%',
        minimumResultsForSearch: Infinity,
        theme: 'default select2-white-theme',
    });

    $('.js-select-calc').select2({
        width: '100%',
        minimumResultsForSearch: Infinity,
    });

    $('.js-select-search').select2({
        width: '100%',
        theme: 'default select2-with-search',
        minimumResultsForSearch: 1,
    });

    // end price page

    // questionnaire page

    new Swiper('.slider-gift__container', {
        slidesPerView: 2,
        spaceBetween: 16,
        navigation: {
            nextEl: '.slider-gift__next',
            prevEl: '.slider-gift__prev',
        },
        breakpoints: {
            480: {
                slidesPerView: 2,
                spaceBetween: 16,
            },
            768: {
                slidesPerView: 5,
                spaceBetween: 8,
            },
            1280: {
                slidesPerView: 3
            }
        }
    });

    // slider

    if ($('.slider').length) {
        var $count = $('.slider').find('.slider__gallery-count');
        var $slideLength = $('.slider').find('.slider__gallery-max');

        var galleryThumbs = new Swiper('.slider__thumbs', {
            spaceBetween: 8,
            slidesPerView: 5,
            direction: 'vertical',
        });

        function destroySlider() {
            if ($(window).outerWidth() <= 767) {
                if (galleryThumbs) {
                    galleryThumbs.destroy();
                    galleryThumbs = null;
                }
            } else {
                galleryThumbs = new Swiper('.slider__thumbs', {
                    spaceBetween: 8,
                    slidesPerView: 5,
                    direction: 'vertical',
                });
            }
        };
        destroySlider();

        $(window).on('resize.questionnaire', function () {
            destroySlider();
        });

        var galleryTop = new Swiper('.slider__gallery', {
            // spaceBetween: 10,
            navigation: {
                nextEl: '.slider__gallery-next',
                prevEl: '.slider__gallery-prev',
            },
            pagination: {
                el: '.slider__gallery-pagination',
                type: 'progressbar',
            },
            thumbs: {
                swiper: galleryThumbs,
            },
            breakpoints: {
                768: {
                    width: 578,
                },
                1260: {
                    width: 490,
                },
            },
        });

        var slideCount = galleryTop.$wrapperEl[0].childElementCount < 9
            ? 0 + `${galleryTop.$wrapperEl[0].childElementCount}` : galleryTop.$wrapperEl[0].childElementCount;

        $count.html(0 + `${galleryTop.activeIndex + 1}`);
        $slideLength.html(slideCount);

        galleryTop.on('slideChange', function () {
            var countHtml = galleryTop.activeIndex < 9 ? 0 + `${galleryTop.activeIndex + 1}` : galleryTop.activeIndex + 1;
            $count.html(countHtml);
        });
    }

    // end slider

    // reviews

    $('.reviews').find('.js-checkbox-container').each(function (i, item) {
        var $checkbox = $(item).find('.js-checkbox');
        var rating = $(item).find('.js-reviews-input');

        $checkbox.each(function (index, item) {
            $(item).on('click', function () {
                var target = index;
                rating.val(index + 1);
                $checkbox.each(function (index, input) {
                    index <= target ? input.checked = true : input.checked = false;
                });
            });
        });
    });

    $('.reviews').find('.js-radio-container').each(function (index, item) {
        var $radio = $(item).find('.js-radio');
        var input = $(item).find('.js-reviews-radio');

        $radio.each(function (index, item) {
            var $thisRadio = $(item);
            $thisRadio.on('change', function (e) {
                var target = $(e.currentTarget);
                $radio.not(target).prop('checked', false);
                input.val(target.val())
            })
        });
    })
    // end reviews

    // stickybits
    if ($('.js-sticky').length) {
        stickybits('.js-sticky', {stickyBitStickyOffset: 20});
    }
    // end stickybits

    // bar on mobile

    if ($('.questionnaire__bars').length) {
        function handleBars(e) {
            var $content = $('.questionnaire__bars');

            $.fn.isInViewport = function () {
                var elementTop = $(this).offset().top;
                var elementBottom = elementTop + $(this).outerHeight();
                var viewportTop = $(window).scrollTop();
                var viewportBottom = viewportTop + $(window).height();

                if ($(this).get(0) === $('.questionnaire__breadcrumbs').get(0)) {
                    return elementBottom < viewportTop && elementTop < viewportBottom;
                } else if ($(this).get(0) === $('.questionnaire__bottom').get(0)) {
                    return elementBottom > viewportTop && elementTop < viewportBottom;
                }
            };

            if ($('.questionnaire__breadcrumbs').isInViewport()) {
                $content.addClass('questionnaire__bars--show')
            } else {
                $content.removeClass('questionnaire__bars--show')
            }

            if ($('.questionnaire__bottom').isInViewport()) {
                $content.addClass('questionnaire__bars--top')
            } else {
                $content.removeClass('questionnaire__bars--top')
            }
        };

        function initBars() {
            if ($(window).outerWidth() <= 640) {
                $(window).on('scroll', handleBars);
            }
            if ($(window).outerWidth() > 640) {
                $('.questionnaire__bars').removeClass('questionnaire__bars--show questionnaire__bars--top');
                $(window).off('scroll');
            }
        };
        initBars();

        $(window).on('resize', initBars);
    }

    // end bar on mobile

    // end questionnaire page


    // popups

    // messages popup
    $('.js-message-popup').magnificPopup({
        type: 'inline',
        mainClass: 'gifts-popup',
        callbacks: {
            open: function () {
                $('.js-popup-file').off().on('change', function (e) {
                    if (e.target.files[0]) {
                        $(e.target).next().text(e.target.files[0].name);
                    }
                });
            }
        }
    });

    // popup not auth
    $('.js-not-auth-popup').magnificPopup({
        type: 'inline',
        mainClass: 'gifts-popup',
    });

    // balance popup
    $('.js-balance-popup').magnificPopup({
        type: 'inline',
        mainClass: 'gifts-popup',
    });
    $('.js-select-amount').select2({
        width: '100%',
        minimumResultsForSearch: Infinity,
        theme: 'default select2-white-theme',
    });

    // popup tip
    $('.js-tip-popup').magnificPopup({
        type: 'inline',
        mainClass: 'gifts-popup',
    });

    $('.js-select-amount').select2({
        width: '100%',
        minimumResultsForSearch: Infinity,
        theme: 'default select2-white-theme',
    })

    // gift popup
    $('[data-type="gifts"]').magnificPopup({
        type: 'inline',
        mainClass: 'gifts-popup',
    });

    // popup dispatch
    $('[data-type="dispatch"]').magnificPopup({
        type: 'inline',
        mainClass: 'gifts-popup',
    });

    function initFilterContent(price) {
        $('.popup__inner').addClass('popup__inner--hidden');
        $('.popup__header-left').addClass('popup__header-left--present');
        $('.popup__header-title span').html(price);
        $('.popup__list').filter(`[data-price='${price}']`).removeClass('popup__list--hidden');
    };
    $('.js-gifts-show').on('click', function (e) {
        e.preventDefault();
        initFilterContent($(this).data('price'));
    });
    $('.js-gifts-hidden, [data-type="submit"]').on('click', function () {
        $('.popup__inner').removeClass('popup__inner--hidden');
        $('.popup__header-left').removeClass('popup__header-left--present');
        $('.popup__list').addClass('popup__list--hidden');
    });

    // popup submit
    $('[data-type="submit"]').magnificPopup({
        type: 'inline',
        mainClass: 'gifts-popup',
        callbacks: {
            beforeOpen: function () {
                console.log(1);
                $('.popup__inner').removeClass('popup__inner--hidden');
                $('.popup__header-left').removeClass('popup__header-left--present');
                $('.popup__list').addClass('popup__list--hidden');
            }
        }
    });
    // reset
    $('.js-back').magnificPopup({
        type: 'inline',
        mainClass: 'gifts-popup',
    });

    // end popups

    // questionnaire gallery
    $('.js-gallery').magnificPopup({
        type: 'image',
        mainClass: 'popup-gallery',
        gallery: {
            enabled: true,
            arrowMarkup: '<button title="%title%" type="button" class="mfp-arrow mfp-arrow-%dir% popup-gallery__%dir%"></button>'
        },
        callbacks: {
            elementParse(item) {
                if (item.el.hasClass('js-gallery-video')) {
                    item.type = 'iframe';
                } else {
                    item.type = 'image';
                }
            },
        },
    });
    // end questionnaire gallery


    // ############################################################################## Cabinet ########################################################## //

    // dropdown

    $('.js-cabinet-navigation').select2({
        width: '100%',
        minimumResultsForSearch: Infinity,
        theme: 'default theme-cabinet-dropdown',
    });
    $('.js-settings-tabs').select2({
        width: '100%',
        minimumResultsForSearch: Infinity,
        theme: 'default theme-cabinet-dropdown',
    });

    // $('.js-settings-tabs').on('change', function(event) {
    //   var $select = $(event.target);
    //   var $parent = $select.closest('.settings');
    //   var $panels = $parent.find('.settings__item');
    //   var selectedIndex = $select.prop('selectedIndex');

    //   $parent.find('.settings__tab').removeClass('settings__tab--active').eq(selectedIndex).addClass('settings__tab--active');
    //   $panels.removeClass('settings__item--active').eq(selectedIndex).addClass('settings__item--active');
    // });

    // end dropdown

    // filter

    $('.js-filter-toggle').on('click', function () {
        var $toggle = $(this);
        var $parent = $toggle.closest('.cabinet-filter');
        var $body = $parent.find('.cabinet-filter__body');

        $parent.toggleClass('cabinet-filter--show');
        $body.fadeToggle(300);

        $(document).on('click.cabinet-filter', function (event) {
            if ($(event.target).closest('.cabinet-filter').length) return;
            $parent.removeClass('cabinet-filter--show');
            $body.fadeOut(200);
            $(document).off('click.cabinet-filter');
        });
    });

    // end filter

    // balance replenishment page

    $('.js-select-balance').select2({
        width: '100%',
        minimumResultsForSearch: Infinity,
        theme: 'default select2-white-theme',
    })

    // end balance replenishment page

    // select country

    $('.js-country').select2({
        width: '100%',
        minimumResultsForSearch: Infinity,
        theme: 'default select2-white-theme'
    })

    // end select country

    // banners feature

    $('.js-feature-file > input').on('change', function () {
        var $parent = $(this).parent();
        var $root = $parent.closest('.banners-feature');
        var $image = $root.find('.banners-feature__image');
        var $span = $parent.find('span');
        var file = $parent.find('input').get(0).files[0];
        if (file) {
            $image.attr('src', URL.createObjectURL(file));
            $span.text(file.name);
        }
    });

    // end banners feature

    // banners new

    $('.js-select-all-sections').on('click', function () {
        var $checkbox = $(this);
        var $parent = $checkbox.closest('.banners-new__sections');
        var $checkboxs = $parent.find('tbody .checkbox__input');
        if ($checkbox.prop('checked')) {
            $checkboxs.prop('checked', true);
        } else {
            $checkboxs.prop('checked', false);
        }
    });

    $('.js-select-section').on('click', function () {
        var $buttonSelectAll = $(this).closest('.banners-new__sections').find('.js-select-all-sections');
        var isAllChecked = $('.js-select-section').filter(function (index, element) {
            return $(element).prop('checked');
        }).length;
        var totalLength = $('.js-select-section').length;

        if (totalLength === isAllChecked) {
            $buttonSelectAll.prop('checked', true);
        } else {
            $buttonSelectAll.prop('checked', false);
        }
    });

    // end banners new

    // favorites parameters

    if ($('.favorites-search').length) {
        $('.js-toggle-parameters').on('click', function () {
            var $button = $(this);
            $button.next().get(0).style.setProperty('--width-parameters-list', `${$button.closest('.favorites-search__item').width()}px`);
            $button.parent().toggleClass('favorites-parameters--open');
            $button.next().slideToggle();
        });

        $(window).on('resize', function () {
            $('.js-toggle-parameters').next().each(function (index, element) {
                element.style.setProperty('--width-parameters-list', `${$(element).closest('.favorites-search__item').width()}px`);
            });
        });
    }

    // end favorites parameters

    // reviews send

    $('.js-edit').on('click', function () {
        $(this).parents('.reviews-item').find('.js-textarea').prop('disabled', false).focus();
    })

    // end reviews send

    // settings page

    $('.settings-personal__file > input').on('change', function () {
        var $root = $(this).parent();
        var url = URL.createObjectURL($root.find('.profile-picture__input').get(0).files[0]);
        $root.find('.profile-picture__img').attr('src', url);
    });

    $('.js-settings').find('.settings__tab').on('click', function () {
        var $tab = $(this);
        var selectedIndex = $tab.index();
        if ($tab.hasClass('settings__tab--active')) return;
        $('.js-settings').find('.settings__tab').removeClass('settings__tab--active');
        $tab.addClass('settings__tab--active');
        $('.js-settings').find('.settings__item').removeClass('settings__item--active').eq(selectedIndex).addClass('settings__item--active');

        $('.js-settings').find('.js-settings-tabs')
            .val('US')
            .prop('selectedIndex', selectedIndex)
            .trigger('change.select2');
    });

    // end settings page

    // messenger page

    $('.js-messege-checked').find('input').on('change', function () {
        if ($(this)[0].checked === true) {
            $('.messenger-list__item').find('input').prop('checked', true);
        } else {
            $('.messenger-list__item').find('input').prop('checked', false);
        }
    });

    $('.messenger-search').find('.js-show-sort').on('click', function () {
        $('.messenger-search').find('.messenger-search__items').toggleClass('messenger-search__items--show')
    });

    $('.messenger-search').find('.js-drop').on('click', function () {
        $('.messenger-search').find('.messenger-search__drop').toggleClass('messenger-search__drop--show')
    });


    $('.messenger-item').find('.js-show-sort').on('click', function () {
        $('.messenger-item__show').toggleClass('messenger-item__show--open');
    })
    // end messenger page

    // new questionnaire

    // $(name).select2({
    //   width: '100%',
    //   minimumResultsForSearch: Infinity,
    // })

    // var formSelect = new Select('.js-select', {
    //   theme: 'default',
    // });

    // var formSelectSearch = new Select('.js-select-search', {
    //   theme: 'default',
    //   minimumResultsForSearch: 1,
    // });

    $('.js-checkbox-all-day').on('click', function (e) {
        var $radioWrap = $('.js-radio');

        if ($(this).prop('checked') === true) {
            $radioWrap.each(function (index, radio) {
                var $radio = $(radio);
                $radio.prop('checked', false).attr('disabled', 'disabled');
            })
        } else {
            $radioWrap.each(function (index, radio) {
                var $radio = $(radio);
                $radio.removeAttr('disabled');
            })
        }
    });

    function handleDisabledInput(checkbox, parentElem, input) {
        $(checkbox).on('click', function (e) {
            var $this = $(this);
            var checked = $this.prop('checked');
            var $input = $this.parents(parentElem).find(input);

            if (checked === true) {
                $input.prop("disabled", true).val('');
            } else {
                $input.prop("disabled", false)
            }
        });
    };
    handleDisabledInput('.js-checkbox .checkbox__input', '.questionnaire-form__elem', '.questionnaire-form__elem-input input');
    handleDisabledInput('.js-time-disabled .checkbox__input', '.questionnaire-form__clock', '.questionnaire-form__input-time');

    $('.js-select-form').select2({
        width: '100%',
        minimumResultsForSearch: Infinity,
    });

    $('.js-select-form-search').select2({
        theme: 'default',
        width: '100%',
        minimumResultsForSearch: 1,
    });


    // time
    var valueHoursWith = $('.js-time-hours-with').val();
    var valueMinutesWith = $('.js-time-minutes-with').val();
    var valueHoursTo = $('.js-time-hours-to').val();
    var valueMinutesTo = $('.js-time-minutes-to').val();

    function handleValueInput() {
        $inputTime.val(`c ${valueHoursWith}:${valueMinutesWith} до ${valueHoursTo}:${valueMinutesTo}`);
    };

    $('.js-time').each(function (index, item) {
        var $target = $(item);
        var $targetInput = $target.find('.js-time-input');

        $targetInput.on('input', function () {
            var $thisInput = $(this);
            switch (true) {
                case $thisInput.hasClass('js-time-hours-with') :
                    valueHoursWith = $thisInput.val();
                    handleValueInput();
                    break;
                case $thisInput.hasClass('js-time-minutes-with') :
                    valueMinutesWith = $thisInput.val();
                    handleValueInput();
                    break;
                case $thisInput.hasClass('js-time-hours-to'):
                    valueHoursTo = $thisInput.val();
                    handleValueInput();
                    break;
                case $thisInput.hasClass('js-time-minutes-to') :
                    valueMinutesTo = $thisInput.val();
                    handleValueInput();
                    break;
            }
        });
    })
    // end time

    //  services
    $('.js-select-services').select2({
        width: '100%',
        minimumResultsForSearch: Infinity,
    });
    $('.js-select-services').prop("disabled", true);

    $('.services').find('.js-checkbox .checkbox__input').on('click', function () {
        var $this = $(this);
        var checked = $this.prop('checked');
        var $select = $this.parents('.services__elem').find('.js-select-services');

        if (checked === true) {
            $select.prop("disabled", false)
        } else {
            $select.prop("disabled", true)
        }
    });
    //  end services

    // media

    if ($('.js-form').length) {
        new Media($('.js-form')).initDropzoneImg();
    }

    // end media

    $('.js-clear-form').on('click', function () {
        myDropzoneImage.removeAllFiles();
        $('.js-form').trigger('reset');
        $('.js-input-hidden').val('');
        $('.js-select-services').prop('disabled', true).trigger('change.select2');
        $('.js-select-form').trigger('change.select2');
        $('.js-select-form-search').trigger('change.select2');
        $('.media__preview').remove();
        $('.media__items--video').removeClass('media__items--show');
    })

    // end new questionnaire

    // tippy

    if ($('[data-controls]').length) {
        tippy('[data-controls]', {
            theme: 'chat',
            placement: 'bottom-start',
            arrow: false,
        });

        tippy('[data-controls-mobile]', {
            theme: 'chat',
            placement: 'top-start',
            arrow: false,
        });
    }

    // end tippy

    // chat page

    $('.js-show-actions').on('click', function () {
        $(this).next().fadeToggle();
    });

    $(document).on('click', function (event) {
        if (!event.target.closest('.chat-actions')) {
            $('.js-show-actions').next().fadeOut();
        }
    });

    // end chat page

    // emojionearea

    if ($('#emojiarea').length) {
        window.emojioneVersion = '3.1.2';

        $('#emojiarea').emojioneArea({
            placeholder: 'Напишите сообщение...',
            standalone: false,
            search: false,
            buttonTitle: '',
            tones: false,
            inline: false,
            pickerPosition: 'top',
            recentEmojis: false,
            hidePickerOnBlur: false,
            events: {
                click(editor, event) {
                    $('#emojiarea')[0].emojioneArea.hidePicker();
                },
            },
            filters: {
                recent: false,
                smileys_people: {
                    icon: 'yum',
                },
                animals_nature: false,
                travel_places: false,
                activity: false,
                food_drink: false,
                objects: false,
                symbols: false,
                flags: false,
            },
        });
    }

    // end emojionearea

    // simplebar

    if ($('[data-simplebar]').length || $('.chat').length) {
        var instanceBody = SimpleBar.instances.get(document.querySelector('.chat__body')).getScrollElement();
        instanceBody.scrollTop = instanceBody.scrollHeight;
    }

    // end simplebar

    // gifts modal

    $('.js-gifts-modal').magnificPopup({
        type: 'inline',
        midClick: true,
        showCloseBtn: false,
    });

    // end gifts modal
})
