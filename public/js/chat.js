  // Variables

  var $window = $(window);
  var $chat = $('.chat');
  var $messagesSection = $chat.find('[data-section="messages"]');
  var $dialogSection = $chat.find('[data-section="dialog"]');
  var $messageOut = $('.message-out');
  var $messageEnter = $('.message-enter');
  var outLength = 0;
  var enterLength = 0;
  var totalSelected = 0;


  // Helpers

  function hideInfoOnDesktop () {
    $('[data-section="info"], [data-section="actions"]').fadeOut(200, function() {
      $('[data-section="selected-messages"], [data-section="controls"]').fadeIn();
    });
  };
  function showInfoOnDesktop () {
    $('[data-section="selected-messages"], [data-section="controls"]').fadeOut(200, function() {
      $('[data-section="info"], [data-section="actions"]').fadeIn();
    });
  };
  function hideInfoOnMobile () {
    $('[data-section="form"]').fadeOut(200, function() {
      $('[data-section="controls-mobile"]').fadeIn();
    });
  };
  function showInfoOnMobile () {
    $('[data-section="controls-mobile"]').fadeOut(200, function() {
      $('[data-section="form"]').fadeIn();
    });
  };
  function displayNumberSelectedMessages () {
    totalSelected = outLength + enterLength;
    $('[data-section="selected-messages"]').find('.value').text(totalSelected);
  };
  function handleSelectedOnDesktop () {
    if (enterLength || outLength) {
      hideInfoOnDesktop();
    } else {
      showInfoOnDesktop();
    }
  };
  function handleSelectedOnMobile () {
    if (enterLength || outLength) {
      $('[data-section="check"').fadeIn().css('box-shadow', 'inset 0 0 1px 1px #ff9900');
      hideInfoOnMobile();
    } else {
      $('[data-section="check"').fadeOut().css('box-shadow', 'none');
      showInfoOnMobile();
    }
  };
  function resetSelected () {
    outLength = 0;
    enterLength = 0;
    $messageOut.removeClass('message-out--checked');
    $messageEnter.removeClass('message-enter--checked');
    showInfoOnDesktop();
  };

  // Events

  function onOut () {
    $messageOut.on('click', function () {
      $(this).toggleClass('message-out--checked');
      outLength = $('.message-out--checked').length;

      if ($window.width() > 767) {
        displayNumberSelectedMessages();
        handleSelectedOnDesktop();
      } else {
        handleSelectedOnMobile();
      }
    });
  };
  function onEnter () {
    $messageEnter.on('click', function () {
      $(this).toggleClass('message-enter--checked');
      enterLength = $('.message-enter--checked').length;

      if ($window.width() > 767) {
        displayNumberSelectedMessages();
        handleSelectedOnDesktop();
      } else {
        handleSelectedOnMobile();
      }
    });
  };
  function onDeleteSelected () {
    $('.js-delete-selected').on('click', function() {
      $('.message-enter--checked').remove();
      $('.message-out--checked').remove();
      resetSelected();
    });
  };


  // Mobile

  function handleDialogSection () {
    $('[data-type="chat"]').on('click', function() {
      $messagesSection.fadeOut(200, function() {
        $dialogSection.fadeIn();
        $chat.css('height', '100vh');
        $('html, body')
          .animate({
            scrollTop: $chat.offset().top,
          }, 600)
          .css('overflow', 'hidden');
      });
    });
  };
  function handleMessagesSection () {
    $('.js-back-to-messages').on('click', function() {
      $dialogSection.fadeOut(200, function() {
        $messagesSection.fadeIn();
        $chat.css('height', 'initial');
        $('html, body').css('overflow', 'visible');
      });
    });
  };


  // Initial

  onOut();
  onEnter();
  onDeleteSelected();

  function initial () {
    if ($window.width() < 767) {
      handleDialogSection();
      handleMessagesSection();

      resetSelected();
    } else {
      $messagesSection.removeAttr('style');
      $dialogSection.removeAttr('style');
      $('[data-type="chat"]').off();
      resetSelected();
      $('[data-section="check"').css('box-shadow', 'none');
      showInfoOnMobile();
    }
  };

  initial();
  $window.on('resize', initial);
