<?php

namespace App\DTO;

class QuestionnaireDto extends Dto
{

    public $type;
    public $name;
    public $phone;
    public $viber;
    public $whatsapp;
    public $telegram;
    public $time;
    public $aroundClock;

    public $country_id;
    public $region_id;
    public $city_id;
    public $district_id;
    public $subway_id;

    public $sex;
    public $age;
    public $height;
    public $weight;

}
