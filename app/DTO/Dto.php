<?php

namespace App\DTO;

abstract class Dto
{

    public function setFromArray(array $array, bool $isUseSetters = true)
    {
        $setFields = [];
        foreach ($array as $key => $value) {
            $setterName = 'set' . ucfirst($key);
            if ($isUseSetters && method_exists($this, $setterName)) {
                $this->$setterName($value);
                $setFields[$key] = [$setterName => $value];
                continue;
            }
            if (property_exists($this, $key)) {
                $this->$key = $value;
                $setFields[$key] = [$key => $value];
            }
        }
        return $setFields;
    }

    public function setFromObject(object $object, bool $isUseSetters = true)
    {
        return $this->setFromArray((array) $object, $isUseSetters);
    }

}
