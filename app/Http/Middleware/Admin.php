<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Auth\Middleware\Authenticate as Middleware;
use Illuminate\Support\Facades\Auth;

class Admin extends Middleware
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param \Illuminate\Http\Request $request
     * @param Closure $next
     * @param string[] ...$guards
     * @return string|null
     */
    public function handle($request, Closure $next, ...$guards)
    {
        // проверяем принадлежность пользователя
        if (Auth::check() && Auth::user()) {
            return $next($request);
        }

        return redirect('/');
    }
}
