<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BlacklistStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id' => 'integer',
            'category_id' => 'integer:required',
            'country_id' => 'integer:required',
            'region_id' => 'integer:required',
            'city_id' => 'integer:required',
            'sex' => 'required',
            'phone' => 'required',
            'comment' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'sex.required' => 'Выберите пол'
        ];
    }
}
