<?php

namespace App\Http\Controllers;

use App\Helpers\EloquentHelper;
use App\Services\Questionnaire\SearchService;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function query(Request $request)
    {
        return $request->getQueryString();
    }

    public function search(Request $request)
    {
        $filters = $request->get('filters');
        $services = $request->get('services');
        $prices = $request->get('prices');

        $searchService = new SearchService();
        if ($filters) $searchService->setFilters($filters);
        if ($services) $searchService->setServices($services);
        if ($prices) $searchService->setPrices($prices);

        $searchService->searchByAll();

        //dd($filters, $services, $prices, EloquentHelper::getSqlWithBindings($searchService->getQueryBuilder()));
        dd($filters, $services, $prices, $searchService->getQueryBuilder()->get());
    }
}
