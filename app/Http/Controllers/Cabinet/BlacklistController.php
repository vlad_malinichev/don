<?php

namespace App\Http\Controllers\Cabinet;

use App\Http\Controllers\Controller;
use App\Http\Requests\BlacklistStoreRequest;
use App\Models\Blacklist;
use App\Models\Questionnaire;
use App\Services\BlacklistService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class BlacklistController extends Controller
{

    protected $blacklistService;

    public function __construct(BlacklistService $blacklistService)
    {
        $this->blacklistService = $blacklistService;
        $this->middleware('auth');
    }

    public function index()
    {
        $records = Blacklist::all();
        return view('cabinet.blacklist.index', compact('records'));
    }

    public function my()
    {
        $records = Blacklist::where('user_id', Auth::id())->get();
        return view('cabinet.blacklist.index', compact('records'));
    }

    public function create()
    {
        return view('cabinet.blacklist.create');
    }

    public function store(BlacklistStoreRequest $request)
    {
        $validated = $request->validated();

        $post = $request->post();
        $post['user_id'] = Auth::id();
        if ($add = $this->blacklistService->add($post)) {
            return redirect()->back()->with('success', 'Запись успешно добавлена');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Questionnaire  $questionnaire
     * @return \Illuminate\Http\Response
     */
    public function show(Questionnaire $questionnaire)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Questionnaire  $questionnaire
     * @return \Illuminate\Http\Response
     */
    public function edit(Questionnaire $questionnaire)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Questionnaire  $questionnaire
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Questionnaire $questionnaire)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Questionnaire  $questionnaire
     * @return \Illuminate\Http\Response
     */
    public function destroy(Questionnaire $questionnaire)
    {
        //
    }
}
