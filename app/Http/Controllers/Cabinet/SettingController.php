<?php

namespace App\Http\Controllers\Cabinet;

use App\Http\Controllers\Controller;
use App\Http\Requests\QuestionnaireStoreRequest;
use App\Models\Blacklist;
use App\Models\Questionnaire;
use App\Services\BlacklistService;
use App\Services\QuestionnaireService;
use App\Services\SettingService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SettingController extends Controller
{

    protected $settingService;

    public function __construct(SettingService $settingService)
    {
        $this->settingService = $settingService;
        $this->middleware('auth');
    }

    public function edit(Request $request)
    {
        return view('cabinet.setting.edit');
    }

    public function update(Request $request)
    {
        $post = $request->post();
        $post['user_id'] = Auth::id();
        if ($this->settingService->add($post)) {
            return redirect()->back()->with('success', 'Запись успешно добавлена');
        }
    }
}
