<?php

namespace App\Http\Controllers;

use App\Models\Page;

class PageController extends Controller
{

    public function privacy()
    {
        return view('page.privacy');
    }

    public function feedback()
    {
        return view('page.feedback');
    }

    public function customerInstructions()
    {
        return view('page.customer-instructions');
    }

    public function modelInstructions()
    {
        return view('page.model-instructions');
    }

    public function show(string $slug)
    {
        $page = Page::where('slug', $slug)->first();

        return view('page.show', compact('page'));
    }

}
