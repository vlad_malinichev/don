<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\RegionResource;
use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use App\Models\Region;
use Illuminate\Http\Response;

class RegionController extends Controller
{

    protected $region;

    public function __construct(Region $region)
    {
        $this->region = $region;
    }

    /**
     * Get Regions
     *
     * @return \Illuminate\Http\Response
     */
    public function index($countryId)
    {
        $regions = Region::where('country_id', $countryId)->get();

        if ($regions) {
            $data['message'] = config('app_messages.ShowProvinceList');
            return $data = $regions;
        } else {
            $data['message'] = config('app_messages.NoProvinceFound');
            $statusCode = 200;
        }
        return $data;
    }
}
