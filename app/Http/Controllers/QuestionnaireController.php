<?php

namespace App\Http\Controllers;

use App\Models\Page;
use App\Models\Questionnaire;

class QuestionnaireController extends Controller
{


    public function show(Questionnaire $questionnaire)
    {
        //dd($questionnaire);
        return view('questionnaire.show', compact('questionnaire'));
    }

}
