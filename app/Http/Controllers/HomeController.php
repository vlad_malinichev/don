<?php

namespace App\Http\Controllers;

use App\Models\Questionnaire;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        //$questionnaires = Questionnaire::paginate();
        $vipQuestionnaires = Questionnaire::where('is_vip', '=', '1')->paginate(6);
        $questionnaires = Questionnaire::paginate(10);

        return view('home.index', compact('questionnaires', 'vipQuestionnaires'));
    }

    public function search(Request $request)
    {
        $query = $request->get('query');
    }
}
