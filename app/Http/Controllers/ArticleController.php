<?php

namespace App\Http\Controllers;

use App\Models\Article;

class ArticleController extends Controller
{

    public function index()
    {
        return view('page.model-instructions');
    }

    public function show(string $slug)
    {
        $page = Article::where('slug', $slug)->first();

        return view('page.show', compact('page'));
    }

}
