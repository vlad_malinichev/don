<?php

namespace App\Services\Questionnaire\Search;

class GeneratePrices extends Generate
{

    const FIELDS = [
        'currencyId' => ['currency_id', 'int'],
        'hour_in_apartments' => 'range',
        'hour_in_departures' => 'range',
        'two_hours_in_apartments' => 'range',
        'two_hours_in_departures' => 'range',
        'night_in_apartments' => 'range',
        'night_in_departures' => 'range',
    ];

}
