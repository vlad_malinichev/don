<?php

namespace App\Services\Questionnaire\Search;

abstract class Generate
{

    protected $data = [];

    protected $filters = [];

    public function __construct(array $data)
    {
        $this->data = $data;
    }

    protected function getField(string $key): array
    {
        $name = $key;
        $format = 'string';
        if (isset(static::FIELDS[$key])) {
            $field = static::FIELDS[$key];

            if (is_array($field)) {
                switch (count($field)) {
                    case 1:
                        $name = $key;
                        $format = $field[0];
                        break;
                    case 2:
                        $name = $field[0];
                        $format = $field[1];
                        break;
                    default:
                        $name = $key;
                        $format = 'string';
                        break;
                }
            } elseif (is_string($field)) {
                $name = $key;
                $format = $field;
            }
        }

        return [$name, $this->getFormatValue($name, $format), $this->getMethodByFormat($format)];
    }

    protected function getFormatValue($field, $format)
    {
        if (!isset($this->data[$field])) {
           return null;
        }

        $value = $this->data[$field];

        switch ($format) {
            case 'int':
            case 'integer':
                return (int) $value;
            case 'string':
                return (string) $value;
            case 'bool':
            case 'boolean':
                return (bool) $value;
            case 'array':
                return (array) $value;
            case 'range':
                return explode(';', $value);
        }

        return null;
    }

    public function getMethodByFormat($format): string
    {
        if ($format === 'range') {
            return 'whereBetween';
        }

        return 'where';
    }

    public function create(): array
    {
        $result = [];

        foreach ($this->data as $key => $value) {
            [$field, $value, $method] = $this->getField($key);
            if (empty($value)) {
                continue;
            }
            $result[] = [$field, $value, $method];
        }

        return $result;
    }

    public function get(): array
    {
        return $this->create();
    }

}
