<?php

namespace App\Services\Questionnaire\Search;

class GenerateLocation extends Generate
{

    const FIELDS = [
        'countryId' => ['country_id', 'int'],
        'regionId' => ['region_id', 'int'],
        'cityId' => ['city_id', 'int'],
        'districtId' => ['district_id', 'int'],
        'subwayId' => ['subway_id', 'int'],
    ];

}
