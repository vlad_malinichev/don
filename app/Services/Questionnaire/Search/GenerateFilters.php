<?php

namespace App\Services\Questionnaire\Search;

class GenerateFilters extends Generate
{

    const FIELDS = [
        'typeId' => ['type_id', 'int'],
        'nationalityId' => ['nationality_id', 'int'],
        'orientationId' => ['orientation_id', 'int'],
        'hairColorId' => ['hair_color_id', 'int'],
        'intimateHaircutId' => ['intimate_haircut_id', 'int'],
        'name' => 'string',
        'phone' => 'string',
        'viber' => 'bool',
        'whatsapp' => 'bool',
        'telegram' => 'bool',
        'sex' => 'int',
        'age' => 'range',
        'height' => 'range',
        'weight' => 'range',
        'breast_size' => 'range',
        'callTimeFrom' => ['call_time_from', 'string'],
        'callTimeTo' => ['call_time_to', 'string'],
        'isAroundClock' => ['is_around_clock', 'bool'],
        'isScars' => ['is_scars', 'bool'],
        'isPiercing' => ['is_piercing', 'bool'],
        'isTattoos' => ['is_tattoos', 'bool'],
        'about' => 'string',
    ];

}
