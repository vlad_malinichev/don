<?php

namespace App\Services\Questionnaire;

use App\Services\Questionnaire\Search\GenerateFilters;
use App\Services\Questionnaire\Search\GenerateLocation;
use App\Services\Questionnaire\Search\GeneratePrices;
use App\Services\Questionnaire\Search\GenerateServices;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Facades\DB;

class SearchService
{

    protected $filters = [];
    protected $services = [];
    protected $prices = [];
    protected $location = [];

    protected $queryBuilder;

    public function __construct()
    {
        $this->queryBuilder = DB::table('questionnaires');
        $this->queryBuilder->selectRaw('questionnaires.*');
    }

    public function setFilters(array $filters)
    {
        $this->filters = $filters;
    }

    public function getFilters(): array
    {
        return $this->filters;
    }

    public function setServices(array $services)
    {
        $this->services = $services;
    }

    public function getServices(): array
    {
        return $this->services;
    }

    public function setPrices(array $prices)
    {
        $this->prices = $prices;
    }

    public function getPrices(): array
    {
        return $this->prices;
    }

    public function setLocation(array $location)
    {
        $this->location = $location;
    }

    public function getLocation(): array
    {
        return $this->location;
    }

    public function getQueryBuilder(): Builder
    {
        return $this->queryBuilder;
    }

    public function searchByFilters(array $filters = [])
    {
        if (!empty($filters)) {
            $this->setFilters($filters);
        }

        $filters = $this->getFilters();
        if (empty($filters)) {
            return;
        }
        $filters = (new GenerateFilters($filters))->get();

        foreach ($filters as $condition) {
            $method = array_pop($condition);
            $this->queryBuilder->$method(...$condition);
        }
    }

    public function searchByServices(array $services = [])
    {
        if (!empty($services)) {
            $this->setServices($services);
        }

        $services = $this->getServices();
        if (empty($services)) {
            return;
        }

        $this->queryBuilder->leftJoin(
            'questionnaire_services',
            'questionnaires.id',
            '=',
            'questionnaire_services.questionnaire_id'
        );

        $services = (new GenerateServices($services))->get();

        foreach ($services as $condition) {
            $method = array_pop($condition);
            $condition[0] = 'questionnaire_services.' . $condition[0];
            $this->queryBuilder->$method(...$condition);
        }
    }

    public function searchByPrices(array $prices = [])
    {
        if (!empty($prices)) {
            $this->setPrices($prices);
        }

        $prices = $this->getPrices();
        if (empty($prices)) {
            return;
        }

        $this->queryBuilder->leftJoin(
            'questionnaire_prices',
            'questionnaires.id',
            '=',
            'questionnaire_prices.questionnaire_id'
        );

        $prices = (new GeneratePrices($prices))->get();

        foreach ($prices as $condition) {
            $method = array_pop($condition);
            $condition[0] = 'questionnaire_prices.' . $condition[0];
            $this->queryBuilder->$method(...$condition);
        }
    }

    public function searchByLocation(array $location = [])
    {
        if (!empty($location)) {
            $this->setLocation($location);
        }

        $location = $this->getLocation();
        if (empty($location)) {
            return;
        }

        $location = (new GenerateLocation($location))->get();

        foreach ($location as $condition) {
            $method = array_pop($condition);
            $this->queryBuilder->$method(...$condition);
        }
    }

    public function searchByAll()
    {
        $this->searchByFilters();
        $this->searchByServices();
        $this->searchByPrices();
        $this->searchByLocation();
    }

}
