<?php

namespace App\Services;

use App\Models\City;
use App\Models\Country;
use App\Models\Region;

class GeoService
{

    public function getCountries()
    {
        return Country::all();
    }

    public function getRegions()
    {
        return Region::all();
    }

    public function getCities()
    {
        return City::all();
    }

}
