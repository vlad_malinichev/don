<?php

namespace App\Services;

use App\Models\Questionnaire;

class QuestionnaireService
{

    protected $questionnairePriceService;
    protected $questionnaireServiceService;

    public function __construct(
        QuestionnairePriceService $questionnairePriceService,
        QuestionnaireServiceService $questionnaireServiceService
    )
    {
        $this->questionnairePriceService = $questionnairePriceService;
        $this->questionnaireServiceService = $questionnaireServiceService;
    }

    public function create(array $data)
    {
        $questionnaire = new Questionnaire();
        $questionnaire->fill($data);
        $questionnaireSave = $questionnaire->save();

        if ($questionnaireSave) {
            $questionnaireId = $questionnaire->id;
            if (!empty($data['Price'])) {
                $this->questionnairePriceService->create(
                  array_merge($data['Price'], ['questionnaire_id' => $questionnaireId])
                );
            }
            if (!empty($data['Service'])) {
                $this->questionnaireServiceService->create(
                  array_merge($data['Service'], ['questionnaire_id' => $questionnaireId])
                );
            }
        }

        return $questionnaireSave;
    }

}
