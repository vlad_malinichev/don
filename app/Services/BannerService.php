<?php

namespace App\Services;

use App\Models\Banner;

class BannerService
{

    public function add(array $data)
    {
        $blacklist = new Banner();
        $blacklist->fill($data);
        return $blacklist->save();
    }

}
