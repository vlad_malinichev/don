<?php

namespace App\Services;

use App\Models\Message;

class MessengerService
{

    public function __construct()
    {
    }

    public function create(array $data)
    {
        $message = new Message();
        $message->fill($data);
        return $message->save();
    }

    public function getDialogs($from_id)
    {

    }

    public function getCountDialogs($from_id)
    {

    }

    public function getAllByFromId($from_id)
    {

    }

    public function getAllByToId($to_id)
    {

    }

    public function getAll()
    {

    }

    public function getCountByFromId($from_id)
    {

    }

    public function getCountByToId($to_id)
    {

    }

    public function getCountAll()
    {

    }

    public function getOneById($id)
    {

    }

    public function getByCondition(array $condition)
    {

    }

}
