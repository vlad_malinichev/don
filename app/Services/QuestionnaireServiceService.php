<?php

namespace App\Services;

use App\Models\QuestionnaireService;

class QuestionnaireServiceService
{

    public function create(array $data)
    {
        $questionnaireService = new QuestionnaireService();
        $questionnaireService->fill($data);
        return $questionnaireService->save();
    }

}
