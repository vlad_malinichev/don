<?php

namespace App\Services;

use App\Models\QuestionnairePrice;

class QuestionnairePriceService
{

    public function create(array $data)
    {
        $questionnairePrice = new QuestionnairePrice();
        $questionnairePrice->fill($data);
        return $questionnairePrice->save();
    }

}
