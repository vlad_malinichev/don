<?php

namespace App\Services;

use App\Models\Review;

class ReviewService
{

    public function add(array $data)
    {
        $review = new Review();
        $review->fill($data);
        return $review->save();
    }

}
