<?php

namespace App\Services;

use App\Models\Blacklist;

class BlacklistService
{

    public function add(array $data)
    {
        $blacklist = new Blacklist();
        $blacklist->fill($data);
        return $blacklist->save();
    }

}
