<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int $user_id
 * @property int $type_id
 * @property int $nationality_id
 * @property int $orientation_id
 * @property int $country_id
 * @property int $region_id
 * @property int $city_id
 * @property int $district_id
 * @property int $subway_id
 * @property int $hair_color_id
 * @property int $intimate_haircut_id
 * @property string $name
 * @property string $phone
 * @property boolean $viber
 * @property boolean $whatsapp
 * @property boolean $telegram
 * @property string $call_time_from
 * @property string $call_time_to
 * @property boolean $is_around_clock
 * @property int $sex
 * @property int $age
 * @property int $height
 * @property int $weight
 * @property string $breast_size
 * @property boolean $is_scars
 * @property boolean $is_piercing
 * @property boolean $is_tattoos
 * @property string $about
 * @property boolean $is_vip
 * @property int $created_at
 * @property int $updated_at
 */
class Questionnaire extends Model
{

    use HasFactory;

    /**
     * Indicates if the model should be timestamped.
     *
     * @var boolean
     */
    public $timestamps = false;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'questionnaires';
    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id';
    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'type_id',
        'nationality_id',
        'orientation_id',
        'country_id',
        'region_id',
        'city_id',
        'district_id',
        'subway_id',
        'hair_color_id',
        'intimate_haircut_id',
        'name',
        'phone',
        'viber',
        'whatsapp',
        'telegram',
        'call_time_from',
        'call_time_to',
        'is_around_clock',
        'sex',
        'age',
        'height',
        'weight',
        'breast_size',
        'is_scars',
        'is_piercing',
        'is_tattoos',
        'about',
        'active',
        'is_vip',
        'created_at',
        'updated_at'
    ];
    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [

    ];
    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'user_id' => 'int',
        'type_id' => 'int',
        'nationality_id' => 'int',
        'orientation_id' => 'int',
        'country_id' => 'int',
        'region_id' => 'int',
        'city_id' => 'int',
        'district_id' => 'int',
        'subway_id' => 'int',
        'hair_color_id' => 'int',
        'intimate_haircut_id' => 'int',
        'name' => 'string',
        'phone' => 'string',
        'viber' => 'boolean',
        'whatsapp' => 'boolean',
        'telegram' => 'boolean',
        'call_time_from' => 'string',
        'call_time_to' => 'string',
        'is_around_clock' => 'boolean',
        'sex' => 'int', 'age' => 'int',
        'height' => 'int',
        'weight' => 'int',
        'breast_size' => 'string',
        'is_scars' => 'boolean',
        'is_piercing' => 'boolean',
        'is_tattoos' => 'boolean',
        'about' => 'string',
        'active' => 'boolean',
        'is_vip' => 'boolean',
        'created_at' => 'timestamp',
        'updated_at' => 'timestamp'
    ];
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at', 'updated_at'
    ];
    protected $attributes = [
        'viber' => 0,
        'whatsapp' => 0,
        'telegram' => 0,
        'breast_size' => 0,
        'is_scars' => 0,
        'is_piercing' => 0,
        'is_tattoos' => 0,
        'is_around_clock' => 0,
    ];

    public function price(): \Illuminate\Database\Eloquent\Relations\HasOne
    {
        return $this->hasOne(QuestionnairePrice::class);
    }

    public function service(): \Illuminate\Database\Eloquent\Relations\HasOne
    {
        return $this->hasOne(QuestionnaireService::class);
    }

    public function file(): \Illuminate\Database\Eloquent\Relations\HasOne
    {
        return $this->hasOne(QuestionnairePrice::class);
    }

    // Scopes...

    // Functions ...

    // Relations ...
}
