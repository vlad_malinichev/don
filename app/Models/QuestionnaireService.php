<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $questionnaire_id
 * @property boolean $vaginal
 * @property int $vaginal_price
 * @property boolean $anal
 * @property int $anal_price
 * @property boolean $group
 * @property int $group_price
 * @property boolean $lesbian
 * @property int $lesbian_price
 * @property boolean $mouth
 * @property int $mouth_price
 * @property boolean $face
 * @property int $face_price
 * @property boolean $chest
 * @property int $chest_price
 * @property boolean $frank
 * @property int $frank_price
 * @property boolean $lung
 * @property int $lung_price
 * @property boolean $escort
 * @property int $escort_price
 * @property boolean $photo_video
 * @property int $photo_video_price
 * @property boolean $family_services
 * @property int $family_services_price
 * @property boolean $virtual
 * @property int $virtual_price
 * @property boolean $banjo
 * @property int $banjo_price
 * @property boolean $madam
 * @property int $madam_price
 * @property boolean $slave
 * @property int $slave_price
 * @property boolean $light_domination
 * @property int $light_domination_price
 * @property boolean $erotic_games
 * @property int $erotic_games_price
 * @property boolean $flogging
 * @property int $flogging_price
 * @property boolean $fetish
 * @property int $fetish_price
 * @property boolean $foot_fetish
 * @property int $foot_fetish_price
 * @property boolean $trampling
 * @property int $trampling_price
 * @property boolean $condom
 * @property int $condom_price
 * @property boolean $dont_condom
 * @property int $dont_condom_price
 * @property boolean $deep
 * @property int $deep_price
 * @property boolean $in_car
 * @property int $in_car_price
 * @property boolean $cunnilingus
 * @property int $cunnilingus_price
 * @property boolean $anilingus
 * @property int $anilingus_price
 * @property boolean $pro
 * @property int $pro_price
 * @property boolean $not_pro
 * @property int $not_pro_price
 * @property boolean $classical
 * @property int $classical_price
 * @property boolean $professional
 * @property int $professional_price
 * @property boolean $relaxing
 * @property int $relaxing_price
 * @property boolean $thai
 * @property int $thai_price
 * @property boolean $urological
 * @property int $urological_price
 * @property boolean $point
 * @property int $point_price
 * @property boolean $erotic
 * @property int $erotic_price
 * @property boolean $sakura_branch
 * @property int $sakura_branch_price
 * @property boolean $strapon
 * @property int $strapon_price
 * @property boolean $golden_shower_issue
 * @property int $golden_shower_issue_price
 * @property boolean $golden_shower_reception
 * @property int $golden_shower_reception_price
 * @property boolean $copro_issue
 * @property int $copro_issue_price
 * @property boolean $copro_reception
 * @property int $copro_reception_price
 * @property boolean $fisting_anal
 * @property int $fisting_anal_price
 * @property boolean $fisting_vaginal
 * @property int $fisting_vaginal_price
 * @property boolean $sex_toys
 * @property int $sex_toys_price
 * @property int $created_at
 * @property int $updated_at
 */
class QuestionnaireService extends Model
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var boolean
     */
    public $timestamps = false;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'questionnaire_services';
    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id';
    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'questionnaire_id',
        'vaginal',
        'vaginal_price',
        'anal',
        'anal_price',
        'group',
        'group_price',
        'lesbian',
        'lesbian_price',
        'mouth',
        'mouth_price',
        'face',
        'face_price',
        'chest',
        'chest_price',
        'frank',
        'frank_price',
        'lung',
        'lung_price',
        'escort',
        'escort_price',
        'photo_video',
        'photo_video_price',
        'family_services',
        'family_services_price',
        'virtual',
        'virtual_price',
        'banjo',
        'banjo_price',
        'madam',
        'madam_price',
        'slave',
        'slave_price',
        'light_domination',
        'light_domination_price',
        'erotic_games',
        'erotic_games_price',
        'flogging',
        'flogging_price',
        'fetish',
        'fetish_price',
        'foot_fetish',
        'foot_fetish_price',
        'trampling',
        'trampling_price',
        'condom',
        'condom_price',
        'dont_condom',
        'dont_condom_price',
        'deep',
        'deep_price',
        'in_car',
        'in_car_price',
        'cunnilingus',
        'cunnilingus_price',
        'anilingus',
        'anilingus_price',
        'pro',
        'pro_price',
        'not_pro',
        'not_pro_price',
        'classical',
        'classical_price',
        'professional',
        'professional_price',
        'relaxing',
        'relaxing_price',
        'thai',
        'thai_price',
        'urological',
        'urological_price',
        'point',
        'point_price',
        'erotic',
        'erotic_price',
        'sakura_branch',
        'sakura_branch_price',
        'strapon',
        'strapon_price',
        'golden_shower_issue',
        'golden_shower_issue_price',
        'golden_shower_reception',
        'golden_shower_reception_price',
        'copro_issue',
        'copro_issue_price',
        'сopro_reception',
        'сopro_reception_price',
        'fisting_anal',
        'fisting_anal_price',
        'fisting_vaginal',
        'fisting_vaginal_price',
        'sex_toys',
        'sex_toys_price',
        'created_at',
        'updated_at'
    ];
    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [

    ];
    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'questionnaire_id' => 'int',
        'vaginal' => 'boolean',
        'vaginal_price' => 'int',
        'anal' => 'boolean',
        'anal_price' => 'int',
        'group' => 'boolean',
        'group_price' => 'int',
        'lesbian' => 'boolean',
        'lesbian_price' => 'int',
        'mouth' => 'boolean',
        'mouth_price' => 'int',
        'face' => 'boolean',
        'face_price' => 'int',
        'chest' => 'boolean',
        'chest_price' => 'int',
        'frank' => 'boolean',
        'frank_price' => 'int',
        'lung' => 'boolean',
        'lung_price' => 'int',
        'escort' => 'boolean',
        'escort_price' => 'int',
        'photo_video' => 'boolean',
        'photo_video_price' => 'int',
        'family_services' => 'boolean',
        'family_services_price' => 'int',
        'virtual' => 'boolean',
        'virtual_price' => 'int',
        'banjo' => 'boolean',
        'banjo_price' => 'int',
        'madam' => 'boolean',
        'madam_price' => 'int',
        'slave' => 'boolean',
        'slave_price' => 'int',
        'light_domination' => 'boolean',
        'light_domination_price' => 'int',
        'erotic_games' => 'boolean',
        'erotic_games_price' => 'int',
        'flogging' => 'boolean',
        'flogging_price' => 'int',
        'fetish' => 'boolean',
        'fetish_price' => 'int',
        'foot_fetish' => 'boolean',
        'foot_fetish_price' => 'int',
        'trampling' => 'boolean',
        'trampling_price' => 'int',
        'condom' => 'boolean',
        'condom_price' => 'int',
        'dont_condom' => 'boolean',
        'dont_condom_price' => 'int',
        'deep' => 'boolean',
        'deep_price' => 'int',
        'in_car' => 'boolean',
        'in_car_price' => 'int',
        'cunnilingus' => 'boolean',
        'cunnilingus_price' => 'int',
        'anilingus' => 'boolean',
        'anilingus_price' => 'int',
        'pro' => 'boolean',
        'pro_price' => 'int',
        'not_pro' => 'boolean',
        'not_pro_price' => 'int',
        'classical' => 'boolean',
        'classical_price' => 'int',
        'professional' => 'boolean',
        'professional_price' => 'int',
        'relaxing' => 'boolean',
        'relaxing_price' => 'int',
        'thai' => 'boolean',
        'thai_price' => 'int',
        'urological' => 'boolean',
        'urological_price' => 'int',
        'point' => 'boolean',
        'point_price' => 'int',
        'erotic' => 'boolean',
        'erotic_price' => 'int',
        'sakura_branch' => 'boolean',
        'sakura_branch_price' => 'int',
        'strapon' => 'boolean',
        'strapon_price' => 'int',
        'golden_shower_issue' => 'boolean',
        'golden_shower_issue_price' => 'int',
        'golden_shower_reception' => 'boolean',
        'golden_shower_reception_price' => 'int',
        'copro_issue' => 'boolean',
        'copro_issue_price' => 'int',
        'сopro_reception' => 'boolean',
        'сopro_reception_price' => 'int',
        'fisting_anal' => 'boolean',
        'fisting_anal_price' => 'int',
        'fisting_vaginal' => 'boolean',
        'fisting_vaginal_price' => 'int',
        'sex_toys' => 'boolean',
        'sex_toys_price' => 'int',
        'created_at' => 'timestamp',
        'updated_at' => 'timestamp'
    ];
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at', 'updated_at'
    ];

    public $attributes = [
        'vaginal' => false,
        'vaginal_price' => 0,
        'anal' => false,
        'anal_price' => 0,
        'group' => false,
        'group_price' => 0,
        'lesbian' => false,
        'lesbian_price' => 0,
        'mouth' => false,
        'mouth_price' => 0,
        'face' => false,
        'face_price' => 0,
        'chest' => false,
        'chest_price' => 0,
        'frank' => false,
        'frank_price' => 0,
        'lung' => false,
        'lung_price' => 0,
        'escort' => false,
        'escort_price' => 0,
        'photo_video' => false,
        'photo_video_price' => 0,
        'family_services' => false,
        'family_services_price' => 0,
        'virtual' => false,
        'virtual_price' => 0,
        'banjo' => false,
        'banjo_price' => 0,
        'madam' => false,
        'madam_price' => 0,
        'slave' => false,
        'slave_price' => 0,
        'light_domination' => false,
        'light_domination_price' => 0,
        'erotic_games' => false,
        'erotic_games_price' => 0,
        'flogging' => false,
        'flogging_price' => 0,
        'fetish' => false,
        'fetish_price' => 0,
        'foot_fetish' => false,
        'foot_fetish_price' => 0,
        'trampling' => false,
        'trampling_price' => 0,
        'condom' => false,
        'condom_price' => 0,
        'dont_condom' => false,
        'dont_condom_price' => 0,
        'deep' => false,
        'deep_price' => 0,
        'in_car' => false,
        'in_car_price' => 0,
        'cunnilingus' => false,
        'cunnilingus_price' => 0,
        'anilingus' => false,
        'anilingus_price' => 0,
        'pro' => false,
        'pro_price' => 0,
        'not_pro' => false,
        'not_pro_price' => 0,
        'classical' => false,
        'classical_price' => 0,
        'professional' => false,
        'professional_price' => 0,
        'relaxing' => false,
        'relaxing_price' => 0,
        'thai' => false,
        'thai_price' => 0,
        'urological' => false,
        'urological_price' => 0,
        'point' => false,
        'point_price' => 0,
        'erotic' => false,
        'erotic_price' => 0,
        'sakura_branch' => false,
        'sakura_branch_price' => 0,
        'strapon' => false,
        'strapon_price' => 0,
        'golden_shower_issue' => false,
        'golden_shower_issue_price' => 0,
        'golden_shower_reception' => false,
        'golden_shower_reception_price' => 0,
        'copro_issue' => false,
        'copro_issue_price' => 0,
        'сopro_reception' => false,
        'сopro_reception_price' => 0,
        'fisting_anal' => false,
        'fisting_anal_price' => 0,
        'fisting_vaginal' => false,
        'fisting_vaginal_price' => 0,
        'sex_toys' => false,
        'sex_toys_price' => 0,
    ];

    // Scopes...

    // Functions ...

    // Relations ...
}
