<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int $currency_id
 * @property float $hour_in_apartments
 * @property boolean $hour_in_apartments_negotiated
 * @property float $two_hours_in_apartments
 * @property boolean $two_hours_in_apartments_negotiated
 * @property float $night_in_apartments
 * @property boolean $night_in_apartments_negotiated
 * @property float $hour_in_departures
 * @property boolean $hour_in_departures_negotiated
 * @property float $two_hours_in_departures
 * @property boolean $two_hours_in_departures_negotiated
 * @property float $night_in_departures
 * @property boolean $night_in_departures_negotiated
 * @property boolean $is_departure_to_office
 * @property boolean $is_departure_to_sauna
 * @property boolean $is_departure_to_hotel
 * @property boolean $is_departure_to_apartment
 * @property boolean $is_departure_to_country_house
 * @property boolean $is_departure_to_any_place
 * @property int $created_at
 * @property int $updated_at
 */
class QuestionnairePrice extends Model
{

    use HasFactory;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'questionnaire_prices';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'questionnaire_id',
        'currency_id',
        'hour_in_apartments',
        'hour_in_apartments_negotiated',
        'two_hours_in_apartments',
        'two_hours_in_apartments_negotiated',
        'night_in_apartments',
        'night_in_apartments_negotiated',
        'hour_in_departures',
        'hour_in_departures_negotiated',
        'two_hours_in_departures',
        'two_hours_in_departures_negotiated',
        'night_in_departures',
        'night_in_departures_negotiated',
        'is_departure_to_office',
        'is_departure_to_sauna',
        'is_departure_to_hotel',
        'is_departure_to_apartment',
        'is_departure_to_country_house',
        'is_departure_to_any_place',
        'created_at',
        'updated_at'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [

    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'questionnaire_id' => 'int',
        'currency_id' => 'int',
        'hour_in_apartments' => 'double',
        'hour_in_apartments_negotiated' => 'boolean',
        'two_hours_in_apartments' => 'double',
        'two_hours_in_apartments_negotiated' => 'boolean',
        'night_in_apartments' => 'double',
        'night_in_apartments_negotiated' => 'boolean',
        'hour_in_departures' => 'double',
        'hour_in_departures_negotiated' => 'boolean',
        'two_hours_in_departures' => 'double',
        'two_hours_in_departures_negotiated' => 'boolean',
        'night_in_departures' => 'double',
        'night_in_departures_negotiated' => 'boolean',
        'is_departure_to_office' => 'boolean',
        'is_departure_to_sauna' => 'boolean',
        'is_departure_to_hotel' => 'boolean',
        'is_departure_to_apartment' => 'boolean',
        'is_departure_to_country_house' => 'boolean',
        'is_departure_to_any_place' => 'boolean',
        'created_at' => 'timestamp',
        'updated_at' => 'timestamp'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at', 'updated_at'
    ];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var boolean
     */
    public $timestamps = false;

    public $attributes = [
        'currency_id' => 1,
        'hour_in_apartments' => 0,
        'two_hours_in_apartments' => 0,
        'night_in_apartments' => 0,
        'hour_in_departures' => 0,
        'two_hours_in_departures' => 0,
        'night_in_departures' => 0,
        // Boolean
        'hour_in_apartments_negotiated' => 0,
        'two_hours_in_apartments_negotiated' => 0,
        'night_in_apartments_negotiated' => 0,
        'hour_in_departures_negotiated' => 0,
        'two_hours_in_departures_negotiated' => 0,
        'night_in_departures_negotiated' => 0,
        'is_departure_to_office' => 0,
        'is_departure_to_sauna' => 0,
        'is_departure_to_hotel' => 0,
        'is_departure_to_apartment' => 0,
        'is_departure_to_country_house' => 0,
        'is_departure_to_any_place' => 0,
    ];

    // Scopes...

    // Functions ...

    // Relations ...
}
