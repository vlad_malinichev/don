<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int $currency_id
 * @property float $hour_in_apartments
 * @property boolean $hour_in_apartments_negotiated
 * @property float $two_hours_in_apartments
 * @property boolean $two_hours_in_apartments_negotiated
 * @property float $night_in_apartments
 * @property boolean $night_in_apartments_negotiated
 * @property float $hour_in_departures
 * @property boolean $hour_in_departures_negotiated
 * @property float $two_hours_in_departures
 * @property boolean $two_hours_in_departures_negotiated
 * @property float $night_in_departures
 * @property boolean $night_in_departures_negotiated
 * @property boolean $is_departure_to_office
 * @property boolean $is_departure_to_sauna
 * @property boolean $is_departure_to_hotel
 * @property boolean $is_departure_to_apartment
 * @property boolean $is_departure_to_country_house
 * @property boolean $is_departure_to_any_place
 * @property int $created_at
 * @property int $updated_at
 */
class QuestionnaireRating extends Model
{

    use HasFactory;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'questionnaire_ratings';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'questionnaire_id',
        'value',
        'created_at',
        'updated_at'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [

    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'questionnaire_id' => 'int',
        'value' => 'float',
        'created_at' => 'timestamp',
        'updated_at' => 'timestamp'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at', 'updated_at'
    ];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var boolean
     */
    public $timestamps = false;

    // Scopes...

    // Functions ...

    // Relations ...
}
