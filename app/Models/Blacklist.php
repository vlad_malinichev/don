<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Blacklist extends Model
{

    use HasFactory;

    protected $table = 'blacklists';

    protected $fillable = ['user_id', 'category_id', 'country_id', 'region_id', 'city_id', 'sex', 'phone', 'comment'];

}
